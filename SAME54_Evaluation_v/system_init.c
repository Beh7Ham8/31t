/*
 * system_init.c
 *
 * Created: 5/8/2020 2:43:30 PM
 *  Author: BehroozH
 */ 
#include "system_definitions.h"
#include "atmel_start_pins.h"
#include <driver_init.h>
#include "debugMon.h"
#include "lib/cfg_device.h"
#include "common.h"
#include "flash.h"
#include "sd_mmc/sd_logger.h"
#include "lib/systick.h"
#include "drivers.h"

#define MANUAL_SWITCH_DEBOUNCE		60000

SYSTEM_FLAGS sFlags;

pSYSTEM_FLAGS pSystemStatus;

PERF_STATE_DATA performanceData;

uint8_t wakeUpPowerFlags[12];

SW_TIMER heartBeatTimer;
pSW_TIMER pHeartBeatTimer;

static volatile APP_DATA App_Data;

enum
{
	SWITCH_ON_STABLE,	
	SWITCH_ON_CHANGING_OFF,	
	SWITCH_OFF_STABLE,	
	SWITCH_OFF_CHANGING_ON
};

static void Fault_Detected_Int(void);
static void Ext_ADC_Int(void);
static void ShuntCurrent_Detected_Int(void);
static void Button_Int(void);

void Init_WatchDog();

void initialzePinStates(void) 
{
}

void Init_App ()
{   
    // Initialize System variables
    pSystemStatus = &sFlags;
    pSystemStatus->word = 0;
	
	pSystemStatus->flags.CellsBalanced = 1;
	
	App_Data.numOfInt_Fault = 0;
	App_Data.numOfInt_Rdy = 0;
	App_Data.numOfInt_WakeUp = 0;
	
	App_Data.fault_Occured = false;
	App_Data.fault_Serviced = false;
	
	if( ManualSwitchStatus() == true)
	{
		App_Data.system_mode = LOW_POWER_MODE;
		App_Data.manualSwitch = SWITCH_ON_STABLE;
	}
	else
	{
		App_Data.system_mode = STORAGE_MODE;
		App_Data.manualSwitch = SWITCH_OFF_STABLE;
	}
	
	pHeartBeatTimer = &heartBeatTimer;

	timerInit(pHeartBeatTimer, PERIODS.HEARTBEAT);
}


void App_Monitor()
{
	static uint64_t tick;
	uint64_t tick_diff,old_tick;
	static SW_TIMER ManualSwitchTimer;
	
	if(Is_Unserviced_Fault_Exist())
	{
		// To be modified
		if(Is_OVP_Enabled() || Is_SCP_Enabled() || Is_UVP_Enabled() || Is_OTP_Enabled())
		{
			Open_Contactor();			
		}
		
		//App_Data.fault_Occured = false;
		App_Data.fault_Serviced = true;
		(App_Data.numOfInt_Fault)++;
		Queue_Event("Fault Detected");
		debug_print(DBG_APP,  " APP___FSM |   %s","Fault Detected.");
	}
	else if(App_Data.wakeUpCurrent_Detected)
	{
	  App_Data.wakeUpCurrent_Detected = false;
	  (App_Data.numOfInt_WakeUp)++;
	  Queue_Event("WakeUp Current Detected");
	  debug_print(DBG_APP,  " APP___FSM |   %s"," WakeUp Current Detected. ");
	}
	else if (timerExpired(pHeartBeatTimer))
	{
		old_tick = tick;
		tick = HAL_SYSTICK_GetTicks();
		tick_diff = tick - old_tick;
		timerReset(pHeartBeatTimer);
		
		debug_print(DBG_APP,  " APP___FSM | %09llu  Fault_Int:%u   Rdys_Int:%u   WakeUp_Int:%u",tick,App_Data.numOfInt_Fault,App_Data.numOfInt_Rdy,App_Data.numOfInt_WakeUp);
	}
	
	// requirement clarification. Fault is cleared when contractor is closed manually???
	if(false)
	{
		App_Data.fault_Occured = false;
	}
	
	//Handling Manual Switch 
	#pragma region ManualSwitch
	
	switch(App_Data.manualSwitch)
	{
		case SWITCH_ON_STABLE:
		
				if(ManualSwitchStatus() == false)
				{
					App_Data.manualSwitch = SWITCH_ON_CHANGING_OFF;
					timerInit(&ManualSwitchTimer,  MANUAL_SWITCH_DEBOUNCE);
				}
				else
				{
					App_Data.manualSwitch = SWITCH_ON_STABLE;
				}
		
		break;
		
		case SWITCH_ON_CHANGING_OFF:
		
				if(timerExpired(&ManualSwitchTimer))
				{
					// the switch stayed OFF long enough, change the state to stable_off
					App_Data.manualSwitch = SWITCH_OFF_STABLE;
					timerSetActive (&ManualSwitchTimer, false);
				}
				else if(ManualSwitchStatus() == true)
				{
					// it was a quick on-off-on change go back to stable_on state
					App_Data.manualSwitch = SWITCH_ON_STABLE;
					timerSetActive (&ManualSwitchTimer, false);
				}
		break;
		
		case SWITCH_OFF_STABLE:
		
				if( ManualSwitchStatus() == true)
				{
					App_Data.manualSwitch = SWITCH_OFF_CHANGING_ON;
					timerInit(&ManualSwitchTimer,  MANUAL_SWITCH_DEBOUNCE);
				}
				else
				{
					App_Data.manualSwitch = SWITCH_OFF_STABLE;
				}
		break;
		
		case SWITCH_OFF_CHANGING_ON:
		
				if(timerExpired(&ManualSwitchTimer))
				{
					// the switch stayed ON long enough, change the state to stable_on
					App_Data.manualSwitch = SWITCH_ON_STABLE;
					timerSetActive (&ManualSwitchTimer, false);
				}
				else if(ManualSwitchStatus() == false)
				{
					// it was a quick off-on-off change go back to stable_off state
					App_Data.manualSwitch = SWITCH_OFF_STABLE;
					timerSetActive (&ManualSwitchTimer, false);
				}
		break;
		
	}
	
	#pragma endregion ManualSwitch
	
	
}

void Init_Interrupts()
{
	// make sure the Pull configuration is OFF for all interrupt pins, otherwise you will get multiple interrupt per each edge.
	ext_irq_register(FAULT_DETECTED, Fault_Detected_Int);
	
	ext_irq_register(ALERT_RDY, Ext_ADC_Int);
	
	ext_irq_register(UC_INT_SHUNT, ShuntCurrent_Detected_Int);
	
	ext_irq_register(BUTTON, Button_Int);
	
	Init_WatchDog();
}

bool Is_Faulted()
{
	return App_Data.fault_Occured;
}

bool Is_Unserviced_Fault_Exist()
{
	return ((App_Data.fault_Occured == true) && (App_Data.fault_Serviced == false));
}

static void Fault_Detected_Int(void)
{
	App_Data.fault_Occured = true;
	App_Data.fault_Serviced = false;
}

static void Ext_ADC_Int(void)
{
	debug_print(DBG_APP,  " 6810__FSM |   %s","Ext ADC Interrupt");
	(App_Data.numOfInt_Rdy)++;
}

static void ShuntCurrent_Detected_Int(void)
{
	App_Data.wakeUpCurrent_Detected = true;
}

static void Button_Int(void)
{
	 Queue_Event("Push Button Interrupt Received.");
}

void Init_WatchDog()
{
	if(0 == wdt_set_timeout_period(&WDT_0,1000,16384))
	{
		WDT->EWCTRL.bit.EWOFFSET = 0xA;                   // Set the Early Warning Interrupt Time Offset to 8 seconds
		//REG_WDT_EWCTRL = WDT_EWCTRL_EWOFFSET_8K;

		WDT->INTENSET.bit.EW = 1;                         // Enable the Early Warning interrupt
		//REG_WDT_INTENSET = WDT_INTENSET_EW;               // Is this the equivalent analog comparator register definition?

		// Configure and enable WDT interrupt
		NVIC_DisableIRQ(WDT_IRQn);
		NVIC_ClearPendingIRQ(WDT_IRQn);
		NVIC_SetPriority(WDT_IRQn, 0);
		NVIC_EnableIRQ(WDT_IRQn);
		wdt_enable(&WDT_0);
	}	
}

// Watchdog interrupt service routine (ISR)
void WDT_Handler()
{
	WDT->INTFLAG.reg |= WDT_INTFLAG_EW;             // Clear the Early Warning interrupt flag
	
	//REG_WDT_INTFLAG = WDT_INTFLAG_EW;
	WDT->CLEAR.bit.CLEAR = 0xA5;                    // Clear the Watchdog Timer and restart time-out period
	//REG_WDT_CLEAR = WDT_CLEAR_CLEAR_KEY;
	//while (WDT-> SYNCBUSY);               // Await synchronization of registers between clock domains
	
	//printf("\n%s  WDT fed.",CurrentTime());	
}

void Disable_WatchDog()
{
	//NVIC_DisableIRQ(10);
	wdt_disable(&WDT_0);
}


void Enable_WatchDog()
{
	//NVIC_DisableIRQ(10);
	wdt_enable(&WDT_0);
}
