/*
 * sleep.c
 *
 * Created: 5/8/2020 2:18:28 PM
 *  Author: BehroozH
 */ 
#include "lib/stateMachine.h"
#include "lib/timing.h"
#include "sleep.h"
#include "system_definitions.h"
#include <stddef.h>
#include "debugMon.h"
#include "atmel_start_pins.h"
#include "hal_sleep.h"
#include "drivers.h"
#include "common.h"
#include "battCells.h"
#include "system_init.h"
#include "flash.h"
#include "lib/systick.h"

const pStateFunc sleepStateVectorArray[SLEEP_NUM_STATES] = 
{
	(pStateFunc) state_sleep_Init,
	(pStateFunc) state_sleep_ON,
	(pStateFunc) state_sleep_OFF,
};

char* sleepStateString[SLEEP_NUM_STATES] =
{
	"sleep_Init",
	"sleep_ON",
	"sleep_OFF"
};

STATE_MACHINE slpFSM;
pSTATE_MACHINE pSleepFSM;


SW_TIMER sleepMonitorTimer;
pSW_TIMER pSleepMonitorTimer;

const uint32_t NUM_SLEEP_CYCLES = 1;
const uint32_t SLEEP_CYCLE_LENGTH = 8000;
uint32_t sleepCycleLength = 8000;

const float ENGINE_ON_DELTA = 0.0200;

const uint8_t  SLEEP_MODE_STANDBY  = 0x04;
const uint8_t  SLEEP_MODE_IDLE  = 0x02;


// ----------------------------------------------------

void Init_Sleep(void) 
{
	pSleepFSM = &slpFSM;
	pSleepMonitorTimer = &sleepMonitorTimer;

	// We will monitor the sleep state to check if the system is allowed to go to sleep
	timerInit(pSleepMonitorTimer, PERIODS.SLEEP);

	// initialize state machine
	stateInit(pSleepFSM, sleepStateVectorArray, SLEEP_NUM_STATES, SLEEP_INIT);
	
		
	/*
	hri_oscctrl_write_DFLLCTRLA_reg(OSCCTRL, 0);
	hri_supc_clear_BOD33_ENABLE_bit(SUPC);
	hri_nvmctrl_write_CTRLA_PRM_bf(NVMCTRL, 1);
	hri_pm_write_STDBYCFG_reg(PM, PM_STDBYCFG_FASTWKUP(1) | PM_STDBYCFG_RAMCFG(0x1));
	*/
	
    //hri_rtcmode0_write_BKUP_reg(RTC, 0, 2);
    //hri_pm_write_STDBYCFG_reg(PM, PM_STDBYCFG_FASTWKUP(0) | PM_STDBYCFG_RAMCFG(0x1));
}

void tickTimersleep() 
{
	uint32_t timerIncrement = 1;

	event_handler(pSleepFSM, evTIMER_TICK, &timerIncrement);
}

float getSleepFSMState()
{
	//UNIVERSAL_VAL   param;
	
	//param.value.iVal = pSleepFSM->currentState;
	//return param.value.fVal;
	return 0;
}

void Sleep_Monitor() 
{
	if( Is_SleepMode_Enabled() == false)
	{
		return;
	}
	// Fire the appropriate event to the state machine depending on
	// what the system has experienced
	if (timerExpired(pSleepMonitorTimer))
	 {
		 
		   if((FSM_counters.Sleep_counter++ % 1)==0)
		   {
			   debug_print(DBG_SLEEP_MODE, " Sleep_FSM |   %s",sleepStateString[pSleepFSM->currentState]);
		   }
		// Send timer event to the appropriate state
		tickTimersleep();

		// Reset timer for next monitor cycle
		timerReset(pSleepMonitorTimer);
	}
}

void state_sleep_Init(int event, void *param)
{
	   switch (event) 
	   {
		   case evENTER_STATE:
		   pSleepFSM->nextState = SLEEP_OFF;
		   //updateRemoteState(SET_SLEEP_STATE, SLEEP_INIT, INTEGERX, 0);
		   break;

		   case evTIMER_TICK:
		   break;

		   case evEXIT_STATE:
		   break;
	   }
}

void state_sleep_ON(int event, void *param)
{
	 static uint32_t cycleCountValue = 0,sleep_ts,wakeup_ts;
	 SW_TIMER wetOffTimer;

	 volatile float chargingCurrent;
	 volatile float dischargingCurrent;
	 volatile float vRef;
	 volatile float delta;
	 volatile float bankVoltage;

	 static int8_t vBatSettlingTime;
	 bool cellsAreInOverVoltage;
	 bool cellsAreInUnderVoltage;
	 
	 static uint8_t loopCnt = 0;
	
	
	 switch (event)
	  {
		 case evENTER_STATE:
		 // Wait 10s before measuring the ignition on threshold
		 vBatSettlingTime = 1;//10;

		 pSystemStatus->flags.mcuInSleep = 1;
		 //debug_print(DBG_SLEEP_MODE, "Entering sleepON state. Closing Contactor. BSOC(%2.4f)\r\n", batterySOC);
		 
		// if (pSystemStatus->flags.contactorOffOverrideIsInEffect != 1)
		// event_handler(pContactorFSM, evCONTACTOR_ON, "Sender: Sleep ON Enter");

		 pSystemStatus->flags.ChargerEnabled = 0;
		 
		 break;

		 case evTIMER_TICK:
		  		 
		 // if the engine is ON , we are NOT gonna go to sleep
		 if (!IsEngineOff() || (pSystemStatus->flags.bootloaderRunning == 1) )
		 {
			 pSleepFSM->nextState = SLEEP_OFF;
			 break;
		 }

		 cellsAreInOverVoltage = IsOverVoltageDetected();
		 cellsAreInUnderVoltage = IsUnderVoltageDetected();

		 // Let the battery settle, if NOT in deep sleep mode
		 if ( vBatSettlingTime > 0 )
		 {
			 vBatSettlingTime--;
		 }
		 else
		 {
			 // If cells are balanced OR it's in deep sleep mode, go to sleep
			 // if (pSystemStatus->flags.CellsBalanced == 1)
			 if(true)
			 {							 
				//debug_print(DBG_SLEEP_MODE, "About to sleep. BSOC(%2.4f)\r\n", batterySOC);
				
				if (pSystemStatus->flags.shouldSkipSleep == 0)
				{
					prepareToSleep();
					
					sleep_ts = capture_time_sleep();
					
					Go_Sleep(SLEEP_MODE_STANDBY);
					
					wakeup_ts = capture_time_wakeup();

					prepareToWakeUp(wakeup_ts - sleep_ts);
				}
			 }
		 }


		 // Calculate the ignition state of charge  ignSOC
		 chargingCurrent = 0;//getC_MON_Value(VOLTAGE_REFERENCE_TP1, CMON);

		 //debug_print(DBG_SLEEP_MODE, "Woke up! chargingCurrent(%2.4f), IGN_ON_THRESHOLD (%2.4f)\n", chargingCurrent, IGN_ON_THRESHOLD);
		 
		 break;

		 case evSLEEP_OFF:
		 debug_print(DBG_SLEEP_MODE, "Going to sleep off, %s \r\n", (char *)param);
		 debug_print(DBG_SLEEP_MODE, "Got sleep_off (%d),  event (%d)\n", evSLEEP_OFF, event);
		 pSleepFSM->nextState = SLEEP_OFF;
		 
		 break;
		 
		 case evEXIT_STATE:
		
		 break;
	 }
}

 
void state_sleep_OFF(int event, void *param)
{
	
	 volatile float dischargingCurrent;
	 volatile float delta;
	 static uint8_t sleepONcountdown = 2;
	 static SW_TIMER sleepOffTimer;
	 static uint8_t cycleCountValue = 0;

	 switch (event) 
	 {
		 case evENTER_STATE:
		// debug_print(DBG_SLEEP_MODE, "Entering SLEEP_OFF state... %s \r\n", "");
		 
		 pSystemStatus->flags.mcuInSleep = 0;

		 sleepONcountdown = 2;
		 
		 // Set to a very small value by default
		 timerInit(&sleepOffTimer, ONE_MILLISECOND);
		 
		 break;

		 case evTIMER_TICK:

		 // Calculate the current delta to see if any change is seen to determine if its ok to go to sleep
		 dischargingCurrent = Get_DisChargingCurrent();		
         delta = 0;//Get_DeltaVoltage();
		 
		 if  ((delta < ENGINE_ON_DELTA) &&
		 IsEngineOff() &&
		 (pSystemStatus->flags.overUnderVoltageExists == 0) &&
		 (pSystemStatus->flags.CellsBalanced == 1) &&
		 (pSystemStatus->flags.bootloaderRunning == 0) &&
		 timerExpired(&sleepOffTimer))
		 {
			 // Met our thresholds, prepare to sleep
			
			debug_print(DBG_SLEEP_MODE, "engineIsOff = %d %s \r\n", IsEngineOff(), "");
			debug_print(DBG_SLEEP_MODE, "bootloaderRunning = %d %s \r\n", pSystemStatus->flags.bootloaderRunning, "");			
		    debug_print(DBG_SLEEP_MODE, "Going from SLEEP_OFF to SLEEP_ON state... %s \r\n", "");
			
			pSleepFSM->nextState = SLEEP_ON;
			 
		 }
		 else
		 {
			 
			 debug_print(DBG_SLEEP_MODE, "Cannot go to sleep because one of these:\n\tengineIsOff (%d), \n\tor cellsBallanced (%d), \n", IsEngineOff(), pSystemStatus->flags.CellsBalanced);
		 }


		 break;

		 case evSLEEP_ON:
		 // Calculate the current delta to see if any change is seen to determine if its ok to go to sleep
		 dischargingCurrent = Get_DisChargingCurrent();
		 delta = 0;//Get_DeltaVoltage();
		 
		 if  (IsEngineOff() &&
		 (delta < ENGINE_ON_DELTA) &&
		 (pSystemStatus->flags.overUnderVoltageExists == 0) &&
		 (pSystemStatus->flags.CellsBalanced == 1) &&
		 (pSystemStatus->flags.bootloaderRunning == 0))
		 {
			 pSleepFSM->nextState = SLEEP_ON;		 
		 }
		 else
		 {
			 debug_print(DBG_SLEEP_MODE, "Received event to enter sleep, but ignoring because either engineIsON (%d), or cellsNotBallanced (%d), or heaterOn (%d)\n", !IsEngineOff(), !pSystemStatus->flags.CellsBalanced, pSystemStatus->flags.bootloaderRunning);
		 }
		 
		 break;

		 case evEXIT_STATE:

		 break;
	 }
}



void Go_Sleep(uint8_t mode)
{	
	sleep(mode);	
}

void shutdown_Hw(void)
{
	gpio_set_pin_level(WKUP_UC_3V3,false);
}

void powerup_Hw(void)
{
	gpio_set_pin_level(WKUP_UC_3V3,true);
}

void prepareToSleep()
{
	Disable_Balancing();
	
	//We MUST Disable the sys tick.
   HAL_SYSTICK_Stop();
	
	shutdown_Hw();
	
    Disable_WatchDog();
	
	//debug_print(DBG_PRINTS_ALL, " Going to sleep");
}

void prepareToWakeUp(uint32_t sleep_time) 
{
	Enable_WatchDog();
	
	HAL_SYSTICK_Update((sleep_time)*1000);
	
	HAL_SYSTICK_Start();
	
	//debug_print(DBG_PRINTS_ALL, " Wake up after %lu seconds\n", sleep_time);
	
	powerup_Hw();

}
