#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys\errno.h>

#define MAGIC_WORD  0x57E1B9CA
#define HDR_SIZE    512
#define PAGE_SIZE   512

typedef struct ImgHdr {
    uint32_t magic1;    // magic word
    uint32_t magic2;    // bitwise complement of the magic word
    uint32_t version;   // header version (=0)
    uint32_t reserve1;
    uint32_t h_offset;  // header offset on target board
    uint32_t h_length;  // header length (=HDR_SIZE)
    uint32_t h_chksum;  // header checksum
    uint32_t reserve2;
    uint32_t f_offset;  // image offset (=0)
    uint32_t f_length;  // image length (not includeing header)
    uint32_t f_chksum;  // image checksum (not including header)
    uint32_t reserve3;
    uint32_t reserve4[4];
    uint8_t tlv_info[HDR_SIZE - 64];
} ImgHdrT;

enum {
    TAG_VER = 1,
    TAG_BUILDTIME,
    TAG_BUILDHOST,
    TAG_URL,
    TAG_BRANCH,
    TAG_REV,
    TAG_INFO
};

static bool create_filename(const char *image, char *newfile);
static bool validate_image(FILE *fp);
static bool create_header(FILE *fin, const char *version, const char *info, ImgHdrT *header);
static bool generate_image(FILE *fin, FILE *fout, ImgHdrT *header);

static void print_usage(const char *cmd)
{
    printf("%s Version 1.0\n"
           "<options>\n"
           "\t-f:   firmware image (.bin file)\n"
           "\t-v:   firmware version string\n"
           "\t-i:   firmware info string\n"
           "\t-h:   this help message\n"
           , cmd
           );
}

int main(int argc, char *argv[])
{
    char *image = NULL;
    char *version = NULL;
    char *info = NULL;
    char output[128];
    ImgHdrT header;
    FILE *fin, *fout;
    char c;

    opterr = 0;
    while ((c = getopt(argc, argv, ":f:v:i:h")) != EOF)
    {
        if (optarg && *optarg == '-')
        {
            optarg = NULL;
            optind--;
        }
        if (c == ':')
        {
            c = optopt;
            optarg = NULL;
        }
        switch (c)
        {
        case 'f':
            image = optarg;
            break;
        case 'v':
            version = optarg;
            break;
        case 'i':
            info = optarg;
            break;
        case 'h':
            print_usage(argv[0]);
            return 0;
        }
    }
    if (image == NULL)
    {
        fprintf(stderr, "A firmware image file (.bin) must be specified.\n");
        return 1;
    }
    if (version == NULL)
    {
        fprintf(stderr, "The firmware version must be specified.\n");
        return 2;
    }
    if (!create_filename(image, output))
    {
        fprintf(stderr, "The firmware image file name (%s) is invalid.\n", image);
        return 3;
    }
    fin = fopen(image, "rb");
    if (fin == NULL)
    {
        fprintf(stderr, "%s: %s.\n", image, strerror(errno));
        return 4;
    }
    if (!validate_image(fin))
    {
        fclose(fin);
        fprintf(stderr, "\"%s\" is a invalid firmware image file.\n", image);
        return 5;
    }
    fout = fopen(output, "wb");
    if (fout == NULL)
    {
        fclose(fin);
        fprintf(stderr, "%s: %s.\n", output, strerror(errno));
        return 6;
    }
    if (!create_header(fin, version, info, &header))
    {
        fclose(fout);
        remove(output);
        fclose(fin);
        fprintf(stderr, "Failed to create a header for the firmware image.\n");
        return 7;
    }
    if (!generate_image(fin, fout, &header))
    {
        fclose(fout);
        remove(output);
        fclose(fin);
        fprintf(stderr, "Failed to create the new firmware image.\n");
        return 8;
    }
    fclose(fout);
    fclose(fin);
    fprintf(stdout, "%s firmware image is generated successfully.", output);
    return 0;
}

static bool create_filename(const char *image, char *newfile)
{
    int i, j;
    for (i = j = 0; image[i]; i++)
    {
        newfile[i] = image[i];
        if (image[i] == '.') j = i;
    }
    newfile[j] = '\0';
    strcat(newfile, ".idp");
    return (j > 0 && strcmp(image, newfile));
}

static bool validate_image(FILE *fp)
{
    uint8_t buffer[512];
    if (fread(buffer, 1, sizeof(buffer), fp) != sizeof(buffer)) return false;
    return (buffer[3] == 0x20 && buffer[7] == 0x00);
}

static int get_datetime(uint8_t *buf)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    return sprintf((char *)buf, "%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

static int run_popen(const char *cmd, uint8_t *msg, size_t size)
{
    FILE *fp = popen(cmd, "r");
    if (fp)
    {
        uint8_t buffer[512];
        int len = 0;
        if (fscanf(fp, "%s", buffer) > 0)
        {
            len = strlen((char *)buffer);
            if (len > size) len = size;
            memcpy(msg, buffer, len);
        }
        pclose(fp);
        return len;
    }
    return 0;
}

static bool create_header(FILE *fin, const char *version, const char *info, ImgHdrT *header)
{
    int i, j;
    bool git_ready;
    uint8_t *ptr8;
    uint32_t sum, *ptr32;
    uint32_t buffer[PAGE_SIZE/sizeof(uint32_t)];

    memset(header, 0xFF, sizeof(*header));
    header->magic1 = MAGIC_WORD;
    header->magic2 = ~header->magic1;
    header->version = 0;
    header->h_length = HDR_SIZE;
    header->h_chksum = 0;
    header->f_offset = 0;
    header->f_chksum = 0;

    if (fseek(fin, 0L, SEEK_END) != 0) return false;
    header->f_length = ftell(fin);
    header->h_offset = PAGE_SIZE * ((header->f_length + PAGE_SIZE - 1) / PAGE_SIZE);

    rewind(fin);
    for (i = 0; i < header->h_offset; i += PAGE_SIZE)
    {
        memset(buffer, 0xFF, sizeof(buffer));
        if (fread(buffer, 1, PAGE_SIZE, fin) <= 0) return false;
        for (j = 0; j < 128; j++) header->f_chksum ^= buffer[j];
    }

    /* VERSION */
    ptr8 = (uint8_t *)&header->tlv_info;
    i = strlen(version);
    *ptr8++ = TAG_VER;
    *ptr8++ = i;
    memcpy(ptr8, version, i);
    fprintf(stdout, "Version:\t%*.*s\n", i, i, ptr8);
    ptr8 += i;

    /* BUILD DATE/TIME */
    i = get_datetime(ptr8 + 2);
    *ptr8++ = TAG_BUILDTIME;
    *ptr8++ = i;
    fprintf(stdout, "Build Time:\t%*.*s\n", i, i, ptr8);
    ptr8 += i;

    /* BUILD HOST */
    i = run_popen("hostname", ptr8 + 2, 32);
    if (i > 0)
    {
        *ptr8++ = TAG_BUILDHOST;
        *ptr8++ = i;
        fprintf(stdout, "Host Name:\t%*.*s\n", i, i, ptr8);
        ptr8 += i;
    }

    /* BUILD URL */
    git_ready = false;
    i = run_popen("git config --get remote.origin.url", ptr8 + 2, 128);
    if (i > 4 && memcmp(ptr8 + 2, "git@", 4) == 0)
    {
        *ptr8++ = TAG_URL;
        *ptr8++ = i;
        fprintf(stdout, "Build URL:\t%*.*s\n", i, i, ptr8);
        ptr8 += i;
        git_ready = true;
    }

    if (git_ready)
    {
        /* BUILD BRANCH */
        i = run_popen("git symbolic-ref --short HEAD", ptr8 + 2, 32);
        if (i > 0)
        {
            *ptr8++ = TAG_BRANCH;
            *ptr8++ = i;
            fprintf(stdout, "Build Branch:\t%*.*s\n", i, i, ptr8);
            ptr8 += i;
        }

        /* BUILD REVISION */
        i = run_popen("git describe --abbrev=8 --dirty --always --tags", ptr8 + 2, 32);
        if (i > 0)
        {
            *ptr8++ = TAG_REV;
            *ptr8++ = i;
            fprintf(stdout, "Build Revision:\t%*.*s\n", i, i, ptr8);
            ptr8 += i;
        }
    }

    if (info)
    {
        i = strlen(info);
        if (i > 0)
        {
            *ptr8++ = TAG_INFO;
            *ptr8++ = i;
            memcpy(ptr8, info, i);
            fprintf(stdout, "Firmware Info:\t%*.*s\n", i, i, ptr8);
            ptr8 += i;
        }
    }
    /* Calculate checksum */
    sum = 0xFFFFFFFF;
    ptr32 = (uint32_t *)header;
    for (i = 0; i < HDR_SIZE/4; i++) sum ^= *ptr32++;
    header->h_chksum = sum;
    return true;
}

static bool generate_image(FILE *fin, FILE *fout, ImgHdrT *header)
{
    uint8_t buffer[PAGE_SIZE];
    uint32_t offset;

    rewind(fin);
    rewind(fout);
    if (fwrite(header, 1, HDR_SIZE, fout) != HDR_SIZE) return false;
    for (offset = 0; offset < header->h_offset; offset += PAGE_SIZE)
    {
        memset(buffer, 0xFF, sizeof(buffer));
        if (fread(buffer, 1, PAGE_SIZE, fin) <= 0) return false;
        if (fwrite(buffer, 1, PAGE_SIZE, fout) != PAGE_SIZE) return false;
    }
    return true;
}
