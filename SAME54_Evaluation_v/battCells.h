/*
 * battCells.h
 *
 * Created: 5/11/2020 3:15:00 PM
 *  Author: BehroozH
 */ 


#ifndef BATTCELLS_H_
#define BATTCELLS_H_
#include "lib\stateMachine.h"
#include "adc.h"

extern const uint8_t  ADC1_ICHARGE_CH; 
extern const uint8_t  ADC1_IDISCHARGE_HIGH_CH; 
extern const uint8_t  ADC1_IDISCHARGE_LOW_CH;

extern const uint8_t  ADC0_VCELL2_CH;
extern const uint8_t  ADC0_VCELL3_CH;
extern const uint8_t  ADC0_VCELL4_CH;
extern const uint8_t  ADC0_VREF_CH;
extern const uint8_t  ADC1_VBUS_CH;

    /* Events specific to the protocol SM */
    enum 
	{
	    evBALANCER_INIT_DONE = evMAX_COMMON_EVENTS,
	    evBALANCER_BALANCED,
	    evNOT_BALANCED,
	    evCELLS_UNDER_VOLTAGE,
	    evCELLS_OVER_VOLTAGE,
    };

    /* Balancer State Machine Functions (ROM)-----------------------------------------------------*/
    enum 
	{
	    BALANCER_INIT,
	    BALANCER_NOT_BALANCED,
	    BALANCER_BALANCED,
	    BALANCER_CELLS_UNDER_VOLTAGE,
	    BALANCER_CELLS_OVER_VOLTAGE,
	    BALANCER_NUM_STATES
    };

    uint8_t Get_MinVoltageCell(void);
    void setMinCellVoltages();
    
    void Init_BatteryCells(void);

    void Cells_Monitor(void);

    float Read_Pack_V();

    float getBalancerFSMState();

    bool IsOverVoltageDetected(void);
    bool IsUnderVoltageDetected(void);
    bool IsBalancingNeeded(void);
    void Read_CellVoltages(void);
    uint8_t getEnabledOptoCount();
	float Get_DeltaVoltage();
	float Get_Cell_1_Volt();
	float Get_Cell_2_Volt();
	float Get_Cell_3_Volt();
	float Get_Cell_4_Volt();
	float Get_PackVoltage();
	float Get_TermVoltage();
    float Get_BankVoltage();
	
    // Module globals
    extern float SystemMinCellVoltage;
	
    extern float FVC1, FVC2, FVC3, FVC4;

    extern STATE_MACHINE battCellsFSM;
    extern pANALOG_VALUE pBattModuleTemperature;

    extern pSTATE_MACHINE pBatteryBalancerFSM;


#endif /* BATTCELLS_H_ */