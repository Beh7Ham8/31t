/*
 * drivers.c
 *
 * Created: 5/9/2020 10:52:40 PM
 *  Author: BehroozH
 */ 
#include <sys\_stdint.h>
#include "driver_init.h"
#include "hal_calendar.h"
#include "debugMon.h"
#include "common.h"
#include "drivers.h"
#include "system_definitions.h"

uint32_t Battery_Status_Word;


void Enable_LowPowerPath(void)
{
	bool val = true;
	gpio_set_pin_level(LV_Test,val);
}

void Disable_LowPowerPath(void)
{
	bool val = false;
	gpio_set_pin_level(LV_Test,val);
}

void Enable_Balancing(void)
{
	bool val = true;
	//gpio_set_pin_level(LV_Test,val);
}

void Disable_Balancing(void)
{
	bool val = false;
	//gpio_set_pin_level(LV_Test,val);
}

void TurnON_Heater(void)
{
	bool val = true;
	//gpio_set_pin_level(LV_Test,val);
}

void TurnOFF_Heater(void)
{
	bool val = false;
	//gpio_set_pin_level(LV_Test,val);
}

void Open_Contactor(void)
{
	
}

bool ManualSwitchStatus()
{
	if(gpio_get_pin_level(LV_Test))
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

bool IsShelfMode()
{
	return ManualSwitchStatus();
}


bool IsEngineOff()
{
	float dis_current = 0.0;
	float char_current = 0.0;
	
	return true;
	
	if(IsShelfMode())
	{
		return true;
	}
	else
	{
		 char_current = Get_ChargingCurrent();
		 dis_current  = Get_DisChargingCurrent();
		 
		 if(char_current > 0  || dis_current > 0.005)
		 {
			 return false;
		 }
		 else
		 {
			 return true;
		 }
		
	}
	
	return true;
}

bool IsEngineOn()
{
	float dis_current = 0.0;
	float char_current = 0.0;
	
	if(IsShelfMode())
	{
		return false;
	}
	else
	{
		 char_current = Get_ChargingCurrent();
		 dis_current  = Get_DisChargingCurrent();
		 
		  if(char_current > 0  || dis_current > 0.005)
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	}
	
}


void Enable_ADCs()
{
	
}

void Disable_ADCs()
{
	
}


uint32_t capture_time_sleep()
{
	
	uint32_t retVal;
	
	retVal = _calendar_get_counter(&CALENDAR_0.device);
	
	return retVal;
}

uint32_t capture_time_wakeup()
{	
	uint32_t retVal;
	
	retVal = _calendar_get_counter(&CALENDAR_0.device);
	
	return retVal;
}

void Update_Battery_Status_Word()
{
	uint32_t temp = 0;
	//Voltages
	temp |= ((pSystemStatus->flags.cellUnderVoltageExists)& 0x01);
	temp |= (((pSystemStatus->flags.cellOverVoltageExists)& 0x01) << 1);
		
	//Temperatures
	temp |= (((pSystemStatus->flags.overTemperatureExists)& 0x01) << 8);
	temp |= (((pSystemStatus->flags.overTemperature_Bank_Exist)& 0x01) << 9);
	temp |= (((pSystemStatus->flags.overTemperature_Gnd_Exist)& 0x01) << 10);
	
	//Rest
	temp |= (((pSystemStatus->flags.BalancingInProgress)& 0x01) << 16);
	temp |= (((pSystemStatus->flags.HeaterON)& 0x01) << 17);
	temp |= (((pSystemStatus->flags.ContactorIsOpen)& 0x01) << 18);
	temp |= (((pSystemStatus->flags.engineON)& 0x01) << 19);
	
	
	
	
	Battery_Status_Word = temp;
	
}

uint32_t Get_Battery_Status_Word()
{
	return Battery_Status_Word;
}




