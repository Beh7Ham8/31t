/*
 * system_definitions.h
 *
 * Created: 5/8/2020 10:08:32 AM
 *  Author: BehroozH
 */ 


#ifndef SYSTEM_DEFINITIONS_H_
#define SYSTEM_DEFINITIONS_H_
#include <sys\_stdint.h>
#include "lib/timing.h"

void Init_Interrupts();
void Init_App();
bool Is_Faulted();
bool Is_Unserviced_Fault_Exist();

typedef struct _TAG_SYSTEM_BITS
{
	uint8_t HeaterON:1;
	uint8_t overrideHeaterOn:1;
	uint8_t CellsBalanced:1;
	uint8_t BalancingInProgress:1;
	uint8_t ignOnIntOccurred:1;
	uint8_t engineON:1;
	uint8_t powerInterruptOccurred:1;
	uint8_t powerONOccurred:1;
	uint8_t balancerDelayEnabled:1;
	uint8_t shortCircuitOccurred:1;
	uint8_t dbgUartIntOccurred:1;
	uint8_t overCurrentTimerRunning:1;
	uint8_t ContactorIsOpen:1;
	uint8_t PowerSaveModeIsOn:1;
	uint8_t remoteEngineState:1;
	uint8_t mcuInSleep:1;
	uint8_t remoteChargerState:1;
	uint8_t ChargerEnabled:1;
	uint8_t ChargerDone:1;
	uint8_t firstChargerOn:1;
	uint8_t SystemTestsEnabled:1;
	uint8_t overVoltageExists:1;
	uint8_t underVoltageExists:1;
	uint8_t cellOverVoltageExists:1;
	uint8_t cellUnderVoltageExists:1;
	uint8_t overUnderVoltageExists:1;
	uint8_t overTemperatureExists:1;
	uint8_t overTemperature_Gnd_Exist:1;
	uint8_t overTemperature_Bank_Exist:1;
	uint8_t overcurrentConditionExists:1;
	uint8_t engineOffWarningExists:1;
	uint8_t systemIsInStorageMode:1;
	uint8_t systemIsInSafeMode:1;
	uint8_t hardwareFaultExists:1;
	uint8_t chargerFaultExists:1;
	uint8_t contactorFaultExists:1;
	uint8_t heaterFaultExists:1;
	uint8_t moduleFaultExists:1;
	uint8_t deepSleepModeIsInEffect:1;
	uint8_t batteryProtectModeIsInEffect:1;
	uint8_t bootloaderRunning:1;
	uint8_t unsafeShortCktModeIsInEffect:1;
	uint8_t contactorOnOverrideIsInEffect:1;
	uint8_t contactorOffOverrideIsInEffect:1;
	uint8_t serviceModeEnabled:1;
	uint8_t lowPowerModeIsEnabled:1;
	uint8_t inEngineOFFtrialMode:1;
	uint8_t shouldSkipSleep:1;
	uint8_t deepCycleMode:1;
	uint8_t calibrationModeEnabled:1;

}SYSTEM_BITS, *pSYSTEM_BITS;


typedef struct _TAG_SYSTEM_CONFIG
{
	uint8_t Disable_SleepMode;
	uint8_t Disable_Balancer;
	uint8_t Disable_Heater;
	uint8_t Disable_OV_Protection;
	uint8_t Disable_UV_Protection;
	uint8_t Disable_OT_Protection;
	uint8_t Disable_SC_Protection;
	
}SYSTEM_CONFIG, *pSYSTEM_CONFIG;

typedef union _TAG_SYSTEM_FLAGS
{
	SYSTEM_BITS flags;
	uint64_t    word;
} SYSTEM_FLAGS, *pSYSTEM_FLAGS;

extern pSYSTEM_FLAGS pSystemStatus;


#define FORCED_STORAGE_MODE_THRESHOLD           10.2
#define LOW_POWER_PATH_UNDERVOLTAGE_THRESHOLD   11.0
#define UNDER_VOLTAGE_THRESHOLD                 12.0
#define UNSAFE_SHORT_VOLTAGE_THRESHOLD          12.5
#define CHARGER_VOLTAGE_THRESHOLD               14.0
#define CHARGING_TOP_OFF_VOLTAGE_THRESHOLD      14.45
#define CHARGER_OFF_VOLTAGE_THRESHOLD           13.8
#define OVER_VOLTAGE_THRESHOLD                  15.2
#define EXTREME_OVER_VOLTAGE_THRESHOLD          18.0


typedef enum CauseOfReset
{
	POWER_ON_RESET,
	BOD_CORE,
	BOD_VDD,
	NVM,
	EXT,
	WATCH_DOG_TIMEOUT,
	SYST
}CauseOfReset;


typedef struct __attribute__((__packed__)) _GUID_STRUCT {
	uint32_t Data1;
	uint16_t Data2;
	uint16_t Data3;
	uint8_t Data4[8];
} GUID_STRUCT, *pGUID_STRUCT;

typedef union _GUID {
	GUID_STRUCT structure;
	uint8_t bytes[16];
} GUID, *pGUID;


typedef struct __attribute__((__packed__)) _TAG_UUIDS_HANDLES {
	uint16_t handle;
	GUID     guid;

}UUIDS_HANDLES, *pUUIDS_HANDLES;

#define MAX_AD_PACKET_SIZE 18

typedef struct __attribute__((__packed__)) TAG_AD_FIELDS {
	// Pointer to the data for the packet
	uint8_t         length;
	uint8_t         type;
	GUID            uuid;
}AD_PACKET_FIELDS, *pAD_PACKET_FIELDS;

typedef union _TAG_AD_PACKET {
	AD_PACKET_FIELDS fields;
	uint8_t       bytes[MAX_AD_PACKET_SIZE];
}AD_PACKET, *pAD_PACKET;

#pragma pack(push, 1)
typedef union _TAG_RTC_TIME_STAMP
{
	uint8_t bytes[7];
	
	struct 
	{
		uint16_t year;
		uint8_t month;
		uint8_t day;
		uint8_t dayOfWeek;
		uint8_t hour;
		uint8_t minute;
	};
}RTC_TIME_STAMP, *pRTC_TIME_STAMP;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct _TAG_SERVICE_HANDLES_ 
{
	uint16_t start;
	uint16_t end;
	uint16_t characteristicHandle;
	uint16_t characteristicValueHandle;
	
}SERVICE_HANDLES, *pSERVICE_HANDLES;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union _TAG_TIME_STAMP
{
	uint8_t bytes[7];
	
	struct {
		uint16_t year;
		uint8_t month;
		uint8_t day;
		uint8_t hour;
		uint8_t minute;
		uint8_t second;
	};
}TIME_STAMP, *pTIME_STAMP;
#pragma pack(pop)

enum {
	ACTIVE_MODE = 0,
	STORAGE_MODE = 1,
	LOW_POWER_MODE = 2,
	BLE_TOGGLE_MODE = 3
};


#pragma pack(push, 1)
typedef union _TAG_PERSONALIZATION_DATA
 {
	uint8_t bytes[420];
	
	struct 
	{
		char startSig[9];
		
		uint16_t structVersion;
		char systemSerialNumber[16];
		char systemPartNumber[16];
		char basePartNumber[16];
		char baseSerialNumber[16];
		char contactorSerialNumber[16];
		char pcbSerialNumber[16];
		uint8_t numCells;
		uint32_t cellSerialNumbers[8];
		TIME_STAMP birthDate;
		TIME_STAMP sunriseDate;
		TIME_STAMP sunsetDate;
		float initialSOC;
		float cMonCal;

		bool bleEnabled;
		bool bleUseHighPower;
		bool isCentralRole;
		
		uint8_t txPowerLevel;
		uint32_t wakeCycleLength;

		uint32_t perfDataTransactionId;
		uint32_t perfDataFlashAddress;
		
		bool accidentDetectionEnable;
		bool isBondToAndroid;
		
		uint8_t deepCycleMode;
		
		
		// When modifying this struct, change the struct version in
		// initPersonalizationData()
		// Also add default values to the new struct members
		
		char endSig[9];
	};
}PERSONALIZATION_DATA, *pPERSONALIZATION_DATA;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union _TAG_PERF_STATE_DATA
{
	uint8_t bytes[116];
	
	struct 
	{
		uint8_t ignitionStartCount;
		uint8_t contactorStateChangeCount;
		uint8_t balancerEngageCount;
		uint8_t chargeStateCount;
		uint8_t heaterOnCount;
		uint8_t underVoltageCount;
		float TermVolt_Max;
		float TermVolt_Min;
		float PackVolt_Max;
		float PackVolt_Min;
		float Curr_Max;
		float Curr_Min;
		float Curr_Avg;
		float Curr_Sum;
		int32_t Cur_Num_Samp;
		float C1_Max;
		float C1_Min;
		float C1_Avg;
		float C1_Sum;
		int32_t C1_Num_Samp;
		float C2_Max;
		float C2_Min;
		float C2_Avg;
		float C2_Sum;
		int32_t C2_Num_Samp;
		float C3_Max;
		float C3_Min;
		float C3_Avg;
		float C3_Sum;
		int32_t C3_Num_Samp;
		float C4_Max;
		float C4_Min;
		float C4_Avg;
		float C4_Sum;
		int32_t C4_Num_Samp;
		float BodyTemperature_Max;
		float BodyTemperature_Min;
		float CellTemperature_Max;
		float CellTemperature_Min;
		float SOC_Max;
		float SOC_Min;
		TIME_STAMP shortCktStartTime;
		TIME_STAMP shortCktStopTime;
	};
}PERF_STATE_DATA, *pPERF_STATE_DATA;
#pragma pack(pop)

// Holds all the performance data
extern PERF_STATE_DATA performanceData;

#pragma pack(push, 1)
typedef union _TAG_PERF_DATA_RECORD
{
	uint8_t bytes[131];
	
	struct {
		uint32_t transactionId;
		uint8_t nodeId;
		uint8_t length;
		uint64_t elapsedTimeSeconds;
		PERF_STATE_DATA perfStateValues;
		uint8_t checksum;
	};
}PERF_DATA_RECORD, *pPERF_DATA_RECORD;
#pragma pack(pop)


struct StateMachineTracker
{
	uint32_t BattSOC_counter;
	uint32_t Temp_counter;
	uint32_t BattCells_counter;
	uint32_t Sleep_counter;
	
};

typedef enum
{
	/* Application's state machine's initial state. */
	APP_STATE_INIT=0,
	APP_STATE_SERVICE_TASKS,

	/* TODO: Define states used by the application state machine. */

} APP_STATES;


typedef struct APP_DATA
{
	uint32_t startSig; 
	uint16_t structVersion;
	APP_STATES state; 
	uint16_t numOfInt_Fault;
	uint16_t numOfInt_WakeUp;
	uint16_t numOfInt_Rdy;
	bool fault_Occured;
	bool fault_Serviced;
	bool wakeUpCurrent_Detected;
	uint32_t system_mode;
	uint32_t manualSwitch;
} __attribute__((packed)) APP_DATA, *pAPP_DATA;


extern struct StateMachineTracker FSM_counters;


// Different battery model part numbers
#define BATTERY_MODEL_108 "010-01050-00"
#define BATTERY_MODEL_110 "010-01060-00"
#define BATTERY_MODEL_120 "010-01070-00"
#define BATTERY_MODEL_210 "010-01080-00"
#define BATTERY_MODEL_220 "010-01090-00"

extern SW_TIMER bootloaderModeTimer;
extern SW_TIMER serviceModeTimer;




#endif /* SYSTEM_DEFINITIONS_H_ */