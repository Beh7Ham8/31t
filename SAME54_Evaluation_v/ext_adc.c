/*
 * ext_adc.c
 *
 * Created: 6/11/2020 1:17:53 PM
 *  Author: BehroozH
 * This code works for ADS1114 ultra-small low-power i2c-compatible, 16-Bit ADC (Texas Instrument chip)
 */ 
#include "driver_init.h"
#include "lib\timing.h"
#include "debugMon.h"
#include "ext_adc.h"
#include "hal_io.h"
#include "common.h"

struct io_descriptor *I2C_0_io;

//if ADDR pin is connected to GND, then i2c address is 0x48
//if ADDR pin is connected to VDD, then i2c address is 0x49
uint8_t EXT_ADC_CHIP_ADDR = 0x48;

#define EXT_ADC_CONVERSION_REG		0x00
uint8_t   EXT_ADC_CONFIG_REG =		0x01;
#define EXT_ADC_LOW_THRESHOLD_REG	0x02
#define EXT_ADC_HI_THRESHOLD_REG	0x03

const uint8_t  COMP_QUE = 0x00;
const uint8_t  COMP_LAT = 0x01;
const uint8_t  COMP_POL = 0x00;
const uint8_t  COMP_MODE = 0x00;
const uint8_t  DR = 0x04;
const uint8_t  MODE = 0x01;
const uint8_t  PGA = 0x02;
const uint8_t  MUX = 0x00;
const uint8_t  OS = 0x01;

const uint16_t POS_SATURATION_VAL = 0x7FFF;
const uint16_t NEG_SATURATION_VAL = 0x8000;

uint16_t CONFIG_WORD = 0x8583;
uint32_t gain;

float coefficient = 0.0625;//62.5 micro volt per count

uint8_t Conversion_Result[2];
float f_current_voltage = 0;
float Terminal_Current = 0;

SW_TIMER shuntTimer;
pSW_TIMER pShuntTimer;

static void _Config_ADS_111x();
static int32_t _Start_Conversion();
static int32_t _Read_Conversion_Result();
static void _SetGain(eADSGain_t value);


void Init_Shunt_ADS111x()
{
	uint32_t res = 0;
	pShuntTimer = &shuntTimer;
	
	// init the interface
	i2c_m_sync_get_io_descriptor(&I2C_0, &I2C_0_io);
	i2c_m_sync_enable(&I2C_0);
	i2c_m_sync_set_slaveaddr(&I2C_0, EXT_ADC_CHIP_ADDR, I2C_M_SEVEN);
	
	_Config_ADS_111x();
		
	timerInit(pShuntTimer, PERIODS.SHUNT);
}

void Test_ADS_111x_Monitor()
{
	int32_t result0 = -1000,result1 = -1000;
	int16_t voltage = 0;
	float fvoltage = 0;
	uint8_t rdbk[2];
		
	rdbk[0] = rdbk[1]= 0x00;
		
	if (timerExpired(pShuntTimer))
	{	
		timerReset(pShuntTimer);		
		
again:	result0 = i2c_m_sync_cmd_read(&I2C_0, EXT_ADC_CONFIG_REG , rdbk, 2);
		
		// make sure the ADC is not busy doing a conversion
		if(result0 == 0 && ((rdbk[1] & 0x80) == 0x80))
		{		
			_Start_Conversion();
			
			result1 = _Read_Conversion_Result();
			
			if(result1 == 0)
			{				
				voltage =  (((uint16_t)Conversion_Result[1] | (uint16_t)Conversion_Result[0]<<8));
				
				if(((voltage & 0xFFFF) == POS_SATURATION_VAL  || (voltage & 0xFFFF) == NEG_SATURATION_VAL) && (gain > eGAIN_TWOTHIRDS))
				{
					  _SetGain(gain-1);
					  _Start_Conversion();
					  debug_print(DBG_SOC, " Shunt___ADC |    V= %X  Gain adjusted to = %lu",voltage,gain);					 
					  goto again;
				}
				
				fvoltage = (voltage * coefficient)/1000;
		
				debug_print(DBG_SOC, " Shunt___ADC |    V= %d     V= %X   Voltage = %2.6f",voltage, voltage,fvoltage);
				Conversion_Result[0] = 0x00;
				Conversion_Result[1] = 0x00;
			}
			else
			{
				debug_print(DBG_SOC, " DBG_SOC |  Error in reading Shunt ADC voltage  %ld   ",result1);
			}
		}
		else
		{
			debug_print(DBG_SOC, " DBG_SOC |  Error in reading Shunt ADC status %ld   ",result1);
		}				
	}
}

void Read_Current()
{
	int32_t result0 = -1000, result1 = -1000;
	int16_t voltage = 0; // must be signed int
	
	uint8_t rdbk[2];
	
	rdbk[0] = rdbk[1]= 0x00;
	
	again:	result0 = i2c_m_sync_cmd_read(&I2C_0, EXT_ADC_CONFIG_REG , rdbk, 2);
	
	// make sure the ADC is not busy doing a conversion
	if(result0 == 0 && ((rdbk[1] & 0x80) == 0x80))
	{
		_Start_Conversion();
		
		result1 = _Read_Conversion_Result();
		
		if(result1 == 0)
		{
			voltage =  (((uint16_t)Conversion_Result[1] | (uint16_t)Conversion_Result[0]<<8));
			
			if(((voltage & 0xFFFF) == POS_SATURATION_VAL  || (voltage & 0xFFFF) == NEG_SATURATION_VAL) && (gain > eGAIN_TWOTHIRDS))
			{
				_SetGain(gain-1);
				_Start_Conversion();
				debug_print(DBG_SOC, " Shunt___ADC |    V= %X  Gain adjusted to = %lu",voltage,gain);
				goto again;
			}
					
			// convert to volt
			f_current_voltage = (voltage * coefficient)/1000;
			
			Terminal_Current = (f_current_voltage / R_SHUNT);
			
			Conversion_Result[0] = 0x00;
			Conversion_Result[1] = 0x00;
		}
		else
		{
			debug_print(DBG_SOC, " DBG_SOC |  Error in reading Shunt ADC voltage  %ld   ",result1);
		}
	}
	else
	{
		debug_print(DBG_SOC, "DBG_SOC |  Error in reading Shunt ADC status %ld   ",result1);
	}
}

float Get_Current()
{
	return Terminal_Current;
}

static int32_t _Read_Conversion_Result()
{
	int32_t retVal =0;
	
	retVal = i2c_m_sync_cmd_read(&I2C_0, EXT_ADC_CONVERSION_REG , Conversion_Result, 2);
	
	return retVal;	
}

static void _Config_ADS_111x()
{
	 int32_t retVal1 = 0,retVal2 = 0,retVal3 = 0; 
	 int32_t retVal1_rd = 0, retVal2_rd = 0,retVal3_rd = 0;
	 
	 uint8_t config_data[2];
	 uint8_t config_data_rdbk[2];
	 
	 uint8_t temp[3];
	  
	config_data_rdbk[0] = config_data_rdbk[1] = 0;
	
	 config_data[1] = (uint8_t)(CONFIG_WORD & 0xFF);
   	 config_data[0] = (uint8_t)((CONFIG_WORD >> 8) & 0xFF);

      temp[0] = EXT_ADC_CONFIG_REG;
	  temp[2] = config_data[1];
   	  temp[1] = config_data[0];
	 
	  retVal1 = io_write(I2C_0_io,temp, 3);
	  
	  retVal1_rd = i2c_m_sync_cmd_read(&I2C_0, EXT_ADC_CONFIG_REG , config_data_rdbk, 2);
	 
	  
	   if(( (config_data[0] & 0x7F) != config_data_rdbk[0])  || ( config_data[1] != config_data_rdbk[1]) || (retVal1 != 3) || (retVal1_rd != 0) )
	   {
		  debug_print(DBG_SOC, " DBG_SOC |   %s   ","Failed programming Configuration register.");
	   }
	   
	  _SetGain(eGAIN_SIXTEEN);

	 return;
}

static int32_t _Start_Conversion()
{
	int32_t RetVal =0;
	
	uint16_t conversion_word = (CONFIG_WORD | 0x8000);
	
	uint8_t config_data[3];
	
	config_data[0] = EXT_ADC_CONFIG_REG;
	config_data[2] = (uint8_t)(conversion_word & 0xFF);
	config_data[1] = (uint8_t)((conversion_word >> 8) & 0xFF);
	
	RetVal = io_write(I2C_0_io, config_data, 3);
	
	return RetVal;
}

static void _SetGain(eADSGain_t value)
{
	int32_t    retVal1 = 0;
	int32_t    retVal2 = 0;
	uint8_t    rd_buff[2];
	uint8_t    rdbk_buff[2];
	uint8_t    wr_buff[3];
	uint8_t    temp;
	
    retVal1 = i2c_m_sync_cmd_read(&I2C_0, EXT_ADC_CONFIG_REG ,  rd_buff, 2);
	
	temp = rd_buff[0];
	temp &= ~((uint8_t) (0x07 << 1));
	temp |= ((uint8_t) (value << 1));
	
	wr_buff[0] = EXT_ADC_CONFIG_REG;
	wr_buff[1] = temp;
	wr_buff[2] = rd_buff[1];
	
	CONFIG_WORD = (((uint16_t)wr_buff[2] | (uint16_t)wr_buff[1]<<8));
	
	 retVal2 = io_write(I2C_0_io, wr_buff, 3);
	
	gain = value; 
	
	switch(value)
	{
		case eGAIN_TWO:
		coefficient = 0.0625;
		break;
		case eGAIN_TWOTHIRDS:
		coefficient = 0.1875;
		break;
		case eGAIN_ONE:
		coefficient = 0.125;
		break;
		case eGAIN_FOUR:
		coefficient = 0.03125;
		break;
		case eGAIN_EIGHT:
		coefficient = 0.015625;
		break;
		case eGAIN_SIXTEEN:
		coefficient = 0.0078125;
		break;
		default:
		coefficient = 0.0625;
		break;
	}
}

