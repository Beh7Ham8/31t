/*
 * encoder.h
 *
 * Created: 5/8/2020 2:28:52 PM
 *  Author: BehroozH
 */ 


#ifndef ENCODER_H_
#define ENCODER_H_
#include <sys\_stdint.h>

/// The length of the buffers to use in the linked list. This should be set
/// higher than the maximum possible packet size

// Max packet size allowed, specifically when performing Bootloader tasks over the wire
#define MAX_PACKET_SIZE 1032

// Max packet size allowed for Bootloader tasks over Bluetooth
#define BLE_BOOTLOADER_PACKET_SIZE 16

// Default packet size used for host, test, debug commands where 4 byte parameters are exchanged
#define DEF_PACKET_SIZE 9

// The number of bytes of constant fields in a packet, not including the parameter field
#define CONST_PACKET_FIELDS 5

// Struct used for Bootloader parameter field including 1 byte status and 3 byte block number
#pragma pack(push, 1)
typedef union TAG_PARAM_BL 
{
	// Structure for a status message response
	struct 
	{
		int8_t status;
		uint8_t statusData[MAX_PACKET_SIZE - CONST_PACKET_FIELDS - 1];
	};
	
	// Structure for a block of data
	struct 
	{	
		uint8_t blockNumBytes[3];	
		uint8_t dataBlock[MAX_PACKET_SIZE - CONST_PACKET_FIELDS - 3];
	};
}PARAM_BL, *pPARAM_BL;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct _TAG_PARAM
{
	union {
		uint32_t    iVal;
		float       fVal;
	};
	uint8_t paddingArray[MAX_PACKET_SIZE - CONST_PACKET_FIELDS - 4];
}PARAM, *pPARAM;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union _TAG_UNIVERSAL_VAL
{
	PARAM_BL    bootLoader;
	PARAM       value;
	uint8_t     array[MAX_PACKET_SIZE - CONST_PACKET_FIELDS];
}UNIVERSAL_VAL, *pUNIVERSAL_VAL;
#pragma pack(pop)

//__attribute__((__packed__))
#pragma pack(push, 1)
typedef struct  TAG_PACKET_FIELDS
{
	// Pointer to the data for the packet
	uint8_t         classPlusCmdID;
	uint16_t        length;
	uint8_t         typePlusIndex;
	uint8_t         checksum;
	UNIVERSAL_VAL   param;
}PACKET_FIELDS, *pPACKET_FIELDS;
#pragma pack(pop)

#pragma pack(push, 1)
typedef union _TAG_PACKET
{
	PACKET_FIELDS fields;
	uint8_t       bytes[MAX_PACKET_SIZE];
}PACKET, *pPACKET;
#pragma pack(pop)


// SLIP special characters
/// Character used to signal the start or end of a frame
#define PACKET_END          0xC0
/// Character used to escape the use of special characters in the data
#define BYTE_ESC            0xDB
/// Character used to replace a SLIP_END character in the data
#define BYTE_ESC_END        0xDC
/// Character used to replace a SLIP_ESC character in the data
#define BYTE_ESC_ESC        0xDD


void initEncoder();
uint16_t encode(uint8_t *dest, uint16_t dest_size, uint8_t *source, uint16_t source_size);
uint16_t decode(uint8_t *source, uint16_t length, pPACKET pckt);
uint8_t readAndDecodeByteWired(uint8_t c, pPACKET pckt);
uint8_t readAndDecodeByteMobile(uint8_t c, pPACKET pckt);
uint8_t readAndDecodeByteBattery(uint8_t c, pPACKET pckt);





#endif /* ENCODER_H_ */