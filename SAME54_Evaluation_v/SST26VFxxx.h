#ifndef _FLASH_SST26VFxxxB_H_
#define _FLASH_SST26VFxxxB_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <stdint.h>

/* FLASH TYPE */
#define SST26VF016B     0   /* 16Mbits */
#define SPANSION_32MBIT 1   /* 32Mbits */
#define SST26VF064B     2   /* 64Mbits */

#define USE_FLASH       SST26VF064B

#if (USE_FLASH == SST26VF016B)
#define SPI_FLASH_MANUFACTURER_ID   0xBF
#define SPI_FLASH_DEVICE_TYPE       0x26
#define SPI_FLASH_DEVICE_ID         0x41

#elif (USE_FLASH == SPANSION_32MBIT)
#define SPI_FLASH_MANUFACTURER_ID   0x01
#define SPI_FLASH_DEVICE_TYPE       0x40
#define SPI_FLASH_DEVICE_ID         0x15

#elif (USE_FLASH == SST26VF064B)
#define SPI_FLASH_MANUFACTURER_ID   0xBF
#define SPI_FLASH_DEVICE_TYPE       0x26
#define SPI_FLASH_DEVICE_ID         0x43

#else
#error !!No Flash Type Defined!!
#endif

// Flash definitions
#define RSTEN   0x66   //Reset Enable
#define RST5    0x99   //Reset Memory
#define EQIO    0x38   //Enable Quad I/O
#define RSTQIO6 0xFF    //Reset Quad I/O
#define RDSR    0x05    // Read Status Register
#define WRSR    0x01    //  Write Status Register
#define RDCR    0x35    // Read Configuration Register

// READ
#define READ    0x03    //  Read Memory
#define READHS  0x0B    // Memory at Higher Speed
#define SQOR7   0x6B    // SPI Quad Output Read
#define SQIOR8  0xEB    //  SPI Quad I/O Read
#define SDOR9   0x3B    //  SPI Dual Output Read
#define SDIOR10 0xBB    //  SPI Dual I/O Read
#define SB      0xC0    //  Set Burst Length
#define RBSQI   0x0C    //  SQI Read Burst with Wrap
#define RBSPI8  0xEC    //  SPI Read Burst with Wrap

// Identification
#define JEDECID 0x9F    // JEDEC-ID Read
#define QJID    0xAF    //  Quad I/O J-ID Read
#define SFDP    0x5A    //  Serial Flash Discoverable Parameters

// Write
#define WREN    0x06    //  Write Enable
#define WRDI    0x04    //  Write Disable
#define SE11    0x20    //  Erase 4 KBytes of Memory Array
#define BE12    0xD8    //  Erase 64, 32 or 8 KBytes of Memory Array
#define CE      0xC7    //  Erase Full Array
#define PP      0x02    //  Page Program
#define SPIQ    0x32    //  uad PP7 SQI Quad Page Program

#define WRSU    0xB0    //  Suspends Program/Erase
#define WRRE    0x30    //  Resumes Program/Erase

// Protection
#define RBPR    0x72    //  Read Block-Protection Register
#define WBPR    0x42    //  Write Block-Protection Register
#define LBPR    0x8D    //  Lock Down Block-Protection Register
#define nVWLDR  0xE8    //  non-Volatile Write LockDown Register
#define ULBPR   0x98    //  Global Block Protection Unlock
#define RSID    0x88    //  Read Security ID
#define PSID    0xA5    //  Program User Security ID area
#define LSID    0x85    //  Lockout Security ID Programming
#define PSDPD   0xB9    //  Deep Power-down Mode
#define RDPD    0xAB    //  Release from Deep Powerdown and Read ID


#define MAX_BYTE_COUNT_PER_SECTOR   4096
#define MAX_BYTE_COUNT_PER_PAGE     256
#define MAX_SPI_TX_BUFFER_SIZE      512
#define PAGES_PER_SECTOR            16

typedef struct _TAG_FLASH_CHIP_ {
    uint8_t manufacturer;
    uint8_t deviceType;
    uint8_t deviceID;
} FLASH_CHIP, *pFLASH_CHIP;

extern FLASH_CHIP spiFlashChip;

/* Macro definitions for compatible with the old firmware */
#define readChipID                      Flash_GetID
#define eraseSector                     Flash_EraseSector
#define readPage                        Flash_ReadPage
#define readBytes                       Flash_ReadBytes
#define writePage                       Flash_WritePage
#define writeBytes                      Flash_WriteBytes
#define enableWrite                     Flash_EnableWrite
#define disableWrite                    Flash_DisableWrite
#define globalBlockProtectionUnlock     Flash_UnlockAll
#define readBlockProtectionRegister     Flash_GetBlockFlags

#define readStatusRegister              Flash_GetStatus
#define readConfigRegister              Flash_GetConfig
#define enableFlashForWriting           Flash_EnableWriteAndDone
#define writePageToFlash                Flash_WritePageAndDone
#define writeBytesToFlash               Flash_WriteBytesAndDone
#define readPageFromFlash               Flash_ReadPageAndLock
#define readBytesFromFlash              Flash_ReadBytesAndLock
#define eraseSectorFromFlash            Flash_EraseSectorAndDone

#define writeSectorToFlash              Flash_WriteSectorAndDone
#define readSectorFromFlash             Flash_ReadSectorAndLock



void Flash_Reset(void);
bool Flash_GetID(void);

uint8_t Flash_GetBlockFlags(void);
uint8_t Flash_GetStatus(void);
uint8_t Flash_GetConfig(void);

void Flash_UnlockAll(void);
void Flash_DisableWrite(void);
void Flash_EnableWrite(void);
void Flash_EnableWriteAndDone();

bool Flash_ReadPage(uint32_t pageNum, uint8_t *buf);
void Flash_ReadPageAndLock(uint8_t *pData, uint32_t pageAddr); // internal used

bool Flash_ReadBytes(uint32_t pageAddr, uint8_t *buf, uint32_t bufSize); // internal used
void Flash_ReadBytesAndLock(uint8_t *pData, uint32_t pageAddr, uint32_t numOfBytes); /// not used

void Flash_ReadSectorAndLock(uint8_t *dataBlock, uint32_t sectorNum);

bool Flash_WritePage(uint32_t pageAddr, uint8_t *buf);
void Flash_WritePageAndDone(uint8_t *pData, uint32_t pageAddr);

void Flash_WriteBytes(uint32_t pageAddr, uint8_t *buf, uint8_t bufSize); // internal used
uint8_t Flash_WriteBytesAndDone(uint32_t pageAddr, uint8_t *buf, uint8_t bufSize); /// not used

int8_t Flash_WriteSectorAndDone(uint8_t *dataBlock, uint32_t sectorNum);

bool Flash_EraseSector(uint32_t sectorNum); // internal used
void Flash_EraseSectorAndDone(uint32_t sectorNum);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif
