/** FATFS application code
 *
 *  This code deals with the partition table; FATFS mount, fs-format (f_mkfs), and unmount
 *  logic; and data-format (CSV) write logic.  Each section is summarized below, and
 *  documented in more detail through the code.
 *
 *  The enum part_t lists the number of partitions this code is expected to handle.  For
 *  each partition, a canned partition table entry is stored; if the entry on the disk
 *  inserted does not match, it is corrected to the one in the firmware, and the first
 *  sector of the partition zeroed out to trigger a call to f_mkfs at mount time.  The
 *  function fatfs_parts() is called from the sd_mmc FSM while loading the card.
 *
 *  The function fatfs_mount() attempts to mount each partition's filesystem; if this
 *  fails due to error FR_NO_FILESYSTEM it attempts to format the partition with f_mkfs()
 *  and remount.  Partitions mounted successfully have the corresponding bit set in the
 *  bitmap part_open, which is checked by fatfs_unmount().  Ths is called by the sd_mmc
 *  FSM when the card is ejected, or USB-MSC takes the card over.
 *
 *  The function fatfs_write() writes datalog records to the open partitions.  Which
 *  channels are written to which partitions is controlled by part_mask[].  This calls the
 *  datalog_full() to get the path, mkdir_parent() to ensure directories are made, creates
 *  the file and writes the header returned by datalog_head() if necessary, writes the
 *  data row returned by datalog_data(), and closes the file.
 *
 *  fatfs_write() and fatfs_mount() internally call the data reaping code to ensure each
 *  channel stays within its assigned quota of disk usage.  A reserved percentage of space
 *  is deducted from each partition's size, then each channel's quota is apportioned based
 *  on the percentage assigned.  When the total size of each channel's data on each
 *  partition exceeds this quota, the oldest file is removed until the total size is
 *  within quota.
 */
#include <stdio.h>
#include <string.h>

#include <driver_init.h>
#include <lib/csv.h>
#include "fatfs.h"
#include "parts.h"
#include "ff.h"


#define debug(fmt, ...) do{ }while(0)
#define info(fmt, ...)  printf(fmt, ##__VA_ARGS__)
#define error(fmt, ...) printf(fmt, ##__VA_ARGS__)


/* Max length of a filename for this code */
#define PATH_MAX 48

/* Reserved space percentage: if defined, the size of each partition is reduced by this
 * many percent before calculating the quota for each channel on it. */
#define RESV_SPACE 10


/* Each bit in this bitmap represents an open filesystem, it is set by fatfs_mount(),
 * cleared by fatfs_unmount(), and checked by fatfs_write() to skip partitions that are
 * unmounted. */
static uint8_t   part_open;

/* Each partition gets an entry in part_mask, and each bit in part_mask[part] allows a
 * particular data channel to be written on a particular partition. 
 *
 * By default all channels are written to all open partitions; to block a particular
 * channel for a given partition, clear the appropriate bit.  For example, to prevent
 * channel DATALOG_SECRET from being written to PART_USER, set
 *   part_mask[PART_USER] &= ~(1 << DATALOG_SECRET);
 *
 * It may be useful to fix this at compile time, in which case we could specify that the
 * USER partition gets only DATALOG_FOO and DATALOG_BAR, while the SERV partition gets all
 * channels, and make the mask const to prevent runtime changes:
 *   static const unsigned  part_mask[PART_MAX] =
 *   {
 *     [PART_USER] = (1 << DATALOG_FOO) | (1 << DATALOG_BAR),
 *     [PART_SERV] = ~0,
 *   };
 */
static unsigned  part_mask[PART_MAX] =
{
	[PART_USER] = ~0,
	[PART_SERV] = ~0,
};


/* Reap data includes the state of each channel on each partition, though only open parts
 * and enabled channels are used */
static struct reap_data
{
	uint64_t  quota;   /* Quota in bytes */
	uint64_t  bytes;   /* Bytes total at last scan */
	char      newest[13];   /* Newest filename found in last scan */
	char      oldest[13];   /* Oldest filename found in last scan */
	char      current[13];  /* Current filename */
}
reap_data[PART_MAX][DATALOG_NUM_CHANNELS];


/** Unmount all mounted partitions
 *
 *  Each partition in part_t is checked against part_open, if open it's unmounted.
 */
void fatfs_unmount (void)
{
	FRESULT  res;
	part_t   part;
	char     path[3] = { '0', ':', '\0' };

	for ( part = PART_USER; part < PART_MAX; part++ )
		if ( part_open & (1 << part) )
		{
			path[0] = part + '0';

			res = f_mount(NULL, path, 0);
			ASSERT(FR_OK == res);

			part_open &= ~(1 << part);
		}
}


/** Return free space in bytes for a mounted part */
static uint64_t fatfs_free (part_t part)
{
	FRESULT   res;
	FATFS    *fatfs;
	char      path[3] = { '0' + part, ':', '\0' };
	uint32_t  clusters;
	uint64_t  bytes;

	if ( FR_OK != (res = f_getfree(path, &clusters, &fatfs)) )
	{
		error("fatfs_free(%d): %d, stop\r\n", part, res);
		return 0;
	}

	bytes  = clusters;
	bytes *= fatfs->csize;
	bytes *= SECTOR_SIZE;
	return bytes;
}


/** Mount all partitions, formatting if appropriate
 *
 *  Each partition in part_t is checked against part_open, if open it's skipped.  If the
 *  first mount attempt fails with the result FR_NO_FILESYSTEM, an attempt is made to
 *  format the partition with f_mkfs() and remount.
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
static FATFS  part_fatfs[PART_MAX];
FRESULT fatfs_mount (void)
{
	FRESULT  res = FR_OK;
	part_t   part;
	char     path[3] = { '0', ':', '\0' };

	debug("fatfs: attempt mount/mkfs...\r\n");
	for ( part = PART_USER; part < PART_MAX; part++ )
	{
		/* Skip already opened partitions */
		if ( part_open & (1 << part) )
		{
			debug("fatfs: part %d already open\r\n", part);
			continue;
		}

		/* Attempt mount */
		path[0] = part + '0';
		switch ( (res = f_mount(&part_fatfs[part], path, 1)) )
		{
			/* Success, set flag and move on to next */
			case FR_OK:
				info("fatfs: part %d mount: OK\r\n", part);
				part_open |= 1 << part;
				break;

			/* Need to format filesystem.  If f_mkfs() fails then abort */
			case FR_NO_FILESYSTEM:
				info("fatfs: part %d has no FS, try mkfs...\r\n", part);
				if ( FR_OK != (res = f_mkfs(path, 0, 0)) )
				{
					error("fatfs: part %d mkfs: %d, stop\r\n", part, res);
					return res;
				}

				/* Remount should succeed */
				res = f_mount(&part_fatfs[part], path, 1);
				info("fatfs: part %d mkfs OK, mount: %d\r\n", part, res);
				ASSERT(FR_OK == res);
				part_open |= 1 << part;
				break;

			/* Unusual result: abort */
			default:
				error("fatfs: part %d mount: %d, stop\r\n", part, res);
				return res;
		}

		/* Display free space */
		uint64_t  bytes = fatfs_free(part);
		info("fatfs: part %d has %lluMB (%llu bytes) free\r\n", part, 
		     (unsigned long long)bytes >> 20, (unsigned long long)bytes);

		/* Quash compiler warnings if info() undefined */
		(void)bytes;
	}

	/* After all mounts succeed, do a full reap pass to calculate current values */
	return fatfs_reap_slow();
}

/** Make parent directory chain for the given path.
 *
 *  \param  path  Full pathname (ie 0:/dir/file.txt)
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
static FRESULT mkdir_parent (char *path)
{
	char *b = path;
	if ( path[0] && path[1] == ':' )
		b += 2;

	/* Starting from the left side, save and zero each separator char to terminate the
	 * string there, f_mkdir() the fragment, then restore the saved separator. */
	char *p = b;
	char *q;
	while ( (q = strchr(p + 1, '/')) || (q = strchr(p + 1, '\\')) )
	{
		FRESULT  r;
		char     t = *p;

		*q = '\0';
		if ( q > b )
		{
			r = f_mkdir(path);
			switch ( r )
			{
				case FR_OK:
				case FR_EXIST:
					break;

				default:
					*q = t;
					return r;
			}
		}

		*q = t;
		p = q;
	}
  
	return FR_OK;
}


/** Round up a file size to the partition's cluster size for quota checks */
static uint32_t round_fsize (const FATFS *fs, uint32_t size)
{
	uint32_t  cluster = SECTOR_SIZE * fs->csize;

	if ( size % cluster )
	{
		size /= cluster;
		size++;
		size *= cluster;
	}

	return size;
}


/** Print a FILINFO as a directory listing */
static void print_filinfo (const FILINFO *inf)
{
	debug("  %-13s %10lu %04u-%02u-%02u %02u:%02u:%02u %c%c%c%c%c%c\r\n",
	      inf->fname, (unsigned long)inf->fsize,
	      (inf->fdate >> 9) + 1980,
	      (inf->fdate >> 5) & 0x0F,
	      inf->fdate & 0x1F,
	      inf->ftime >> 11,
	      (inf->ftime >> 5) & 0x3F,
	      (inf->fdate & 0x1F) << 2,
	      inf->fattrib & AM_RDO ? 'R' : '-',
	      inf->fattrib & AM_HID ? 'H' : '-',
	      inf->fattrib & AM_SYS ? 'S' : '-',
	      inf->fattrib & AM_VOL ? 'V' : '-',
	      inf->fattrib & AM_DIR ? 'D' : '-',
	      inf->fattrib & AM_ARC ? 'A' : '-');
}


/** Scan channel on part, update reap_data */
static FRESULT fatfs_reap_scan (char *path, int max, datalog_channel_t  chan, part_t  part)
{
	struct reap_data *rd = &reap_data[part][chan];
	FRESULT           res;
	FILINFO           cur;
	DIR               dir;
	int               cnt = 0;

	/* latch first file, total bytes, and find oldest/newest file */
	rd->bytes = 0;
	memset(rd->oldest, 0, sizeof(rd->oldest));
	memset(rd->newest, 0, sizeof(rd->newest));

	/* Format path; if you assert here then increase PATH_MAX above */
	int len = snprintf(path, max, "%s", datalog_path(chan));
	path[0] = '0' + part;
	ASSERT(len < max);

	/* scan dir for first file */
	res = f_findfirst(&dir, &cur, path, datalog_pattern(chan));
	switch ( res )
	{
		case FR_OK:
			break;

		case FR_NO_FILE:
		case FR_NO_PATH:
			debug("scan: findfirst: NO_FILE or NO_PATH, ok\r\n");
			return FR_OK;

		default:
			error("findfirst: %d\r\n", res);
			return res;
	}

	/* while we have results: print, total bytes, save oldest/newest file */
	memcpy(rd->oldest, cur.fname, sizeof(rd->oldest));
	memcpy(rd->newest, cur.fname, sizeof(rd->newest));
	while ( cur.fname[0] && FR_OK == res )
	{
		cnt++;
		print_filinfo(&cur);

		rd->bytes += round_fsize(&part_fatfs[part], cur.fsize);

		if ( strcmp(cur.fname, rd->oldest) < 0 )
			memcpy(rd->oldest, &cur.fname, sizeof(rd->oldest));

		if ( strcmp(cur.fname, rd->newest) > 0 )
			memcpy(rd->newest, cur.fname, sizeof(rd->newest));

		memset(&cur, 0, sizeof(cur));
		res = f_findnext(&dir, &cur);
	}
	f_closedir(&dir);
	debug("  files:%6d  %10llu bytes used (%llu%% of quota)\r\n",
	      cnt, (unsigned long long)rd->bytes,
	      (unsigned long long)((rd->bytes * 100) / rd->quota));

	debug("\r\nscan: \r\n\toldest %s\r\n\tnewest %s\r\n", rd->oldest, rd->newest);
	info("scan: bytes %llu quota %llu: ",
	      (unsigned long long)rd->bytes,
	      (unsigned long long)rd->quota);
	if ( rd->bytes > rd->quota )
		info("\e[1;31mneed to reap %llu", (unsigned long long)(rd->bytes - rd->quota));
	else
		info("\e[1;32mno action needed");

	info("\e[m\r\n");
	debug("\r\n");
	return res;
}


/** Common/internal data reap
 *
 *  \param  part   Partition ID
 *  \param  chan   Channel ID
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
static FRESULT fatfs_reap_fast (datalog_channel_t  chan, part_t  part)
{
	struct reap_data *rd  = &reap_data[part][chan];
	FRESULT           res;
	FILINFO           old;
	char              path[PATH_MAX];

	debug("reap_fast(%d, %d): init scan:\r\n", chan, part);
	if ( FR_OK != (res = fatfs_reap_scan(path, sizeof(path), chan, part)) )
		return res;

	/* While over quota, reap oldest, then rescan to find next oldest */
	while ( rd->bytes > rd->quota && FR_OK == res )
	{
		debug("reap_fast(%d, %d): loop: \r\n\toldest %s\r\n\tnewest %s\r\n",
		      chan, part, rd->oldest, rd->newest);

		/* Need at least two files to reap */
		if ( !rd->oldest[0] || !strcmp(rd->oldest, rd->newest) )
		{
			debug("reap_fast(%d, %d): not enough files, skip\r\n", chan, part);
			break;
		}

		/* Format candidate path and show info; if you assert here then increase PATH_MAX
		 * above */
		int len = snprintf(path, sizeof(path), "%s/%s", datalog_path(chan), rd->oldest);
		path[0] = '0' + part;
		ASSERT(len < sizeof(path));
		debug("reap_fast(%d, %d): path: '%s'\r\n", chan, part, path);

		// free space before unlink
		int64_t   before = fatfs_free(part);

		/* Save current oldest we're about to reap, check after rescan */
		memcpy(&old, &rd->oldest, sizeof(old));
		if ( FR_OK != (res = f_unlink(path)) )
			return res;

		// free space after unlink, and compare to expected gain
		int64_t  after = fatfs_free(part);
		debug("reap_fast(%d, %d): freed %lld (%llu to %llu)\r\n", chan, part,
		      (unsigned long long)(after - before),
		      (unsigned long long)(before),
		      (unsigned long long)(after));

		if ( FR_OK != (res = fatfs_reap_scan(path, sizeof(path), chan, part)) )
			return res;

		/* Verify rescan found next-oldest file; if you assert here the f_unlink is not
		 * working despite returning a FR_OK */
		ASSERT(strcmp(old.fname, rd->oldest));

		/* Quash compiler warnings if debug() undefined */
		(void)before;
		(void)after;
	}

	debug("reap_fast(%d, %d): after: \r\n\toldest %s\r\n\tnewest %s\r\n",
	      chan, part, rd->oldest, rd->newest);
	debug("\r\n");

	return res;
}


/** Reap old data to maintain quota
 *
 *  Each channel enabled on each open part is checked to see if it's using more than its
 *  quota.  While it is, the oldest matching file is deleted until it's below quota.
 *
 *  \note  This function is called after a successful fatfs_mount, and should be called
 *         periodically (ie. daily, weekly) afterwards.
 *
 *  \note  This function calculates quota data used by fatfs_write() to stay under quota
 *         while writing new data.
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
FRESULT fatfs_reap_slow (void)
{
	datalog_channel_t  chan;
	struct reap_data  *rd;
	part_t             part;
	FRESULT            res = FR_OK;
	uint32_t           denom = 0;
	uint64_t           bytes;

	/* Calculate quotas denominator first, min 100. */
	for ( chan = DATALOG_HEALTH; chan < DATALOG_NUM_CHANNELS; chan++ )
		denom += datalog_quota(chan);
	if ( denom < 100 )
		denom = 100;

	/* Reset reap data and calculate quota as share of part size. */
	memset(&reap_data, 0, sizeof(reap_data));
	for ( chan = DATALOG_HEALTH; chan < DATALOG_NUM_CHANNELS; chan++ )
		for ( part = PART_USER; part < PART_MAX; part++ )
		{
			rd = &reap_data[part][chan];
			bytes  = part_fatfs[part].n_fatent - 2;
			bytes *= part_fatfs[part].csize;
			bytes *= SECTOR_SIZE;
#if defined(RESV_SPACE) && RESV_SPACE > 0 && RESV_SPACE < 100
			bytes *= (100 - RESV_SPACE);
			bytes /=  100;
#endif
			bytes *= datalog_quota(chan);
			bytes /= denom;
			rd->quota = bytes;
			info("chan %d part %d quota %luMB\r\n", chan, part,
			     (unsigned long)(rd->quota >> 20));
		}

	/* Do an initial reap on each channel active on each part */
	for ( chan = DATALOG_HEALTH; chan < DATALOG_NUM_CHANNELS; chan++ )
		for ( part = PART_USER; part < PART_MAX; part++ )
		{
			debug("slow: start: chan %d part %d quota %luMB\r\n", chan, part,
			      (unsigned long)(rd->quota >> 20));

			rd = &reap_data[part][chan];

			/* Ignore unmounted partition */
			if ( !(part_open & (1 << part)) )
			{
				debug("fatfs: skip part %d, not open\r\n", part);
				continue;
			}

			/* Don't write this record to this partition, it's masked off */
			if ( !(part_mask[part] & (1 << chan)) )
			{
				debug("fatfs: skip part %d, masked: %08x\r\n", part, part_mask[part]);
				continue;
			}

			/* Attempt reap if necessary */
			if ( FR_OK != (res = fatfs_reap_fast(chan, part)) )
				return res;
		}

	return res;
}


/** Write a record
 *
 *  \param  record  Pointer to a datalog_record
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
FRESULT fatfs_write (const struct datalog_record *record, char *buf, unsigned max)
{
	FRESULT  res = FR_OK;
	FILINFO  inf;
	FIL      fil;
	part_t   part;

	ASSERT(record);

	debug("fatfs: write record chan %d date %s time %s\r\n", record->channel,
		  csv_date(&record->datetime.date), csv_time(&record->datetime.time));
	ASSERT(record->channel >= 0);
	ASSERT(record->channel < DATALOG_NUM_CHANNELS);

	for ( part = PART_USER; part < PART_MAX; part++ )
	{
		/* Can't write to an unmounted partition */
		if ( !(part_open & (1 << part)) )
		{
			debug("fatfs: skip part %d, not open\r\n", part);
			continue;
		}

		/* Don't write this record to this partition, it's masked off */
		if ( !(part_mask[part] & (1 << record->channel)) )
		{
			debug("fatfs: skip part %d, masked: %08x\r\n", part, part_mask[part]);
			continue;
		}

		/* Calculate path for datalog file */
		int  len = datalog_full(buf, max, part, record);
		ASSERT(len > 0);
		ASSERT(len < max);

		/* Make parent directories if needed */
		if ( FR_OK != (res = mkdir_parent(buf)) )
		{
			error("fatfs: mkdir_parent(%s): %d, stop\r\n", buf, res);
			return res;
		}

		/* Stat file to check for existance and get size, open if possible */
		switch ( (res = f_stat(buf, &inf)) )
		{
			/* Existing file, open with FA_OPEN_EXISTING */
			case FR_OK:
				debug("stat(%s): %lu bytes, append\r\n", buf, (unsigned long)inf.fsize);
				res = f_open(&fil, buf, FA_WRITE|FA_OPEN_EXISTING);
				break;

			/* New file, open with FA_CREATE_NEW */
			case FR_NO_FILE:
				debug("stat(%s): NO_FILE, create\r\n", buf);
				if ( FR_OK == (res = f_open(&fil, buf, FA_WRITE|FA_CREATE_NEW)) )
					f_stat(buf, &inf);
				break;

			/* Other error, abort */
			default:
				error("stat(%s): %d, stop\r\n", buf, res);
				return res;
		}
		if ( FR_OK != res )
		{
			error("f_open(%s): %d, stop\r\n", buf, res);
			return res;
		}

		/* Empty/new file: create header row, passing NULL record to format */
		if ( 0 == inf.fsize )
		{
			len = datalog_head(buf, buf + max, record);
			ASSERT(len < max);

			/* This handles writes up to 64KiB, maybe larger, internally */
			uint32_t  out;
			if ( FR_OK != (res = f_write(&fil, buf, len, &out)) )
			{
				error("f_write(fil, buf, %d): %d\r\n", len, res);
				f_close(&fil);
				return res;
			}

			/* This happens when the disk fills up, which shouldn't happen but can if
			 * quotas are too small for the data flow, or the user's put files on the
			 * disk.  Trigger a fast reap, then complete the write. */
			if ( out < len )
			{
				error("\e[1;31mtried to write %d, accomplished %lu\e[m\n",
				      len, (unsigned long)out);

				fatfs_reap_fast(record->channel, part);
				f_write(&fil, buf + out, len - out, &out);
			}

			inf.fsize += len;
		}
		/* Non-empty file: seek to end, FATFS doesn't seem to support append */
		else if ( FR_OK != (res = (f_lseek(&fil, inf.fsize))) )
		{
			error("f_lseek(%lu): %d, stop\r\n", (unsigned long)inf.fsize, res);
			f_close(&fil);
			return res;
		}

		/* Write CSV record as a data row regardless */
		len = datalog_data(buf, buf + max, record);
		ASSERT(len < max);

		/* This handles writes up to 64KiB, maybe larger, internally */
		uint32_t  out;
		if ( FR_OK != (res = f_write(&fil, buf, len, &out)) )
		{
			error("f_write(fil, buf, %d): %d\r\n", len, res);
			f_close(&fil);
			return res;
		}

		/* This happens when the disk fills up, which shouldn't happen but can if quotas
		 * are too small for the data flow, or the user's put files on the disk.  Trigger
		 * a fast reap, then complete the write. */
		if ( out < len )
		{
			error("\e[1;31mtried to write %d, accomplished %lu\e[m\n",
			      len, (unsigned long)out);

			fatfs_reap_fast(record->channel, part);
			f_write(&fil, buf + out, len - out, &out);
		}

		inf.fsize += len;

		/* Close file: buffering mean write can succeed and then fail at close time... */
		if ( FR_OK != (res = f_close(&fil)) )
		{
			error("f_close: %d\r\n", res);
			return res;
		}

		/* If this is a new filename, do a fast reap. */
		struct reap_data *rd = &reap_data[part][record->channel];
		if ( !rd->current[0] || strcmp(rd->current, inf.fname) )
		{
			if ( FR_OK != (res = fatfs_reap_fast(record->channel, part)) )
				return res;
			memcpy(rd->current, inf.fname, sizeof(rd->current));
		}

		uint64_t  bytes = fatfs_free(part);
		debug("fatfs: %-23s %8lu - %lluMB (%llu bytes) free\r\n",
		      inf.fname, (unsigned long)inf.fsize,
		      (unsigned long long)bytes >> 20, (unsigned long long)bytes);

		/* Quash compiler warnings if debug() undefined */
		(void)bytes;
	}

	return res;
}

