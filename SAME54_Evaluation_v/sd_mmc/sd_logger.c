/*
 * sd_logger.c
 *
 * Created: 6/9/2020 4:10:28 PM
 *  Author: BehroozH
 */ 
#include <string.h>
#include "lib\timing.h"
#include "lib\datalog.h"
#include "temperature.h"
#include "calendar.h"
#include "battCells.h"
#include "SOC.h"
#include "encoder.h"
#include "system_definitions.h"
#include "sd_logger.h"
#include "debugMon.h"
#include "hal_calendar.h"
#include "common.h"
#include "ext_adc.h"
#include "state.h"

SW_TIMER       loggerMonitorTimer;
pSW_TIMER      pLoggerMonitorTimer;

SW_TIMER       updaterTimer;
pSW_TIMER      pUpdaterTimer;

#define CB_SIZE		16

static struct datalog_record datalog_record_perf;
static struct datalog_record datalog_rec_events;
extern struct calendar_descriptor CALENDAR_0;

void _queue_event(struct datalog_record rec);
bool _Is_event_available();
void  _Log_events();
void  _Log_perfrormance_data();

struct EVENT_BUFFER
{
	struct datalog_record  CB[CB_SIZE];
	int read_ptr;
	int write_ptr;
	bool overflow;
	uint8_t overflow_count;
};

struct EVENT_BUFFER Events;



void Init_Logger (void)
{
	pLoggerMonitorTimer = &loggerMonitorTimer;
	pUpdaterTimer = &updaterTimer;
	
	timerInit(pLoggerMonitorTimer, PERIODS.LOGGER);
	timerInit(pUpdaterTimer,PERIODS.UPDATER);
	
	Events.read_ptr = 0;
	Events.write_ptr = 0;
}

void Logger_Monitor (void)
{
	uint8_t data_changed = 0;
	
	if(Events.overflow == true)
	{
		Queue_Event("Event logging buffer overflew.");
		Events.overflow = false;
	}
	
	if(timerExpired(pUpdaterTimer))
	{
		data_changed = UpdatePerformanceData_MinMax(false);
	}
	
	if (timerExpired(pLoggerMonitorTimer) || (data_changed == 1))
	{
		data_changed = 0;
		
	_Log_perfrormance_data();
		
	_Log_events();
	
	UpdatePerformanceData_MinMax(true);
	 
	 timerReset(pLoggerMonitorTimer);	
	}
	else
	{
		
	}
	
}

void Queue_Event(char*  event_desc)
{
	 datalog_rec_events.channel = DATALOG_EVENTS;
	 
	 strcpy(datalog_rec_events.data.events.entry, event_desc);
	 
	 calendar_get_date_time(&CALENDAR_0, &(datalog_rec_events.datetime));
	 
	 _queue_event( datalog_rec_events);	 	
	 
	 debug_print(DBG_LOGGING," Queued event %s             wr_ptr = %u ",event_desc,Events.write_ptr); 
}

uint8_t UpdatePerformanceData_MinMax(bool reset)
{
	float pack_voltage;
	float term_voltage;
	float c_mon;
	float term_current;
	float body_temp;
	float cell_temp;
	float soc;
	float C1V,C2V,C3V,C4V;
	
	UNIVERSAL_VAL value;

	uint8_t emptyBytes[160] = {0};

	uint8_t performanceDataChanged = 0;

	term_voltage = Get_TermVoltage();
	pack_voltage = Get_BankVoltage();
	term_current = Get_Current();
	body_temp = Get_Term_Temp_Inst();
	cell_temp = Get_Cell_Temp_Inst();
	C1V = Get_Cell_1_Volt();
	C2V = Get_Cell_2_Volt();
	C3V = Get_Cell_3_Volt();
	C4V = Get_Cell_4_Volt();
	soc = Get_SOC();
	
	// Check if the performance data is empty and assign initial values
	if ((memcmp(&performanceData.bytes[0], &emptyBytes[0], sizeof (PERF_STATE_DATA)) == 0) || (reset == true)) 
	{
		performanceData.Curr_Min = term_current;
		performanceData.Curr_Max = term_current;
		performanceData.Curr_Avg = term_current;
		performanceData.Curr_Sum = term_current;
		performanceData.Cur_Num_Samp = 1;
		
		performanceData.TermVolt_Min = term_voltage;
		performanceData.TermVolt_Max = term_voltage;

		performanceData.PackVolt_Min = pack_voltage;
		performanceData.PackVolt_Max = pack_voltage;

		performanceData.BodyTemperature_Min = body_temp;
		performanceData.BodyTemperature_Max = body_temp;

		performanceData.CellTemperature_Min = cell_temp;
		performanceData.CellTemperature_Max = cell_temp;

		performanceData.SOC_Min = soc;
		performanceData.SOC_Max = soc;
		
		performanceData.C1_Min = C1V;
		performanceData.C1_Max = C1V;
		performanceData.C1_Sum = C1V;
		performanceData.C1_Avg = C1V;
		performanceData.C1_Num_Samp = 1;
		
		performanceData.C2_Min = C2V;
		performanceData.C2_Max = C2V;
		performanceData.C2_Sum = C2V;
		performanceData.C2_Avg = C2V;
		performanceData.C2_Num_Samp = 1;
		
		performanceData.C3_Min = C3V;
		performanceData.C3_Max = C3V;
		performanceData.C3_Sum = C3V;
		performanceData.C3_Avg = C3V;
		performanceData.C3_Num_Samp = 1;
		
		performanceData.C4_Min = C4V;
		performanceData.C4_Max = C4V;
		performanceData.C4_Sum = C4V;
		performanceData.C4_Avg = C4V;
		performanceData.C4_Num_Samp = 1;
		
		performanceData.shortCktStartTime = Current_DateTime_Short();
	}
	else
	{
		performanceData.Curr_Sum += term_current;
		performanceData.Cur_Num_Samp++;
		
		if (term_current > performanceData.Curr_Max)
		{
			performanceData.Curr_Max = term_current;
			performanceData.Curr_Avg = (performanceData.Curr_Sum/performanceData.Cur_Num_Samp);
			performanceDataChanged = 1;
		}
		else if (term_current < performanceData.Curr_Min)
		{
			performanceData.Curr_Min = term_current;
			performanceData.Curr_Avg = (performanceData.Curr_Sum/performanceData.Cur_Num_Samp);
			performanceDataChanged = 1;
		}
		
		
		performanceData.C1_Sum += C1V;
		performanceData.C1_Num_Samp++;	
		if (C1V > performanceData.C1_Max)
		{
			performanceData.C1_Max = C1V;
			performanceData.C1_Avg = (performanceData.C1_Sum/performanceData.C1_Num_Samp);
			performanceDataChanged = 1;
		}
		else if (C1V < performanceData.C1_Min)
		{
			performanceData.C1_Min = C1V;
			performanceData.C1_Avg = (performanceData.C1_Sum/performanceData.C1_Num_Samp);
			performanceDataChanged = 1;
		}
		
		performanceData.C2_Sum += C2V;
		performanceData.C2_Num_Samp++;
		if (C2V > performanceData.C2_Max)
		{
			performanceData.C2_Max = C2V;
			performanceData.C2_Avg = (performanceData.C2_Sum/performanceData.C2_Num_Samp);
			performanceDataChanged = 1;
		}
		else if (C2V < performanceData.C2_Min)
		{
			performanceData.C2_Min = C2V;
			performanceData.C2_Avg = (performanceData.C2_Sum/performanceData.C2_Num_Samp);
			performanceDataChanged = 1;
		}
		
		performanceData.C3_Sum += C3V;
		performanceData.C3_Num_Samp++;
		if (C3V > performanceData.C3_Max)
		{
			performanceData.C3_Max = C3V;
			performanceData.C3_Avg = (performanceData.C3_Sum/performanceData.C3_Num_Samp);
			performanceDataChanged = 1;
		}
		else if (C3V < performanceData.C3_Min)
		{
			performanceData.C3_Min = C3V;
			performanceData.C3_Avg = (performanceData.C3_Sum/performanceData.C3_Num_Samp);
			performanceDataChanged = 1;
		}
		
		performanceData.C4_Sum += C4V;
		performanceData.C4_Num_Samp++;
		if (C4V > performanceData.C4_Max)
		{
			performanceData.C4_Max = C4V;
			performanceData.C4_Avg = (performanceData.C4_Sum/performanceData.C4_Num_Samp);
			performanceDataChanged = 1;
		}
		else if (C4V < performanceData.C4_Min)
		{
			performanceData.C4_Min = C4V;
			performanceData.C4_Avg = (performanceData.C4_Sum/performanceData.C4_Num_Samp);
			performanceDataChanged = 1;
		}
		


		if (term_voltage > performanceData.TermVolt_Max)
		{
			performanceData.TermVolt_Max = term_voltage;
			performanceDataChanged = 1;
		}
		else if (term_voltage < performanceData.TermVolt_Min)
		{
			performanceData.TermVolt_Min = term_voltage;
			performanceDataChanged = 1;
		}

		if (pack_voltage > performanceData.PackVolt_Max)
		{
			performanceData.PackVolt_Max = pack_voltage;
			performanceDataChanged = 1;
		}
		else if (pack_voltage < performanceData.PackVolt_Min)
		{
			performanceData.PackVolt_Min = pack_voltage;
			performanceDataChanged = 1;
		}

		if (body_temp > performanceData.BodyTemperature_Max)
		{
			performanceData.BodyTemperature_Max = body_temp;
			performanceDataChanged = 1;
		}
		else if (body_temp < performanceData.BodyTemperature_Min)
		{
			performanceData.BodyTemperature_Min = body_temp;
			performanceDataChanged = 1;
		}

		if (cell_temp > performanceData.CellTemperature_Max)
		{
			performanceData.CellTemperature_Max = cell_temp;
			performanceDataChanged = 1;
		}
		else if (cell_temp < performanceData.CellTemperature_Min)
		{
			performanceData.CellTemperature_Min = cell_temp;
			performanceDataChanged = 1;
		}

		if (soc > performanceData.SOC_Max)
		{
			performanceData.SOC_Max = soc;
			performanceDataChanged = 1;
		}
		else if (soc < performanceData.SOC_Min)
		{
			performanceData.SOC_Min = soc;
			performanceDataChanged = 1;
		}
		
		
		if(performanceDataChanged == 1)
		{
			performanceData.shortCktStartTime = Current_DateTime_Short();
		}
	}
	
	return performanceDataChanged;
}

void _queue_event(struct datalog_record rec)
{
	Events.CB[(Events.write_ptr)%CB_SIZE] = rec;
	
	if( ((Events.read_ptr - 1) == (Events.write_ptr)) || (( Events.write_ptr == (CB_SIZE - 1)) && ( Events.read_ptr == 0 ) ))
	{
		Events.overflow = true;
		Events.overflow_count++;
	}
	
	if(Events.write_ptr >= (CB_SIZE - 1))
	{
		Events.write_ptr =0;
	}
	else
	{
		Events.write_ptr++;
	}
}

bool _Is_event_available()
{
	if(Events.write_ptr == Events.read_ptr)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void  _Log_events()
{
	struct datalog_record rec;
	
	if(_Is_event_available() && Is_SDCard_Logging())
	{
		rec = Events.CB[(Events.read_ptr)%CB_SIZE];
		
		if(Events.read_ptr >= (CB_SIZE - 1))
		{
			Events.read_ptr =0;
		}
		else
		{
			Events.read_ptr++;
		}
		
		datalog_write(&rec);
		
		debug_print(DBG_LOGGING," Logged event %s    rd_ptr = %u ",rec.data.events.entry,Events.read_ptr);
	}
}

void  _Log_perfrormance_data()
{
	
	datalog_record_perf.channel = DATALOG_HEALTH;
	datalog_record_perf.data.health.soc = Get_SOC();
	datalog_record_perf.data.health.soh = Get_SOH();
	datalog_record_perf.data.health.C1_MIN = performanceData.C1_Min;
	datalog_record_perf.data.health.C1_AVG = performanceData.C1_Avg;
	datalog_record_perf.data.health.C1_MAX = performanceData.C1_Max;
	
	datalog_record_perf.data.health.C2_MIN = performanceData.C2_Min;
	datalog_record_perf.data.health.C2_AVG = performanceData.C2_Avg;
	datalog_record_perf.data.health.C2_MAX = performanceData.C2_Max;
	
	
	datalog_record_perf.data.health.C3_MIN = performanceData.C3_Min;
	datalog_record_perf.data.health.C3_AVG = performanceData.C3_Avg;
	datalog_record_perf.data.health.C3_MAX = performanceData.C3_Max;
	
	datalog_record_perf.data.health.C4_MIN = performanceData.C4_Min;
	datalog_record_perf.data.health.C4_AVG = performanceData.C4_Avg;
	datalog_record_perf.data.health.C4_MAX = performanceData.C4_Max;
	
	
	datalog_record_perf.data.health.T1 = Get_Ground_Temp();
	datalog_record_perf.data.health.T2 = Get_Cell_Temp();
	datalog_record_perf.data.health.T3 = Get_Cell_Temp();
	datalog_record_perf.data.health.T4 = Get_Cell_Temp();
	datalog_record_perf.data.health.T5 = Get_Term_Temp();
	datalog_record_perf.data.health.I_MAX = performanceData.Curr_Max;
	datalog_record_perf.data.health.I_MIN = performanceData.Curr_Min;
	datalog_record_perf.data.health.I_AVG = performanceData.Curr_Avg;
	
	datalog_timestamp_and_write(&datalog_record_perf);
	
}

