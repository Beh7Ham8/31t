/** FATFS application code
 *
 *  This code deals with the FATFS filesystems: mount, fs-format (f_mkfs), and unmount
 *  logic; and data-format (CSV) write logic.  Each section is summarized below, and
 *  documented in more detail through the code.
 *
 *  Partition code is now moved to a separate module to clean up integration with USB MSC.
 *
 *  The function fatfs_mount() attempts to mount each partition's filesystem; if this
 *  fails due to error FR_NO_FILESYSTEM it attempts to format the partition with f_mkfs()
 *  and remount.  Partitions mounted successfully have the corresponding bit set in the
 *  bitmap part_open, which is checked by fatfs_unmount().  Ths is called by the sd_mmc
 *  FSM when the card is ejected, or USB-MSC takes the card over.
 *
 *  The function fatfs_write() writes datalog records to the open partitions.  Which
 *  channels are written to which partitions is controlled by part_mask[].  This calls the
 *  datalog_path() to get the path, mkdir_parent() to ensure directories are made, creates
 *  the file and writes the header returned by datalog_head() if necessary, writes the
 *  data row returned by datalog_data(), and closes the file.
 *
 *  TODO:
 *  - stress testing
 *  - data reaping
 */
#ifndef SD_MMC_FATFS_H
#define SD_MMC_FATFS_H
#include "ff.h"
#include "diskio.h"
#include <lib/datalog.h>


/* We only care about the first disk (SD/MMC) */
#define FATFS_DISK_NUM 0


/** Unmount all mounted partitions
 *
 *  Each partition in part_t is checked against part_open, if open it's unmounted.
 */
void    fatfs_unmount (void);


/** Mount all partitions, formatting if appropriate
 *
 *  Each partition in part_t is checked against part_open, if open it's skipped.  If the
 *  first mount attempt fails with the result FR_NO_FILESYSTEM, an attempt is made to
 *  format the partition with f_mkfs() and remount.
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
FRESULT fatfs_mount   (void);


/** Write a record
 *
 *  \param  record  Pointer to a datalog_record
 *  \param  buf     Pointer to buffer (should be at least SECTOR_SIZE, may be more)
 *  \param  max     Size of buffer in bytes
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
FRESULT fatfs_write (const struct datalog_record *record, char *buf, unsigned max);


/** Reap old data to maintain quota
 *
 *  Each channel enabled on each open part is checked to see if it's using more than its
 *  quota.  While it is, the oldest matching file is deleted until it's below quota.
 *
 *  \note  This function is called after a successful fatfs_mount, and should be called
 *         periodically (ie. daily, weekly) afterwards.
 *
 *  \note  This function calculates quota data used by fatfs_write() to stay under quota
 *         while writing new data.
 *
 *  \return  FR_OK on success, otherwise a FRESULT error code (see fatfs/src/ff.h)
 */
FRESULT fatfs_reap_slow (void);


/** Return number of MB in a number of sectors, rounded down, avoiding 64-bit math */
#if SECTOR_SIZE != 512
#error SECTOR_SIZE must be 512
#endif
static inline uint32_t  fatfs_sectors_to_mbytes (unsigned long  sectors)
{
	return sectors >> (20 - 9);
}


#endif /* SD_MMC_FATFS_H */
