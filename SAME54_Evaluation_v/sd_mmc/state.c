#include <stdio.h>
#include <stdlib.h>
#include <hal/gpio.h>
#include <hal/usb_cdc.h>
#include <hal/usb_msc.h>
#include <hal/usb_common.h>
#include <lib/systick.h>
#include <sd_mmc_start.h>
#include <lib/timing.h>
#include <lib/stateMachine.h>

#include "state.h"
#include "diskio.h"
#include "parts.h"
#include "fatfs.h"
#include "debugMon.h"



enum
{
    stSD_MMC_EJECT,   /* Card is ejected */
    stSD_MMC_PROBE,   /* Card is being probed after insert*/
    stSD_MMC_PARTS,   /* Partition check / repair */
    stSD_MMC_MOUNT,   /* FATFS mount / format */
    stSD_MMC_FAILED,  /* Card is not usable*/

    stSD_MMC_FATFS,   /* Card is mounted for datalogging */
    stSD_MMC_USB_MSC, /* Card belongs to USB MSC */

    SD_MMC_NUM_STATES
};

static STATE_MACHINE  state;
static SW_TIMER       timer;

static bool           usb_curr;
static bool           usb_prev;
static sd_mmc_limit_t usb_limit = SD_MMC_LIMIT_USER_ONLY_RO;

static uint8_t        sector[SECTOR_SIZE];
static unsigned long  sector_count;

static bool last_log_status = true;

bool Is_SDCard_Logging()
{	
	if((state.currentState == stSD_MMC_FATFS) && (SD_CD_STATE() == SD_CD_INSERT))
	{
		return last_log_status;
	}
	else
	{
		return false;
	}	
}


static void sd_mmc_eject (int event, void *data)
{
    switch (event)
    {
        case evENTER_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   EJECT: evENTER_STATE: set timer to 100ms.");
            timerInit(&timer, 100);
            return;

        case evTIMER_TICK:
            if ( SD_CD_STATE() == SD_CD_INSERT )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   EJECT: card inserted, wait for settle, then probe.");
                sd_mmc_stack_init();
                state.nextState = stSD_MMC_PROBE;
                timerInit(&timer, 500);
            }
            return;

        case evSD_MMC_LOG_DATA:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: dropped record.");
            break;
    }
}


static void sd_mmc_probe (int event, void *data)
{
    dstatus_t      ret;

    switch (event)
    {
        case evENTER_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evENTER_STATE: set timer to 1s.");
            timerInit(&timer, 500);
            return;

        case evTIMER_TICK:
		    debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK:");
            if ( SD_CD_STATE() != SD_CD_INSERT )
                return;

			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK: init card...");
            if ( ERR_NONE != (ret = disk_initialize(FATFS_DISK_NUM)) )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK: init: %d, failed.", ret);
                state.nextState = stSD_MMC_FAILED;
                return;
            }

			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK: check disk_status...");
            if ( ERR_NONE != (ret = disk_status(FATFS_DISK_NUM)) )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK: disk_status: %d, failed.\r\n", ret);
                state.nextState = stSD_MMC_FAILED;
                return;
            }

            /* Sector count wants an unsigned long, sector size doesn't seem to work */
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK: check sector_count...");
            if ( ERR_NONE != (ret = disk_ioctl(FATFS_DISK_NUM, GET_SECTOR_COUNT, &sector_count)))
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evTIMER_TICK: sector_count: %d, failed.", ret);
                state.nextState = stSD_MMC_FAILED;
                return;
            }

            debug_print(DBG_SDMMC," SDMMC_FSM |   PROBE: card inserted: %lu sectors -> %luMB",sector_count, (unsigned long)fatfs_sectors_to_mbytes(sector_count));

            /* Enforce min size */
            if ( sector_count < FATFS_DISK_MIN )
            {
                debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: card inserted: %luMB < min %luMB, stop\r\n",(unsigned long)fatfs_sectors_to_mbytes(sector_count),(unsigned long)fatfs_sectors_to_mbytes(FATFS_DISK_MIN));
					   
                state.nextState = stSD_MMC_FAILED;
                return;
            }

            /* If USB is plugged, we now have enough data to hand it to MSC */
            if ( HAL_USB_IsPlugged() )
            {
                state.nextState = stSD_MMC_USB_MSC;
                timerInit(&timer, 100);
                return;
            }

            state.nextState = stSD_MMC_PARTS;
            return;

        case evSD_MMC_LOG_DATA:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: data record dropped.");
            return;

        case evSD_MMC_USB_CONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PROBE: evSD_MMC_USB_CONNECT.");
            fatfs_unmount();
            state.nextState = stSD_MMC_USB_MSC;
            return;
    }
}

static void sd_mmc_parts (int event, void *data)
{
    dstatus_t  ret;

    switch (event)
    {
        case evENTER_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: evENTER_STATE.");
            timerInit(&timer, 500);
            return;

        case evTIMER_TICK:
            if ( SD_CD_STATE() != SD_CD_INSERT )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: evTIMER_TICK: card removed, go to EJECT.");
                state.nextState = stSD_MMC_EJECT;
                return;
            }

            /* Read MBR sector to check partitions */
            if ( ERR_NONE != (ret = disk_read(FATFS_DISK_NUM, sector, 0, 1)) )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: evTIMER_TICK: read: %d, try to recover.", ret);
                state.nextState = stSD_MMC_FAILED;
                return;
            }

            if ( ERR_NONE != (ret = parts_check(sector)) )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: evTIMER_TICK: failed to fix parts.");
                state.nextState = stSD_MMC_FAILED;
                return;
            }

			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: evTIMER_TICK: valid parts, mount.");
            state.nextState = stSD_MMC_MOUNT;
            return;

        case evSD_MMC_LOG_DATA:
		    debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: data record dropped.");
            return;

        case evSD_MMC_USB_CONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   PARTS: evSD_MMC_USB_CONNECT.");
            fatfs_unmount();
            state.nextState = stSD_MMC_USB_MSC;
            return;
    }
}

static void sd_mmc_mount (int event, void *data)
{
    FRESULT  res;

    switch (event)
    {
        case evTIMER_TICK:
            if ( SD_CD_STATE() != SD_CD_INSERT )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   MOUNT: evTIMER_TICK: card removed, go to EJECT.");
                state.nextState = stSD_MMC_EJECT;
                return;
            }

            if ( FR_OK != (res = fatfs_mount()) )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   MOUNT: evTIMER_TICK: mount: %d, stop", res);
                state.nextState = stSD_MMC_FAILED;
                return;
            }
            
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   MOUNT: evTIMER_TICK: mounted, go to FATFS.");
            state.nextState = stSD_MMC_FATFS;
            return;

        case evSD_MMC_LOG_DATA:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   data record dropped.");
            return;

        case evSD_MMC_USB_CONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   MOUNT: evSD_MMC_USB_CONNECT.");
            fatfs_unmount();
            state.nextState = stSD_MMC_USB_MSC;
            return;
    }
}

static void sd_mmc_failed (int event, void *data)
{
    switch (event)
    {
        case evENTER_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   FAILED: evENTER_STATE: set retry timer to 60 sec.");
            timerInit(&timer, 60000);
            return;

        case evTIMER_TICK:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   FAILED: evTIMER_TICK:");
            if ( SD_CD_STATE() == SD_CD_INSERT )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   MOUNT: evTIMER_TICK: card insert, go to EJECT.");
                state.nextState = stSD_MMC_EJECT;
            }
            return;

        case evSD_MMC_LOG_DATA:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   FAILED: data record dropped.");
            return;

        case evSD_MMC_USB_CONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   FAILED: evSD_MMC_USB_CONNECT.");
            fatfs_unmount();
            state.nextState = stSD_MMC_USB_MSC;
            break;
    }
}

static void sd_mmc_fatfs (int event, void *data)
{
    FRESULT  res;

    switch (event)
    {
        case evENTER_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   FATFS: evENTER_STATE.");
            timerInit(&timer, 1000);
            return;

        case evTIMER_TICK:
            if ( SD_CD_STATE() != SD_CD_INSERT )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   FATFS: evTIMER_TICK: card removed, go to EJECT");
                fatfs_unmount();
                state.nextState = stSD_MMC_EJECT;
                return;
            }
            break;

        case evSD_MMC_LOG_DATA:
            if ( FR_OK != (res = fatfs_write(data, (char *)sector, sizeof(sector))) )
            {
				debug_print(DBG_SDMMC,  " SDMMC_FSM |   FATFS: evSD_MMC_LOG_DATA: write record: %d", res);
                fatfs_unmount();
                state.nextState = stSD_MMC_EJECT;
				last_log_status = false;
                return;
            }
			else
			{
				last_log_status = true;
			}

            break;

        case evSD_MMC_USB_CONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   FATFS: evSD_MMC_USB_CONNECT.");
            fatfs_unmount();
            state.nextState = stSD_MMC_USB_MSC;
            break;
    }
}

static void sd_mmc_usb_limit (sd_mmc_limit_t limit)
{
    usb_limit = limit;
	debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB limit %d:",usb_limit);

    if ( SD_MMC_LIMIT_USER_ONLY_RO == usb_limit )
    {
        parts_limit(PART_USER);
        HAL_MSC_UpdateSize(sector_count, parts_disk_size(sector_count));
    }
    else
    {
        parts_limit(PART_MAX);
        HAL_MSC_UpdateSize(sector_count, sector_count);
    }

    if ( SD_MMC_LIMIT_WHOLE_DISK_RW == usb_limit )
        HAL_MSC_UpdateReadOnly(false);
    else
        HAL_MSC_UpdateReadOnly(true);
}

static void sd_mmc_usb_msc (int event, void *data)
{
    switch (event)
    {
        case evENTER_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: evENTER_STATE.");
            fatfs_unmount();
            sd_mmc_usb_limit(usb_limit);

            /* Enable MSC */
            HAL_MSC_UpdateReady(true);
            timerInit(&timer, 100);
            return;

        case evTIMER_TICK:
            if ( SD_CD_STATE() != SD_CD_INSERT )
            {
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: evTIMER_TICK. card removed, go to EJECT.");
                state.nextState = stSD_MMC_EJECT;
            }
            return;

        case evEXIT_STATE:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: evEXIT_STATE.");
            /* Disable MSC */
            HAL_MSC_UpdateReady(false);
            break;

        case evSD_MMC_LOG_DATA:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: dropped record.");
            break;

        case evSD_MMC_USB_CONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: evSD_MMC_USB_CONNECT.");
            break;

        case evSD_MMC_USB_DISCONNECT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: evSD_MMC_USB_DISCONNECT.");
            state.nextState = stSD_MMC_EJECT;
            timerInit(&timer, 500);
            break;


        case evSD_MMC_PARTS_LIMIT:
			debug_print(DBG_SDMMC,  " SDMMC_FSM |   USB_MSC: evSD_MMC_PARTS_LIMIT: %p.",data);
            sd_mmc_usb_limit((sd_mmc_limit_t)data);
            break;
    }
}

static const pStateFunc  state_list[SD_MMC_NUM_STATES] =
{
    [stSD_MMC_EJECT]   = sd_mmc_eject,
    [stSD_MMC_PROBE]   = sd_mmc_probe,
    [stSD_MMC_PARTS]   = sd_mmc_parts,
    [stSD_MMC_MOUNT]   = sd_mmc_mount,
    [stSD_MMC_FAILED]  = sd_mmc_failed,
    [stSD_MMC_FATFS]   = sd_mmc_fatfs,
    [stSD_MMC_USB_MSC] = sd_mmc_usb_msc,
};


void Init_SDMMC (void)
{
    timerInit(&timer, 1000);

    stateInit(&state, state_list, ARRAY_SIZE(state_list), stSD_MMC_EJECT);
}

void SDMMC_Monitor (void)
{
    // may need to debounce this
    if ( (usb_curr = HAL_USB_IsPlugged()) != usb_prev )
    {
        if ( usb_curr )
            event_handler(&state, evSD_MMC_USB_CONNECT, NULL);
        else
            event_handler(&state, evSD_MMC_USB_DISCONNECT, NULL);

        usb_prev = HAL_USB_IsPlugged();
        return;
    }

    if ( !timerExpired(&timer) )
        return;

    event_handler(&state, evTIMER_TICK, NULL);
    timerReset(&timer);
}

void sd_mmc_Event (sd_mmc_event_t event, void *param)
{
    event_handler(&state, event, param);
}

