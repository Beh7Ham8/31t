/*
 * sd_logger.h
 *
 * Created: 6/9/2020 4:10:43 PM
 *  Author: BehroozH
 */ 


#ifndef SD_LOGGER_H_
#define SD_LOGGER_H_

void Init_Logger (void);
void Logger_Monitor (void);
void Queue_Event(char*  event_desc);
uint8_t UpdatePerformanceData_MinMax(bool reset);


#endif /* SD_LOGGER_H_ */