/** Partition table code
 *
 *  This code deals with the partition table, formerly combined with the FATFS application
 *  code.
 *
 *  The enum part_t lists the number of partitions this code is expected to handle.  For
 *  each partition, a canned partition table entry is stored; if the entry on the disk
 *  inserted does not match, it is corrected to the one in the firmware, and the first
 *  sector of the partition zeroed out to trigger a call to f_mkfs at mount time.  The
 *  function parts_check() is called from the sd_mmc FSM while loading the card.
 */
#ifndef SD_MMC_PARTS_H
#define SD_MMC_PARTS_H
#include "fatfs.h"
#include "diskio.h"


/* Set nonzero to support a smaller disk (~2GB, with 2x 950MB partitions).
 * Set to zero to require a larger disk (~8GiB, with 2x 3.4GiB partitions).  */
#define FATFS_SMALL_DISK 0
#if FATFS_SMALL_DISK
#define FATFS_DISK_MIN 3893248UL
#else
#define FATFS_DISK_MIN 14338048UL
#endif


/* The part_t enum gives names for the partitions this code will manage, max of 4.  Other
 * partitions may exist on the disk if the user creates them.  The partition numbers,
 * starting at 0, are also used as drive "letters" with FATFS, so the root of PART_USER is
 *   0:\
 *
 * This number is passed to datalog_full() to create the full filepath for a given record,
 * is used as an index in arrays below sized with PART_MAX, and is used as a bit position
 * in bitmaps such as part_open.
 */
typedef enum
{
	PART_USER,
	PART_SERV,
	PART_MAX
}
part_t;


/** Check and fix partition table
 *
 *  Check the MBR provided and fix if necessary.  The DOS disk magic (0x55,0xAA) is
 *  checked and set.  The DOS disk identifier is set if it's all-zero or all-one,
 *  otherwise it's left alone.
 *
 *  Each partition in part_t is checked against part_slot[] and reset if it differs; if
 *  this happens the first sector of the partition is zeroed as well to ensure that
 *  f_mount() fails and fatfs_mount() formats the partition.
 *
 *  \note  This function modifies and reuses the sector buffer, do not depend on it being
 *         unchanged after calling.
 *
 *  \param  sector  Pointer to SECTOR_SIZE bytes of storage, containing the MBR
 *
 *  \return  RES_OK  on success, otherwise a dresult_t error code (see diskio/diskio.h)
 */
dstatus_t parts_check (uint8_t *sector);


/** Set the last partition visible via USB
 *
 *  This has two effects:
 *  - parts_censor_mbr() will edit the sector passed to it, assumed to be an MBR, to remove
 *    all partitions beyond the passed partition ID.
 *  - parts_disk_size() will return the user-visible size of the disk, based on the LBA
 *    address of the last visible partition.
 *
 *  \param  last  Last partition to make visible, or PART_MAX for entire disk
 */
void parts_limit (part_t  last);


/** Censor a loaded MBR sector's partition table for USB
 *
 *  \param  sector  Pointer to buffer of SECTOR_SIZE bytes
 */
void parts_censor_mbr (uint8_t *sector);


/** Limit the disk size to report to USB
 *
 *  \param  sectors  Actual number of sectors from GET_SECTOR_COUNT ioctl
 *
 *  \return  Number of sectors to report to USB
 */
uint32_t  parts_disk_size (unsigned long  sectors);


#endif /* SD_MMC_PARTS_H */
