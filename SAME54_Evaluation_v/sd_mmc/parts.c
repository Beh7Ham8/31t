/** Partition table code
 *
 *  This code deals with the partition table, formerly combined with the FATFS application
 *  code.
 *
 *  The enum part_t lists the number of partitions this code is expected to handle.  For
 *  each partition, a canned partition table entry is stored; if the entry on the disk
 *  inserted does not match, it is corrected to the one in the firmware, and the first
 *  sector of the partition zeroed out to trigger a call to f_mkfs at mount time.  The
 *  function parts_check() is called from the sd_mmc FSM while loading the card.
 */
#include <stdio.h>
#include <string.h>
#include <driver_init.h>
#include <utils.h>

#include "ff.h"
#include "fatfs.h"
#include "diskio.h"
#include "parts.h"


#define debug(fmt, ...) printf(fmt, ##__VA_ARGS__)
#define info(fmt, ...)  printf(fmt, ##__VA_ARGS__)
#define error(fmt, ...) printf(fmt, ##__VA_ARGS__)


/* Each partition needs a slot in the partition table; this should be modified by
 * partitioning the card in a PC, hexdumping the first sector, and copying the bytes from
 * offset 0x1D8.
 *
 * - If FATFS_SMALL_DISK is nonzero, the table sets up the first two partitions as 950MiB
 *   FAT16 (Type=0x06): FATFS will rewrite each boot if not at this value.
 * - If FATFS_SMALL_DISK is zero, the table sets up the first two partitions as 3.4GByte
 *   FAT32 LBA, so the CHS addresses are unused.
 *
 * Further disk space can be used by the user or left empty to allow the SD card
 * controller to improve card life by wear-levelling.
 */
static const uint8_t  part_slot[PART_MAX][16] COMPILER_ALIGNED(4) =
{
#if FATFS_SMALL_DISK
//    Boot  First (CHS)     Type  Last (CHS)      First (LBA)          Size (LBA)
	{ 0x00, 0x21,0x03,0x00, 0x06, 0x29,0x6a,0xfa, 0x00,0x08,0x00,0x00, 0x00,0xb0,0x1d,0x00 },
	{ 0x00, 0x29,0x6b,0xfa, 0x06, 0x32,0xd4,0xf4, 0x00,0xb8,0x1d,0x00, 0x00,0xb0,0x1d,0x00 },
#else
//    Boot  First (CHS)     Type  Last (CHS)      First (LBA)          Size (LBA)
	{ 0x00, 0x20,0x21,0x00, 0x0c, 0x50,0x52,0xbe, 0x00,0x08,0x00,0x00, 0x00,0x60,0x6d,0x00 },
	{ 0x00, 0x50,0x53,0xbe, 0x0c, 0x80,0xc4,0x7c, 0x00,0x68,0x6d,0x00, 0x00,0x60,0x6d,0x00 },
#endif
};

/* This is needed by FATFS to map logical drives onto physical partitions. */
PARTITION VolToPart[PART_MAX] =
{
	{ FATFS_DISK_NUM, 1 },
	{ FATFS_DISK_NUM, 2 },
};


/* Specifies the last partition to make visible via USB, set by parts_limit(). */
static part_t  parts_last = PART_MAX;


/* Extract / calculate partition LBA numbers.
 * XXX: depends on little-endian CPU
 * XXX: may depend on COMPILER_ALIGNED(4) above */
static inline uint32_t  slot_lba_first (const uint8_t *slot)
{
	return *((uint32_t *)(slot + 8));
}

static inline uint32_t  slot_lba_size (const uint8_t *slot)
{
	return *((uint32_t *)(slot + 12));
}

static inline uint32_t  slot_lba_next (const uint8_t *slot)
{
	return slot_lba_first(slot) + slot_lba_size(slot);
}

static inline uint32_t  slot_lba_last (const uint8_t *slot)
{
	return slot_lba_next(slot) - 1;
}


/** Check and fix partition table
 *
 *  Check the MBR provided and fix if necessary.  The DOS disk magic (0x55,0xAA) is
 *  checked and set.  The DOS disk identifier is set if it's all-zero or all-one,
 *  otherwise it's left alone.
 *
 *  Each partition in part_t is checked against part_slot[] and reset if it differs; if
 *  this happens the first sector of the partition is zeroed as well to ensure that
 *  f_mount() fails and fatfs_mount() formats the partition.
 *
 *  \note  This function modifies and reuses the sector buffer, do not depend on it being
 *         unchanged after calling.
 *
 *  \param  sector  Pointer to SECTOR_SIZE bytes of storage, containing the MBR
 *
 *  \return  RES_OK  on success, otherwise a dresult_t error code (see diskio/diskio.h)
 */
dstatus_t  parts_check (uint8_t *sector)
{
	dstatus_t  ret;
	unsigned   fix = 0;
	uint32_t   lba[PART_MAX] = { 0 };

	/* Check/set DOS disk magic */
	if ( sector[510] != 0x55 || sector[511] != 0xAA )
	{
		info("parts: DOS disk magic %02x,%02x, fix\r\n", sector[510], sector[511]);
		sector[510] = 0x55;
		sector[511] = 0xAA;
		fix = 0xFF;
	}

	/* Check/set empty identifier */
	if ( (sector[440] | sector[441] | sector[442] | sector[443]) == 0x00 ||
		 (sector[440] & sector[441] & sector[442] & sector[443]) == 0xFF )
	{
		info("parts: DOS Ident %02x,%02x,%02x,%02x, set\r\n",
			 sector[440], sector[441], sector[442], sector[443]);
		rand_sync_read_buf8(&RAND, &sector[440], 4);
		fix = 0xFF;
	}

	/* Check/set partition table entries we care about; if the user adds more after ours
	 * we don't care, but if they modify the ones we care about we'll rewrite them */
	part_t  part;
	for ( part = PART_USER; part < PART_MAX; part++ )
	{
		uint8_t *ptr = sector + 446 + (part * 16);

		if ( memcmp(ptr, part_slot[part], 16) )
		{
			info("parts: part %d is wrong, fix\r\n", part);
			memcpy(ptr, part_slot[part], 16);
			fix |= 1 << part;

			/* record first sector so we can clear it. */
			lba[part] = slot_lba_first(part_slot[part]);
			debug("parts: part %d will start at sector %lu\r\n", part,
			      (unsigned long)lba[part]);
		}
	}

	/* No fixes needed, carry on */
	if ( !fix )
	{
		info("parts: partition table looks ok\r\n");
		return RES_OK;
	}

	/* Fixes needed, rewrite sector */
	if ( RES_OK != (ret = disk_write(FATFS_DISK_NUM, sector, 0, 1)) )
	{
		error("parts: writing partition table: %d\r\n", ret);
		return ret;
	}

	/* Zero first sector of each part we reset */
	memset(sector, 0, SECTOR_SIZE);
	for ( part = PART_USER; part < PART_MAX; part++ )
	{
		if ( !(fix & 1 << part) )
			continue;

		debug("parts: clear first sector of part %d (%lu)\r\n", part, (unsigned long)lba[part]);
		if ( ERR_NONE != (ret = disk_write(FATFS_DISK_NUM, sector, lba[part], 1)) )
		{
			error("parts: clearing part %d: %d\r\n", part, ret);
			return ret;
		}
	}

	return ret;
}


/** Set the last partition visible via USB
 *
 *  This has two effects:
 *  - parts_censor_mbr() will edit the sector passed to it, assumed to be an MBR, to remove
 *    all partitions beyond the passed partition ID.
 *  - parts_disk_size() will return the user-visible size of the disk, based on the LBA
 *    address of the last visible partition.
 *
 *  \param  last  Last partition to make visible, or PART_MAX for entire disk
 */
void parts_limit (part_t  last)
{
	parts_last = last;
}


/** Censor a loaded MBR sector's partition table for USB
 *
 *  \param  sector  Pointer to buffer of SECTOR_SIZE bytes
 */
void parts_censor_mbr (uint8_t *sector)
{
	/* XXX: works on entire partition table, not just parts we manage */
	part_t  part;
	for ( part = 0; part < 4; part++ )
	{
		uint8_t *ptr = sector + 446 + (part * 16);
		if ( part > parts_last )
			memset(ptr, 0, 16);
	}
}


/** Return the disk size to report to USB
 *
 *  \return  Number of sectors, or 0 on error
 */
uint32_t  parts_disk_size (unsigned long  sectors)
{
	uint32_t  ret = sectors;

	if ( parts_last < PART_MAX )
	{
		ret = slot_lba_last(part_slot[parts_last]) + 1;

		/* Should never happen, but depends on FATFS_DISK_MIN being enforced */
		ASSERT(ret <= sectors);
	}

	return ret;
}

