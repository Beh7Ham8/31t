#ifndef SD_MMC_STATE_H
#define SD_MMC_STATE_H
#include "lib/stateMachine.h"
#include "lib/datalog.h"


typedef enum
{
    evSD_MMC_LOG_DATA = evMAX_COMMON_EVENTS,
    evSD_MMC_USB_CONNECT,
    evSD_MMC_USB_DISCONNECT,
    evSD_MMC_PARTS_LIMIT,
    evSD_MMC_MAX
}
sd_mmc_event_t;


void Init_SDMMC (void);
void SDMMC_Monitor    (void);
bool Is_SDCard_Logging();
void sd_mmc_Event      (sd_mmc_event_t event, void *param);


typedef enum
{
    SD_MMC_LIMIT_USER_ONLY_RO,
    SD_MMC_LIMIT_WHOLE_DISK_RO,
    SD_MMC_LIMIT_WHOLE_DISK_RW
}
sd_mmc_limit_t;

static inline void sd_mmc_PartsLimit (sd_mmc_limit_t  limit)
{
    sd_mmc_Event(evSD_MMC_PARTS_LIMIT, (void *)limit);
}


#endif /* SD_MMC_STATE_H */
