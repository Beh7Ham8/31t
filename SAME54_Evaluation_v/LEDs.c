#include <atmel_start.h>
#include "lib/timing.h"
#include "lib/stateMachine.h"
#include "leds.h"
#include "common.h"

#define LED_OFF     0
#define LED_ON      1

/* Heater State Machine Functions (ROM)-----------------------------------------------------*/
enum
 {
	LEDS_INIT,
	LEDS_IDLE,
	LEDS_ON,
	LEDS_OFF,
	
	BLUE_LED_ON,
	RED_LED_ON,
	GREEN_LED_ON,
	YELLOW_LED_ON,
	PURPLE_LED_ON,
	WHITE_LED_ON,

	BLUE_LED_BLINK,
	RED_LED_BLINK,
	GREEN_LED_BLINK,
	YELLOW_LED_BLINK,
	PURPLE_LED_BLINK,
	WHITE_LED_BLINK,
	
	LEDS_NUM_STATES
};



const pStateFunc ledsStateVectorArray[LEDS_NUM_STATES] = {

	(pStateFunc)state_leds_Init,
	(pStateFunc)state_leds_Idle,
	(pStateFunc)state_leds_ON,
	(pStateFunc)state_leds_OFF,
	(pStateFunc)state_blue_led_ON,
	(pStateFunc)state_red_led_ON,
	(pStateFunc)state_green_led_ON,
	(pStateFunc)state_yellow_led_ON,
	(pStateFunc)state_purple_led_ON,
	(pStateFunc)state_white_led_ON,

	(pStateFunc)state_blue_led_blink,
	(pStateFunc)state_red_led_blink,
	(pStateFunc)state_green_led_blink

};

STATE_MACHINE ledsFSM;
pSTATE_MACHINE pLedsFSM;

SW_TIMER ledsMonitorTimer;
pSW_TIMER pLedsMonitorTimer;

void set_RGB(uint8_t red, uint8_t green, uint8_t blue)
{
	gpio_set_pin_level(RED_LED,red);
	__NOP();
	//gpio_set_pin_level(GREEN_LED,green);
	__NOP();
	gpio_set_pin_level(BLUE_LED,blue);
	__NOP();
}
void set_RED_LED(uint8_t status)
{
	gpio_set_pin_level(RED_LED,status);
	__NOP();
}
void set_GREEN_LED(uint8_t status)
{
	//gpio_set_pin_level(GREEN_LED,status);
	__NOP();
}
void set_BLUE_LED(uint8_t status)
{
	gpio_set_pin_level(BLUE_LED,status);
	__NOP();
}

// ----------------------------------------------------

void Init_LEDs(void) 
{
	
    set_RGB(LED_OFF,LED_OFF,LED_OFF);
	pLedsFSM = &ledsFSM;
	pLedsMonitorTimer = &ledsMonitorTimer;

	// We will monitor the accelerometer every second
	timerInit(pLedsMonitorTimer, PERIODS.LED);

	// initialize state machine
	stateInit(pLedsFSM, ledsStateVectorArray, LEDS_NUM_STATES, LEDS_INIT);

}

void tickTimerLeds() 
{
	uint32_t timerIncrement = 1;

	event_handler(pLedsFSM, evTIMER_TICK, &timerIncrement);
}

void LEDs_Monitor() 
{
	if (timerExpired(pLedsMonitorTimer)) 
	{
		// Send timer event to the appropriate state
		tickTimerLeds();

		// Reset timer for next monitor cycle
		timerReset(pLedsMonitorTimer);
	}

}

void state_leds_Init(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;

	case evTIMER_TICK:
		pLedsFSM->nextState = LEDS_IDLE;
		break;

	case evEXIT_STATE:

		break;
	}
}



void state_leds_ON(int event, void *param)
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_ON,LED_ON,LED_ON);
		break;

	case evTIMER_TICK:
		pLedsFSM->nextState = BLUE_LED_ON;
		break;

	case evEXIT_STATE:

		break;
	}
}

void state_leds_OFF(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;

	case evTIMER_TICK:
		pLedsFSM->nextState = LEDS_ON;
		break;

	case evEXIT_STATE:
		break;
	}
}

void state_leds_Idle(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:

		break;

	case evTIMER_TICK:

		// Switch on the LEDs depending on the state of the heater
		//if (pSystemStatus->flags.heaterON == 1) 
		{
			pLedsFSM->nextState = RED_LED_ON;
		}
		break;

	case evEXIT_STATE:

		break;
	}
}


void state_blue_led_ON(int event, void *param) 
{
	switch (event)
	 {
	case evENTER_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_ON);
		break;

	case evTIMER_TICK:
		//pLedsFSM->nextState = RED_LED_ON;
		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;

	}
}

void state_red_led_ON(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_ON,LED_OFF,LED_OFF);
		break;

	case evTIMER_TICK:
		// pLedsFSM->nextState =;
		break;

	case evLEDS_IDLE:
		pLedsFSM->nextState = LEDS_IDLE;
		break;
	case evRED_LED_ON:
		pLedsFSM->nextState = RED_LED_ON;
		break;
	case evGREEN_LED_ON:
		pLedsFSM->nextState = GREEN_LED_ON;
		break;
	case evBLUE_LED_ON:
		pLedsFSM->nextState = BLUE_LED_ON;
		break;
	case evYELLOW_LED_ON:
		pLedsFSM->nextState = YELLOW_LED_ON;
		break;
	case evPURPLE_LED_ON:
		pLedsFSM->nextState = PURPLE_LED_ON;
		break;
	case evWHITE_LED_ON:
		pLedsFSM->nextState = WHITE_LED_ON;
		break;
	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}

void state_green_led_ON(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_OFF,LED_ON,LED_OFF);
		break;

	case evTIMER_TICK:
		// pLedsFSM->nextState =;
		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}


void state_yellow_led_ON(int event, void *param) 
{


	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_ON,LED_ON,LED_OFF);
		break;

	case evTIMER_TICK:
	// pLedsFSM->nextState =;
		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}


void state_purple_led_ON(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_ON,LED_OFF,LED_ON);
		break;

	case evTIMER_TICK:
	// pLedsFSM->nextState =;
		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}


void state_white_led_ON(int event, void *param) 
{
	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_ON,LED_ON,LED_ON);
		break;

	case evTIMER_TICK:
		// pLedsFSM->nextState =;
		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}

void state_blue_led_blink(int event, void *param) 
{

	static uint8_t blinkCount = 0;
	static uint8_t currState;

	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		blinkCount = 0;
		currState = LED_OFF;
		break;

	case evTIMER_TICK:
		// Blink BLUE led three times (3 seconds off, three seconds on
		if (blinkCount++ > 6) 
		{
			pLedsFSM->nextState = LEDS_OFF;
		} 		
		else 
		{
			currState = (currState == LED_ON) ? LED_OFF : LED_ON;

		}

		set_RGB(LED_OFF,LED_OFF,currState);

		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}

void state_red_led_blink(int event, void *param) 
{

	static uint8_t blinkCount = 0;
	static uint8_t currState;

	switch (event) 
	{
	case evENTER_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		blinkCount = 0;
		currState = LED_OFF;

		break;

	case evTIMER_TICK:
		// Blink BLUE led three times (3 seconds off, three seconds on
		if (blinkCount++ > 6) 
		{
			pLedsFSM->nextState = LEDS_OFF;
		}
		else
		{
			currState = (currState == LED_ON) ? LED_OFF : LED_ON;
		}

		set_RGB(currState,LED_OFF,LED_OFF);

		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}

void state_green_led_blink(int event, void *param) 
{
	static uint8_t blinkCount = 0;
	static uint8_t currState;

	switch (event) 
	{
	case evENTER_STATE:
		// Start with all LEDs off
        set_RGB(LED_OFF,LED_OFF,LED_OFF);
		blinkCount = 0;
		currState = LEDS_OFF;
		break;

	case evTIMER_TICK:
		// Blink BLUE led three times (3 seconds off, three seconds on
		if (blinkCount++ > 6) 
		{
			pLedsFSM->nextState = LEDS_OFF;
		} 
		else 
		{
			currState = (currState == LED_ON) ? LED_OFF : LED_ON;
		}
	
		set_RGB(LED_OFF,currState,LED_OFF);
		break;

	case evEXIT_STATE:
		set_RGB(LED_OFF,LED_OFF,LED_OFF);
		break;
	}
}








