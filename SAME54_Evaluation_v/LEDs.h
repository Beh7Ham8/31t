/* 
 * File:   leds.h
 * Author: juan.y
 *
 * Created on March 6, 2015, 2:49 PM
 */

#ifndef LEDS_H
#define	LEDS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_definitions.h"
#include "lib/stateMachine.h"


// Function prototypes
// ----------------------------------------------------

void Init_LEDs(void);


// FSM Functions
void LEDs_Monitor(void);
void state_leds_Init(int event, void *param);
void state_leds_ON(int event, void *param);
void state_leds_OFF(int event, void *param);
void state_leds_blink(int event, void *param);
void state_leds_Idle(int event, void *param);

void state_blue_led_ON(int event, void *param);
void state_red_led_ON(int event, void *param);
void state_green_led_ON(int event, void *param);

void state_yellow_led_ON(int event, void *param);
void state_purple_led_ON(int event, void *param);
void state_white_led_ON(int event, void *param);

void state_blue_led_blink(int event, void *param);
void state_red_led_blink(int event, void *param);
void state_green_led_blink(int event, void *param);

// led FSM events

enum
 {
	evLEDS_IDLE=evMAX_COMMON_EVENTS, 
	evRED_LED_ON, 
	evGREEN_LED_ON, 
	evBLUE_LED_ON, 
	evYELLOW_LED_ON, 
	evPURPLE_LED_ON, 
	evWHITE_LED_ON, 
	evMAX_LED_EVENTS
};

enum {BLUE, RED, GREEN, YELLOW, PURPLE, WHITE, CYAN};
// LED State machine
extern pSTATE_MACHINE pLedsFSM;


#ifdef	__cplusplus
}
#endif

#endif	/* LEDS_H */

