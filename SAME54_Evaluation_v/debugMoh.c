/*
 * debugMoh.h
 *
 * Created: 7/13/2020 2:14:08 PM
 *  Author: BehroozH
 */ 
#include <sys\_stdint.h>
#include <debugMon.h>
#include "flash.h"

uint8_t CDC_Buff[128];
uint32_t Active_Debug_Traces;


void Init_Debug()
{
    if (!nvm_read_dbgcfg(&Active_Debug_Traces)) {
        Active_Debug_Traces = DEBUG_FLAGS_ENABLED;
    }
}

void Enable_Trace(uint32_t trace)
{
    Active_Debug_Traces |= trace;
    nvm_write_dbgcfg(Active_Debug_Traces);
}

void Disable_Trace(uint32_t trace)
{
    Active_Debug_Traces &= (~trace);
    nvm_write_dbgcfg(Active_Debug_Traces);
}

void Update_Trace(uint32_t trace)
{
    Active_Debug_Traces = trace;
}
