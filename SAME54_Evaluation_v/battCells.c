/*
 * battCells.c
 *
 * Created: 5/11/2020 3:14:43 PM
 *  Author: BehroozH
 */ 

#include <stdbool.h>
#include <string.h>
#include <atmel_start.h>

#include "adc.h"
#include "encoder.h"
#include "lib/stateMachine.h"
#include "lib/timing.h"
#include "battCells.h"
#include "system_definitions.h"
#include "SOC.h"
#include "drivers.h"
#include "debugMon.h"
#include "common.h"
#include "temperature.h"
#include "LTC_6810.h"
#include "ext_adc.h"

const float  BIT_RES_ADC_SCALE = 0.0008057;

const uint8_t  ADC1_ICHARGE_CH = 7;
const uint8_t  ADC1_IDISCHARGE_HIGH_CH = 9;
const uint8_t  ADC1_IDISCHARGE_LOW_CH = 8;

const uint8_t  ADC0_VPACK_CH = 0;
const uint8_t  ADC0_VTERM_CH = 1;
const uint8_t  ADC0_BUS_MON_CH = 4;
//const uint8_t  ADC0_VREF_CH = 5;
const uint8_t  ADC0_VCELL1_CH = 6;
const uint8_t  ADC0_VCELL2_CH = 7;
const uint8_t  ADC0_VCELL3_CH = 8;
const uint8_t  ADC0_VCELL4_CH = 9;

const uint8_t  VOLTAGE_MUX_CH = 12;
const uint8_t  ADC0_VREF_CH = 1;




const float ADC_TERM_VOLT_OPAMP_GAIN = 1;//10.00;	//1/0.1
const float ADC_CELL_VOLT_OPAMP_GAIN = 1;//1.33;   //1/0.75
const float ADC_PACK_VOLT_OPAMP_GAIN = 1;//4.673;   //1/0.214
const float ADC_HTR_VOLT_OPAMP_GAIN = 0.05;   //1/20
const float ADC_CHARG_CURR_OPAMP_GAIN = 1;
const float ADC_DISCHARG_CURR_OPAMP_LOW_GAIN = 1;
const float ADC_DISCHARG_CURR_OPAMP_HIGH_GAIN = 1;
const float SHUNT_RES = 0.00025;

const float R58K = 0.025;

float ChargingCurrent = 0.0;
float DischargingCurrent = 0.0;
float DeltaVoltage = 0.0;

float const CELL_VOLTAGE_MARGIN  = 0.05; //cell voltages are matched to within this voltage tolerance

// ADC 1.241 (counts/mV); MATCH ~15mV->18counts, ~20mV->24co

STATE_MACHINE balFSM;
pSTATE_MACHINE pBatteryBalancerFSM;

float ModuleBatteryVoltage;
float SystemBatteryVoltage;
float SystemMinCellVoltage;

// Isolated cell voltages
float FVC1, FVC2, FVC3, FVC4;
float PackVoltage,TermVoltage;

char MinCell, ModuleOK;
float ModuleMinCellVoltage;
char Balanced;
uint8_t Mask;


ANALOG_VALUE Cell1T;
ANALOG_VALUE Cell2T;
ANALOG_VALUE Cell3T;
ANALOG_VALUE Cell4T;



pANALOG_VALUE pC1;
pANALOG_VALUE pC2;
pANALOG_VALUE pC3;
pANALOG_VALUE pC4;


SW_TIMER cellMonitorTimer;
pSW_TIMER pCellMonitorTimer;

void state_balancer_Init(int event, void *param);
void state_balancer_NotBalanced(int event, void *param);
void state_balancer_Balanced(int event, void *param);
void state_balancer_cells_under_voltage(int event, void *param);
void state_balancer_cells_over_voltage(int event, void *param);

void select_Mux_VC1();
void select_Mux_VC2();
void select_Mux_VC3();
void select_Mux_VC4();
void select_Mux_Pack();
void select_Mux_Term();

uint8_t Get_CellDiffStatus(uint8_t minCellNum);


const pStateFunc balancerStateVectorArray[BALANCER_NUM_STATES] = 
{
	(pStateFunc) state_balancer_Init,
	(pStateFunc) state_balancer_NotBalanced,
	(pStateFunc) state_balancer_Balanced,
	(pStateFunc) state_balancer_cells_under_voltage,
	(pStateFunc) state_balancer_cells_over_voltage
};


char* balancerStateString[BALANCER_NUM_STATES] =
{
	"Init",
	"NotBalanced",
	"Balanced",
	"cells_under_voltage",
	"cells_over_voltage"
};

float getBalancerFSMState()
{
	UNIVERSAL_VAL   param;
	
	param.value.iVal = pBatteryBalancerFSM->currentState;
	return param.value.fVal;
	
}

void Init_BatteryCells(void) 
{

	SystemMinCellVoltage = 3.0;

	pBatteryBalancerFSM = &balFSM;

	pC1 = &Cell1T;
	pC2 = &Cell2T;
	pC3 = &Cell3T;
	pC4 = &Cell4T;
	
	memset (pC1, 0, sizeof(ANALOG_VALUE));
	memset (pC2, 0, sizeof(ANALOG_VALUE));
	memset (pC3, 0, sizeof(ANALOG_VALUE));
	memset (pC4, 0, sizeof(ANALOG_VALUE));

	pCellMonitorTimer = &cellMonitorTimer;

	timerInit(pCellMonitorTimer, PERIODS.VOLTAGE);

	stateInit(pBatteryBalancerFSM, balancerStateVectorArray, BALANCER_NUM_STATES, BALANCER_INIT);
}



void Cells_Monitor(void) 
{
	uint32_t timerIncrement = 1;
		
	if (timerExpired(pCellMonitorTimer)) 
	{
		
		timerReset(pCellMonitorTimer);
		
		Read_CellVoltages_6810();
		
		Read_CellVoltages();
		
		Read_Current();
		
		
		if (pSystemStatus->flags.cellUnderVoltageExists == 0 &&  IsUnderVoltageDetected())
		{
			debug_print(DBG_VOLTAGE_BALANCER, " Cells_FSM |   Cells are in UNDER voltage\r\n");

			// Send timer event to the appropriate state in the balancer FSM
			event_handler(pBatteryBalancerFSM, evCELLS_UNDER_VOLTAGE, &timerIncrement);
			
		}
		else if (pSystemStatus->flags.cellOverVoltageExists == 0 && IsOverVoltageDetected())
		{
			debug_print(DBG_VOLTAGE_BALANCER, " Cells_FSM |   Cells are in OVER voltage\r\n");

			event_handler(pBatteryBalancerFSM, evCELLS_OVER_VOLTAGE, &timerIncrement);
		}
		
		
		 if((FSM_counters.BattCells_counter++ % 10)==0)
		 {
			 //debug_print(DBG_FSM_BALANCER, " Cells_FSM |   %s        C1: %3.2f          C2: %3.2f          C3: %3.2f          C4: %3.2f",balancerStateString[pBatteryBalancerFSM->currentState],FVC1,FVC2,FVC3,FVC4);
		 }


		// Send timer event to the appropriate state in the balancer FSM
		event_handler(pBatteryBalancerFSM, evTIMER_TICK, &timerIncrement);

	}

}

void state_balancer_Init(int event, void *param) 
{
	switch (event) 
	{
		case evENTER_STATE:
		
		#if defined(MASTER)
		SystemMinCellVoltage = 3.2;
		#else
		SystemMinCellVoltage = 3.0;
		#endif
	
		Mask = 0;

		pBatteryBalancerFSM->nextState = BALANCER_BALANCED;		
		break;

		case evTIMER_TICK:
		break;

		case evEXIT_STATE:
		break;
	}
}

void state_balancer_NotBalanced(int event, void *param)
{
	float dischargingCurrent;
	float dischargingThreshold = 0.1;
	
	switch (event) 
	{
		case evENTER_STATE:
		
		pSystemStatus->flags.CellsBalanced = 0;
		
		debug_print(DBG_VOLTAGE_BALANCER," Cells_FSM |   Entering 'Not Balanced' state");	
		
		Enable_Balancing();
		
		break;

		case evTIMER_TICK:
		
		dischargingCurrent = Get_DisChargingCurrent();
		
		ModuleBatteryVoltage = FVC1 + FVC2 + FVC3 + FVC4;

		debug_print(DBG_VOLTAGE_BALANCER,  " Cells_FSM |   FVC1 = %02.3f , FVC2 = %02.3f , FVC3 = %02.3f , FVC4 = %02.3f  PackVoltage = %02.3f \n", FVC1, FVC2, FVC3, FVC4, ModuleBatteryVoltage);
		
		// stop balancing if we are done OR we start discharging.
		if ((dischargingCurrent > dischargingThreshold) || (IsBalancingNeeded() == false)/* || (pSystemStatus->flags.engineON == 1)*/)
		{
			pBatteryBalancerFSM->nextState = BALANCER_BALANCED;
			debug_print(DBG_VOLTAGE_BALANCER,  " Cells_FSM |   Batteries are now Balanced. Turn off Balancing.");
			Disable_Balancing();
		}

		break;

		case evBALANCER_BALANCED:
		pBatteryBalancerFSM->nextState = BALANCER_BALANCED;
		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Received evBALANCER_BALANCED event%s", "\r\n");
		break;
		
		case evCELLS_UNDER_VOLTAGE:
		pBatteryBalancerFSM->nextState = BALANCER_CELLS_UNDER_VOLTAGE;
		break;

		case evCELLS_OVER_VOLTAGE:
		pBatteryBalancerFSM->nextState = BALANCER_CELLS_OVER_VOLTAGE;
		break;
		

		case evEXIT_STATE:
		break;
	}
}

void state_balancer_Balanced(int event, void *param) 
{


	switch (event) 
	{
		case evENTER_STATE:
		
		pSystemStatus->flags.CellsBalanced = 1;
		debug_print(DBG_VOLTAGE_BALANCER,  " Cells_FSM |   Entering 'Balanced' State.");
		Disable_Balancing();
				
		break;

		case evTIMER_TICK:
	
		if (IsBalancingNeeded() == true)
		{
			pBatteryBalancerFSM->nextState = BALANCER_NOT_BALANCED;
			debug_print(DBG_VOLTAGE_BALANCER,  " Cells_FSM |   Batteries needs balancing.");
			Enable_Balancing();
		}
		
		break;

		case evNOT_BALANCED:

		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Received NOT BALANCED event%s", "\r\n");
		
		if (!pSystemStatus->flags.moduleFaultExists)
		{
				pBatteryBalancerFSM->nextState = BALANCER_NOT_BALANCED;
		}	
		else
		{
			debug_print(DBG_VOLTAGE_BALANCER,  "Ignoring evNOT_BALANCED event since the module has detected a fault%s", "\r\n");
		}		
		break;

		case evCELLS_UNDER_VOLTAGE:
		pBatteryBalancerFSM->nextState = BALANCER_CELLS_UNDER_VOLTAGE;
		break;

		case evCELLS_OVER_VOLTAGE:
		pBatteryBalancerFSM->nextState = BALANCER_CELLS_OVER_VOLTAGE;
		break;

		case evEXIT_STATE:
		/// SHould never get here

		break;
	}
}

void state_balancer_cells_under_voltage(int event, void *param) 
{

	uint8_t minCell;

	switch (event) 
	{
		case evENTER_STATE:
		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Entering BALANCER UNDER voltage. Opening the contactor.",);

		pSystemStatus->flags.lowPowerModeIsEnabled = 1;
		pSystemStatus->flags.cellUnderVoltageExists = 1;

		pSystemStatus->flags.contactorOffOverrideIsInEffect = 1;
		pSystemStatus->flags.contactorOnOverrideIsInEffect = 0;
		//event_handler(pContactorFSM, evCONTACTOR_LOW_POWER, "Sender: updateCellVoltages - Under Voltage override");

		ModuleMinCellVoltage = 2.5;

		
		break;

		case evTIMER_TICK:
		
		minCell = Get_MinVoltageCell();

		// Keep discharging the cells until all are at 2.2V
		Mask = Get_CellDiffStatus(minCell);
		
		//if ((getEnabledOptoCount() == 0))
		if(Mask == 0)
		{
			pBatteryBalancerFSM->nextState = BALANCER_BALANCED;
			//event_handler(pContactorFSM, evCONTACTOR_ON, "Sender: balancer UNDER");
			debug_print(DBG_VOLTAGE_BALANCER,  "All cells are now below %2.4f -- pC1(%2.4f), pC2(%2.4f), pC3(%2.4f), pC4(%2.4f)\r\n",ModuleMinCellVoltage, FVC1, FVC2, FVC3, FVC4);
		}
		else
		{
			debug_print(DBG_VOLTAGE_BALANCER,  "Discharging cells to %2.4f -- pC1(%2.4f), pC2(%2.4f), pC3(%2.4f), pC4(%2.4f)\r\n",ModuleMinCellVoltage, FVC1, FVC2, FVC3, FVC4);
		}
				
		break;


		case evEXIT_STATE:
		
		pSystemStatus->flags.lowPowerModeIsEnabled = 0;

		pSystemStatus->flags.contactorOffOverrideIsInEffect = 0;
		pSystemStatus->flags.contactorOnOverrideIsInEffect = 0;
		
		pSystemStatus->flags.cellUnderVoltageExists = 0;

		break;
	}
}

void state_balancer_cells_over_voltage(int event, void *param) 
{
	static uint8_t timeCount = 0;
	static SW_TIMER overVoltageTimer;

	switch (event) 
	{
		case evENTER_STATE:
		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Entering BALANCER cells OVER voltage %s", "\r\n");
		ModuleMinCellVoltage = 3.3333;
		timeCount = 0;
		
		pSystemStatus->flags.cellOverVoltageExists = 1;
		
		timerInit(&overVoltageTimer, PERIODS.OVER_VOLTAGE);
		break;

		case evTIMER_TICK:
		
		if (timerExpired(&overVoltageTimer))
		{
			pBatteryBalancerFSM->nextState = BALANCER_NOT_BALANCED;
		}
		
		break;


		case evEXIT_STATE:
		pSystemStatus->flags.cellOverVoltageExists = 0;

		break;
	}
}

uint8_t Get_MinVoltageCell(void) 
{

	int8_t m1, m2, m;
	float c1, c2;

	m1 = m2 = 0;
	c1 = FVC1;
	c2 = FVC3;

	if (FVC1 < FVC2)
	{
		c1 = FVC1;
		m1 = 1; 
	}
	else
	{
		c1 = FVC2;
		m1 = 2;
	}
	
	if (FVC3 < FVC4)
	{
		c2 = FVC3;
		m2 = 3;
	}
	else
	{
		c2 = FVC4;
		m2 = 4;
	}
	
	if (c1 < c2)
	{
		m = m1;
		ModuleMinCellVoltage = c1;
	}
	else
	{
		m = m2;
		ModuleMinCellVoltage = c2;
	}

	//debug_print(DBG_STATE_MACHINE_BALANCER,  "  Min voltage is at cell %d <--> %2.4fVdc)\r\n",m, ModuleMinCellVoltage);

	return m;
}

uint8_t optoOnCnt;

uint8_t getEnabledOptoCount()
{
	return optoOnCnt;
}

uint8_t Get_CellDiffStatus(uint8_t minCellNum) 
{	
	optoOnCnt = 0;
	uint8_t mask = 0;
	
	if ((minCellNum != 1) && (FVC1 > ModuleMinCellVoltage + CELL_VOLTAGE_MARGIN)) 
	{
		mask |= 0x1;
		optoOnCnt++;		
	}
	
	if ((minCellNum != 2) && (FVC2 > ModuleMinCellVoltage + CELL_VOLTAGE_MARGIN))
	{
		mask |= 0x02;
		optoOnCnt++;
	}

	if ((minCellNum != 3) && (FVC3 > ModuleMinCellVoltage + CELL_VOLTAGE_MARGIN))
	 {
		mask |= 0x4;
		optoOnCnt++;
	}
	
	if ((minCellNum != 4) && (FVC4 > ModuleMinCellVoltage + CELL_VOLTAGE_MARGIN)) 
	{
		mask |= 0x8;
		optoOnCnt++;
	}
	
	//debug_print(DBG_STATE_MACHINE_BALANCER,  "Opto mask is now %01X\r\n",Mask);
	return mask;
}

float Get_BankVoltage()
{
	volatile float val;
	
	//val = pC1->correctedAverage + pC2->correctedAverage + pC3->correctedAverage + pC4->correctedAverage;
	
	val = FVC1 + FVC2 + FVC3 + FVC4;

	return val;
}

bool IsOverVoltageDetected(void)
{
	static uint8_t cellFaultCount = 0;
	bool result = false;
	
	//TODO:Remove this
	return false;
	
	float T = CELL_HIGH_VOLTAGE_THRESHOLD;
	
	uint8_t stat = ((FVC1 > T ) || (FVC2 > T ) || (FVC3 > T) || (FVC4 > T)) ? 1:0;

	if (stat == 1)
	{
		cellFaultCount++;
		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Cells are in OVER condition pC1(%2.4f), pC2(%2.4f), pC3(%2.4f), pC4(%2.4f)\r\n",FVC1, FVC2, FVC3, FVC4);
	}
	else
	{
		cellFaultCount = 0;
	}
	
	if (cellFaultCount >= 240)
	{
		cellFaultCount = 240;
		result = true;
	}
	
	return (result);
}

bool IsUnderVoltageDetected(void)
{
	static uint8_t cellFaultCount = 0;
	bool result = false;
	
	//TODO:Remove this
	return false;
		
	float T = CELL_LOW_VOLTAGE_THRESHOLD ;
	
	uint8_t stat = ((FVC1 < T ) || (FVC2 < T ) || (FVC3 < T) || (FVC4 < T)) ? 1:0;
	
	if (stat == 1)
	{
		cellFaultCount++;
		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Cells are in UNDER condition pC1(%2.4f), pC2(%2.4f), pC3(%2.4f), pC4(%2.4f)\r\n",FVC1, FVC2, FVC3, FVC4);
	}
	else
	{
		cellFaultCount = 0;
	}
	
	if (cellFaultCount >= 240)
	{
		cellFaultCount = 240;
		result = true;
	}
	
	return (result);
}

bool IsBalancingNeeded(void)
{
	static uint8_t cellFaultCount = 0;
	uint8_t stat = 0;
	bool result = false;
	float diff_12,diff_13,diff_14,diff_23,diff_24,diff_34;
	
	float T = DELTA_NEED_BALANCING;
	
	if( FVC1 > FVC2)
	{
		diff_12 = FVC1 - FVC2;
	}
	else
	{
		diff_12 = FVC2 - FVC1;
	}
	
	if( FVC1 > FVC3)
	{
		diff_13 = FVC1 - FVC3;
	}
	else
	{
		diff_13 = FVC3 - FVC1;
	}
	
	if( FVC1 > FVC4)
	{
		diff_14 = FVC1 - FVC4;
	}
	else
	{
		diff_14 = FVC4 - FVC1;
	}
	
	if( FVC2 > FVC3)
	{
		diff_23 = FVC2 - FVC3;
	}
	else
	{
		diff_23 = FVC3 - FVC2;
	}
	
	if( FVC2 > FVC4)
	{
		diff_24 = FVC2 - FVC4;
	}
	else
	{
		diff_24 = FVC4 - FVC2;
	}
	
	if( FVC3 > FVC4)
	{
		diff_34 = FVC3 - FVC4;
	}
	else
	{
		diff_34 = FVC4 - FVC3;
	}
	
	/*
	diff_12 = absf(FVC1-FVC2);
	diff_13 = absf(FVC1-FVC3);
	diff_14 = absf(FVC1-FVC4);
	diff_23 = absf(FVC2-FVC3);
	diff_24 = absf(FVC2-FVC4);
	diff_34 = absf(FVC3-FVC4);
	*/
	
	//TODO:Remove this
	return false;
	
	if((diff_12 > T ) || (diff_13 > T ) || (diff_14 > T) || (diff_23 > T)|| (diff_24 > T) || (diff_34 > T))
	{
		stat = 1;
	}
	
	if (stat == 1)
	{
		cellFaultCount++;
		//debug_print(DBG_STATE_MACHINE_BALANCER,  "Cells are in UNDER condition pC1(%2.4f), pC2(%2.4f), pC3(%2.4f), pC4(%2.4f)\r\n",FVC1, FVC2, FVC3, FVC4);
	}
	else
	{
		cellFaultCount = 0;
	}
	
	if (cellFaultCount >= 12)
	{
		cellFaultCount = 12;
		result = true;
	}
	
	return (result);
	
}

void Read_CellVoltages(void) 
{
	uint16_t ref_count,c1_count,c2_count,c3_count,c4_count,term_count,pack_count;
	float c1_volt,c2_volt,c3_volt,c4_volt,term_volt,pack_volt;
	
	
	select_Mux_VC1();
	c1_count = Read_Adc0( VOLTAGE_MUX_CH);	
	
	select_Mux_VC2();
	c2_count = Read_Adc0( VOLTAGE_MUX_CH);
	
	select_Mux_VC3();	
	c3_count = Read_Adc0( VOLTAGE_MUX_CH);
	
	select_Mux_VC4();	
	c4_count = Read_Adc0( VOLTAGE_MUX_CH);	
	
	select_Mux_Term();
	term_count = Read_Adc0( VOLTAGE_MUX_CH);
	
	select_Mux_Pack();
	pack_count = Read_Adc0( VOLTAGE_MUX_CH);
	
	ref_count = Read_Adc0(ADC0_VREF_CH);
	
	c1_volt =VREF_CONST * ((float) c1_count / (float)ref_count);
	c2_volt =VREF_CONST * ((float) c2_count / (float)ref_count);
	c3_volt =VREF_CONST * ((float) c3_count / (float)ref_count);
	c4_volt =VREF_CONST * ((float) c4_count / (float)ref_count);	
	
	term_volt = VREF_CONST * ((float) term_count / (float)ref_count);
	pack_volt = VREF_CONST * ((float) pack_count / (float)ref_count);
	
	FVC1 = (c1_volt * ADC_CELL_VOLT_OPAMP_GAIN);
	FVC2 = (c2_volt * ADC_CELL_VOLT_OPAMP_GAIN);
	FVC3 = (c3_volt * ADC_CELL_VOLT_OPAMP_GAIN);
	FVC4 = (c4_volt * ADC_CELL_VOLT_OPAMP_GAIN);
	
	TermVoltage = term_volt * ADC_TERM_VOLT_OPAMP_GAIN;
	PackVoltage = pack_volt * ADC_PACK_VOLT_OPAMP_GAIN;
	
	DeltaVoltage = (TermVoltage - PackVoltage);
	
	pSystemStatus->flags.moduleFaultExists = IsUnderVoltageDetected() ? 1:0;
	
	debug_print(DBG_VOLTAGE_BALANCER,  " Cells__FSM |   C1 = %2.3f   C2 = %2.3f   C3 = %2.3f   C4 = %2.3f",FVC1,FVC2, FVC3,FVC4);
	
	
}

void Read_Currents_old(void) 
{
	uint16_t ref_count,char_current_count,disch_current_count;
	float char_current_volt,disch_current_volt;
		
	char_current_count = Read_Adc1(ADC1_ICHARGE_CH);	
	disch_current_count = Read_Adc1(ADC1_IDISCHARGE_LOW_CH);	

	ref_count = Read_Adc1(ADC1_VREF_CH);
	
	char_current_volt =VREF_CONST * ((float) char_current_count / (float)ref_count);
	disch_current_volt =VREF_CONST * ((float) disch_current_count / (float)ref_count);
	
	ChargingCurrent = ((char_current_volt * ADC_CHARG_CURR_OPAMP_GAIN)/SHUNT_RES);
	DischargingCurrent = ((disch_current_volt * ADC_DISCHARG_CURR_OPAMP_LOW_GAIN)/SHUNT_RES);
	
}

float Get_ChargingCurrent()
{
	return ChargingCurrent;
}

float Get_DisChargingCurrent()
{
	return DischargingCurrent;
}
float Get_Cell_1_Volt()
{
	return FVC1;
}
float Get_Cell_2_Volt()
{
	return FVC2;
}
float Get_Cell_3_Volt()
{
	return FVC3;
}
float Get_Cell_4_Volt()
{
	return FVC4;
}

float Get_DeltaVoltage()
{
	return DeltaVoltage;
}

float Get_PackVoltage()
{
	return PackVoltage;
}


float Get_TermVoltage()
{
	return TermVoltage;
}

float Read_Term_V()
{
	float scale = BIT_RES_ADC_SCALE;
	uint16_t count = 0;
	float retVal;
	
	count = Read_Adc1(ADC0_VTERM_CH);
	
	retVal = scale * count;
	
	retVal = retVal *  ADC_TERM_VOLT_OPAMP_GAIN;
	
	return retVal;
}

float Read_Pack_V()
{
	float scale = BIT_RES_ADC_SCALE;
	uint16_t count = 0;
	float retVal;
	
	count = Read_Adc0(ADC0_VPACK_CH);
	
	retVal = scale * count;
	
	retVal = retVal *  ADC_PACK_VOLT_OPAMP_GAIN;
	
	return (retVal);
}

float Read_ChargingCurrent()
{
	float scale = BIT_RES_ADC_SCALE;
	float term_voltage = 0.0;
	uint16_t count = 0;
	float retVal,temp;
	
	count = Read_Adc0(ADC1_ICHARGE_CH);
	
	temp = scale * count;
	
	term_voltage = temp *  ADC_CHARG_CURR_OPAMP_GAIN;
	
	retVal = (term_voltage/SHUNT_RES);
	
	ChargingCurrent = retVal;
	
	return (retVal);
}

float Read_DisChargingCurrentHiGain()
{
	float scale = BIT_RES_ADC_SCALE;
	float term_voltage = 0.0;
	uint16_t count = 0;
	float retVal,temp;
	
	count = Read_Adc0(ADC1_IDISCHARGE_HIGH_CH);
	
	temp = scale * count;
	
	term_voltage = temp * ADC_DISCHARG_CURR_OPAMP_LOW_GAIN;
	
	retVal = (term_voltage/SHUNT_RES);
	
	return (retVal);
}

float Read_DisChargingCurrentLowGain()
{
	float scale = BIT_RES_ADC_SCALE;
	float term_voltage = 0.0;
	uint16_t count = 0;
	float retVal,temp;
	
	count = Read_Adc0(ADC1_IDISCHARGE_LOW_CH);
	
	temp = scale * count;
	
	term_voltage = temp *  ADC_DISCHARG_CURR_OPAMP_HIGH_GAIN;
	
	retVal = (term_voltage/SHUNT_RES);
	
	return (retVal);
}

float Read_DisChargingCurrent()
{
	float scale = BIT_RES_ADC_SCALE;
	float term_voltage = 0.0;
	uint16_t count_L = 0;
	uint16_t count_H = 0;
	float retVal,temp;
	
	count_L = Read_Adc0(ADC1_IDISCHARGE_LOW_CH);
	
	if(count_L > 4090)
	{
		count_H = Read_Adc0(ADC1_IDISCHARGE_HIGH_CH);
		
		temp = scale * count_H;
		
		term_voltage = temp *  ADC_DISCHARG_CURR_OPAMP_HIGH_GAIN;
		
		retVal = (term_voltage/SHUNT_RES);
	}
	else
	{
		temp = scale * count_L;
		
		term_voltage = temp *  ADC_DISCHARG_CURR_OPAMP_LOW_GAIN;
		
		retVal = (term_voltage/SHUNT_RES);
	}
	
	DischargingCurrent = retVal;
	
	return retVal;
}


void select_Mux_VC1()
{
	//000
	gpio_set_pin_level(B0_UC,false);
	gpio_set_pin_level(B1_UC,false);
	gpio_set_pin_level(B2_UC,false);
}

void select_Mux_VC2()
{
	//001
	gpio_set_pin_level(B0_UC,true);
	gpio_set_pin_level(B1_UC,false);
	gpio_set_pin_level(B2_UC,false);
}

void select_Mux_VC3()
{
	//002
	gpio_set_pin_level(B0_UC,false);
	gpio_set_pin_level(B1_UC,true);
	gpio_set_pin_level(B2_UC,false);
}

void select_Mux_VC4()
{
	//003
	gpio_set_pin_level(B0_UC,true);
	gpio_set_pin_level(B1_UC,true);
	gpio_set_pin_level(B2_UC,false);
}

void select_Mux_Pack()
{
	//004
	gpio_set_pin_level(B0_UC,false);
	gpio_set_pin_level(B1_UC,false);
	gpio_set_pin_level(B2_UC,true);
}

void select_Mux_Term()
{
	//005
	gpio_set_pin_level(B0_UC,true);
	gpio_set_pin_level(B1_UC,false);
	gpio_set_pin_level(B2_UC,true);
}





