/*
 * common.h
 *
 * Created: 5/28/2020 1:54:28 PM
 *  Author: BehroozH
 */ 


#ifndef COMMON_H_
#define COMMON_H_

extern float const TERM_VOLT_ENGINE_OFF_COUNT;
extern float const TERM_VOLT_ENGINE_OFF_VOLT;
extern float const SELF_DISCHARGE_CURRENT;
extern float const LOW_DISCHARGE_CURRENT;
extern float const CELL_BALANCED_THRESHOLD;
extern float const CELL_NEED_BALANCING_THRESHOLD;
extern float const DELTA_NEED_BALANCING;
extern float const CELL_LOW_VOLTAGE_THRESHOLD;
extern float const CELL_HIGH_VOLTAGE_THRESHOLD;
extern float const ADC0_COUNT_TO_VOLT_SCALE; 
extern float const ADC1_COUNT_TO_VOLT_SCALE;
extern float const R_SHUNT;
 
typedef struct  PERIODS_TAG
{
	uint32_t	VOLTAGE;
	uint32_t    HEARTBEAT;
	uint32_t	TEMPERATURE;
	uint32_t	LOGGER;
	uint32_t	UPDATER;
	uint32_t    SHUNT;
	uint32_t	SLEEP;
	uint32_t	LED;
	uint32_t	OVER_VOLTAGE;
	uint32_t	SOC;
	
}T_PERIODS;

extern T_PERIODS  PERIODS;

void Print_Header();

#endif /* COMMON_H_ */