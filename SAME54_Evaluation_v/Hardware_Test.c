/*
 * Hardware_Test.c
 *
 * Created: 8/5/2020 9:22:16 AM
 *  Author: BehroozH
 */ 
#include "atmel_start_pins.h"

void Hardware_Test()
{
	if(gpio_get_pin_level(A0_UC) == false)
	gpio_set_pin_level(A0_UC,true);
	else
	gpio_set_pin_level(A0_UC,false);
	
	if(gpio_get_pin_level(A1_UC) == false)
	gpio_set_pin_level(A1_UC,true);
	else
	gpio_set_pin_level(A1_UC,false);
	
	if(gpio_get_pin_level(A2_UC) == false)
	gpio_set_pin_level(A2_UC,true);
	else
	gpio_set_pin_level(A2_UC,false);
	
	if(gpio_get_pin_level(B0_UC) == false)
	gpio_set_pin_level(B0_UC,true);
	else
	gpio_set_pin_level(B0_UC,false);
	
	if(gpio_get_pin_level(B1_UC) == false)
	gpio_set_pin_level(B1_UC,true);
	else
	gpio_set_pin_level(B1_UC,false);
	
	if(gpio_get_pin_level(B2_UC) == false)
	gpio_set_pin_level(B2_UC,true);
	else
	gpio_set_pin_level(B2_UC,false);
	
	if(gpio_get_pin_level(IMUX_EN) == false)
	gpio_set_pin_level(IMUX_EN,true);
	else
	gpio_set_pin_level(IMUX_EN,false);
	
	if(gpio_get_pin_level(IMUX_A) == false)
	gpio_set_pin_level(IMUX_A,true);
	else
	gpio_set_pin_level(IMUX_A,false);
	
	if(gpio_get_pin_level(IMUX_B) == false)
	gpio_set_pin_level(IMUX_B,true);
	else
	gpio_set_pin_level(IMUX_B,false);
	
	if(gpio_get_pin_level(WKUP_UC_3V3) == false)
	gpio_set_pin_level(WKUP_UC_3V3,true);
	else
	gpio_set_pin_level(WKUP_UC_3V3,false);
	
	if(gpio_get_pin_level(HTR_UC) == false)
	gpio_set_pin_level(HTR_UC,true);
	else
	gpio_set_pin_level(HTR_UC,false);
	
	if(gpio_get_pin_level(EN_BAL_UC) == false)
	gpio_set_pin_level(EN_BAL_UC,true);
	else
	gpio_set_pin_level(EN_BAL_UC,false);
	
	if(gpio_get_pin_level(BAL_UC) == false)
	gpio_set_pin_level(BAL_UC,true);
	else
	gpio_set_pin_level(BAL_UC,false);
	
}
