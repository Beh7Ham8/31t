#ifndef _HAL_USB_CDC_H_
#define _HAL_USB_CDC_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <stdint.h>
#include "cdcdf_acm.h"
#include "cdcdf_acm_desc.h"

/* start/stop USB CDC ACM */
void HAL_CDC_Initialize(void);
void HAL_CDC_Destroy(void);

/* check if DTS state (Data Terminal Ready) */
bool HAL_CDC_IsConnected(void);

/* get USB state (see 'enum usbd_state' in usb/device/usbdc.h) */
uint8_t HAL_CDC_GetState(void);

/* check if it's ready for a transmission */
bool HAL_CDC_Ready(void);

/* write data */
int32_t HAL_CDC_Send(void* buf, uint32_t size);

/* read received data if any */
uint32_t HAL_CDC_Read(void* buf, uint32_t size);

/* check received data size */
uint32_t HAL_CDC_Peak(void);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif
