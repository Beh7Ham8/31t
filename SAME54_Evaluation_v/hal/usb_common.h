#ifndef _HAL_USB_COMMON_H_
#define _HAL_USB_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <stdint.h>

/* start/stop USB */
void HAL_USB_Initialize(void);
void HAL_USB_Destroy(void);

/* check if USB Cable is connected */
bool HAL_USB_IsPlugged(void);

/** Set serial number reported by USB
 *
 *  \note  The serial number must be the same length, in characters, as the string
 *         configured in Atmel Start.
 *
 *  \param  sn  Serial Number in ASCII, read from EEPROM, zero-terminated
 *
 *  \return  ERR_NONE (0) on success, nonzero on error
 */
int HAL_USB_Serial (const char *sn);


#ifdef __cplusplus
}
#endif // __cplusplus

#endif
