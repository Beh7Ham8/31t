#ifndef _HAL_SPI_H_
#define _HAL_SPI_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <stdint.h>

void HAL_SPI_Initialize(void);
int32_t HAL_SPI_WriteRead(void* txBuff, void* rxBuff, uint32_t size);
int32_t HAL_SPI_DummyWriteRead();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif
