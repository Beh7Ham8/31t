#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "atmel_start.h"
#include "usb_cdc.h"
#include "usb_msc.h"
#include "cdcdf_acm.h"
#include "cdcdf_acm_desc.h"
#include "mscdf_desc.h"
#include "gpio.h"
#include <lib/hexdump.h>

static uint8_t combined_desc_bytes[] =
{
	/* Device descriptor from CDC/ACM config */
	USB_DEV_DESC_BYTES(CONF_USB_CDCD_ACM_BCDUSB,
	                   0x00, 0x00, 0x00,
	                   CONF_USB_CDCD_ACM_BMAXPKSZ0,
	                   CONF_USB_CDCD_ACM_IDVENDER,
	                   CONF_USB_CDCD_ACM_IDPRODUCT,
	                   CONF_USB_CDCD_ACM_BCDDEVICE,
	                   CONF_USB_CDCD_ACM_IMANUFACT,
	                   CONF_USB_CDCD_ACM_IPRODUCT,
	                   CONF_USB_CDCD_ACM_ISERIALNUM,
	                   CONF_USB_CDCD_ACM_BNUMCONFIG),

	/* Combined config descriptor: CDC/ACM config leads, but requires extra length for
	 * combined interface descriptors.  Consists of a fixed 9-byte descriptor which
	 * includes a total length:
	 *   MSC was 9 + 23 = 32, CDC was 9 + 58 = 67, so total here is 9 + 23 + 58 = 90.
	 *   +8 bytes for Interface Association Descriptor
	 * Descriptor also needs a count of number of interfaces: 2 for CDC, 1 for MSC. */
	USB_CONFIG_DESC_BYTES(98, 3, // combined
	                      CONF_USB_CDCD_ACM_BCONFIGVAL,
	                      CONF_USB_CDCD_ACM_ICONFIG,
	                      CONF_USB_CDCD_ACM_BMATTRI,
	                      CONF_USB_CDCD_ACM_BMAXPOWER),

	/* Use MSC interface descriptors as generated */
	MSC_IFACE_DESCES,

	/* Interface Association Descriptor avoids driver install for CDC on Windows 10 */
	0x08, 0x0B, 0x01, 0x02, 0x2, 0x2, 0x0, CONF_USB_CDCD_ACM_IPRODUCT,

	/* Use CDC interface descriptors as generated */
	CDCD_ACM_COMM_IFACE_DESCES,
	CDCD_ACM_DATA_IFACE_DESCES,

	/* String descriptors from CDC/ACM config */
	CDCD_ACM_STR_DESCES
};

static struct usbd_descriptors combined_desc[] =
{
    {combined_desc_bytes, combined_desc_bytes + sizeof(combined_desc_bytes)},
};


/** Set serial number reported by USB
 *
 *  \note  The serial number must be the same length, in characters, as the string
 *         configured in Atmel Start.
 *
 *  \param  sn  Serial Number in ASCII, read from EEPROM, zero-terminated
 *
 *  \return  ERR_NONE (0) on success, nonzero on error
 */
int HAL_USB_Serial (const char *sn)
{
	unsigned char *p = combined_desc[0].sod;
	uint8_t        n = combined_desc_bytes[16] + 1;

	/* iterate through descriptors until we hit the nth string descriptor */
	while ( p < combined_desc[0].eod )
		if ( 0x03 == p[1] && ! --n )
			break;
		else
			p += *p;

	/* Verify string found; if you assert here check your iSerialNum string is
	 * is enabled in Atmel Start and regenerate if necessary */
	ASSERT(p < combined_desc[0].eod);

	/* S/N string passed must be exactly the same length as template string set in
	 * Atmel Start; if you assert here, change the iSerialNum string template in the
	 * USB_DEVICE_CDC_ACM component and regenerate */
	n = strlen(sn);
	ASSERT(n == (p[0] - 2) / 2);

	/* Copy bytes, converting ASCII to 16-bit Unicode */
	p += 2;
	while ( *sn )
	{
		*p++ = *sn++;
		*p++ = '\0';
	}
	
	return ERR_NONE;
}


/** Ctrl endpoint buffer */
static uint8_t ctrl_buffer[64];
void HAL_USB_Initialize(void)
{
	usbdc_init(ctrl_buffer);

	HAL_CDC_Initialize();
	HAL_MSC_Initialize();

	usbdc_start(combined_desc);
	usbdc_attach();
}

void HAL_USB_Destroy(void)
{
	HAL_CDC_Destroy();
	HAL_MSC_Destroy();

	usbdc_detach();
	usbdc_stop();

	usbdc_deinit();
}

bool HAL_USB_IsPlugged(void)
{
	return HAL_GPIO_GET(GPIO_USB_VBUS);
}

