/** USB Mass-Storage Class (MSC) implementation
 *
 *  The MSC can provide a host PC access to the SD card; read-only support is stable,
 *  write support has had limited testing.  The SD/MMC FSM is responsible for
 *  coordinating between the FATFS filesystem, used for datalogging, and the USB MSC
 *  function, based on whether the USB cable is plugged in.  This is done through the
 *  HAL_MSC_UpdateReady() function.
 *
 *  USB MSC can reveal the entire card to the host PC, if it has been unlocked by a
 *  service tool such as ELTTerminal, or restrict the end-user's visibility to just the 
 *  PART_USER partition.  This is managed by the sd_mmc_PartsLimit() function, which calls
 *  HAL_MSC_UpdateSize() at the appropriate times.
 *
 *  If write support (MSC_SUPPORT_WRITE) is enabled, sd_mmc_PartsLimit() can call the 
 *  HAL_MSC_UpdateReadOnly() function to enable/disable writing to the SD card.  If
 *  MSC_SUPPORT_WRITE this has no effect.  Write support and enable are reported to the
 *  host PC as a Write-Protect status in the MODE SENSE(6) command.
 */
#ifndef HAL_USB_MSC_H
#define HAL_USB_MSC_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/** USB MSC timeslice
 *
 *  This should be called from the main loop as often as practical; it will perform the
 *  SDIO read/write operations interlocked with the IRQ-time callbacks from the USB device
 *  stack.
 */
void HAL_MSC_Monitor (void);

/** Initialize USB MSC support, setup callbacks, and reset state vars */
void HAL_MSC_Initialize (void);

/** Shut down USB MSC support */
void HAL_MSC_Destroy (void);

/** Update ready state
 *
 *  SD-card FSM will set this when it has unmounted FATFS on USB insert, and it is safe
 *  for USB to access the SD-card
 *
 *  \param  ready  True if USB can access the card, False if not.
 */
void HAL_MSC_UpdateReady (bool  ready);

/** Update advertised size
 *
 *  SD-card FSM will set this after checking the disk size, limited by the parts_limit()
 *  setting, to prevent user access to service data partitions
 *
 *  \param  hard  Number of sectors on physical disk
 *  \param  soft  Number of sectors to expose via USB
 */
void HAL_MSC_UpdateSize (uint32_t  hard, uint32_t  soft);

/** Update read-only flag
 *
 *  - If MSC_SUPPORT_WRITE is nonzero, controls whether writes to the disk will be
 *    honored, or return an access-denied code to the USB host.
 *  - If MSC_SUPPORT_WRITE is zero, has no effect, writes will always be denied.
 */
void HAL_MSC_UpdateReadOnly (bool  val);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif /* HAL_USB_MSC_H */
