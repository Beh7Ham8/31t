#ifndef _HAL_USB_HOST_H_
#define _HAL_USB_HOST_H_

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include <stdint.h>
#include <stdbool.h>
#include "usbhc.h"
#include "cdchf_acm.h"

/* start/stop USB HOST CDC ACM */
void HAL_HOST_Initialize(void);
void HAL_HOST_Destroy(void);

/* check if a device is connected */
bool HAL_HOST_DeviceReady(void);

/* close the device connection */
void HAL_HOST_DeviceClose(void);

/* send data */
bool HAL_HOST_Write(const uint8_t *buf, uint16_t size);

/* receive data */
uint16_t USB_HOST_Read(uint8_t *buf, uint16_t size);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif
