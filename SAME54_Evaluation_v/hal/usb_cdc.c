#include <stdio.h>
#include <string.h>
#include "atmel_start.h"
#include "usb_cdc.h"
#include "gpio.h"

/** Buffers to receive and echo the communication bytes. */
static uint8_t usbd_cdc_buffer[2][1024] __attribute__(( aligned(32)));

static volatile uint8_t  tx_busy;
static volatile bool     tx_ready;
static volatile uint8_t  rx_index;
static volatile uint8_t  rx_failed;
static volatile uint16_t rx_count;
static volatile usb_cdc_control_signal_t usbd_cdc_state;

static bool usb_device_cb_bulk_out(const uint8_t ep, const enum usb_xfer_code rc, const uint32_t count)
{
    if (rc == USB_XFER_DONE && count > 0) {
        rx_index = !rx_index;
        rx_failed = cdcdf_acm_read(usbd_cdc_buffer[rx_index], sizeof(usbd_cdc_buffer[rx_index]));
        rx_count = count;
    }
    /* No error. */
    return false;
}

static bool usb_device_cb_bulk_in(const uint8_t ep, const enum usb_xfer_code rc, const uint32_t count)
{
    tx_busy = 0;
    /* No error. */
    return false;
}

/**
 * \brief Callback invoked when Line State Change
 */
static bool usb_device_cb_state_c(usb_cdc_control_signal_t state)
{
    usbd_cdc_state = state;
    if (cdcdf_acm_is_enabled())
    {
        /* Callbacks must be registered after endpoint allocation */
        cdcdf_acm_register_callback(CDCDF_ACM_CB_READ, (FUNC_PTR)usb_device_cb_bulk_out);
        cdcdf_acm_register_callback(CDCDF_ACM_CB_WRITE, (FUNC_PTR)usb_device_cb_bulk_in);
        tx_ready = true;
        /* Start Rx */
        cdcdf_acm_read(usbd_cdc_buffer[rx_index], sizeof(usbd_cdc_buffer[rx_index]));
    }
    /* No error. */
    return false;
}

void HAL_CDC_Initialize(void)
{
    tx_busy = 0;
    rx_index = 0;
    rx_count = 0;
    rx_failed = 0;

    cdcdf_acm_init();

    cdcdf_acm_register_callback(CDCDF_ACM_CB_STATE_C, (FUNC_PTR)usb_device_cb_state_c);
}

void HAL_CDC_Destroy(void)
{
    cdcdf_acm_stop_xfer();

    cdcdf_acm_deinit();
}

bool HAL_CDC_IsConnected(void)
{
    return tx_ready;
}

uint8_t HAL_CDC_GetState(void)
{
    return usbdc_get_state();
}

bool HAL_CDC_Ready(void)
{
    return (!tx_busy && usbd_cdc_state.rs232.DTR);
}

int32_t HAL_CDC_Send(void* buf, uint32_t size)
{
	if(!tx_ready)
	  return ERR_NOT_READY;
		
    if (!tx_busy)
    {
        tx_busy = 1;
        cdcdf_acm_write(buf, size);
        return size;
    }
    return ERR_BUSY;
}

uint32_t HAL_CDC_Read(void* buf, uint32_t size)
{
    volatile uint16_t count = rx_count;
    volatile uint8_t index = !rx_index;
    if (count > 0)
    {
        if (size > count) size = count;
        memcpy(buf, usbd_cdc_buffer[index], size);
        rx_count = 0;
        return size;
    }
    return 0;
}

uint32_t HAL_CDC_Peak(void)
{
    volatile uint16_t size;
    // recover the USB receiver if it's failed previously
    if (rx_failed != USB_OK) {
        rx_failed = cdcdf_acm_read(usbd_cdc_buffer[rx_index], sizeof(usbd_cdc_buffer[rx_index]));
    }
    size = rx_count;
    return size;
}
