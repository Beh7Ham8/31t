#ifndef _HAL_GPIO_H_
#define _HAL_GPIO_H_

#ifdef	__cplusplus
extern "C" {
#endif

#include <hal_gpio.h>
#include "atmel_start_pins.h"

#define GPIO_BTN                BTN
#define GPIO_LED                LED
#define GPIO_USB_VBUS           USB_VBUS_SENSE
#define GPIO_N_CSM              SPI_CS
#define GPIO_SD_DETECT          CARD_DETECT_0
#define GPIO_SD_PROTECT         WRITE_PROTECT_0


#define DIR_OUT                 0
#define DIR_IN                  1

#define LVL_LOW                 0
#define LVL_HIGH                1

#define HAL_GPIO_DIR(gpio,dir)  gpio_set_pin_direction(gpio,(dir)?GPIO_DIRECTION_IN:GPIO_DIRECTION_OUT)
#define HAL_GPIO_SET(gpio,val)  gpio_set_pin_level(gpio,val)
#define HAL_GPIO_GET(gpio)      gpio_get_pin_level(gpio)
#define HAL_GPIO_TOGGLE(gpio)   gpio_toggle_pin_level(gpio)

#define BTN_PRESSED     0
#define BTN_RELEASED    (!BTN_PRESSED)
#define BTN_STATE()     HAL_GPIO_GET(GPIO_BTN)
#define LED_ON()        HAL_GPIO_SET(GPIO_LED, LVL_LOW)
#define LED_OFF()       HAL_GPIO_SET(GPIO_LED, LVL_HIGH)
#define LED_TOGGLE()    HAL_GPIO_TOGGLE(GPIO_LED)

#define SD_CD_INSERT    0
#define SD_CD_STATE()   HAL_GPIO_GET(GPIO_SD_DETECT)

#ifdef	__cplusplus
}
#endif

#endif
