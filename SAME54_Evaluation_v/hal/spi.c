#include <stdio.h>
#include <string.h>
#include "atmel_start.h"
#include "spi.h"
#include "gpio.h"

void HAL_SPI_Initialize(void)
{
    spi_m_sync_enable(&SPI_0);
}

int32_t HAL_SPI_WriteRead(void* txBuff, void* rxBuff, uint32_t size)
{
    int32_t result;
    struct spi_xfer xfer;
    xfer.txbuf = txBuff;
    xfer.rxbuf = rxBuff;
    xfer.size = size;
    HAL_GPIO_SET(GPIO_N_CSM, LVL_LOW);
    result = spi_m_sync_transfer(&SPI_0, &xfer);
    HAL_GPIO_SET(GPIO_N_CSM, LVL_HIGH);
    return result;
}

int32_t HAL_SPI_DummyWriteRead()
{
    int32_t result;
	char txBuff[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	char rxBuff[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};	
    struct spi_xfer xfer;
    xfer.txbuf = txBuff;
    xfer.rxbuf = rxBuff;
    xfer.size = 16;  
    result = spi_m_sync_transfer(&SPI_0, &xfer);
    return result;
}
