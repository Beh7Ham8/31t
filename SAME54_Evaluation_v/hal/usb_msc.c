/** USB Mass-Storage Class (MSC) implementation
 *
 *  The MSC can provide a host PC access to the SD card; read-only support is stable,
 *  write support has had limited testing.  The SD/MMC FSM is responsible for
 *  coordinating between the FATFS filesystem, used for datalogging, and the USB MSC
 *  function, based on whether the USB cable is plugged in.  This is done through the
 *  HAL_MSC_UpdateReady() function.
 *
 *  USB MSC can reveal the entire card to the host PC, if it has been unlocked by a
 *  service tool such as ELTTerminal, or restrict the end-user's visibility to just the 
 *  PART_USER partition.  This is managed by the sd_mmc_PartsLimit() function, which calls
 *  HAL_MSC_UpdateSize() at the appropriate times.
 *
 *  If write support (MSC_SUPPORT_WRITE) is enabled, sd_mmc_PartsLimit() can call the 
 *  HAL_MSC_UpdateReadOnly() function to enable/disable writing to the SD card.  If
 *  MSC_SUPPORT_WRITE this has no effect.  Write support and enable are reported to the
 *  host PC as a Write-Protect status in the MODE SENSE(6) command.
 */
#include <stdio.h>
#include <string.h>

#include <driver_init.h>
#include "mscdf.h"

#include "sd_mmc/fatfs.h"
#include "sd_mmc/parts.h"
#include "diskio.h"
#include "usb_common.h"



/* Define nonzero to support writing to SD card over USB */
#define MSC_SUPPORT_WRITE 0
#if MSC_SUPPORT_WRITE
#warning MSC_SUPPORT_WRITE is not well-tested
#endif


#define debug(fmt, ...) do{ }while(0)
#define info(fmt, ...)  printf(fmt, ##__VA_ARGS__)
#define error(fmt, ...) printf(fmt, ##__VA_ARGS__)


static uint8_t sector[SECTOR_SIZE] COMPILER_ALIGNED(4);


static volatile uint32_t  sect_addr;
static volatile uint16_t  sect_left = 0;
static volatile bool      xfer_read;
static volatile bool      xfer_idle;
static volatile bool      usb_ready;

#if MSC_SUPPORT_WRITE
static volatile bool      xfer_write;
static volatile bool      sect_ready;
static volatile bool      read_only = true;
#endif


/* Inquiry Information */
#define CONF_USB_MSC_LUN0_TYPE 0x00
#define CONF_USB_MSC_LUN0_RMB 0x1
#define CONF_USB_MSC_LUN0_ISO 0x00
#define CONF_USB_MSC_LUN0_ECMA 0x00
#define CONF_USB_MSC_LUN0_ANSI 0x00
#define CONF_USB_MSC_LUN0_REPO 0x01
#define CONF_USB_MSC_LUN0_FACTORY 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
#define CONF_USB_MSC_LUN0_PRODUCT 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
#define CONF_USB_MSC_LUN0_PRODUCT_VERSION 0x00, 0x00, 0x00, 0x00

static uint8_t inquiry_info[36] =
{
	CONF_USB_MSC_LUN0_TYPE,
	(CONF_USB_MSC_LUN0_RMB << 7),
	((CONF_USB_MSC_LUN0_ISO << 6) + (CONF_USB_MSC_LUN0_ECMA << 3) + CONF_USB_MSC_LUN0_ANSI),
	CONF_USB_MSC_LUN0_REPO,
	31,
	0x00,
	0x00,
	0x00,
	CONF_USB_MSC_LUN0_FACTORY,
	CONF_USB_MSC_LUN0_PRODUCT,
	CONF_USB_MSC_LUN0_PRODUCT_VERSION
};


/** Update ready state
 *
 *  SD-card FSM will set this when it has unmounted FATFS on USB insert, and it is safe
 *  for USB to access the SD-card
 *
 *  \param  ready  True if USB can access the card, False if not.
 */
void HAL_MSC_UpdateReady (bool  ready)
{
	debug("usb-msc: ready: set %s\r\n", ready ? "TRUE" : "false");
	usb_ready = ready;
}


/* Disk capacity returned by :
 * - last block address (in sectors) set by fatfs.c via HAL_MSC_UpdateSize()
 * - block size fixed at sector size
 */
static uint32_t capacity_hard = FATFS_DISK_MIN;
static uint32_t capacity_soft = FATFS_DISK_MIN;
static uint8_t capacity_bytes[8] =
{
	(uint8_t)((uint32_t)(FATFS_DISK_MIN - 1) >> 24),
	(uint8_t)((uint32_t)(FATFS_DISK_MIN - 1) >> 16),
	(uint8_t)((uint32_t)(FATFS_DISK_MIN - 1) >> 8),
	(uint8_t)((uint32_t)(FATFS_DISK_MIN - 1) >> 0),
	(uint8_t)((uint32_t)(SECTOR_SIZE) >> 24),
	(uint8_t)((uint32_t)(SECTOR_SIZE) >> 16),
	(uint8_t)((uint32_t)(SECTOR_SIZE) >> 8), 
	(uint8_t)((uint32_t)(SECTOR_SIZE) >> 0)  
};


/** Update advertised size
 *
 *  SD-card FSM will set this after checking the disk size, limited by the parts_limit()
 *  setting, to prevent user access to service data partitions
 *
 *  \param  hard  Number of sectors on physical disk
 *  \param  soft  Number of sectors to expose via USB
 */
void HAL_MSC_UpdateSize (uint32_t  hard, uint32_t  soft)
{
	debug("usb-msc: size: hard %lu, soft %lu\r\n", hard, soft);

	/* uint32_t capacity checked in msc_start_read_disk() and msc_start_write_disk() */
	CRITICAL_SECTION_ENTER();
	capacity_hard = hard;
	capacity_soft = soft;

	/* capacity_bytes returned by msc_get_disk_capacity(), -1 because it's actually the
	 * address of the last block on the disk */
	ASSERT(soft);
	soft--;
	capacity_bytes[0] = 0xFF & (soft >> 24);
	capacity_bytes[1] = 0xFF & (soft >> 16);
	capacity_bytes[2] = 0xFF & (soft >>  8);
	capacity_bytes[3] = 0xFF &  soft;
	CRITICAL_SECTION_LEAVE();

	debug("capacity bytes: ");
//	hexdump_line(capacity_bytes, NULL, sizeof(capacity_bytes));
}


/** Update read-only flag
 *
 *  - If MSC_SUPPORT_WRITE is nonzero, controls whether writes to the disk will be
 *    honored, or return an access-denied code to the USB host.
 *  - If MSC_SUPPORT_WRITE is zero, has no effect, writes will always be denied.
 */
void HAL_MSC_UpdateReadOnly (bool  val)
{
#if MSC_SUPPORT_WRITE
	CRITICAL_SECTION_ENTER();
	read_only = val;
	CRITICAL_SECTION_LEAVE();
#endif
}


static uint8_t *msc_inquiry_disk      (uint8_t lun);
static uint8_t *msc_get_disk_capacity (uint8_t lun);
static int32_t  msc_check_write_ok    (uint8_t lun);
static int32_t  msc_start_read_disk   (uint8_t lun, uint32_t addr, uint32_t nblocks);
static int32_t  msc_start_write_disk  (uint8_t lun, uint32_t addr, uint32_t nblocks);
static int32_t  msc_eject_disk        (uint8_t lun);
static int32_t  msc_test_disk_ready   (uint8_t lun);
static int32_t  msc_xfer_blocks_done  (uint8_t lun);

/**
 * \brief Callback invoked when inquiry data command received
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static uint8_t *msc_inquiry_disk (uint8_t lun)
{
	if ( lun > 0 )
		return NULL;

	sect_left = 0;
	xfer_idle = true;
	xfer_read = false;
#if MSC_SUPPORT_WRITE
	xfer_write = false;
	sect_ready = false;
#endif

	return inquiry_info;
}

/**
 * \brief Callback invoked when read format capacities command received
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static uint8_t *msc_get_disk_capacity (uint8_t lun)
{
	ASSERT(0 == lun);

	return capacity_bytes;
}

/**
 * \brief Callback invoked when mode sense(6) command received
 * \param[in] lun logic unit number
 * \return ERR_NONE if write is allowed, anything else if read-only
 */
static int32_t  msc_check_write_ok (uint8_t lun)
{
#if MSC_SUPPORT_WRITE
	return read_only ? ERR_DENIED : ERR_NONE;
#else
	return ERR_DENIED;
#endif
}


/**
 * \brief Callback invoked when a new read blocks command received
 * \param[in] lun logic unit number
 * \param[in] addr start address of disk to be read
 * \param[in] nblocks block amount to be read
 * \return Operation status.
 */
static int32_t msc_start_read_disk (uint8_t lun, uint32_t addr, uint32_t nblocks)
{
	ASSERT(0 == lun);

	int32_t  rc  = msc_test_disk_ready(lun);
	if (rc != ERR_NONE)
	{
		//trace_mesg("!ready");
		return rc;
	}

	if ( !xfer_idle )
	{
		//trace_mesg("busy");
	}

	if ( addr > capacity_hard || addr + nblocks > capacity_hard )
	{
		//trace_mesg("address");
		return ERR_BAD_ADDRESS;
	}

	if ( nblocks > 0xFFFF )
	{
		//trace_mesg("too big");
		return ERR_INVALID_ARG;
	}

	xfer_read = true;
	sect_addr = addr;
	sect_left = nblocks;
#if MSC_SUPPORT_WRITE
	sect_ready = false;
#endif

	return ERR_NONE;
}

/**
 * \brief Callback invoked when a new write blocks command received
 * \param[in] lun logic unit number
 * \param[in] addr start address of disk to be written
 * \param[in] nblocks block amount to be written
 * \return Operation status.
 */
static int32_t msc_start_write_disk (uint8_t lun, uint32_t addr, uint32_t nblocks)
{
#if MSC_SUPPORT_WRITE
	int32_t  rc  = msc_test_disk_ready(lun);
	if (rc != ERR_NONE)
	{
		//trace_mesg("!ready");
		return rc;
	}

	if ( !xfer_idle )
	{
		//trace_mesg("busy");
	}

	/* If this is the MBR, or read-only flag set, return denied */
	if ( read_only || !addr )
	{
		//trace_mesg("read_only");
		return ERR_DENIED;
	}

	if ( addr > capacity_hard || addr + nblocks > capacity_hard )
	{
		//trace_mesg("address");
		return ERR_BAD_ADDRESS;
	}

	if ( nblocks > 0xFFFF )
	{
		//trace_mesg("too big");
		return ERR_INVALID_ARG;
	}

	xfer_write = true;
	sect_addr = addr;
	sect_left = nblocks;
	sect_ready = false;

	return ERR_NONE;
#else
	//trace_mesg("!MSC_SUPPORT_WRITE");
	return ERR_DENIED;
#endif
}

/**
 * \brief Eject Disk
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static int32_t msc_eject_disk (uint8_t lun)
{
	ASSERT(0 == lun);

	return ERR_NONE;
}

/**
 * \brief Inquiry whether Disk is ready
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static int32_t msc_test_disk_ready (uint8_t lun)
{
	ASSERT(0 == lun);

	return usb_ready ? ERR_NONE : ERR_NOT_INITIALIZED;;
}

/**
 * \brief Callback invoked when a blocks transfer is done
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static int32_t msc_xfer_blocks_done (uint8_t lun)
{
	ASSERT(0 == lun);

	CRITICAL_SECTION_ENTER();
	/* read: sector has been transmitted, update addr/left */
	if ( xfer_read )
	{
		ASSERT(sect_left);
		sect_addr++;
		sect_left--;
		if ( !sect_left )
			xfer_read = false;
	}

	/* write: sector[] now contains something we can write */
#if MSC_SUPPORT_WRITE
	if ( xfer_write )
		sect_ready = true;
#endif

	/* transfer done either way */
	xfer_idle = true;
	CRITICAL_SECTION_LEAVE();

	return ERR_NONE;
}


void HAL_MSC_Monitor (void)
{
	dstatus_t  ds;
	int32_t    us = ERR_NONE;
	bool       idle;

	CRITICAL_SECTION_ENTER();
	idle = xfer_idle;
	CRITICAL_SECTION_LEAVE();

	if ( !mscdf_is_enabled() )
		return;
		
	/* read: if usb stack idle then try to read disk, censor MBR if necessary, then do
	 * transfer in critical section... if successful clear idle to wait for xfer-done IRQ
	 */
	if ( xfer_read && idle )
	{
		/* If this sector is past the end of the section we're showing the user, skip the
		 * actual read and return a sector of zeros, otherwise do an actual disk read */
		if ( sect_addr >= capacity_soft )
		{
			debug("usb-msc: read: sect %lu >= soft %lu, zero\r\n", sect_addr, capacity_soft);
			memset(sector, 0, sizeof(sector));
		}
		else if ( ERR_NONE != (ds = disk_read(FATFS_DISK_NUM, sector, sect_addr, 1)) )
		{
			error("usb-msc: read: %u\r\n", ds);
			return;
		}

		/* If this is the MBR, pass the read sector to parts first so it can enforce
		 * maximum visible partition. */
		if ( 0 == sect_addr )
			parts_censor_mbr(sector);

		CRITICAL_SECTION_ENTER();
		if ( ERR_NONE == (us = mscdf_xfer_blocks(true, sector, 1)) )
			xfer_idle = false;
		CRITICAL_SECTION_LEAVE();

		if ( ERR_BUSY == us )
			error("usb-msc: busy, retry\r\n");
		else if ( ERR_NONE != us )
			error("usb-msc: xfer: %ld\r\n", us);
	}

#if MSC_SUPPORT_WRITE
	if ( xfer_write && idle )
	{
		/* xfer-done IRQ set this, so buffer contains host data, write to disk */
		if ( sect_ready )
		{
			/* If this sector is within the section we're showing the user, do the actual
			 * write, otherwise silently discard it */
			if ( sect_addr >= capacity_soft )
				debug("usb-msc: write: sect %lu >= soft %lu, discard\r\n", sect_addr, capacity_soft);
			else if ( ERR_NONE != (ds = disk_write(FATFS_DISK_NUM, sector, sect_addr, 1)) )
				error("usb-msc: write: %u\r\n", ds);

			sect_addr++;
			sect_left--;
		}

		/* more sectors to receive, transfer next */
		if ( sect_left )
		{
			CRITICAL_SECTION_ENTER();
			if ( ERR_NONE == (us = mscdf_xfer_blocks(false, sector, 1)) )
				xfer_idle = false;
			CRITICAL_SECTION_LEAVE();

			if ( ERR_BUSY == us )
				error("usb-msc: busy, retry\r\n");
			else if ( ERR_NONE != us )
				error("usb-msc: xfer: %ld\r\n", us);

		}
		else /* transfer done, clean up stack */
		{
			mscdf_xfer_blocks(false, NULL, 0);
			xfer_write = false;
		}
	}
#endif
}


void HAL_MSC_Initialize (void)
{
	msc_inquiry_disk(0);

	/* usbdc_register_funcion inside */
	mscdf_init(0);

	mscdf_register_callback(MSCDF_CB_INQUIRY_DISK,      (FUNC_PTR)msc_inquiry_disk);
	mscdf_register_callback(MSCDF_CB_GET_DISK_CAPACITY, (FUNC_PTR)msc_get_disk_capacity);
	mscdf_register_callback(MSCDF_CB_CHECK_WRITE_OK,    (FUNC_PTR)msc_check_write_ok);
	mscdf_register_callback(MSCDF_CB_START_READ_DISK,   (FUNC_PTR)msc_start_read_disk);
	mscdf_register_callback(MSCDF_CB_START_WRITE_DISK,  (FUNC_PTR)msc_start_write_disk);
	mscdf_register_callback(MSCDF_CB_EJECT_DISK,        (FUNC_PTR)msc_eject_disk);
	mscdf_register_callback(MSCDF_CB_TEST_DISK_READY,   (FUNC_PTR)msc_test_disk_ready);
	mscdf_register_callback(MSCDF_CB_XFER_BLOCKS_DONE,  (FUNC_PTR)msc_xfer_blocks_done);
}

void HAL_MSC_Destroy (void)
{
	mscdf_deinit();
}

