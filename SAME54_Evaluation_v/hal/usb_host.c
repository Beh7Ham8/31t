#include <stdio.h>
#include <string.h>
#include <atmel_start.h>
#include "usb_host.h"

#define CDC_HOST_TX_SIZE    1024
#define CDC_HOST_RX_SIZE    1024

extern struct usb_h_desc USB_HOST_INSTANCE_inst;
/** USB Host Core driver instance */
static struct usbhc_driver USB_HOST_CORE_INSTANCE_inst;
/** USB Host CDC ACM function driver instance */
static struct cdchf_acm USB_HOST_CDC_ACM_inst;

/** Ctrl requests process buffer size */
#define CTRL_BUFFER_SIZE CONF_USBH_CTRL_BUF_SIZE
/** Ctrl requests process buffer for enumeration */
static uint32_t USB_HOST_CORE_INSTANCE_ctrl_buf[CTRL_BUFFER_SIZE / 4];

COMPILER_ALIGNED(4) static uint8_t tx_data[CDC_HOST_TX_SIZE];
COMPILER_ALIGNED(4) static uint8_t rx_data[CDC_HOST_RX_SIZE];
static volatile uint16_t rx_offset;
static uint16_t tx_offset, tx_length;
static bool usb_host_connected;

static void HAL_HOST_ReadComplete(struct cdchf_acm *cdc, uint32_t count)
{
    if (count) rx_offset = count;
}

static void HAL_HOST_WriteComplete(struct cdchf_acm *cdc, uint32_t count)
{
	tx_offset += count;
	if (tx_offset >= tx_length)
    {
    	tx_length = 0;
    	tx_offset = 0;
    	cdchf_acm_write_flush(cdc);
    }
    else
    {
    	cdchf_acm_write(cdc, &tx_data[tx_offset], tx_length - tx_offset);
	}
}

void HAL_HOST_Initialize(void)
{
    usb_host_connected = false;
    rx_offset = 0;
    tx_offset = 0;
    tx_length = 0;
    usbhc_init(&USB_HOST_CORE_INSTANCE_inst, &USB_HOST_INSTANCE_inst, (uint8_t *)USB_HOST_CORE_INSTANCE_ctrl_buf, CTRL_BUFFER_SIZE);
    cdchf_acm_init(&USB_HOST_CORE_INSTANCE_inst, &USB_HOST_CDC_ACM_inst);
    usbhc_start(&USB_HOST_CORE_INSTANCE_inst);
}

void HAL_HOST_Destroy(void)
{
    HAL_HOST_DeviceClose();
    usbhc_stop(&USB_HOST_CORE_INSTANCE_inst);
    cdchf_acm_deinit(&USB_HOST_CORE_INSTANCE_inst, &USB_HOST_CDC_ACM_inst);
    usbhc_deinit(&USB_HOST_CORE_INSTANCE_inst);
}

bool HAL_HOST_DeviceReady(void)
{
    static usb_cdc_line_coding_t lncoding = {115200, CDC_STOP_BITS_1, CDC_PAR_NONE, 8};
    if (cdchf_acm_is_enabled(&USB_HOST_CDC_ACM_inst))
    {
        if (usb_host_connected) return true;
	    cdchf_acm_register_callback(&USB_HOST_CDC_ACM_inst, CDCHF_ACM_READ_CB, (FUNC_PTR)HAL_HOST_ReadComplete);
	    cdchf_acm_register_callback(&USB_HOST_CDC_ACM_inst, CDCHF_ACM_WRITE_CB, (FUNC_PTR)HAL_HOST_WriteComplete);
        if (cdchf_acm_open(&USB_HOST_CDC_ACM_inst, &lncoding) == ERR_NONE)
        {
            printf("USB Host: Device Connected.\n");
            while (1)
            {
                if (cdchf_acm_is_error(&USB_HOST_CDC_ACM_inst)) break;
                if (!cdchf_acm_is_enabled(&USB_HOST_CDC_ACM_inst)) break;
                if (cdchf_acm_is_open(&USB_HOST_CDC_ACM_inst))
                {
                    printf("USB Host: Device Opened.\n");
                    rx_offset = 0;
                    usb_host_connected = true;
                    return true;
                }
            }
            cdchf_acm_close(&USB_HOST_CDC_ACM_inst);
             printf("USB Host: Device Open Failed.\n");
        }
    }
    HAL_HOST_DeviceClose();
    return false;
}

void HAL_HOST_DeviceClose(void)
{
    if (usb_host_connected)
    {
        cdchf_acm_close(&USB_HOST_CDC_ACM_inst);
        printf("USB Host: Device Disconnected.\n");
        usb_host_connected = false;
        rx_offset = 0;
        tx_offset = 0;
        tx_length = 0;
    }
}

bool HAL_HOST_Write(const uint8_t *buf, uint16_t size)
{
    if (size == 0 || size > sizeof(tx_data)) return false;
    if (HAL_HOST_DeviceReady() && !cdchf_acm_is_writing(&USB_HOST_CDC_ACM_inst))
    {
        memcpy(tx_data, buf, size);
        tx_offset = 0;
        tx_length = size;
        if (cdchf_acm_write(&USB_HOST_CDC_ACM_inst, tx_data, tx_length) == ERR_NONE) return true;
        tx_length = 0;
    }
    return false;
}

uint16_t USB_HOST_Read(uint8_t *buf, uint16_t size)
{
    uint16_t length = 0;
    if (HAL_HOST_DeviceReady())
    {
        length = rx_offset;
        if (length)
        {
            rx_offset = 0;
            if (length > size) length = size;
            memcpy(buf, rx_data, length);
        }
        if (!cdchf_acm_is_reading(&USB_HOST_CDC_ACM_inst))
        {
            if (cdchf_acm_read(&USB_HOST_CDC_ACM_inst, rx_data, sizeof(rx_data)) != ERR_NONE)
            {
                HAL_HOST_DeviceClose();
            }
        }
    }
    return length;
}
