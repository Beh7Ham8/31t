/*
 * sleep.h
 *
 * Created: 5/8/2020 2:18:57 PM
 *  Author: BehroozH
 */ 


#ifndef SLEEP_H_
#define SLEEP_H_
#include "lib\stateMachine.h"

/* Events specific to the protocol SM */
enum 
{
	evSLEEP_INIT = evMAX_COMMON_EVENTS,
	evSLEEP_ON,
	evSLEEP_OFF
};


enum
{
	SLEEP_INIT,
	SLEEP_ON,
	SLEEP_OFF,
	SLEEP_NUM_STATES
};

// Function prototypes

// ----------------------------------------------------

extern pSTATE_MACHINE pSleepFSM;

void InitSlepp(void);

// FSM Functions
void Sleep_Monitor(void);
void state_sleep_Init(int event, void *param);
void state_sleep_ON(int event, void *param);
void state_sleep_OFF(int event, void *param);

void prepareToSleep();
void prepareToWakeUp();
float getSleepFSMState();

void Init_Sleep(void);
void Go_Sleep(uint8_t mode);

#endif /* SLEEP_H_ */