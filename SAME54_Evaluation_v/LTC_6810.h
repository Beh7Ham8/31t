/*
 * LTC_6810.h
 *
 * Created: 7/15/2020 10:31:12 AM
 *  Author: BehroozH
 */ 


#ifndef LTC_6810_H_
#define LTC_6810_H_

#define MD_422HZ_1KHZ 0
#define MD_27KHZ_14KHZ 1
#define MD_7KHZ_3KHZ 2
#define MD_26HZ_2KHZ 3

#define ADC_OPT_ENABLED 1
#define ADC_OPT_DISABLED 0

#define CELL_CH_ALL 0
#define CELL_CH_1and7 1
#define CELL_CH_2and8 2
#define CELL_CH_3and9 3
#define CELL_CH_4and10 4
#define CELL_CH_5and11 5
#define CELL_CH_6and12 6

#define SELFTEST_1 1
#define SELFTEST_2 2

#define AUX_CH_ALL 0
#define AUX_CH_GPIO1 1
#define AUX_CH_GPIO2 2
#define AUX_CH_GPIO3 3
#define AUX_CH_GPIO4 4
#define AUX_CH_GPIO5 5
#define AUX_CH_VREF2 6

#define STAT_CH_ALL 0
#define STAT_CH_SOC 1
#define STAT_CH_ITEMP 2
#define STAT_CH_VREGA 3
#define STAT_CH_VREGD 4

#define REG_ALL 0
#define REG_1 1
#define REG_2 2
#define REG_3 3
#define REG_4 4
#define REG_5 5
#define REG_6 6

#define DCP_DISABLED 0
#define DCP_ENABLED 1

#define PULL_UP_CURRENT 1
#define PULL_DOWN_CURRENT 0

#define NUM_RX_BYT 8
#define CELL 1
#define AUX 2
#define STAT 3
#define CFGR 0
#define CFGRB 4
#define CS_PIN 10


typedef struct
{
	uint8_t tx_data[6];
	uint8_t rx_data[8];
	uint8_t rx_pec_match; //!< If a PEC error was detected during most recent read cmd
} ic_register;

typedef struct
{
	uint16_t c_codes[6]; //!< Cell Voltage Codes
	uint8_t pec_match[6]; //!< If a PEC error was detected during most recent read cmd
} cv;

void Init_LTC6810();
void Read_CellVoltages_6810();
#endif /* LTC_6810_H_ */