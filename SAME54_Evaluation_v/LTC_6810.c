/*
 * LTC_6810.c
 *
 * Created: 7/15/2020 10:30:36 AM
 *  Author: BehroozH
 */ 
#include <sys\_stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "driver_init.h"
#include "debugMon.h"
#include "LTC_6810.h"


ic_register config;
cv  cells;
struct io_descriptor *SPI_0_io;
uint16_t pec15Table[256];

const uint16_t CRC15_POLY = 0x4599;
const uint8_t ADC_CONVERSION_MODE = MD_7KHZ_3KHZ; //!< ADC Mode
const uint8_t ADC_DCP = DCP_DISABLED; //!< Discharge Permitted 
const uint8_t CELL_CH_TO_CONVERT = CELL_CH_ALL; //!< Channel Selection for ADC conversion
const uint8_t SEL_ALL_REG = REG_ALL; //!< Register Selection 
const uint8_t CMD_WR_CFGR = 0x01;
const uint8_t CMD_RD_CFGR = 0x02;
const uint8_t CMD_RD_CVA = 0x04;
const uint8_t CMD_RD_CVB = 0x06;
const uint16_t VUV_CONST = 1250;//20000 micro volt /16
const uint16_t VOV_CONST = 2373;//38000 micro volt /16

int8_t parse_cells(uint8_t cell_reg,uint8_t cell_data[],uint16_t *cell_codes,uint8_t *ic_pec);
void LTC681x_wr_cfgr_reg(bool refon,bool adcopt);
void LTC6810_rd_cfgr_reg(uint8_t *data);
void LTC6810_adcv( uint8_t MD, uint8_t DCP, uint8_t CH);
uint8_t LTC6810_rdcv();
uint32_t LTC6810_pollAdc();

static void _enable_chip_select();
static void _disable_chip_select();
static uint16_t _pec15_calc(uint8_t len, uint8_t *data);
static int8_t _read_68(uint8_t tx_cmd[2],uint8_t *rx_data);
static void _write_68(uint8_t tx_cmd[2],uint8_t data[]);
static void _init_PEC15_Table();
static void _wakeup_sleep();

void Init_LTC6810()
{
	 uint8_t rdbak_data[8];
	 
	 memset(rdbak_data,0x00,8);
	 
	spi_m_sync_get_io_descriptor(&SPI_0, &SPI_0_io);
	spi_m_sync_set_mode(&SPI_0, SPI_MODE_3);
	spi_m_sync_set_char_size(&SPI_0, SPI_CHAR_SIZE_8);
	spi_m_sync_set_data_order(&SPI_0, SPI_DATA_ORDER_MSB_1ST);
	spi_m_sync_enable(&SPI_0);
	
	_init_PEC15_Table();
	
	LTC681x_wr_cfgr_reg(true,false);
	 
	LTC6810_rd_cfgr_reg(rdbak_data);
	   
}

void Read_CellVoltages_6810()
{
	_wakeup_sleep();
	
	LTC6810_adcv(MD_7KHZ_3KHZ,DCP_DISABLED, CELL_CH_ALL);
	
	//LTC681x_pollAdc();
	
	LTC6810_rdcv();
}

void LTC681x_wr_cfgr_reg(bool refon,bool adcopt)
{
	 uint16_t tmp;
	 uint8_t cmd[2] = {0x00 , CMD_WR_CFGR} ;
	 
	 if (refon)
	 { 
		 config.tx_data[0] = config.tx_data[0]|0x04;
	 }
	 else 
	 {
		 config.tx_data[0] = config.tx_data[0]&0xFB;
	 }
	 
	 if (adcopt) 
	 {
		 config.tx_data[0] = config.tx_data[0]|0x01;
	 }
	 else 
	 {
		 config.tx_data[0] = config.tx_data[0]&0xFE;
	 }
	 
	 //config.tx_data[0] = (config.tx_data[0] | (0x0F << 3)); 
	  config.tx_data[0] = (config.tx_data[0] & ( 0x00 << 3));
	  
	  tmp = (VUV_CONST/16)-1;
	  config.tx_data[1] = 0x00FF & tmp;
	  config.tx_data[2] = config.tx_data[2]&0xF0;
	  config.tx_data[2] = config.tx_data[2]|((0x0F00 & tmp)>>8);
	  
	  tmp = (VOV_CONST/16);
	  config.tx_data[3] = 0x00FF & (tmp>>4);
	  config.tx_data[2] = config.tx_data[2]&0x0F;
	  config.tx_data[2] = config.tx_data[2]|((0x000F & tmp)<<4);
	   
	  _write_68(cmd, config.tx_data);
	  	
}

void LTC6810_rd_cfgr_reg(uint8_t *data)
{
	uint8_t cmd[2] = {0x00 , CMD_RD_CFGR} ;

	_read_68(cmd, data);
}

// MD,  ADC Mode
// DCP, Discharge Permit
// CH,  Cell Channels to be measured
void LTC6810_adcv( uint8_t MD, uint8_t DCP, uint8_t CH )
{
	uint8_t cmd[4];
	uint8_t md_bits;
	uint16_t cmd_pec;
	uint32_t cnt1 = 0;
	
	md_bits = (MD & 0x02) >> 1;
	cmd[0] = md_bits + 0x02;
	md_bits = (MD & 0x01) << 7;
	cmd[1] =  md_bits + 0x60 + (DCP<<4) + CH;
	cmd_pec = _pec15_calc(2, cmd);
	cmd[2] = (uint8_t)(cmd_pec >> 8);
	cmd[3] = (uint8_t)(cmd_pec);
	
	_enable_chip_select();
	cnt1 = io_write(SPI_0_io,cmd,4);
	_disable_chip_select();	
		
		
}

void LTC6810_rdcv_reg(uint8_t reg, uint8_t *data)
{
	uint8_t cmd[4];
	uint16_t cmd_pec;
	uint32_t cnt1 = 0,cnt2 = 0;

    cmd[0] = 0x00;
	
	if (reg == 1)     //1: RDCVA
	{
		cmd[1] = CMD_RD_CVA;		
	}
	else if (reg == 2) //2: RDCVB
	{
		cmd[1] = CMD_RD_CVB;
	}
	
	cmd_pec = _pec15_calc(2, cmd);
	cmd[2] = (uint8_t)(cmd_pec >> 8);
	cmd[3] = (uint8_t)(cmd_pec);
	
	_enable_chip_select();
	cnt1 = io_write(SPI_0_io,cmd,4);
	cnt2 = io_read(SPI_0_io,data,8);
	_disable_chip_select();
}

uint8_t LTC6810_rdcv()
{
	int8_t pec_error = 0;
	uint8_t cell_data[NUM_RX_BYT];
	float fc1,fc2,fc3,fc4,fc5,fc6;
	
	memset(cell_data,0x00,NUM_RX_BYT);

	// only read 1: RDCVA - 2: RDCVB
	for (uint8_t cell_reg = 1; cell_reg < 3; cell_reg++) //executes once for each of the LTC6811 cell voltage registers
	{
		LTC6810_rdcv_reg(cell_reg,cell_data);
		
		pec_error = pec_error + parse_cells(cell_reg, cell_data,cells.c_codes,cells.pec_match);
	}
		
	if(pec_error == 0)	
	{
		fc1 = cells.c_codes[0]*0.0001; // LSB is 100 micro volt = 0.0001 volt
		fc2 = cells.c_codes[1]*0.0001;
		fc3 = cells.c_codes[2]*0.0001;
		fc4 = cells.c_codes[3]*0.0001;
		fc5 = cells.c_codes[4]*0.0001;
		fc6 = cells.c_codes[5]*0.0001;
			
		debug_print(DBG_VOLTAGE_BALANCER,  " 6810__FSM |   C1 = %2.3f   C2 = %2.3f   C3 = %2.3f   C4 = %2.3f   C5 = %2.3f   C6 = %2.3f ",fc1,fc2, fc3,fc4,fc5,fc6);
	}
	else
	{
		debug_print(DBG_VOLTAGE_BALANCER,  " 6810__FSM |   %s","CRC error occurred when reading from LTC6810 chip. ");
	}
				
	return(pec_error);
}

int8_t parse_cells(uint8_t cell_reg,uint8_t cell_data[],uint16_t *cell_codes,uint8_t *ic_pec)
{
	const uint8_t BYT_IN_REG = 6;
	const uint8_t CELL_IN_REG = 3;
	int8_t pec_error = 0;
	uint16_t parsed_cell,received_pec,data_pec;
	uint8_t data_counter = 0,index =0;

   // This loop parses the read back data into cell voltages, it loops once for each of the 3 cell voltage codes in the register
	for (uint8_t current_cell = 0; current_cell<CELL_IN_REG; current_cell++)  
	{																			
		//Each cell code is received as two bytes and is combined to
		parsed_cell = cell_data[data_counter] + (cell_data[data_counter + 1] << 8);
		
		index = current_cell  + ((cell_reg - 1) * CELL_IN_REG);
		
		cell_codes[index] = parsed_cell;
		
		//Because cell voltage codes are two bytes the data counter must increment by two for each parsed cell code
		data_counter = data_counter + 2;                       
	}

	received_pec = (cell_data[data_counter] << 8) | cell_data[data_counter+1]; //The received PEC for the current_ic is transmitted as the 7th and 8th
	
	data_pec = _pec15_calc(BYT_IN_REG, &cell_data[0]);

	if (received_pec != data_pec)
	{
		pec_error = 1;                             //The pec_error variable is simply set negative if any PEC errors
		ic_pec[cell_reg-1]=1;
	}
	else
	{
		ic_pec[cell_reg-1]=0;
	}
	
	return(pec_error);
}

uint32_t LTC6810_pollAdc()
{
  uint32_t counter = 0;
  uint8_t finished = 0;
  bool SDO_status = 0;
  uint8_t cmd[4];
  uint16_t cmd_pec;

  cmd[0] = 0x07;//PLDAC
  cmd[1] = 0x14;
  cmd_pec = _pec15_calc(2, cmd);
  cmd[2] = (uint8_t)(cmd_pec >> 8);
  cmd[3] = (uint8_t)(cmd_pec);

 _enable_chip_select();
  
  io_write(SPI_0_io,cmd,4);

  while ((counter<20000000) && (finished == 0))
  {
	
    SDO_status =  gpio_get_pin_level(PB29);
	
    if (SDO_status  == true)
    {
      finished = 1;
    }
    else
    {
      counter = counter + 10;
    }	
  }

  _disable_chip_select();

  return(counter);
}

static void _write_68(uint8_t tx_cmd[2],uint8_t data[])
{
	const uint8_t BYTES_IN_REG = 6;
	const uint8_t CMD_LEN = (4+8);
	uint8_t cmd[12];
	uint32_t count = 0;
	
	uint16_t data_pec;
	uint16_t cmd_pec;
	uint8_t cmd_index;
	
	cmd[0] = tx_cmd[0];
	cmd[1] = tx_cmd[1];
	cmd_pec = _pec15_calc(2, cmd);
	cmd[2] = (uint8_t)(cmd_pec >> 8);
	cmd[3] = (uint8_t)(cmd_pec);
	cmd_index = 4;
	
	for (uint8_t current_byte = 0; current_byte < BYTES_IN_REG; current_byte++)
	{
		cmd[cmd_index] = data[current_byte];
		cmd_index = cmd_index + 1;
	}
	data_pec = (uint16_t)_pec15_calc(BYTES_IN_REG, &data[0]);  
	cmd[cmd_index] = (uint8_t)(data_pec >> 8);
	cmd[cmd_index + 1] = (uint8_t)data_pec;
	
	_enable_chip_select();
	count = io_write(SPI_0_io,cmd,CMD_LEN);
	_disable_chip_select();
	
}

static int8_t _read_68(uint8_t tx_cmd[2],uint8_t *rx_data)
{
	const uint8_t BYTES_IN_REG = 8;
	uint8_t cmd[4];
	uint8_t data[256];
	int8_t pec_error = 0;
	uint32_t count1 = 0,count2 = 0;
	uint16_t cmd_pec,data_pec,received_pec;
	
	memset(data,0x00,256);
	
	cmd[0] = tx_cmd[0];
	cmd[1] = tx_cmd[1];
	cmd_pec = _pec15_calc(2, cmd);
	cmd[2] = (uint8_t)(cmd_pec >> 8);
	cmd[3] = (uint8_t)(cmd_pec);
	
	_enable_chip_select();
	count1 = io_write(SPI_0_io,cmd,4);
	count2 = io_read(SPI_0_io,data,BYTES_IN_REG);
	_disable_chip_select();
	
	for (uint8_t current_byte = 0; current_byte < BYTES_IN_REG; current_byte++)
	{
		rx_data[current_byte] = data[current_byte];
	}
	
	received_pec = (rx_data[6]<<8) + rx_data[7];
	data_pec = _pec15_calc(6, &rx_data[0]);
	
	if (received_pec != data_pec)
	{
		pec_error = -1;
	}
	
	return(pec_error);
}

static uint16_t _pec15_calc(uint8_t len, uint8_t *data)
{
	uint16_t remainder,addr;
	remainder = 16;//initialize the PEC
	
	for (uint8_t i = 0; i<len; i++) // loops for each byte in data array
	{
		addr = ((remainder>>7)^data[i])&0xff;//calculate PEC table address
		
		remainder = (remainder<<8)^pec15Table[addr];
	}
	return(remainder*2);//The CRC15 has a 0 in the LSB so the remainder must be multiplied by 2
}

static void _enable_chip_select()
{
	gpio_set_pin_level(CSM,false);
}

static void _disable_chip_select()
{
	gpio_set_pin_level(CSM,true);
}

/* Generic wakeup command to wake the LTC681x from sleep state */
static void _wakeup_sleep()
{
	_enable_chip_select();
	//delay_us(300); // Guarantees the LTC681x will be in standby
	timerDelay (1);
	_disable_chip_select();
	//delay_us(10);
	timerDelay (1);
}

static void _init_PEC15_Table()
{
	uint16_t remainder =0;
	
	for (int i = 0; i < 256; i++)
	{  remainder = i << 7;
		for (int bit = 8; bit > 0; --bit)
		{   if (remainder & 0x4000)
			{
				remainder = ((remainder << 1));
				remainder = (remainder ^ CRC15_POLY);
			}
			else
			{
				remainder = ((remainder << 1));
			}
			
		}
		pec15Table[i] = remainder&0xFFFF;
	}
}