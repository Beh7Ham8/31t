/*
 * adc.c
 *
 * Created: 5/8/2020 10:35:14 PM
 *  Author: BehroozH
 */ 
#include <sys\_stdint.h>
#include <stdbool.h>
#include <string.h>
#include <atmel_start.h>
#include <hpl_user_area.h>
#include "driver_init.h"
#include "adc.h"
#include "drivers.h"

//0x0 INTREF internal bandgap reference, refer to the SUPC.VREF.SEL register for more details
//x01 Reserved
//0x2 INTVCC0 1/2 VDDANA (only for VDDANA > 2.0v)
//0x3 INTVCC1 VDDANA
//0x4 AREFA External reference
//0x5 AREFB External reference
//0x6 AREFC External reference (ADC1 only)
//other - Reserved

//adc_sync_set_reference(&ADC_0,0x03);
//adc_sync_set_reference(&ADC_1,0x03);


uint16_t Read_Adc1(uint8_t ch)
{
	uint8_t buffer[2];
	uint16_t value,retVal;
	uint16_t sum =0,i=0;
	
	adc_sync_enable_channel(&ADC_1, ch);
	
	for(i = 0;i<4;i++)
	{
		adc_sync_read_channel(&ADC_1, ch, buffer, 2);
		value = (buffer[1]<<8) + buffer[0];
		sum += value;
	}
	
	adc_sync_disable_channel(&ADC_1, ch);
	
	retVal = sum>>2;
	
	return (retVal);
}

uint16_t Read_Adc0(uint8_t ch)
{
	uint8_t buffer[2];
	uint16_t value,retVal;
	uint16_t sum =0,i=0;
	
	adc_sync_enable_channel(&ADC_0, ch);
	
	for(i = 0;i<4;i++)
	{
		adc_sync_read_channel(&ADC_0, ch, buffer, 2);
		value = (buffer[1]<<8) + buffer[0];
		sum += value;
	}
	
	adc_sync_disable_channel(&ADC_0, ch);
	
	retVal = sum>>2;
	
	return (retVal);
}

void initAnalogVal(pANALOG_VALUE pVal)
{
    pVal->headPtr = 0;
    pVal->tailPtr = 0;
    pVal->average = 0;
    pVal->previousAverage = 0;
    
	memset(pVal->history, 0, MAX_ADC_READINGS * sizeof (uint32_t));
}

bool isBufferFilled(pANALOG_VALUE pVal)
{
    return (pVal->headPtr >= (MAX_ADC_READINGS - 1));
}

void updateAnalogAverage(pANALOG_VALUE pVal) 
{
    uint8_t i;

    // Add latest instantaneous value to the history of measurements
    pVal->history[(pVal->headPtr++) % MAX_ADC_READINGS] = pVal->instantaneous;

    pVal->average = 0;

    for (i = 0; i < MAX_ADC_READINGS; i++)
        pVal->average += pVal->history[i];

    pVal->average >>= 4;
}

/*
Word 0: 0x008061FC
Word 1: 0x00806010
Word 2: 0x00806014
Word 3: 0x00806018
*/

#define ADC_CALBIBRATION_FLASH_ADDR		((const void *)0x00800080)

uint8_t ADC0_BIAS_COMP = 0x07;
uint8_t ADC0_BIAS_REFBUF = 0x07;
uint8_t ADC0_BIAS_R2R = 0x07;

uint8_t ADC1_BIAS_COMP = 0x07;
uint8_t ADC1_BIAS_REFBUF = 0x07;
uint8_t ADC1_BIAS_R2R = 0x07;

void Calibrate_ADCs()
{
	static uint8_t data[4];
	
	_user_area_read(ADC_CALBIBRATION_FLASH_ADDR, 0x00, data, 4);
	
	ADC0_BIAS_COMP = (data[0]>>2) & 0x07;
	ADC0_BIAS_REFBUF = (data[0]>>5) & 0x07;
	ADC0_BIAS_R2R = (data[1]) & 0x07;
	
	ADC1_BIAS_COMP = (data[2]>>2) & 0x07;
	ADC1_BIAS_REFBUF = (data[2]>>5) & 0x07;
	ADC1_BIAS_R2R = (data[3]) & 0x07;
	
	hri_adc_set_CALIB_BIASR2R_bf(ADC0,ADC0_BIAS_R2R);
	hri_adc_set_CALIB_BIASCOMP_bf(ADC0,ADC0_BIAS_COMP);
	hri_adc_set_CALIB_BIASREFBUF_bf(ADC0,ADC0_BIAS_REFBUF);
	
	hri_adc_set_CALIB_BIASR2R_bf(ADC1,ADC1_BIAS_R2R);
	hri_adc_set_CALIB_BIASCOMP_bf(ADC1,ADC1_BIAS_COMP);
	hri_adc_set_CALIB_BIASREFBUF_bf(ADC1,ADC1_BIAS_REFBUF);
	
	
	return;
	
}
