/*
 * bluenrg_data.c
 *
 * Created: 10/16/2020 11:14:28 AM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include <string.h>
#include "lib/systick.h"
#include "lib/packet.h"
#include "bluenrg_app.h"
#include "bluenrg_monitor.h"

static uint64_t link_time;
static const uint8_t sn[16] = "123456789ABCDEF";
static uint32_t GetUInt(uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4)
{
    uint32_t data;
    data = (uint32_t)d1;
    data |= ((uint32_t)d2) << 8;
    data |= ((uint32_t)d3) << 16;
    data |= ((uint32_t)d4) << 24;
    return data;
}
static uint32_t GetBleMacAddr1(void)
{
    uint8_t addr[6];
    BlueNRG_GetMACAddr(addr);
    return GetUInt(addr[0], addr[1], addr[2], addr[3]);
}
static uint32_t GetBleMacAddr2(void)
{
    uint8_t addr[6];
    BlueNRG_GetMACAddr(addr);
    return GetUInt(addr[4], addr[5], 0, 0);
}
static uint32_t getSystemSerialNumber1(void)
{
    return GetUInt(sn[0], sn[1], sn[2], sn[3]);
}
static uint32_t getSystemSerialNumber2(void)
{
    return GetUInt(sn[4], sn[5], sn[6], sn[7]);
}
static uint32_t getSystemSerialNumber3(void)
{
    return GetUInt(sn[8], sn[9], sn[10], sn[11]);
}
static uint32_t getSystemSerialNumber4(void)
{
    return GetUInt(sn[12], sn[13], sn[14], sn[15]);
}
static uint32_t getNumBatteryNodes(void) { return 1; }
static uint32_t getAppVersion(void) { return 0x004C0801; }
static uint32_t getSystemPartNumber1(void) { return 0x2D303130; }
static uint32_t getSystemPartNumber2(void) { return 0x36303130; }
static uint32_t getSystemPartNumber3(void) { return 0x30302D30; }
static uint32_t getSystemPartNumber4(void) { return 0x95101100; }
static uint32_t getOneLastStateOnFSMState(void) { return 0; }
static uint32_t getHeaterFSMState(void) { return 2; }
static uint32_t getContactorFSMSState(void) { return 0; }
static uint32_t getSafemodeFSMSState(void) { return 0; }
static uint32_t getIgnitionFSMState(void) { return 3; }
static uint32_t getRotationModeData(void) { return 00002100; }
static uint32_t getDeepCycleMode(void) { return 0; }
static float getCMON_Value(void) { return 0.565; }
static float getBankVoltage(void) { return 11.5; }
static float getBusMonVoltage(void) { return 12.6; }
static float getCellTemperature(void) { return 3.077; }
static uint32_t getSOC_Value(void) { return (uint32_t)0.921f; }
static uint32_t getSOF_total_Value(void) { return (uint32_t)0.916f; }
static float getCMon_BattNode1(void) { return 0.0; }
static float getBankV_BattNode1(void) { return 0.0; }
static float getCellTemp_BattNode1(void) { return 0.0; }
static float getSOC_BattNode1(void) { return 0.0; }
static float getCMon_BattNode2(void) { return 0.0; }
static float getBankV_BattNode2(void) { return 0.0; }
static float getCellTemp_BattNode2(void) { return 0.0; }
static float getSOC_BattNode2(void) { return 0.0; }
static float getCMon_BattNode3(void) { return 0.0; }
static float getBankV_BattNode3(void) { return 0.0; }
static float getCellTemp_BattNode3(void) { return 0.0; }
static float getSOC_BattNode3(void) { return 0.0; }

static float getC_ICHARGE_Value(void) { return 0.0; }
static float getFCV1(void) { return 0.0; }
static float getFCV2(void) { return 0.0; }
static float getFCV3(void) { return 0.0; }
static float getFCV4(void) { return 0.0; }
static float getBodyTemperature(void) { return 0.0; }
static uint32_t getSOH_Value(void) { return 0; }
static uint32_t getSOF_Value(void) { return 0; }
static uint32_t getBootVersion(void) { return 0; }
static uint32_t GetBaudRate(void) { return 0; }
static uint32_t getChargerFSMState(void) { return 0; }
static uint32_t getSleepFSMState(void) { return 0; }
static uint32_t getBalancerFSMState(void) { return 0; }
static uint32_t getAccidentDetectionMode(void) { return 0; }
static uint32_t getBaseSerialNumber1(void) { return 0; }
static uint32_t getBaseSerialNumber2(void) { return 0; }
static uint32_t getBaseSerialNumber3(void) { return 0; }
static uint32_t getBaseSerialNumber4(void) { return 0; }
static uint32_t getBasePartNumber1(void) { return 0; }
static uint32_t getBasePartNumber2(void) { return 0; }
static uint32_t getBasePartNumber3(void) { return 0; }
static uint32_t getBasePartNumber4(void) { return 0; }
static uint32_t getNumCells(void) { return 0; }
static uint32_t getCellSerialNumber1(void) { return 0; }
static uint32_t getCellSerialNumber2(void) { return 0; }
static uint32_t getCellSerialNumber3(void) { return 0; }
static uint32_t getCellSerialNumber4(void) { return 0; }
static uint32_t getCellSerialNumber5(void) { return 0; }
static uint32_t getCellSerialNumber6(void) { return 0; }
static uint32_t getCellSerialNumber7(void) { return 0; }
static uint32_t getCellSerialNumber8(void) { return 0; }
static uint32_t getBirthDate1(void) { return 0; }
static uint32_t getBirthDate2(void) { return 0; }
static uint32_t getSunriseDate1(void) { return 0; }
static uint32_t getSunriseDate2(void) { return 0; }
static uint32_t getSunsetDate1(void) { return 0; }
static uint32_t getSunsetDate2(void) { return 0; }
static float getCMonCal(void) { return 0.0; }
static float getInitialSoc(void) { return 0.0; }
static uint32_t getBlePowerLevel(void) { return 0; }
static uint32_t getContactorSerialNumber1(void) { return 0; }
static uint32_t getContactorSerialNumber2(void) { return 0; }
static uint32_t getContactorSerialNumber3(void) { return 0; }
static uint32_t getContactorSerialNumber4(void) { return 0; }
static uint32_t getPcbSerialNumber1(void) { return 0; }
static uint32_t getPcbSerialNumber2(void) { return 0; }
static uint32_t getPcbSerialNumber3(void) { return 0; }
static uint32_t getPcbSerialNumber4(void) { return 0; }

volatile HOST_COMMAND_TYPE mobileCmds[MAX_HOST_CMD_IDS] = {
    // Central specific data needed by app
    {HOST_COMMANDS,     SET_BLE_MAC_ADDR,           INTINDEXED, 0, (pParamFunc)GetBleMacAddr1},                          // cmd 0
    {HOST_COMMANDS,     SET_BLE_MAC_ADDR,           INTINDEXED, 1, (pParamFunc)GetBleMacAddr2},
    {HOST_COMMANDS,     SET_NUM_BATT_NODES,         INTEGERX,   0, (pParamFunc)getNumBatteryNodes},
    {HOST_COMMANDS,     SET_APP_VER,                INTEGERX,   0, (pParamFunc)getAppVersion},

    // Personalization data needed by app to determine system password and system SOC
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 0, (pParamFunc)getSystemSerialNumber1},           // cmd 4
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 1, (pParamFunc)getSystemSerialNumber2},
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 2, (pParamFunc)getSystemSerialNumber3},
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 3, (pParamFunc)getSystemSerialNumber4},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 0, (pParamFunc)getSystemPartNumber1},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 1, (pParamFunc)getSystemPartNumber2},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 2, (pParamFunc)getSystemPartNumber3},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 3, (pParamFunc)getSystemPartNumber4},

    // System states
    {DEBUG_COMMANDS,    ONE_LAST_START,             INTEGERX,   0, (pParamFunc)getOneLastStateOnFSMState},                           // cmd 12
    {DEBUG_COMMANDS,    SET_HEATER_STATE,           INTEGERX,   0, (pParamFunc)getHeaterFSMState},
    {DEBUG_COMMANDS,    SET_CONTACTOR_STATE,        INTEGERX,   0, (pParamFunc)getContactorFSMSState},
    {DEBUG_COMMANDS,    SET_SAFEMODE_STATE,         INTEGERX,   0, (pParamFunc)getSafemodeFSMSState},
    {DEBUG_COMMANDS,    SET_IGNITION_STATE,         INTEGERX,   0, (pParamFunc)getIgnitionFSMState},
    {DEBUG_COMMANDS,    GET_REPROGRAM_MODE,         INTEGERX,   0, (pParamFunc)getRotationModeData},
    {DEBUG_COMMANDS,    SET_DEEP_CYCLE_MODE,        BOOLEAN,    0, (pParamFunc)getDeepCycleMode},
    // Module 1 data
    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  0, getCMON_Value},                                       // cmd 17
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  0, getBankVoltage},
    {HOST_COMMANDS,     SET_BUS_MON,                FLOATINGP,  0, getBusMonVoltage},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  0, getCellTemperature}, 
    {HOST_COMMANDS,     SET_SOC_VAL,                INTEGERX,   0, (pParamFunc)getSOC_Value},
    {HOST_COMMANDS,     SET_SOF_VAL,                INTEGERX,   0, (pParamFunc)getSOF_total_Value},

    // Module 2 data
    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  1, (pParamFunc)getCMon_BattNode1},                       // cmd 22
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  1, (pParamFunc)getBankV_BattNode1},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  1, (pParamFunc)getCellTemp_BattNode1},
    {HOST_COMMANDS,     SET_SOC_VAL,                FLOATINGP,  1, (pParamFunc)getSOC_BattNode1},

    // Module 3 data
    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  2, (pParamFunc)getCMon_BattNode2},                       // cmd 26
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  2, (pParamFunc)getBankV_BattNode2},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  2, (pParamFunc)getCellTemp_BattNode2},
    {HOST_COMMANDS,     SET_SOC_VAL,                FLOATINGP,  2, (pParamFunc)getSOC_BattNode2},

    // Module 4 data
    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  2, (pParamFunc)getCMon_BattNode3},                       // cmd 30
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  2, (pParamFunc)getBankV_BattNode3},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  2, (pParamFunc)getCellTemp_BattNode3},
    {HOST_COMMANDS,     SET_SOC_VAL,                FLOATINGP,  2, (pParamFunc)getSOC_BattNode3}
};

volatile HOST_COMMAND_TYPE mobileDiagCmds[MAX_HOST_DIAG_CMD_IDS] = {
    {HOST_COMMANDS,     SET_BLE_MAC_ADDR,           INTINDEXED, 0, (pParamFunc)GetBleMacAddr1},
    {HOST_COMMANDS,     SET_BLE_MAC_ADDR,           INTINDEXED, 1, (pParamFunc)GetBleMacAddr2},
    {HOST_COMMANDS,     SET_NUM_BATT_NODES,         INTEGERX,   0, (pParamFunc)getNumBatteryNodes},

    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 0, (pParamFunc)getSystemSerialNumber1},
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 1, (pParamFunc)getSystemSerialNumber2},
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 2, (pParamFunc)getSystemSerialNumber3},
    {SERVICE_COMMANDS,  SYSTEM_SERIAL_NUMBER,       INTINDEXED, 3, (pParamFunc)getSystemSerialNumber4},

    {HOST_COMMANDS,     SET_I_CHARGE,               FLOATINGP,  0, getC_ICHARGE_Value},
    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  0, getCMON_Value},
    {HOST_COMMANDS,     SET_BUS_MON,                FLOATINGP,  0, getBusMonVoltage},
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  0, getBankVoltage},
    {HOST_COMMANDS,     SET_FUEL_CELL_V,            FLOATINGP,  0, getFCV1},
    {HOST_COMMANDS,     SET_FUEL_CELL_V,            FLOATINGP,  1, getFCV2},
    {HOST_COMMANDS,     SET_FUEL_CELL_V,            FLOATINGP,  2, getFCV3},
    {HOST_COMMANDS,     SET_FUEL_CELL_V,            FLOATINGP,  3, getFCV4},
    {HOST_COMMANDS,     SET_BODY_TEMPERATURE,       FLOATINGP,  0, getBodyTemperature},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  0, getCellTemperature},  // cmd = 09
    {HOST_COMMANDS,     SET_SOC_VAL,                INTEGERX,   0, (pParamFunc)getSOC_Value},
    {HOST_COMMANDS,     SET_SOH_VAL,                INTEGERX,   0, (pParamFunc)getSOH_Value},
    {HOST_COMMANDS,     SET_SOF_VAL,                INTEGERX,   0, (pParamFunc)getSOF_Value},
    {HOST_COMMANDS,     SET_APP_VER,                INTEGERX,   0, (pParamFunc)getAppVersion},
    {HOST_COMMANDS,     SET_BOOT_VER,               INTEGERX,   0, (pParamFunc)getBootVersion},
    {HOST_COMMANDS,     SET_BAUD_RATE,              INTEGERX,   0, (pParamFunc)GetBaudRate},
    {DEBUG_COMMANDS,    SET_CHARGE_STATE,           INTEGERX,   0, (pParamFunc)getChargerFSMState},
    {DEBUG_COMMANDS,    SET_IGNITION_STATE,         INTEGERX,   0, (pParamFunc)getIgnitionFSMState},
    {DEBUG_COMMANDS,    SET_HEATER_STATE,           INTEGERX,   0, (pParamFunc)getHeaterFSMState},
    {DEBUG_COMMANDS,    SET_CONTACTOR_STATE,        INTEGERX,   0, (pParamFunc)getContactorFSMSState},  // cmd = 19
    {DEBUG_COMMANDS,    SET_SLEEP_STATE,            INTEGERX,   0, (pParamFunc)getSleepFSMState},
    {DEBUG_COMMANDS,    SET_BALANCER_STATE,         INTEGERX,   0, (pParamFunc)getBalancerFSMState},
    {DEBUG_COMMANDS,    SET_SAFEMODE_STATE,         INTEGERX,   0, (pParamFunc)getSafemodeFSMSState},
    {DEBUG_COMMANDS,    ONE_LAST_START,             INTEGERX,   0, (pParamFunc)getOneLastStateOnFSMState},
    {DEBUG_COMMANDS,    GET_REPROGRAM_MODE,         INTEGERX,   0, (pParamFunc)getRotationModeData},
    {DEBUG_COMMANDS,    ENABLE_ACCIDENT_DETECTION,  INTEGERX,   0, (pParamFunc)getAccidentDetectionMode},
    {DEBUG_COMMANDS,    SET_DEEP_CYCLE_MODE,        BOOLEAN,    0, (pParamFunc)getDeepCycleMode},

    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  1, (pParamFunc)getCMon_BattNode1},
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  1, (pParamFunc)getBankV_BattNode1},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  1, (pParamFunc)getCellTemp_BattNode1},
    {HOST_COMMANDS,     SET_SOC_VAL,                FLOATINGP,  1, (pParamFunc)getSOC_BattNode1},

    {HOST_COMMANDS,     SET_C_MON,                  FLOATINGP,  2, (pParamFunc)getCMon_BattNode2},
    {HOST_COMMANDS,     SET_BANK_V,                 FLOATINGP,  2, (pParamFunc)getBankV_BattNode2},
    {HOST_COMMANDS,     SET_CELL_TEMPERATURE,       FLOATINGP,  2, (pParamFunc)getCellTemp_BattNode2},
    {HOST_COMMANDS,     SET_SOC_VAL,                FLOATINGP,  2, (pParamFunc)getSOC_BattNode2},

    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 0, (pParamFunc)getSystemPartNumber1},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 1, (pParamFunc)getSystemPartNumber2},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 2, (pParamFunc)getSystemPartNumber3},
    {SERVICE_COMMANDS,  SYSTEM_PART_NUMBER,         INTINDEXED, 3, (pParamFunc)getSystemPartNumber4},
    {SERVICE_COMMANDS,  BASE_SERIAL_NUMBER,         INTINDEXED, 0, (pParamFunc)getBaseSerialNumber1},
    {SERVICE_COMMANDS,  BASE_SERIAL_NUMBER,         INTINDEXED, 1, (pParamFunc)getBaseSerialNumber2},
    {SERVICE_COMMANDS,  BASE_SERIAL_NUMBER,         INTINDEXED, 2, (pParamFunc)getBaseSerialNumber3},
    {SERVICE_COMMANDS,  BASE_SERIAL_NUMBER,         INTINDEXED, 3, (pParamFunc)getBaseSerialNumber4},
    {SERVICE_COMMANDS,  BASE_PART_NUMBER,           INTINDEXED, 0, (pParamFunc)getBasePartNumber1},
    {SERVICE_COMMANDS,  BASE_PART_NUMBER,           INTINDEXED, 1, (pParamFunc)getBasePartNumber2},
    {SERVICE_COMMANDS,  BASE_PART_NUMBER,           INTINDEXED, 2, (pParamFunc)getBasePartNumber3},
    {SERVICE_COMMANDS,  BASE_PART_NUMBER,           INTINDEXED, 3, (pParamFunc)getBasePartNumber4},
    {SERVICE_COMMANDS,  NUMBER_OF_CELLS,            INTEGERX,   0, (pParamFunc)getNumCells},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 0, (pParamFunc)getCellSerialNumber1},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 1, (pParamFunc)getCellSerialNumber2},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 2, (pParamFunc)getCellSerialNumber3},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 3, (pParamFunc)getCellSerialNumber4},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 4, (pParamFunc)getCellSerialNumber5},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 5, (pParamFunc)getCellSerialNumber6},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 6, (pParamFunc)getCellSerialNumber7},
    {SERVICE_COMMANDS,  SN_CELL,                    INTINDEXED, 7, (pParamFunc)getCellSerialNumber8},
    {SERVICE_COMMANDS,  BIRTH_DATE,                 INTINDEXED, 0, (pParamFunc)getBirthDate1},
    {SERVICE_COMMANDS,  BIRTH_DATE,                 INTINDEXED, 1, (pParamFunc)getBirthDate2},
    {SERVICE_COMMANDS,  SUNRISE_DATE,               INTINDEXED, 0, (pParamFunc)getSunriseDate1},
    {SERVICE_COMMANDS,  SUNRISE_DATE,               INTINDEXED, 1, (pParamFunc)getSunriseDate2},
    {SERVICE_COMMANDS,  SUNSET_DATE,                INTINDEXED, 0, (pParamFunc)getSunsetDate1},
    {SERVICE_COMMANDS,  SUNSET_DATE,                INTINDEXED, 1, (pParamFunc)getSunsetDate2},
    {SERVICE_COMMANDS,  C_MON_CAL,                  FLOATINGP,  0, (pParamFunc)getCMonCal},
    {SERVICE_COMMANDS,  INITIAL_SOC,                FLOATINGP,  0, (pParamFunc)getInitialSoc},
    {SERVICE_COMMANDS,  BLE_USE_HIGH_POWER,         INTEGERX,   0, (pParamFunc)getBlePowerLevel},
    {SERVICE_COMMANDS,  CONTACTOR_SERIAL_NUMBER,    FLOATINGP,  0, (pParamFunc)getContactorSerialNumber1},
    {SERVICE_COMMANDS,  CONTACTOR_SERIAL_NUMBER,    FLOATINGP,  1, (pParamFunc)getContactorSerialNumber2},
    {SERVICE_COMMANDS,  CONTACTOR_SERIAL_NUMBER,    FLOATINGP,  2, (pParamFunc)getContactorSerialNumber3},
    {SERVICE_COMMANDS,  CONTACTOR_SERIAL_NUMBER,    FLOATINGP,  3, (pParamFunc)getContactorSerialNumber4},
    {SERVICE_COMMANDS,  PCB_SERIAL_NUMBER,          FLOATINGP,  0, (pParamFunc)getPcbSerialNumber1},
    {SERVICE_COMMANDS,  PCB_SERIAL_NUMBER,          FLOATINGP,  1, (pParamFunc)getPcbSerialNumber2},
    {SERVICE_COMMANDS,  PCB_SERIAL_NUMBER,          FLOATINGP,  2, (pParamFunc)getPcbSerialNumber3},
    {SERVICE_COMMANDS,  PCB_SERIAL_NUMBER,          FLOATINGP,  3, (pParamFunc)getPcbSerialNumber4}
};

void Init_BLE(void)
{
    BlueNRG_Init();
    BlueNRG_SetTxPower(7);
    //BlueNRG_SetMACAddr(mac_addr);
    BlueNRG_SetSerialNum(sn, 0);
}

static uint8_t BlueNRG_Encode(uint8_t *src, uint8_t len, uint8_t *dest)
{
    uint8_t i, j;

    dest[0] = PACKET_END;
    for (i = j = 0; i < len; i++) {
        uint8_t c = src[i];
        if (c == PACKET_END) {
            dest[++j] = BYTE_ESC;
            dest[++j] = BYTE_ESC_END;
        }
        else if (c == BYTE_ESC) {
            dest[++j] = BYTE_ESC;
            dest[++j] = BYTE_ESC_ESC;
        }
        else {
            dest[++j] = c;
        }
    }
    dest[++j] = PACKET_END;
    return (j + 1);
}

static uint8_t BlueNRG_GetData(uint8_t mode, uint8_t index, uint8_t *buf)
{
    float value;
    uint8_t i, len, checksum;
    volatile HOST_COMMAND_TYPE *data;
    PACKET packet;

    if (mode == BLUENRG_NORM_MODE)
        data = &mobileCmds[index % MAX_HOST_CMD_IDS];
    else
        data = &mobileDiagCmds[index % MAX_HOST_DIAG_CMD_IDS];
    packet.fields.classPlusCmdID = (((data->cmdClass & 0x07) << 5) | (data->cmdID & 0x1F));
    packet.fields.typePlusIndex = (((data->type & 0x07) << 5) | (data->idx & 0x1F));
    packet.fields.length = sizeof(PACKET_FIELDS) + 4;
    packet.fields.checksum = 0;
    value = data->f();
    memcpy(packet.fields.param, (uint8_t *)&value, 4);

    checksum = 0;
    len = packet.fields.length - 1;
    for (i = 0; i < len; i++) {
        checksum ^= packet.bytes[i];
    }
    packet.fields.checksum = checksum;

    len = BlueNRG_Encode(packet.bytes, packet.fields.length, buf);
    return len;
}

void BLE_Monitor(void)
{
    static uint8_t prev_state, data_mode, data_index;
    uint8_t data[64];
    uint8_t len, state;
    BlueNRG_Process();
    state = BlueNRG_GetState();
    switch (state) {
    case BNRG_NONE:
    case BNRG_FAIL:
    case BNRG_DONE:
    case BNRG_SCAN:
    case BNRG_SEND:
        break;
    case BNRG_IDLE:
        data_mode = 0;
        data_index = 0;
        BlueNRG_Enable();
        break;
    case BNRG_LINK:
        // delay for a while to allow event receiving from the mobile
        if ((HAL_SYSTICK_GetTicks() - link_time) <= 150) break;
        len = BlueNRG_RecvData(data, sizeof(data));
        if (len == 0) {
            len = BlueNRG_GetData(data_mode, data_index++, data);
            if (data_index >= MAX_HOST_CMD_IDS) data_index = 0;
            BlueNRG_SendData(data, len);
        }
        else if ((len & 0x80) == 0) { // notification
            data[4] = (INTEGERX << 5);
            if (len > 7) {
                // TODO: application should handle this message and response it with a proper packet
                BlueNRG_SendData(data, len);
            }
        }
        else { // identification
            len &= 0x0F;
            BlueNRG_ConfirmIndication();
        }
        link_time = HAL_SYSTICK_GetTicks();
        break;
    }
    prev_state = state;
}

