/** Application datalogging code
 *
 *  This code deals with submitting and timestamping datalog records; implementation of
 *  the initial "health" channel records; and the code used by the FATFS module to manage 
 *  formatting each channel's data records for writing to the SD card in CSV format.  
 *
 *  The datalog_timestamp_and_write() function sets the current RTC date and time on a
 *  record and passes it on to datalog_write_only(), which can also be used directly for
 *  logging cached data, or data read from another store like SPI flash.  These send the
 *  record pointer through to the sd_mmc FSM for writing.
 *
 *  The DATALOG_HEALTH channel demonstrates the two functions needed for each channel:
 *  - A _path() function which calculates the filename on the SD card, including the
 *    partition number, and
 *  - A _format() function which writes the header row and data row.
 *
 *  The datalog_full(), datalog_head(), and datalog_data() functions are called from the
 *  FATFS module to write the actual data safely.
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <utils_assert.h>

#include <hal_calendar.h>
#include "datalog.h"
#include "csv.h"

#include "sd_mmc/state.h"


/******** Public datalog API ********/


/* Needed to use calendar_get_date_time() */
extern struct calendar_descriptor  CALENDAR_0;

/** Dispatch a single record with a timestamp already set. 
 *
 *  \param  record  Pointer to a record structure 
 */
void datalog_write_only (struct datalog_record *record)
{
	sd_mmc_Event(evSD_MMC_LOG_DATA, record);
}

/** Set the timestamp on a single record and dispatch it
 *
 *  \param  record  Pointer to a record structure 
 */
void datalog_timestamp_and_write (struct datalog_record *record)
{
    int32_t ret = calendar_get_date_time(&CALENDAR_0, &record->datetime);
    ASSERT(!ret);

	datalog_write_only(record);
}

void datalog_write (struct datalog_record *record)
{
	datalog_write_only(record);
}



/******** DATALOG_HEALTH implementation ********/


/** Leaf file name for health record
 *
 *  This must be provided for each datalog channel to be used with the SD card.  The
 *  string prepared is the file name only.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  max     Destination buffer size
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes necessary to store the path 
 */
static int health_leaf (char *buf, unsigned max, const struct datalog_record *record)
{
    return snprintf(buf, max, "%04u%02u%02u.csv",
                    record->datetime.date.year,
					record->datetime.date.month,
					record->datetime.date.day);
}

/** Header or data for health record
 *
 *  This will be called with head = true when a new file is created, to write the header
 *  row with field/column labels.  It will be called with head = false to write the actual
 *  data row.
 *
 *  For CSV, call csv_beg() for the first column, csv_add() for each interior column, and
 *  csv_end() for the last column.  These functions will handle quoting fields and
 *  separating columns with comma characters.  
 *
 *  A single column file is not currently supported, but will be added as csv_one().
 *
 *  \param  buf     Destination buffer pointer
 *  \param  end     Destination buffer end (buf + size)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes written into the buffer; if this is >= its size then the
 *           record was truncated and the buffer must be increased.
 */
static int health_format (char *buf, char *end, bool head, const struct datalog_record *record)
{
	char *ins = buf;

	if ( head )
	{
		ins = csv_beg(ins, end, "date");
		ins = csv_add(ins, end, "time");
		ins = csv_add(ins, end, "SOC");
		ins = csv_add(ins, end, "SOH");
		ins = csv_add(ins, end, "C_1_MIN");
		ins = csv_add(ins, end, "C_1_AVG");
		ins = csv_add(ins, end, "C_1_MAX");
		ins = csv_add(ins, end, "C_2_MIN");
		ins = csv_add(ins, end, "C_2_AVG");
		ins = csv_add(ins, end, "C_2_MAX");
		ins = csv_add(ins, end, "C_3_MIN");
		ins = csv_add(ins, end, "C_3_AVG");
		ins = csv_add(ins, end, "C_3_MAX");
		ins = csv_add(ins, end, "C_4_MIN");
		ins = csv_add(ins, end, "C_4_AVG");
		ins = csv_add(ins, end, "C_4_MAX");
		ins = csv_add(ins, end, "T_1");
		ins = csv_add(ins, end, "T_2");
		ins = csv_add(ins, end, "T_3");
		ins = csv_add(ins, end, "T_4");
		ins = csv_add(ins, end, "T_5");
		ins = csv_add(ins, end, "I_MAX");
		ins = csv_add(ins, end, "I_MIN");
		ins = csv_end(ins, end, "I_AVG");
	}
	else
	{
		ins = csv_beg(ins, end, csv_date(&record->datetime.date));
		ins = csv_add(ins, end, csv_time(&record->datetime.time));
		ins = csv_add(ins, end, csv_float(record->data.health.soc));
		ins = csv_add(ins, end, csv_float(record->data.health.soh));
		ins = csv_add(ins, end, csv_float(record->data.health.C1_MIN));
		ins = csv_add(ins, end, csv_float(record->data.health.C1_AVG));
		ins = csv_add(ins, end, csv_float(record->data.health.C1_MAX));
		ins = csv_add(ins, end, csv_float(record->data.health.C2_MIN));
		ins = csv_add(ins, end, csv_float(record->data.health.C2_AVG));
		ins = csv_add(ins, end, csv_float(record->data.health.C2_MAX));
		ins = csv_add(ins, end, csv_float(record->data.health.C3_MIN));
		ins = csv_add(ins, end, csv_float(record->data.health.C3_AVG));
		ins = csv_add(ins, end, csv_float(record->data.health.C3_MAX));
		ins = csv_add(ins, end, csv_float(record->data.health.C4_MIN));
		ins = csv_add(ins, end, csv_float(record->data.health.C4_AVG));
		ins = csv_add(ins, end, csv_float(record->data.health.C4_MAX));
	    ins = csv_add(ins, end, csv_float(record->data.health.T1));
		ins = csv_add(ins, end, csv_float(record->data.health.T2));
		ins = csv_add(ins, end, csv_float(record->data.health.T3));
		ins = csv_add(ins, end, csv_float(record->data.health.T4));
		ins = csv_add(ins, end, csv_float(record->data.health.T5));
		ins = csv_add(ins, end, csv_float(record->data.health.I_MAX));
		ins = csv_add(ins, end, csv_float(record->data.health.I_MIN));
		ins = csv_end(ins, end, csv_float(record->data.health.I_AVG));
		
	}

	return ins - buf;
}


/** Channel registration struct, currently static, but this could be moved to another file
 *  and extern'd as the number of channels supported grows */
static const struct datalog_channel_ops  health_ops =
{
	.leaf    = health_leaf,
	.format  = health_format,
	.path    = "0:/health",
	.pattern = "*.csv",
	.quota   = 10,
};


/******** DATALOG_STRESS implementation ********/


/** See health_leaf above */
static int stress_leaf (char *buf, unsigned max, const struct datalog_record *record)
{
    return snprintf(buf, max, "%02u%02u%02u%02u.txt",
                    record->datetime.date.month,
                    record->datetime.date.day,
                    record->datetime.time.hour,
					record->datetime.time.min);
}

/** See health_format above */
static int stress_format (char *buf, char *end, bool head, const struct datalog_record *record)
{
	char *ins = buf;

	if ( head )
	{
		ins = csv_beg(ins, end, "date");
		ins = csv_add(ins, end, "time");
		ins = csv_end(ins, end, "padding");
	}
	else
	{
		ins = csv_beg(ins, end, csv_date(&record->datetime.date));
		ins = csv_add(ins, end, csv_time(&record->datetime.time));

		ASSERT(end - ins > 4);
		end -= 4;
		while ( ins < end )
			*ins++ = '.';
		end += 4;

		*ins++ = '\r';
		*ins++ = '\n';
		*ins   = '\0';
	}

	return ins - buf;
}


/** Channel registration struct, currently static, but this could be moved to another file
 *  and extern'd as the number of channels supported grows */
static const struct datalog_channel_ops  stress_ops =
{
	.leaf    = stress_leaf,
	.format  = stress_format,
	.path    = "0:/stress",
	.pattern = "*.txt",
	.quota   = 33,
};


/******** DATALOG_EVENT implementation ********/

static int events_leaf (char *buf, unsigned max,const struct datalog_record *record)
{
	return snprintf(buf, max, "%04u%02u%02u.csv",
	record->datetime.date.year,
	record->datetime.date.month,
	record->datetime.date.day);
}


static int events_format (char *buf, char *end, bool head, const struct datalog_record *record)
{
	char *ins = buf;

	if ( head )
	{
		ins = csv_beg(ins, end, "date");
		ins = csv_add(ins, end, "time");
		ins = csv_end(ins, end, "event");
	}
	else
	{
		ins = csv_beg(ins, end, csv_date(&record->datetime.date));
		ins = csv_add(ins, end, csv_time(&record->datetime.time));
		ins = csv_end(ins, end, csv_text(record->data.events.entry));
		
	}

	return ins - buf;
}

/** Channel registration struct, currently static, but this could be moved to another file
 *  and extern'd as the number of channels supported grows */
static const struct datalog_channel_ops  events_ops =
{
	.leaf    = events_leaf,
	.format  = events_format,
	.path    = "0:/events",
	.pattern = "*.csv",
	.quota   = 10,
};

/******** Channel handler registration and dispatch ********/


/* Each channel supported needs its datalog_channel_ops struct added here so the dispatch
 * code below can find it. */
const struct datalog_channel_ops *channel_list[DATALOG_NUM_CHANNELS] =
{
	[DATALOG_HEALTH] = &health_ops,
	[DATALOG_EVENTS] = &events_ops,
	[DATALOG_STRESS] = &stress_ops,
};

/** Assemble full path using record's path and _leaf handler
 *
 *  This must be provided for each datalog channel to be used with the SD card.  The path
 *  should be fully qualified, starting with the partition number.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  max     Destination buffer size
 *  \param  part    Partition number (see sd_mmc/fatfs.c)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes necessary to store the path 
 */
int datalog_full (char *buf, unsigned max, unsigned part,
                  const struct datalog_record *record)
{
	/* Check buffer and record pointers and size */
	ASSERT(buf);
	ASSERT(max);
	ASSERT(record);

	/* Check channel number in the record; if you assert here you're probably not setting
	 * it to the correct value */
	ASSERT(record->channel >= 0);
	ASSERT(record->channel < DATALOG_NUM_CHANNELS);

	/* Check channel has valid path and leaf handler; if you assert here you're probably
	 * missing a datalog_channel_ops entry in channel_list[] above, or one of the values
	 * needed in the datalog_channel_ops  */
	ASSERT(channel_list[record->channel]);
	ASSERT(channel_list[record->channel]->leaf);
	ASSERT(channel_list[record->channel]->path);

	/* Check path starts with "0:/" or "0:\" - must be fully qualified */
	const char *path = channel_list[record->channel]->path;
	ASSERT(isdigit(path[0]));
	ASSERT(path[1] == ':');
	ASSERT(path[2] == '/' || path[2] == '\\');
	ASSERT(path[3] != '/' && path[3] != '\\');

	/* Format using separate part, path, and leaf call */
	char *dst = buf;
	char *end = buf + max;
	int   len = strlen(path);

	/* Check for undersized buffer; if you assert here it's too small */
	ASSERT(max >= 20);
	ASSERT(max >= len + 13);

	/* Copy path part */
	memset(buf, 0, max);
	*dst++ = '0' + part;
	path++;
	while ( *path )
		*dst++ = *path++;

	/* Separator if necessary */
	if ( *(dst-1) != '/' && *(dst-1) != '\\' )
		*dst++ = channel_list[record->channel]->path[2];

	/* Format leaf portion from record timestamp,  */
	len = channel_list[record->channel]->leaf(dst, end - dst, record);
	ASSERT(len > -1);

	/* Check for undersized buffer; if you assert here it's too small */
	dst += len;
	ASSERT(dst - buf < max);

	return dst - buf;
}

/** Call the appropriate _format handler for this record's channel type.  */
static int datalog_format (char *buf, char *end, bool head,
                           const struct datalog_record *record)
{
	/* Check buffer, end, and record pointers */
	ASSERT(buf);
	ASSERT(end);
	ASSERT(end > buf);
	ASSERT(record);

	/* Check channel number in the record; if you assert here you're probably not setting
	 * it to the correct value */
	ASSERT(record->channel >= 0);
	ASSERT(record->channel < DATALOG_NUM_CHANNELS);

	/* Check channel has valid handlers; if you assert here you're probably missing a
	 * datalog_channel_ops entry in channel_list[] above */
	ASSERT(channel_list[record->channel]);
	ASSERT(channel_list[record->channel]->format);

	return channel_list[record->channel]->format(buf, end, head, record);
}

/** Call the appropriate _format header handler for this record's channel type. 
 *
 *  This will be called when a new file is created, to write the header row with
 *  field/column labels.  See health_format() for an example.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  end     Destination buffer end (buf + size)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes written into the buffer; if this is >= its size then the
 *           record was truncated and the buffer must be increased.
 */
int datalog_head (char *buf, char *end, const struct datalog_record *record)
{
	return datalog_format(buf, end, true, record);
}

/** Call the appropriate _format data handler for this record's channel type. 
 *
 *  This will be called to write the actual data row.  See health_format() for an example.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  end     Destination buffer end (buf + size)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes written into the buffer; if this is >= its size then the
 *           record was truncated and the buffer must be increased.
 */
int datalog_data (char *buf, char *end, const struct datalog_record *record)
{
	return datalog_format(buf, end, false, record);
}

/** Return the path component for a channel
 *
 *  \param  channel  Channel ID 
 *
 *  \return  Path component
 */
const char *datalog_path (datalog_channel_t channel)
{
	/* Check channel number in the record; if you assert here you're probably not setting
	 * it to the correct value */
	ASSERT(channel >= 0);
	ASSERT(channel < DATALOG_NUM_CHANNELS);

	/* Check channel has valid handlers; if you assert here you're probably missing a
	 * datalog_channel_ops entry in channel_list[] above */
	ASSERT(channel_list[channel]);
	ASSERT(channel_list[channel]->path);

	return channel_list[channel]->path;
}

/** Return the pattern string
 *
 *  \param  channel  Channel ID 
 *
 *  \return  Pattern string
 */
const char *datalog_pattern (datalog_channel_t channel)
{
	/* Check channel number in the record; if you assert here you're probably not setting
	 * it to the correct value */
	ASSERT(channel >= 0);
	ASSERT(channel < DATALOG_NUM_CHANNELS);

	/* Check channel has valid handlers; if you assert here you're probably missing a
	 * datalog_channel_ops entry in channel_list[] above */
	ASSERT(channel_list[channel]);
	ASSERT(channel_list[channel]->pattern);

	return channel_list[channel]->pattern;
}

/** Return the quota value
 *
 *  \param  channel  Channel ID 
 *
 *  \return  Quota value
 */
unsigned  datalog_quota (datalog_channel_t channel)
{
	/* Check channel number in the record; if you assert here you're probably not setting
	 * it to the correct value */
	ASSERT(channel >= 0);
	ASSERT(channel < DATALOG_NUM_CHANNELS);

	/* Check channel has valid handlers; if you assert here you're probably missing a
	 * datalog_channel_ops entry in channel_list[] above */
	ASSERT(channel_list[channel]);

	return channel_list[channel]->quota;
}

