/** CSV output formatting code
 *
 *  This code manages formatting CSV files from ASCII strings (handling quoting,
 *  separation, and EOL), and formatting application values into ASCII.
 *
 *  The CSV formatting functions are used to assemble lines in a CSV file:
 *  - csv_beg() should be used to start a line which will have another column after
 *  - csv_add() should be used to continue a line, but not for the last column
 *  - csv_end() should be used to end a line, but not for the first column
 *  - csv_one() should be used to write a line with a single column
 *
 *  The above functions are passed a string value, which may come from the value
 *  formatting functions:
 *  - csv_date() produces an ISO-standard date (YYYY-MM-HH)
 *  - csv_time() produces an ISO-standard time (HH:MM:SS)
 *  - csv_float() produces a decimal number from a float
 *
 *  The value formatting functions use and return an internal static string, which they
 *  will overwrite each time they're called.  Avoid doing something like this:
 *    printf("date range: %s - %s\n", csv_date(&date1), csv_date(&date2));
 *  as the same ASCII string will print for both dates, and which one is undefined.
 *
 *  An example how to use these is provided by health_format(), see lib/datalog.c
 */
#ifndef LIB_CSV_H
#define LIB_CSV_H
#include <hal_calendar.h>
#include "datalog.h"


/******** CSV Formatting Functions ********/


/** Write the first column in a line which will contain more than one column.
 *
 *  \param  ins     Insert pointer into buffer
 *  \param  end     Pointer to end of buffer
 *  \param  string  String value to write
 *
 *  \return  ins after adjusting for amount written
 */
char *csv_beg (char *ins, char *end, const char *string);

/** Write a subsequent column, after the first but not the last 
 *
 *  \param  ins     Insert pointer into buffer
 *  \param  end     Pointer to end of buffer
 *  \param  string  String value to write
 *
 *  \return  ins after adjusting for amount written
 */
char *csv_add (char *ins, char *end, const char *string);

/** Write the last column in a line with at least one column written before
 *
 *  \param  ins     Insert pointer into buffer
 *  \param  end     Pointer to end of buffer
 *  \param  string  String value to write
 *
 *  \return  ins after adjusting for amount written
 */
char *csv_end (char *ins, char *end, const char *string);

/** Write a line containing one column only
 *
 *  \param  ins     Insert pointer into buffer
 *  \param  end     Pointer to end of buffer
 *  \param  string  String value to write
 *
 *  \return  ins after adjusting for amount written
 */
char *csv_one (char *ins, char *end, const char *string);


/******** Value Formatting Functions ********/


/** Write an ISO-standard date from a struct calendar_date
 *
 *  \note This function uses a static buffer; do not call it more than once as arguments
 *        to a function, or keep a pointer to its return value; subsequent calls will
 *        reuse the buffer.
 *
 *  \param  date  Pointer to a struct calendar_date
 *
 *  \return  Pointer to a static string with the formatted date
 */
const char *csv_date (const struct calendar_date *date);

/** Write an ISO-standard time from a struct calendar_time
 *
 *  \note This function uses a static buffer; do not call it more than once as arguments
 *        to a function, or keep a pointer to its return value; subsequent calls will
 *        reuse the buffer.
 *
 *  \param  time  Pointer to a struct calendar_time
 *
 *  \return  Pointer to a static string with the formatted time
 */
const char *csv_time (const struct calendar_time *time);

/** Write a float value
 *
 *  \note This function uses a static buffer; do not call it more than once as arguments
 *        to a function, or keep a pointer to its return value; subsequent calls will
 *        reuse the buffer.
 *
 *  \param  val  Float value to write
 *
 *  \return  Pointer to a static string with the formatted float
 */
const char *csv_float (float val);

const char *csv_text (const char* val);

#endif /* LIB_CSV_H */
