#ifndef LIB_TRACE_H
#define LIB_TRACE_H


/* Log a trace point, with or without message */
#define trace_mesg(mesg) do{ _trace_point(__func__, __LINE__, mesg); }while(0)
#define trace_bare()     do{ _trace_point(__func__, __LINE__, NULL); }while(0)
void _trace_point (const char *file, int line, const char *mesg);


/* Print traces, from main loop */
void trace_print (void);


#endif /* LIB_TRACE_H */
