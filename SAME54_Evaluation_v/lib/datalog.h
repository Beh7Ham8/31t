/** Application datalogging code
 *
 *  This code deals with submitting and timestamping datalog records; implementation of
 *  the initial "health" channel records; and the code used by the FATFS module to manage 
 *  formatting each channel's data records for writing to the SD card in CSV format.  
 *
 *  The datalog_timestamp_and_write() function sets the current RTC date and time on a
 *  record and passes it on to datalog_write_only(), which can also be used directly for
 *  logging cached data, or data read from another store like SPI flash.  These send the
 *  record pointer through to the sd_mmc FSM for writing.
 *
 *  The DATALOG_HEALTH channel demonstrates the two functions needed for each channel:
 *  - A _path() function which calculates the filename on the SD card, including the
 *    partition number, and
 *  - A _format() function which writes the header row and data row.
 *
 *  To write a health record, use something like this, which is currently implemented in
 *  test/fsm.c with #define TEST_DATALOG 1
 *    struct datalog_record health;
 *    health.channel         = DATALOG_HEALTH;
 *    health.data.health.soc = 1234.567;
 *    health.data.health.sof = 2345.678;
 *    health.data.health.soh = 3456.789;
 *    datalog_timestamp_and_write(&health);
 *
 *  The datalog_full(), datalog_head(), and datalog_data() functions are called from the
 *  FATFS module to write the actual data safely.
 */
#ifndef LIB_DATALOG_H
#define LIB_DATALOG_H
#include <hal_calendar.h>


/******** Public datalog API ********/


/** This enum lists each channel which can be formatted by the datalogging code for
 *  writing to the SD card.  To add a channel:
 *  - add a new enum here before DATALOG_NUM_CHANNELS (which must stay last)
 *  - add a new struct below for the data points, similar to struct datalog_health
 *  - add your new struct to union datalog_data
 *  - add a _leaf handler, similar to health_leaf (see lib/datalog.c)
 *  - add a _format handler, similar to health_format (see lib/datalog.c)
 *  - add a struct datalog_channel_ops, similar to health_ops (see lib/datalog.c)
 *    - .leaf    should be your leaf handler
 *    - .format  should be your format handler
 *    - .path    should be the fully qualified directory path
 *    - .pattern should be a filename mask/pattern which matches your leaf names
 *    - .quota   should be the percentage of disk space for this channel
 *  - add your datalog_channel_ops to channel_list[] in lib/datalog.c
 */
typedef enum
{
	DATALOG_HEALTH,
	DATALOG_EVENTS,
    DATALOG_STRESS,
	DATALOG_NUM_CHANNELS
}
datalog_channel_t;

/** Data points for the DATALOG_HEALTH channel */
struct datalog_health
{
    float  soc;
    float  soh;
	float  C1_MIN;
	float  C1_AVG;
	float  C1_MAX;
	float  C2_MIN;
	float  C2_AVG;
	float  C2_MAX;
	float  C3_MIN;
	float  C3_AVG;
	float  C3_MAX;
	float  C4_MIN;
	float  C4_AVG;
	float  C4_MAX;
	float  T1;
	float  T2;
	float  T3;
	float  T4;
	float  T5;	
	float  I_MIN;
	float  I_MAX;
	float  I_AVG;	
};

struct datalog_events
{
	char entry[64];
};

/** Data points for the DATALOG_STRESS channel */
struct datalog_stress
{
	uint16_t  length;
};

/** Union of data points for all datalog channels */
union datalog_data
{
	struct datalog_health  health;
	struct datalog_events  events;
	struct datalog_stress  stress;
};

/** Primary record structure, with channel ID, timestamp, and union of all datalog channel
 *  data points.  To log data, set the structure up and pass:
 *
 *    struct datalog_record  health;
 *    health.channel         = DATALOG_HEALTH;
 *    health.data.health.soc = 1234.567;
 *    health.data.health.sof = 2345.678;
 *    health.data.health.soh = 3456.789;
 *    datalog_timestamp_and_write(&health);
 */
struct datalog_record
{
	datalog_channel_t          channel;
    struct calendar_date_time  datetime;
	union datalog_data         data;
};


/** Set the timestamp on a single record and dispatch it
 *
 *  \param  record  Pointer to a record structure 
 */
void datalog_timestamp_and_write (struct datalog_record *record);


/** Dispatch a single record with a timestamp already set. 
 *
 *  \param  record  Pointer to a record structure 
 */
void datalog_write_only     (struct datalog_record *record);
void datalog_write (struct datalog_record *record);


/******** Channel handler registration and dispatch ********/


/** Assemble full path using record's path and _leaf handler
 *
 *  This must be provided for each datalog channel to be used with the SD card.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  max     Destination buffer size
 *  \param  part    Partition number (see sd_mmc/fatfs.c)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes necessary to store the path 
 */
int datalog_full (char *buf, unsigned max, unsigned part,
                  const struct datalog_record *record);


/** Call the appropriate _format header handler for this record's channel type. 
 *
 *  This will be called when a new file is created, to write the header row with
 *  field/column labels.  See health_format() for an example.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  end     Destination buffer end (buf + size)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes written into the buffer; if this is >= its size then the
 *           record was truncated and the buffer must be increased.
 */
int datalog_head (char *buf, char *end, const struct datalog_record *record);


/** Call the appropriate _format data handler for this record's channel type. 
 *
 *  This will be called to write the actual data row.  See health_format() for an example.
 *
 *  \param  buf     Destination buffer pointer
 *  \param  end     Destination buffer end (buf + size)
 *  \param  record  Pointer to a record
 *
 *  \return  Number of bytes written into the buffer; if this is >= its size then the
 *           record was truncated and the buffer must be increased.
 */
int datalog_data (char *buf, char *end, const struct datalog_record *record);


/** Return the path component for a channel
 *
 *  \param  channel  Channel ID 
 *
 *  \return  Path component
 */
const char *datalog_path (datalog_channel_t channel);


/** Return the pattern string
 *
 *  \param  channel  Channel ID 
 *
 *  \return  Pattern string
 */
const char *datalog_pattern (datalog_channel_t channel);


/** Return the quota value
 *
 *  \param  channel  Channel ID 
 *
 *  \return  Quota value
 */
unsigned  datalog_quota (datalog_channel_t channel);


/******** ********/

/** Type for _leaf handler */
typedef int (* datalog_channel_leaf_f) (char *buf, unsigned max, 
                                        const struct datalog_record *record);

/** Type for _format handler */
typedef int (* datalog_channel_format_f) (char *buf, char *end, bool head,
                                          const struct datalog_record *record);

/** Type for handler registration struct */
struct datalog_channel_ops
{
	datalog_channel_leaf_f    leaf;
	datalog_channel_format_f  format;
	const char               *path;
	const char               *pattern;
	unsigned                  quota;
};


#endif /* LIB_DATALOG_H */
