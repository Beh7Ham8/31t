/*
 * cfg_device.h
 *
 * Created: 6/24/2020 4:36:39 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef __CFG_DEVICE_H__
#define __CFG_DEVICE_H__

#include "packet.h"

#define CFG_MAGIC_WORD  0xa55a6789
#define CFG_VERSION     0x0100
typedef struct DeviceConfig
{
    uint32_t startSig; // = CFG_MAGIC_WORD
    uint16_t structVersion; // = CFG_VERSION
    char systemSerialNumber[16];
    char systemPartNumber[16];
    char basePartNumber[16];
    char baseSerialNumber[16];
    char contactorSerialNumber[16];
    char pcbSerialNumber[16];
    uint8_t reserved; // reserved for data aligned
    uint8_t numCells;
    uint32_t cellSerialNumbers[8];
} __attribute__((packed)) DEV_CFG, *pDEV_CFG;

typedef struct ParametersConfig
{
	uint32_t startSig; // = CFG_MAGIC_WORD
	uint16_t structVersion; // = CFG_VERSION
	uint32_t debugTraceWord;
	uint8_t skipSleepMode;
	char debugTraceNames[128];
} __attribute__((packed)) PARAM_CFG, *pPARAM_CFG;

const pDEV_CFG cfg_device_config(void);

uint16_t cfg_eeprom_size(void);
void *cfg_eeprom_addr(void);
bool cfg_eeprom_isbusy(void);
bool cfg_eeprom_islocked(void);
void cfg_eeprom_lock(void);
void cfg_eeprom_unlock(void);
bool cfg_eeprom_read(uint16_t offset, uint8_t *data, uint16_t size);
bool cfg_eeprom_write(uint16_t offset, uint8_t *data, uint16_t size);

void cfg_device_sn(uint32_t data[4]);

bool cfg_factory_read(uint8_t data[512]);
bool cfg_factory_write(const uint8_t data[512]);

/* Packets communicate with ELTTerminal */
void cfg_eeprom_get(pIPACKET packet);
void cfg_eeprom_set(pIPACKET packet);

void cfg_device_get(pIPACKET packet);
void cfg_device_reset(pIPACKET packet);

void cfg_factory_get(pIPACKET packet);
void cfg_factory_set(pIPACKET packet);

#endif