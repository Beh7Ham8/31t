/*
 * serial_comm.h
 *
 * Created: 6/16/2020 10:22:55 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef __SERIAL_COMM_H__
#define __SERIAL_COMM_H__

#include <stdbool.h>

void Init_SCOMM(void);
void SCOMM_Monitor(void);
bool scom_send(void *packet);
bool scom_ready(void);

#endif