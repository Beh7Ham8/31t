/*
 * fw_upgrade.c
 *
 * Created: 6/16/2020 10:22:14 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include <string.h>
#include "serial_comm.h"
#include "fw_upgrade.h"

static uint32_t header_addr[2];
static bool flasha_mapped; /* 1: Start address of bank A is mapped at 0x0000_0000 */

static uint32_t fw_search_image(uint32_t start_addr);

void Init_FW(void)
{
    header_addr[0] = fw_search_image(FLASHA_ADDR);
    header_addr[1] = fw_search_image(FLASHB_ADDR);
    flasha_mapped = !!hri_nvmctrl_get_STATUS_AFIRST_bit(NVMCTRL);
    //printf("%08lX %08lX %d\n", header_addr[0], header_addr[1], flasha_mapped);
}

void fw_image_get(pIPACKET packet)
{
    packet->header.commandID = READ_MEM_BLOCK;
    if (header_addr[0])
    {
        packet->header.dataIndex = 1;
        if (!flasha_mapped) packet->header.dataIndex |= 4;
        packet->header.length = 4 + HDR_SIZE;
        packet->image.offset = header_addr[0] + HDR_SIZE;
        flash_read(&FLASH, header_addr[0], packet->image.block, HDR_SIZE);
        scom_send(packet);
    }
    if (header_addr[1])
    {
        packet->header.dataIndex = 2;
        if (flasha_mapped) packet->header.dataIndex |= 4;
        packet->header.length = 4 + HDR_SIZE;
        packet->image.offset = header_addr[1] + HDR_SIZE;
        flash_read(&FLASH, FLASHB_ADDR + header_addr[1], packet->image.block, HDR_SIZE);
        while (!scom_ready()) delay_ms(1);
        scom_send(packet);
    }
    if (header_addr[0] == 0 && header_addr[1] == 0)
    {
        packet->header.dataIndex = 0;
        packet->header.length = 0;
        scom_send(packet);
    }
}

void fw_upgrade_start(pIPACKET packet)
{
    struct ImageHeader *phdr = (struct ImageHeader *)(packet->image.block);
    if ((phdr->h_offset + HDR_SIZE) <= FLASHA_SIZE)
    {
        uint32_t block_num = (phdr->h_offset + phdr->h_length + BLOCK_SIZE - 1) / BLOCK_SIZE;
        flash_erase(&FLASH, FLASHB_ADDR, BLOCK_PAGES * block_num);
        header_addr[1] = 0;
        flash_append(&FLASH, FLASHB_ADDR + phdr->h_offset, packet->image.block, phdr->h_length);
        packet->header.commandID = NODE_UPLOAD_PROGRESS;
        packet->header.length = 4;
        scom_send(packet);
    }
}

void fw_upgrade_write(pIPACKET packet)
{
    if (packet->image.offset < FLASHA_SIZE)
    {
        flash_append(&FLASH, FLASHB_ADDR + packet->image.offset - PAGE_SIZE, packet->image.block, PAGE_SIZE);
        packet->header.commandID = NODE_UPLOAD_PROGRESS;
        packet->header.length = 4;
        scom_send(packet);
    }
}

void fw_upgrade_end(pIPACKET packet)
{
    if (packet->image.offset < FLASHA_SIZE)
    {
        flash_append(&FLASH, FLASHB_ADDR + packet->image.offset - PAGE_SIZE, packet->image.block, PAGE_SIZE);
        header_addr[1] = fw_search_image(FLASHB_ADDR);
        if (header_addr[1])
        {
            packet->header.commandID = VERIFY_IMAGE;
            packet->header.dataIndex = 2;
            if (flasha_mapped) packet->header.dataIndex |= 4;
            packet->header.length = 4 + HDR_SIZE;
            packet->image.offset = header_addr[1] + HDR_SIZE;
            flash_read(&FLASH, FLASHB_ADDR + header_addr[1], packet->image.block, HDR_SIZE);
            scom_send(packet);
        }
        else
        {
            packet->header.commandID = VERIFY_IMAGE;
            packet->header.length = 0;
            scom_send(packet);
        }
    }
}

void fw_image_reset(pIPACKET packet)
{
    packet->header.commandID = APPLY_MEM_UPDATE;
    packet->header.length = 0;
    scom_send(packet);
    delay_ms(2000);
    // issue the BKSWRST command to swap the banks and then to reset the device
    hri_nvmctrl_write_CTRLB_reg(NVMCTRL, 0xA517);
}

static uint32_t fw_search_image(uint32_t start_addr)
{
    uint32_t i, sum, addr, data[PAGE_SIZE/4];
    struct ImageHeader *phdr = (struct ImageHeader *)data;

    /* search from 16 KB which supposes a firmware size greater than it. */
    for (addr = 0x4000; addr < FLASHA_SIZE; addr += PAGE_SIZE)
    {
        flash_read(&FLASH, start_addr + addr, (uint8_t *)data, sizeof(data));
        if (phdr->magic1 == MAGIC_WORD && phdr->magic1 == ~phdr->magic2)
        {
            if (phdr->h_length != HDR_SIZE) continue;
            if (phdr->h_offset != addr) continue;
            if (phdr->f_offset != 0) continue;
            if ((phdr->h_offset - phdr->f_length) >= PAGE_SIZE) continue;
            for (i = sum = 0; i < PAGE_SIZE/4; i++) sum ^= data[i];
            if (sum == 0xFFFFFFFF) break;
        }
        //printf("%08lX %08lX %08lX\n", start_addr + addr, phdr->magic1, phdr->magic2);
    }
    if (addr < FLASHA_SIZE)
    {
        uint32_t size = addr, chksum = phdr->f_chksum;
        sum = 0;
        for (addr = 0; addr < size; addr += PAGE_SIZE)
        {
            flash_read(&FLASH, start_addr + addr, (uint8_t *)data, sizeof(data));
            for (i = 0; i < PAGE_SIZE/4; i++) sum ^= data[i];
        }
        if (sum == chksum) return size;
    }
    return 0;
}
