/*
 * stateMachine.h
 *
 * Created: 5/7/2020 3:30:07 PM
 *  Author: BehroozH
 */ 


#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_

#include <stdint.h>

#define TIMER_EXPIRED 0

/* EVENTS */
enum {evENTER_STATE=0, evTIMER_TICK, evEXIT_STATE, evMAX_COMMON_EVENTS};

typedef void (*pStateFunc)(int state_id, void *data);  // declare the vector pointer type
typedef void (*pStateFunc_ext)(void *s, int state_id, int data);  // declare the vector pointer type

//State Machine Handler
typedef struct
{
	unsigned char isInitialized;
	int id;
	int currentState;
	int nextState;
	int prevState;
	long timer;
	int active_field;
	int substate;
	int nextSubstate;
	int forceRenterState;
	const pStateFunc *StateVectorArray;
	const pStateFunc_ext *StateVectorArray_ext;
	int num_states;
	uint8_t state_changed;
} STATE_MACHINE, *pSTATE_MACHINE;

/* Prototypes */
void stateInit(pSTATE_MACHINE state, const pStateFunc *StateVectorArray, int num_states, int start_state);
void enter_state(pSTATE_MACHINE state, int new_state);
int  event_handler(pSTATE_MACHINE state, int event, void *param);

void stateInit_ext(pSTATE_MACHINE state, const pStateFunc_ext *StateVectorArray, int num_states, int start_state);
void enter_state_ext(pSTATE_MACHINE state, int new_state);
void event_handler_ext(pSTATE_MACHINE state, int event, int param);



#endif /* STATEMACHINE_H_ */