/*
 * timing.c
 *
 * Created: 5/7/2020 1:38:44 PM
 *  Author: BehroozH
 */ 

#include <time.h>
#include <stdint.h>

#include "timing.h"
#include "systick.h"


/*******************************************************************************
* Function Name  : timerInit()
* Description    : initialize software timer
* Input          : pointer to software timer structure, timing period
*******************************************************************************/
void timerInit( pSW_TIMER tmr, uint64_t period )
{
    if (tmr != NULL)
    {
        tmr->timerIsActive = true;
        tmr->period = period;
        tmr->reference  = HAL_SYSTICK_GetTicks();
    }
}

void timerSetActive( pSW_TIMER tmr, bool active)
{
    tmr->timerIsActive = active;
}


bool timerIsActive(pSW_TIMER tmr)
{
    return (tmr->timerIsActive);
}

/*******************************************************************************
 * Function Name  : timerExpired()
 * Description    : checks to see if timer expired. Also does range check on period value (max 24 hours)
 * Input          : pointer to software timer structure
 * Return         : TRUE if expired, FALSE if not expired
 *******************************************************************************/
bool timerExpired(pSW_TIMER tmr) 
{
	uint64_t current = HAL_SYSTICK_GetTicks();
	
	if ((current - tmr->reference) >= tmr->period)
	{		
		return (true);
	} 
	else
	{
		return (false);
	}
}
/*******************************************************************************
* Function Name  : timerGetTime()
* Description    : gets current timer value
* Input          : pointer to software timer structure
* Return         : current timer value
*******************************************************************************/
uint64_t timerGetTime( pSW_TIMER tmr )
{
   return ( (HAL_SYSTICK_GetTicks() - tmr->reference) );
}
/*******************************************************************************
* Function Name  : timerReload()
* Description    : adds timer period to reference value
* Input          : pointer to software timer structure
*******************************************************************************/
void timerReload( pSW_TIMER tmr )
{
   tmr->reference += tmr->period;
}
/*******************************************************************************
* Function Name  : timerReset()
* Description    : resets timer reference value to current time
* Input          : pointer to software timer structure
*******************************************************************************/
void timerReset( pSW_TIMER tmr )
{
   tmr->reference = HAL_SYSTICK_GetTicks();
}
/*******************************************************************************
* Function Name  : timerDelay()
* Description    : blocked delay
* Input          : period
*******************************************************************************/
void timerDelay (uint64_t period)
{
    SW_TIMER delayTimer;
 
    timerInit(&delayTimer, period);
    while (!timerExpired(&delayTimer));
}

