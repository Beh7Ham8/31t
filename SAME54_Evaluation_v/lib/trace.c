#include <stdio.h>
#include <string.h>
#include <utils.h>
#include <utils_assert.h>

#include "trace.h"

#define TRACE_BITS 5
#define TRACE_SIZE (1 << TRACE_BITS)
#define TRACE_MASK (TRACE_SIZE - 1)

struct trace
{
	const char *file;
	int         line;
	const char *mesg;
};
static struct trace      trace_list[TRACE_SIZE];
static volatile uint8_t  trace_next;
static volatile uint8_t  trace_last;


/* Log a trace point, with or without message */
void _trace_point (const char *file, int line, const char *mesg)
{
	CRITICAL_SECTION_ENTER();
	trace_list[trace_next].file = file;
	trace_list[trace_next].line = line;
	trace_list[trace_next].mesg = mesg;
	trace_next++;
	trace_next &= TRACE_MASK;
	CRITICAL_SECTION_LEAVE();
}

/* Print traces, from main loop */
void trace_print (void)
{
	bool     loop = true;
	uint8_t  last;

	CRITICAL_SECTION_ENTER();
	loop = trace_last != trace_next;
	CRITICAL_SECTION_LEAVE();

	while ( loop )
	{
		CRITICAL_SECTION_ENTER();
		last = trace_last;
		CRITICAL_SECTION_LEAVE();

		printf("%s[%d]", trace_list[last].file, trace_list[last].line);
		if ( trace_list[last].mesg )
			printf(": %s", trace_list[last].mesg);
		printf("\r\n");

		CRITICAL_SECTION_ENTER();
		trace_last++;
		trace_last &= TRACE_MASK;
		loop = trace_last != trace_next;
		CRITICAL_SECTION_LEAVE();
	}

}


