/*
 * cmd_agent.c
 *
 * Created: 6/16/2020 10:23:40 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include <atmel_start.h>
#include "cmd_agent.h"
#include "packet.h"
#include "serial_comm.h"
#include "fw_upgrade.h"
#include "cfg_device.h"
#include "flash.h"

static void cmd_host(pPPACKET packet);
static void cmd_flash(pIPACKET packet);
static void cmd_service(pPPACKET packet);
static void cmd_test(pPPACKET packet);
static void cmd_debug(pPPACKET packet);
static void cmd_calib(pPPACKET packet);

struct calendar_date  cal_date;
struct calendar_time  cal_time;

void cmd_agent(void *packet)
{
    pHEADER header = packet;
    switch (header->classID)
    {
    case HOST_COMMANDS:
        cmd_host(packet);
        break;
    case NVMEM_COMMANDS:
        cmd_flash(packet);
        break;
    case SERVICE_COMMANDS:
        cmd_service(packet);
        break;
    case TEST_COMMANDS:
        cmd_test(packet);
        break;
    case DEBUG_COMMANDS:
        cmd_debug(packet);
        break;
    case CALIBRATION_COMMANDS:
        cmd_calib(packet);
        break;
    }
}

static void cmd_response_bool(pPPACKET packet, bool value)
{
    packet->param.iVal = value;
    packet->header.dataIndex = 0;
    packet->header.dataType = BOOLEAN;
    scom_send(packet);
}

static void cmd_response_int(pPPACKET packet, uint32_t value, int8_t index)
{
    packet->param.iVal = value;
    packet->header.dataIndex = index;
    packet->header.dataType = INTEGERX;
    scom_send(packet);
}

static void cmd_response_float(pPPACKET packet, float value, int8_t index)
{
    packet->param.fVal = value;
    packet->header.dataIndex = index;
    packet->header.dataType = FLOATINGP;
    scom_send(packet);
}

static void cmd_host(pPPACKET packet)
{
    (void)packet;
}

static void cmd_flash(pIPACKET packet)
{
    switch (packet->header.commandID)
    {
    case GET_XFER_BLOCK_SIZE:
        /* obtain information of the current firmware images */
        fw_image_get(packet);
        break;
    case START_MEM_XFER:
        /* erase flash and program firmware header */
        if (packet->header.dataType == 0)
            fw_upgrade_start(packet);
        /* get device serial number */
        else if (packet->header.dataType == 1)
            cfg_device_get(packet);
        break;
    case READ_MEM_BLOCK:
        /* read smart eeprom data */
        if (packet->header.dataType == 1)
            cfg_eeprom_get(packet);
        /* read user page data */
        else if (packet->header.dataType == 2)
            cfg_factory_get(packet);
    case WRITE_MEM_BLOCK:
        /* program blocks of firmware image */
        if (packet->header.dataType == 0)
            fw_upgrade_write(packet);
        /* write smart eeprom data */
        else if (packet->header.dataType == 1)
            cfg_eeprom_set(packet);
        /* write user page data */
        else if (packet->header.dataType == 2)
            cfg_factory_set(packet);
        break;
    case END_MEM_XFER:
        /* program the last block and verify the image */
        if (packet->header.dataType == 0)
            fw_upgrade_end(packet);
        break;
    case UPDATE_RESET:
        /* switch active bank and reset the device */
        if (packet->header.dataType == 0)
            fw_image_reset(packet);
        break;
    case DEVICE_RESET:
        /* reset the device */
        if (packet->header.dataType == 0)
            cfg_device_reset(packet);
        break;
    case READ_EEPROM:
        packet->image.offset = 0;
        packet->header.length = nvm_read_eeprom(packet->header.dataIndex, packet->image.block);
        if (packet->header.length) packet->header.length += 4;
        scom_send(packet);
        break;
    case WRITE_EEPROM:
        packet->image.offset = 0;
        packet->header.length = nvm_write_eeprom(packet->header.dataIndex, &packet->image.block, packet->header.length - 4);
        if (packet->header.length) packet->header.length += 4;
        scom_send(packet);
        break;
    }
}

static void cmd_service(pPPACKET packet)
{
    (void)packet;
}

static void cmd_test(pPPACKET packet)
{
    switch (packet->header.commandID)
    {
    case INIT_TESTS:
        cmd_response_bool(packet, true);
        break;
    case IDLE_TEST:
        cmd_response_bool(packet, true);
        break;
    case ACCEL_TEST:
        cmd_response_int(packet, 3, 0);
        break;
    case BALANCE_TEST:
        cmd_response_float(packet, 1.402, 0);
        delay_ms(2);
        cmd_response_float(packet, 2.8, 1);
        delay_ms(2);
        cmd_response_float(packet, 4.24, 2);
        delay_ms(2);
        cmd_response_float(packet, 5.7, 3);
        break;
    case BUSMON_TEST:
        cmd_response_float(packet, 13.05, 0);
        break;
    case CHARGER_TEST:
        cmd_response_float(packet, 14.5, 0);
        break;
    case IGNITION_TEST:
        cmd_response_float(packet, 13.0, 0);
        break;
    case HEATER_BUS_TEST:
        cmd_response_float(packet, 0.555, 0);
        break;
    case HEATER_BANK_TEST:
        cmd_response_float(packet, 0.567, 0);
        break;
    case TEMPERATURE_TEST:
        cmd_response_float(packet, 23.45, 0);
        delay_ms(2);
        cmd_response_float(packet, 34.56, 0);
        break;
    case LED_TEST:
        cmd_response_bool(packet, true);
        break;
    case CONTACTOR_TEST:
        cmd_response_float(packet, 3.90, 0);
        break;
    case QUIESCENT_CURRENT_TEST:
        cmd_response_float(packet, 0, 0);
        break;
    case BLUETOOTH_TEST_RESULT:
        cmd_response_bool(packet, false);
        break;
    case BLUETOOTH_TEST:
        cmd_response_bool(packet, false);
        break;
    case FLASHMEM_TEST:
        cmd_response_int(packet, 0x001440EF, 0);
        break;
    case EXT_CHARGER_TEST:
        cmd_response_float(packet, 14.5, 0);
        break;
    case EXIT_SYSTEM_TESTS:
        cmd_response_bool(packet, true);
        break;
    default:
        printf("Unknown Test Command %u\n", packet->header.commandID);
        break;
    }
}

static void cmd_debug(pPPACKET packet)
{
    (void)packet;
}

static void cmd_calib(pPPACKET packet)
{
    (void)packet;
}
