/*
 * systick.c
 *
 * Created: 5/7/2020 1:57:33 PM
 *  Author: BehroozH
 */ 
#include <atmel_start.h>
#include <stdint.h>
#include "systick.h"
#include "timing.h"

volatile uint64_t sys_time;

static void timer_0_systick_cb (const struct timer_task *const timer_task)
{
	CRITICAL_SECTION_ENTER();
	sys_time++;
	CRITICAL_SECTION_LEAVE();
}

static struct timer_task timer_0_systick;
void HAL_SYSTICK_Initialize (void)
{
	timer_0_systick.interval = 1;
	timer_0_systick.cb       = timer_0_systick_cb;
	timer_0_systick.mode     = TIMER_TASK_REPEAT;

	timer_add_task(&TIMER_0, &timer_0_systick);
}

void HAL_SYSTICK_Start (void)
{
	timer_start(&TIMER_0);
}

uint64_t HAL_SYSTICK_GetTicks(void)
{
	uint64_t ret;

	CRITICAL_SECTION_ENTER();
	ret = sys_time;
	CRITICAL_SECTION_LEAVE();

	return ret;
}

void HAL_SYSTICK_Update(uint64_t ticks)
{
	// increment the systick timer
	CRITICAL_SECTION_ENTER();
	sys_time += ticks;
	CRITICAL_SECTION_LEAVE();
}

void HAL_SYSTICK_Stop()
{
	 timer_stop(&TIMER_0);
}

void Nop()
{
	
}

