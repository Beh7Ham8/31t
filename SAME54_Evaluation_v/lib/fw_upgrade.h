/*
 * fw_upgrade.h
 *
 * Created: 6/16/2020 10:22:29 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef FW_UPGRADE_H_
#define FW_UPGRADE_H_

#include <atmel_start.h>
#include "packet.h"

#define FLASHA_SIZE ((FLASH_SIZE / 2) - (4 * NVMCTRL_BLOCK_SIZE))
#define FLASHA_ADDR 0x00000000
#define FLASHB_ADDR 0x00080000

#define MAGIC_WORD  0x57E1B9CA
#define HDR_SIZE    NVMCTRL_PAGE_SIZE
#define PAGE_SIZE   NVMCTRL_PAGE_SIZE
#define BLOCK_SIZE  NVMCTRL_BLOCK_SIZE
#define BLOCK_PAGES (BLOCK_SIZE / PAGE_SIZE)

struct ImageHeader {
    uint32_t magic1;    // magic word
    uint32_t magic2;    // bitwise complement of the magic word
    uint32_t version;   // header version (=0)
    uint32_t reserve1;
    uint32_t h_offset;  // header offset on target board
    uint32_t h_length;  // header length (=HDR_SIZE)
    uint32_t h_chksum;  // header checksum
    uint32_t reserve2;
    uint32_t f_offset;  // image offset (=0)
    uint32_t f_length;  // image length (not includeing header)
    uint32_t f_chksum;  // image checksum (not including header)
    uint32_t reserve3;
    uint32_t reserve4[4];
    uint8_t tlv_info[HDR_SIZE - 64];
};

void Init_FW(void);
void fw_image_get(pIPACKET packet);
void fw_upgrade_start(pIPACKET packet);
void fw_upgrade_write(pIPACKET packet);
void fw_upgrade_end(pIPACKET packet);
void fw_image_reset(pIPACKET packet);

#endif /* FW_UPGRADE_H_ */