/*
 * cfg_device.c
 *
 * Created: 6/24/2020 4:36:25 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include <string.h>
#include <atmel_start.h>
#include <hpl_user_area.h>
#include "cfg_device.h"
#include "serial_comm.h"

/************************************************************************
 * This SmartEEPROM space makes a portion of the NVM appear like an
 * EEPROM. It is mapped to the address 0x44000000 and not accessible
 * from NVM main address. Its size is configurable to 512B, 1KB, 2KB,
 * 4KB, 8KB, 16KB, 32KB, 64KB by factory configuration. The functions
 * below only supports the size of 512B, 1KB, 2KB, 4KB.
 * Trying to read or write outside of the boundaries throws an hardfault
 * exception.
 ************************************************************************/
uint16_t cfg_eeprom_size(void)
{
    uint8_t block = hri_nvmctrl_read_SEESTAT_SBLK_bf(NVMCTRL);
    uint8_t pages = hri_nvmctrl_read_SEESTAT_PSZ_bf(NVMCTRL);
    if (block == 1)
    {
        switch (pages)
        {
        case 0: return 512;
        case 1: return 1024;
        case 2: return 2048;
        case 3: return 4096;
        }
    }
    return 0;
}

void *cfg_eeprom_addr(void)
{
    if (cfg_eeprom_size() > 0)
        return (void *)SEEPROM_ADDR;
    else
        return NULL;
}

bool cfg_eeprom_isbusy(void)
{
    return !!hri_nvmctrl_get_SEESTAT_BUSY_bit(NVMCTRL);
}

bool cfg_eeprom_islocked(void)
{
    /* NOTE: Trying to write the locked eeprom throws an hardfault exception */
    return !!hri_nvmctrl_get_SEESTAT_LOCK_bit(NVMCTRL);
}

void cfg_eeprom_lock(void)
{
    hri_nvmctrl_write_CTRLB_reg(NVMCTRL, 0xA534);
    while (!hri_nvmctrl_get_SEESTAT_LOCK_bit(NVMCTRL));
}

void cfg_eeprom_unlock(void)
{
    hri_nvmctrl_write_CTRLB_reg(NVMCTRL, 0xA535);
    while (hri_nvmctrl_get_SEESTAT_LOCK_bit(NVMCTRL));
}

bool cfg_eeprom_read(uint16_t offset, uint8_t *data, uint16_t size)
{
    if ((offset + size) <= cfg_eeprom_size())
    {
        volatile uint8_t *ptr = (uint8_t *)cfg_eeprom_addr();
        ptr += offset;
        while (size-- > 0)
        {
            while (cfg_eeprom_isbusy());
            *data++ = *ptr++;
        }            
        return true;
    }
    return false;
}

bool cfg_eeprom_write(uint16_t offset, uint8_t *data, uint16_t size)
{
    if ((offset + size) <= cfg_eeprom_size())
    {
        volatile uint8_t *ptr = (uint8_t *)cfg_eeprom_addr();
        ptr += offset;
        while (size-- > 0)
        {
            while (cfg_eeprom_isbusy());
            *ptr++ = *data++;
        }            
        return true;
    }
    return false;
}

void cfg_eeprom_get(pIPACKET packet)
{
    uint16_t size = cfg_eeprom_size();
    packet->header.length = 4 + FLASH_PAGE_SIZE;
    packet->image.offset = 0;
    while (size > 0)
    {
        cfg_eeprom_read(packet->image.offset, packet->image.block, FLASH_PAGE_SIZE);
        scom_send(packet);
        while (!scom_ready()) delay_ms(1);
        packet->image.offset += FLASH_PAGE_SIZE;
        size -= FLASH_PAGE_SIZE;
    }
}

void cfg_eeprom_set(pIPACKET packet)
{
    bool locked = cfg_eeprom_islocked();
    uint16_t size = packet->header.length - 4;
    if (locked) cfg_eeprom_unlock();
    cfg_eeprom_write(packet->image.offset, packet->image.block, size);
    if (locked) cfg_eeprom_lock();
    packet->header.length = 4;
    scom_send(packet);
}

/************************************************************************
 * Each device has a unique 128-bit serial number. The uniqueness of the
 * serial number is guaranteed only when using all 128 bits.
 ************************************************************************/
void cfg_device_sn(uint32_t data[4])
{
    data[0] = *((uint32_t *)0x00806010);
    data[1] = *((uint32_t *)0x00806014);
    data[2] = *((uint32_t *)0x00806018);
    data[3] = *((uint32_t *)0x008061FC);
}

void cfg_device_reset(pIPACKET packet)
{
    if (packet)
    {
        packet->header.length = 0;
        scom_send(packet);
        delay_ms(2000);
    }
    NVIC_SystemReset();
}

void cfg_device_get(pIPACKET packet)
{
    packet->header.length = 4 + 16;
    packet->image.offset = cfg_eeprom_size();
    cfg_device_sn((uint32_t *)packet->image.block);
    scom_send(packet);
}

/************************************************************************
 * This factory space has 512 bytes and can be read at address 0x00804000.
 * This space contains production defined startup configuration. The
 * first eight 32-bit words (32 Bytes) is reserved, and used during the
 * NVMCTRL start-up to automatically configure the device. The remaining
 * 480 Bytes can be used for storing production parameters.
 ************************************************************************/
#define FACTORY_ADDR    0x00804000
#define FACTORY_SIZE    FLASH_USER_PAGE_SIZE /* = 512 */

static DEV_CFG device_config;
static bool device_inited = 0;
const pDEV_CFG cfg_device_config(void)
{
    if (device_inited == 0)
    {
        uint32_t data[FACTORY_SIZE / 4];
        memset(&device_config, 0, sizeof(device_config));
        if (cfg_factory_read((uint8_t *)data))
        {
            int i, n = (FLASH_USER_PAGE_SIZE - sizeof(device_config)) / 4;
            for (i = 8; i < n; i++)
            {
                if (data[i] == CFG_MAGIC_WORD)
                {
                    memcpy(&device_config, (uint8_t *)&data[i], sizeof(device_config));
                    device_inited = 1;
                    break;
                }
            }
        }
#if 1 // DEBUG
        if (device_inited)
        {
            printf("\n===== Device Configuration Data =====\n");
            printf("  System SN: %-*.*s\n", 16, 16, device_config.systemSerialNumber);
            printf("  System PN: %-*.*s\n", 16, 16, device_config.systemPartNumber);
            printf("    Base PN: %-*.*s\n", 16, 16, device_config.basePartNumber);
            printf("    Base SN: %-*.*s\n", 16, 16, device_config.baseSerialNumber);
            printf("  Contactor: %-*.*s\n", 16, 16, device_config.contactorSerialNumber);
            printf("     PCB SN: %-*.*s\n", 16, 16, device_config.pcbSerialNumber);
            for (int i = 0; i < device_config.numCells; i++)
            {
                printf("     Cell %d: %lu\n", i + 1, device_config.cellSerialNumbers[i]);
            }
            printf("\n");
        }
#endif
    }
    return &device_config;
}

bool cfg_factory_read(uint8_t data[512])
{
    int32_t ret;
    CRITICAL_SECTION_ENTER();
    ret = _user_area_read((const void *)FACTORY_ADDR, 0, data, 512);
    CRITICAL_SECTION_LEAVE();
    return (ret == ERR_NONE);
}

bool cfg_factory_write(const uint8_t data[512])
{
    int32_t ret;
    CRITICAL_SECTION_ENTER();
    ret = _user_area_write((void *)FACTORY_ADDR, 0, data, 512);
    CRITICAL_SECTION_LEAVE();
    return (ret == ERR_NONE);
}

void cfg_factory_get(pIPACKET packet)
{
    packet->header.length = 4 + FACTORY_SIZE;
    packet->image.offset = 0;
    cfg_factory_read(packet->image.block);
    scom_send(packet);
}

void cfg_factory_set(pIPACKET packet)
{
    cfg_factory_write(packet->image.block);
    packet->header.length = 0;
    scom_send(packet);
}
