/*
 * serial_comm.c
 *
 * Created: 6/16/2020 10:22:46 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include <string.h>
#include <atmel_start.h>
#include <hal/usb_cdc.h>
#include "packet.h"
#include "serial_comm.h"
#include "cmd_agent.h"
#include "hci_tl_interface.h"

enum SCOM_STATE {
    WAITING_FOR_START_BYTE,
    WAITING_FOR_END_BYTE,
    WAITING_FOR_ESCAPED_CHAR
};

static FFRAME rx_frame;
static uint16_t rx_length;
static uint16_t rx_state;
static FFRAME tx_frame __attribute__((aligned(32)));
static RFRAME raw_frame;

static bool scom_read(uint8_t c);
static void scom_parse(void);

void Init_SCOMM(void)
{
    rx_state = WAITING_FOR_START_BYTE;
}

void SCOMM_Monitor(void)
{
    if (HAL_CDC_Peak() > 0)
    {
        uint8_t rxbuff[1024]; // the size equals to CONF_USB_CDCD_ACM_DATA_BULKIN_MAXPKSZ
        uint16_t i, len = HAL_CDC_Read(rxbuff, sizeof(rxbuff));
        //printf("RX %u\n", len);
        for (i = 0; i < len; i++)
        {
            if (scom_read(rxbuff[i]))
            {
                scom_parse();
                // restart to search the next packet
                rx_state = WAITING_FOR_START_BYTE;
                
            }
            else if (rx_length >= sizeof(rx_frame))
            {
                // packet size overflow, reset to search the next packet
                rx_state = WAITING_FOR_START_BYTE;
            }
        }
    }
}

bool scom_send(void *packet)
{
    uint16_t len;
    pHEADER header = packet;

    if (header->classID == 0xFF)
    {
        pBPACKET ble = packet;
        raw_frame.frame.classPlusCmdID = 0xFF;
        raw_frame.frame.typePlusIndex = 0xFF;
        raw_frame.frame.length = HEADER_SIZE + header->length;
        raw_frame.frame.checksum = 0;
        memcpy(raw_frame.frame.payload, ble->packet, header->length);
    }
    else if (header->classID != NVMEM_COMMANDS)
    {
        if (!pkt_param2frame(packet, &raw_frame)) return false;
    }
    else
    {
        if (!pkt_image2frame(packet, &raw_frame)) return false;
    }
    pkt_calculatesum(&raw_frame);
    len = pkt_encode(&raw_frame, &tx_frame);
    //printf("TX %u\n", len);
    if (len)
        return (HAL_CDC_Send(&tx_frame, len) > 0);
    else
        return false;
}

bool scom_ready(void)
{
    return HAL_CDC_Ready();
}

static bool scom_read(uint8_t c)
{
    //printf("%02X ", c);
    switch (rx_state)
    {
    case WAITING_FOR_START_BYTE:
        if (c == PACKET_END)
        {
            rx_state = WAITING_FOR_END_BYTE;
            rx_frame.bytes[0] = c;
            rx_length = 1;
            // No further processing is necessary
            return false;
        }
        break;

    case WAITING_FOR_END_BYTE:
        rx_frame.bytes[rx_length++] = c;
        if (c == PACKET_END)
        {
            // found the end of the packet, so
            rx_state = WAITING_FOR_START_BYTE;
            //printf("\n");
            return (rx_length > 2);
        }
        break;
    }
    return false;
}

static void scom_parse(void)
{
    // remove escape characters and validate packet by checking sum
    if (pkt_decode(&rx_frame, rx_length, &raw_frame) && pkt_checksum(&raw_frame))
    {
        if (raw_frame.frame.classPlusCmdID == 0xFF && raw_frame.frame.typePlusIndex == 0xFF)
        {
            /* This is a BlueNRG packet */
            hci_tl_lowlevel_receive(&(raw_frame.frame));
        }
        else if ((raw_frame.frame.classPlusCmdID >> 5) != NVMEM_COMMANDS)
        {
            PPACKET packet;
            if (pkt_frame2param(&raw_frame, &packet))
            {
                // handle a param packet
                cmd_agent(&packet);
            }
        }
        else
        {
            IPACKET packet;
            if (pkt_frame2image(&raw_frame, &packet))
            {
                // handle a firmware image packet
                cmd_agent(&packet);
            }
        }
    }
}
