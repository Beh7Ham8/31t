#ifndef _LIB_HEXDUMP_H_
#define _LIB_HEXDUMP_H_


void hexdump (const void *buf, int len);
void hexdump_line (const unsigned char *ptr, const unsigned char *org, int len);


#endif /* _LIB_HEXDUMP_H_ */
