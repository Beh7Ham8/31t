/*
 * stateMachine.c
 *
 * Created: 5/7/2020 3:29:49 PM
 *  Author: BehroozH
 */ 
#include "stateMachine.h"



void stateInit(pSTATE_MACHINE state, const pStateFunc *StateVectorArray, int num_states, int start_state)
{
	state->isInitialized = 0x55;
	state->StateVectorArray = StateVectorArray;
	state->num_states = num_states;
	state->forceRenterState = 0;

	/* monitor for illegal state transition */
	if ((start_state <= 0) || (start_state >= state->num_states))
	start_state = 0;

	state->prevState = start_state;
	state->currentState = start_state;
	state->nextState = start_state;
	state->timer = 0;

	state->StateVectorArray[state->currentState](evENTER_STATE, 0);
}

void enter_state(pSTATE_MACHINE state, int new_state)
{
	/* monitor for uninitialized state machine */
	if (state->isInitialized != 0x55)
	return;

	/* monitor for illegal state transition */
	if ((new_state <= 0) || (new_state >= state->num_states))
	new_state = 0;

	state->prevState = state->currentState;
	state->currentState = new_state;
	state->nextState = new_state;
	state->timer = 0;
	state->forceRenterState = 0;

	state->StateVectorArray[state->currentState](evENTER_STATE, 0);
}

int event_handler(pSTATE_MACHINE state, int event, void *param)
{
	volatile int rtn;

	/* Default to NO STATE CHANGE */
	rtn = 0;
	
	if (state == 0)
	return 0;

	/* monitor for uninitialized state machine */
	if (state->isInitialized != 0x55)
	return (0);

	/* check for valid state */
	if ((state->currentState < 0) || (state->currentState >= state->num_states))
	return (0);

	switch (event)
	{
		case evENTER_STATE:
		break;
		case evTIMER_TICK:
		//            state->timer = ((state->timer + (*((long *)param))) > 0) ? state->timer + (*((long *)param)) : state->timer;
		state->StateVectorArray[state->currentState](evTIMER_TICK, param);
		break;
		case evEXIT_STATE:
		state->StateVectorArray[state->currentState](evEXIT_STATE, param);
		break;
		default:
		state->StateVectorArray[state->currentState](event, param);
		break;
	}

	state->state_changed = 0;

	if (state->nextState != state->currentState || state->forceRenterState)
	{
		/* Exit current state */
		state->StateVectorArray[state->currentState](evEXIT_STATE, 0);
		/* Enter next state */
		enter_state(state, state->nextState);

		/* return 1 to indicate state changed */
		rtn = 1;
		state->state_changed = 1;

	}

	return (rtn);
}

/*** Extended state machine allow multiple states ***/
void stateInit_ext(pSTATE_MACHINE state, const pStateFunc_ext *StateVectorArray, int num_states, int start_state)
{
	state->isInitialized = 0x55;
	state->StateVectorArray_ext = StateVectorArray;
	state->num_states = num_states;
	state->currentState = start_state;
	enter_state_ext(state, start_state);
}

void enter_state_ext(pSTATE_MACHINE state, int new_state)
{
	/* monitor for uninitialized state machine */
	if (state->isInitialized != 0x55)
	return;
	/* monitor for illegal state transition */
	if ((new_state <= 0) || (new_state >= state->num_states))
	new_state = 0;

	state->prevState = state->currentState;
	state->currentState = new_state;
	state->nextState = new_state;
	state->timer = 0;

	state->StateVectorArray_ext[state->currentState](state, evENTER_STATE, 0);
}

void event_handler_ext(pSTATE_MACHINE state, int event, int param)
{
	/* monitor for uninitialized state machine */
	if (state->isInitialized != 0x55)
	return;

	/* check for valid state */
	if ((state->currentState < 0) || (state->currentState >= state->num_states))
	return;

	switch (event)
	{
		case evENTER_STATE:
		break;
		case evTIMER_TICK:
		state->timer = ((state->timer + param) > 0) ? state->timer + param : state->timer;
		state->StateVectorArray_ext[state->currentState](state, evTIMER_TICK, param);
		break;
		case evEXIT_STATE:
		state->StateVectorArray_ext[state->currentState](state, evEXIT_STATE, param);
		break;
		default:
		state->StateVectorArray_ext[state->currentState](state, event, param);
		break;
	}

	if (state->nextState != state->currentState)
	{
		/* Exit current state */
		state->StateVectorArray_ext[state->currentState](state, evEXIT_STATE, 0);
		/* Enter next state */
		enter_state_ext(state, state->nextState);
	}
}

void do_nothing(void)
{
}
