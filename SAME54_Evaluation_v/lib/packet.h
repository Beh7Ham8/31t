#ifndef __PACKET_H__
#define __PACKET_H__

#include <stdint.h>
#include <stdbool.h>
#include "packet_def.h"

#define HEADER_SIZE     5
#define CHECKSUM_OFFSET 4
#define NVM_BLOCK_SIZE  512
#define NVM_PACKET_SIZE (HEADER_SIZE + 4 + NVM_BLOCK_SIZE)
#define MAX_PACKET_SIZE 1024

typedef struct Frame {
    uint8_t  classPlusCmdID;
    uint8_t typePlusIndex;
    uint16_t length;
    uint8_t checksum;
    uint8_t payload[0];
} __attribute__((packed)) FRAME, *pFRAME;

typedef union FullFrame {
    FRAME   frame;
    uint8_t bytes[MAX_PACKET_SIZE];	
} FFRAME, *pFFRAME;

typedef union RawFrame {
    FRAME   frame;
    uint8_t bytes[MAX_PACKET_SIZE];
} RFRAME, *pRFRAME;

typedef struct Header {
    uint8_t classID;
    uint8_t commandID;
    uint8_t dataType;
    uint8_t dataIndex;
    uint16_t length;
} HEADER, *pHEADER;

typedef union Param {
    uint32_t iVal;
    float	 fVal;
    uint8_t  bytes[4];
} PARAM, *pPARAM;

typedef struct PacketParam {
    HEADER  header;
    PARAM   param;
} PPACKET, *pPPACKET;

typedef struct Image {
    uint32_t offset;
    uint8_t  block[NVM_BLOCK_SIZE];
} IMAGE, *pIMAGE;

typedef struct PacketImage {
    HEADER  header;
    IMAGE   image;
} IPACKET, *pIPACKET;

typedef struct PacketBluetooth {
    HEADER  header;
    uint8_t *packet;
} BPACKET, *pBPACKET;

uint16_t pkt_encode(pRFRAME src, pFFRAME dest);
bool pkt_decode(pFFRAME src, uint16_t srclen, pRFRAME dest);
void pkt_calculatesum(pRFRAME frame);
bool pkt_checksum(pRFRAME frame);
bool pkt_frame2param(pRFRAME frame, pPPACKET packet);
bool pkt_frame2image(pRFRAME frame, pIPACKET packet);
bool pkt_param2frame(pPPACKET packet, pRFRAME frame);
bool pkt_image2frame(pIPACKET packet, pRFRAME frame);

#endif
