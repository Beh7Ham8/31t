/*
 * systick.h
 *
 * Created: 5/7/2020 1:56:40 PM
 *  Author: BehroozH
 */ 


#ifndef SYSTICK_H_
#define SYSTICK_H_

extern volatile uint64_t sys_time;

void HAL_SYSTICK_Initialize(void);
void HAL_SYSTICK_Start(void);
uint64_t HAL_SYSTICK_GetTicks(void);

void HAL_SYSTICK_Update(uint64_t ticks);
void HAL_SYSTICK_Stop();

#endif /* SYSTICK_H_ */