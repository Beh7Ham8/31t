/*
 * timing.h
 *
 * Created: 5/7/2020 1:39:08 PM
 *  Author: BehroozH
 */ 


#ifndef SW_TIMING_H_
#define SW_TIMING_H_

#ifdef SW_TIMING_C
#define SW_TIMING_GLOBAL
#else
#define SW_TIMING_GLOBAL extern
#endif

#include <stdint.h>
#include <stdbool.h>

#define TIMER_EXPIRED   0

// Timer period is ms
#define ONE_MILLISECOND 1
#define FIVE_MILLISECOND 5

#define TEN_MILLISECOND 10
#define TWENTY_MILLISECOND 20
#define ONEH_MILLISECOND 100
#define FIVEH_MILLISECOND 500
#define QUARTER_SEC     250
#define ONE_SECOND      1000
#define TWO_SECONDS     2000
#define THREE_SECONDS   3000
#define FOUR_SECONDS    4000
#define TEN_SECONDS   	10000
#define THIRTY_SECONDS  30000

#define FIVE_SECONDS    5000        // 5 * ONE_SECOND
#define SIX_SECONDS     6000        // 6 * ONE_SECOND
#define TWENTY_SECONDS  20000       // 5 * ONE_SECOND
#define ONE_MINUTE      60000       // 60*ONE_SECOND
#define TWO_MINUTES     120000      // 2 * ONE_MINUTE
#define THREE_MINUTES   180000      // 3*ONE_MINUTE
#define FIVE_MINUTES    300000      // 5*ONE_MINUTE
#define TEN_MINUTES     600000      // 10*ONE_MINUTE
#define TWENTY_MINUTES  120000      // 20*ONE_MINUTE
#define THIRTY_MINUTES  1800000     // 30*ONE_MINUTE
#define ONE_HOUR        3600000     // 2*THIRTY_MINUTES
#define FIVE_HOURS      18000000    // 5*ONE_HOUR

typedef struct _SW_TIMER
{
    uint64_t period;
    uint64_t reference;
    bool     timerIsActive;
}
SW_TIMER, *pSW_TIMER;

// function prototypes
void timerInit (pSW_TIMER tmr, uint64_t period);
bool timerExpired (pSW_TIMER tmr);
uint64_t timerGetTime (pSW_TIMER tmr);
void timerReload (pSW_TIMER tmr);
void timerReset (pSW_TIMER tmr);
void timerDelay (uint64_t period);

void timerSetActive (pSW_TIMER tmr, bool active);
bool timerIsActive(pSW_TIMER tmr);

#endif
