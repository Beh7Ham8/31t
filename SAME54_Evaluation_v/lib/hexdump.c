#include <stdio.h>
#include <string.h>
#include <ctype.h>


void hexdump_line (const unsigned char *ptr, const unsigned char *org, int len)
{
	if ( org )
		printf("%04x: ", (unsigned)(ptr - org));

	int i;
	for ( i = 0; i < len; i++ )
		printf("%02x ", ptr[i]);

	for ( ; i < 16; i++ )
		printf("   ");

	for ( i = 0; i < len; i++ )
		printf("%c", isprint(ptr[i]) ? ptr[i] : '.');
	printf("\r\n");
}

void hexdump (const void *buf, int len)
{
	const unsigned char *ptr = (const unsigned char *)buf;

	while ( len >= 16 )
	{
		hexdump_line(ptr, (const unsigned char *)buf, 16);
		ptr += 16;
		len -= 16;
	}
	if ( len )
		hexdump_line(ptr, (const unsigned char *)buf, len);
}

