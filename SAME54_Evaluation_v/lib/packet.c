/*
 * packet.c
 *
 * Created: 6/17/2020 09:22:14 AM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include "packet.h"

uint16_t pkt_encode(pRFRAME src, pFFRAME dest)
{
    uint16_t i, j = 0;
    uint16_t len = src->frame.length;

    if (src == 0 || dest == 0) return 0;
    dest->bytes[j] = PACKET_END;

    for (i = 0; i < len; i++)
    {
        uint8_t c = src->bytes[i];

        if (c == PACKET_END)
        {
            if ((j + 2) >= sizeof(FFRAME)) return 0;
            dest->bytes[++j] = BYTE_ESC;
            dest->bytes[++j] = BYTE_ESC_END;
        }
        else if (c == BYTE_ESC)
        {
            if ((j + 2) >= sizeof(FFRAME)) return 0;
            dest->bytes[++j] = BYTE_ESC;
            dest->bytes[++j] = BYTE_ESC_ESC;
        }
        else
        {
            if ((j + 1) >= sizeof(FFRAME)) return 0;
            dest->bytes[++j] = c;
        }
    }

    if ((j + 1) >= sizeof(FFRAME)) return 0;
    dest->bytes[++j] = PACKET_END;

    return (j + 1);
}

bool pkt_decode(pFFRAME src, uint16_t length, pRFRAME dest)
{
    uint16_t i, j;
    bool beginningFound = false;
    bool escapeChar = false;

    for (i = j = 0; i < length; i++) {
        uint8_t c = src->bytes[i];

        if (c == PACKET_END)
        {
            // End (or beginning) of a packet
            // If we have not decoded any data, this is the beginning
            if (j == 0)
            {
                // beginning of the packet
                beginningFound = 1;
                continue;
            }
            else
            {
                // we have already decoded some data, so this is the
                // end of the packet....
                // valid packet decoded
                return (j >= HEADER_SIZE && j == dest->frame.length);
            }
        }
        else if (escapeChar)
        {
            if (c == BYTE_ESC_END)
                c = PACKET_END;
            else if (c == BYTE_ESC_ESC)
                c = BYTE_ESC;
            else {
                //TODO:
                // ESC should only be used to escape ESC_END and ESC_ESC
                // so if we get here, warn the user
                // Then again, this is an embedded application, so we can
                // just ignore the ESC character.
                // i.e. use the escape sequence to escape nothing?
            }
            escapeChar = false;
        }
        else if (c == BYTE_ESC)
        {
            // If we receive an escape char, read the next char
            escapeChar = true;
            continue;
        }

        // If we get this far the info is data so add it to the buffer
        if (beginningFound)
        {
            // Move the pointer to the current packet
            dest->bytes[j++] = c;
        }
    }
    // we ran out of bytes and we did not find the end of the packet
    return false;
}

void pkt_calculatesum(pRFRAME frame)
{
    uint16_t i, len = frame->frame.length;
    uint8_t sum = 0xFF;
    for (i = 0; i < len; i++)
    {
        if (i != CHECKSUM_OFFSET) sum ^= frame->bytes[i];
    }
    frame->bytes[CHECKSUM_OFFSET] = sum;
}

bool pkt_checksum(pRFRAME frame)
{
    uint16_t i, len = frame->frame.length;
    uint8_t sum = frame->bytes[0];
    for (i = 1; i < len; i++)
    {
        sum ^= frame->bytes[i];
    }
    return (sum == 0xFF);
}

bool pkt_frame2param(pRFRAME frame, pPPACKET packet)
{
    if (frame->frame.length == (HEADER_SIZE + sizeof(PARAM)))
    {
        uint16_t i;
        packet->header.classID = frame->frame.classPlusCmdID >> 5;
        packet->header.commandID = frame->frame.classPlusCmdID & 0x1F;
        packet->header.dataType = frame->frame.typePlusIndex >> 5;
        packet->header.dataIndex = frame->frame.typePlusIndex & 0x1F;
        packet->header.length = frame->frame.length - HEADER_SIZE;
        for (i = 0; i < sizeof(PARAM); i++) packet->param.bytes[i] = frame->frame.payload[i];
        return true;
    }
    return false;
}

bool pkt_frame2image(pRFRAME frame, pIPACKET packet)
{
    if (frame->frame.length >= HEADER_SIZE && frame->frame.length <= NVM_PACKET_SIZE)
    {
        packet->header.classID = frame->frame.classPlusCmdID >> 5;
        packet->header.commandID = frame->frame.classPlusCmdID & 0x1F;
        packet->header.dataType = frame->frame.typePlusIndex >> 5;
        packet->header.dataIndex = frame->frame.typePlusIndex & 0x1F;
        packet->header.length = frame->frame.length - HEADER_SIZE;
        if (packet->header.length > 0)
        {
            int i, len;
            packet->image.offset = frame->frame.payload[0] & 0x0FF; // in little endian
            packet->image.offset |= ((uint32_t)frame->frame.payload[1] & 0x0FF) << 8;
            packet->image.offset |= ((uint32_t)frame->frame.payload[2] & 0x0FF) << 16;
            packet->image.offset |= ((uint32_t)frame->frame.payload[3] & 0x0FF) << 24;
            len = packet->header.length - 4;
            for (i = 0; i < len; i++)
            {
                packet->image.block[i] = frame->frame.payload[i + 4];
            }
        }
        return true;
    }
    return false;
}

bool pkt_param2frame(pPPACKET packet, pRFRAME frame)
{
    uint16_t i;
    frame->frame.classPlusCmdID = (packet->header.classID << 5) | (packet->header.commandID & 0x1F);
    frame->frame.typePlusIndex = (packet->header.dataType << 5) | (packet->header.dataIndex & 0x1F);
    frame->frame.length = HEADER_SIZE + sizeof(PARAM);
    frame->frame.checksum = 0;
    for (i = 0; i < sizeof(PARAM); i++) frame->frame.payload[i] = packet->param.bytes[i];
    return true;
}

bool pkt_image2frame(pIPACKET packet, pRFRAME frame)
{
    frame->frame.classPlusCmdID = (packet->header.classID << 5) | (packet->header.commandID & 0x1F);
    frame->frame.typePlusIndex = (packet->header.dataType << 5) | (packet->header.dataIndex & 0x1F);
    frame->frame.length = HEADER_SIZE + packet->header.length;
    frame->frame.checksum = 0;
    if (packet->header.length > 0)
    {
        int i, len;
        frame->frame.payload[0] = packet->image.offset & 0xFF;
        frame->frame.payload[1] = (packet->image.offset >> 8) & 0xFF;
        frame->frame.payload[2] = (packet->image.offset >> 16) & 0xFF;
        frame->frame.payload[3] = (packet->image.offset >> 24) & 0xFF;
        len = packet->header.length - 4;
        for (i = 0; i < len; i++)
        {
            frame->frame.payload[i + 4] = packet->image.block[i];
        }
    }
    return true;
}
