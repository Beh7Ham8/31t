/*
 * system_init.h
 *
 * Created: 9/10/2020 3:50:36 PM
 */ 


#ifndef SYSTEM_INIT_H_
#define SYSTEM_INIT_H_

void Init_WatchDog();
void WDT_Handler();
void Disable_WatchDog();
void Enable_WatchDog();

void App_Monitor();

#endif /* SYSTEM_INIT_H_ */