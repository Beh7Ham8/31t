#include <stdio.h>
#include <string.h>
#include "atmel_start.h"
#include "hal/spi.h"
#include "SST26VFxxx.h"

#define Dly(ms) delay_ms(ms)

// Enough to program a single page + command/addr bytes = 256 + 4 (cmd + 3 addr bytes)
static uint8_t spiTxBuffer[MAX_SPI_TX_BUFFER_SIZE];

// Enough to read an entire 8K block
static uint8_t spiRxBuffer[512];  //8 * 1024];

static uint16_t spiTxIdx;

static inline void addToSPITxBuffer(uint8_t data) {
    spiTxBuffer[(spiTxIdx++) % MAX_SPI_TX_BUFFER_SIZE] = data;
}

FLASH_CHIP spiFlashChip;

void Flash_Reset(void) {
    /* Enable Reset */
    spiTxIdx = 0;
    addToSPITxBuffer(RSTEN);
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 1);

    /* Reset Flash */
    spiTxIdx = 0;
    addToSPITxBuffer(RST5);
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 1);
    delay_ms(300);
}

bool Flash_GetID(void) {
    bool status = false;

    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(JEDECID);

    addToSPITxBuffer(0);
    addToSPITxBuffer(0);
    addToSPITxBuffer(0);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 4);

    // Fill struct with received ID
    spiFlashChip.manufacturer = spiRxBuffer[1];
    spiFlashChip.deviceType = spiRxBuffer[2];
    spiFlashChip.deviceID = spiRxBuffer[3];

    if ((spiFlashChip.manufacturer      ==  SPI_FLASH_MANUFACTURER_ID) &&
        (spiFlashChip.deviceType    ==  SPI_FLASH_DEVICE_TYPE) &&
        (spiFlashChip.deviceID      ==  SPI_FLASH_DEVICE_ID )) {
        status = true;
    }

    return status;
}

void Flash_UnlockAll(void) {
    // Datasheet says to enable write before global block protection unlock
    Flash_EnableWriteAndDone();

    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(ULBPR);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 1);
}

void Flash_EnableWrite(void) {
    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(WREN);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 1);
}

void Flash_DisableWrite(void) {
    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(WRDI);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 1);
}

uint8_t Flash_GetStatus(void) {
    uint8_t status = 0;
    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(RDSR);
    addToSPITxBuffer(0);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 2);

    status = spiRxBuffer[1];

    return (status);
}

uint8_t Flash_GetConfig(void) {
    uint8_t status = 0;
    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(RDCR);
    addToSPITxBuffer(0);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 2);

    status = spiRxBuffer[1];

    return (status);
}

uint8_t Flash_GetBlockFlags(void) {
    uint8_t i, status = 0;
    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add Read command
    addToSPITxBuffer(RBPR);

    memset(spiRxBuffer, 0, 256);

    for (i = 0; i < 18; i++) {
        addToSPITxBuffer(0);
    }

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, spiTxIdx);

    for (i = 1; i <= 18; i++) {
        status |= spiRxBuffer[i];
    }

    return (status);
}

bool Flash_EraseSector(uint32_t sectorNum) {
    bool status = true;
    uint32_t sectorAddr;

    // Reset buffer index
    spiTxIdx = 0;

    // compute the sector address
    // Within each sector there are 4k bytes,
    // so the actual sector address is sectorNum*4K
    sectorAddr = sectorNum * MAX_BYTE_COUNT_PER_SECTOR;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(SE11);

    addToSPITxBuffer((sectorAddr & 0x00FF0000) >> 16);
    addToSPITxBuffer((sectorAddr & 0x0000FF00) >> 8);
    addToSPITxBuffer((sectorAddr & 0x000000FF));

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, 4);

    return status;
}

bool Flash_ReadPage(uint32_t pageAddr, uint8_t *buf) {
    bool status = true;
    uint16_t byteCount;

    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(READ);
    addToSPITxBuffer((pageAddr & 0x00FF0000) >> 16);
    addToSPITxBuffer((pageAddr & 0x0000FF00) >> 8);
    addToSPITxBuffer((pageAddr & 0x000000FF));

    // Add dummy bytes to get the response from the chip
    for (byteCount = 0; byteCount < MAX_BYTE_COUNT_PER_PAGE; byteCount++)
    addToSPITxBuffer(0);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, spiTxIdx);

    memcpy(buf, &spiRxBuffer[4], 256);

    return status;
}

bool Flash_ReadBytes(uint32_t pageAddr, uint8_t *buf, uint32_t bufSize)
{
    bool status = true;
    uint16_t byteCount;

    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(READ);
    addToSPITxBuffer((pageAddr & 0x00FF0000) >> 16);
    addToSPITxBuffer((pageAddr & 0x0000FF00) >> 8);
    addToSPITxBuffer((pageAddr & 0x000000FF));

    // Add dummy bytes to get the response from the chip
    for (byteCount = 0; byteCount < bufSize; byteCount++)
    addToSPITxBuffer(0);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, spiTxIdx);

    memcpy(buf, &spiRxBuffer[4], bufSize);

    return status;
}

bool Flash_WritePage(uint32_t pageAddr, uint8_t *buf) {
    bool status = true;
    uint16_t byteCount;

    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(PP);
    addToSPITxBuffer((pageAddr & 0x00FF0000) >> 16);
    addToSPITxBuffer((pageAddr & 0x0000FF00) >> 8);
    addToSPITxBuffer((pageAddr & 0x000000FF));

    // Add dummy bytes to get the response from the chip
    for (byteCount = 0; byteCount < MAX_BYTE_COUNT_PER_PAGE; byteCount++)
    addToSPITxBuffer(buf[byteCount]);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, spiTxIdx);

    return status;
}

void Flash_WriteBytes(uint32_t pageAddr, uint8_t *buf, uint8_t bufSize)
{
    uint16_t byteCount;

    // Reset buffer index
    spiTxIdx = 0;

    // Add dummy bytes for the response
    // Add JEDEC read cmd
    addToSPITxBuffer(PP);
    addToSPITxBuffer((pageAddr & 0x00FF0000) >> 16);
    addToSPITxBuffer((pageAddr & 0x0000FF00) >> 8);
    addToSPITxBuffer((pageAddr & 0x000000FF));

    // Add dummy bytes to get the response from the chip
    for (byteCount = 0; byteCount < bufSize; byteCount++)
    addToSPITxBuffer(buf[byteCount]);

    // Issue read/write operation
    HAL_SPI_WriteRead((void *) spiTxBuffer, (void *) spiRxBuffer, spiTxIdx);
}

void Flash_EnableWriteAndDone()
{
    uint8_t status;

    Flash_EnableWrite();

    status = Flash_GetStatus();

    // See if the memory writes are now enabled
    while ( (0x02 & status) != 0x02 ) {
        status = Flash_GetStatus();
        delay_ms(10);
    }
}

void Flash_WritePageAndDone(uint8_t *pData, uint32_t pageAddr)
{
    uint8_t status;

    Flash_EnableWriteAndDone();

    // This chip is write enabled, so...
    Flash_WritePage(pageAddr, pData);

    status = Flash_GetStatus();

    // Wait until the wait operation completes
    while ((0x81 & status) != 0) {
        // Done, go and test the reads (verify what we wrote)
        status = Flash_GetStatus();
        delay_ms(10);
    }
}

uint8_t Flash_WriteBytesAndDone(uint32_t pageAddr, uint8_t *buf, uint8_t bufSize)
{
    uint8_t status;

    Flash_EnableWriteAndDone();

    // This chip is write enabled, so...
    Flash_WriteBytes(pageAddr, &buf[0], bufSize);

    status = Flash_GetStatus();

    // Wait until the wait operation completes
    while ((0x81 & status) != 0) {
        // Done, go and test the reads (verify what we wrote)
        status = Flash_GetStatus();
        delay_ms(10);
    }

    return status;
}

void Flash_ReadPageAndLock(uint8_t *pData, uint32_t pageAddr)
{
    volatile uint8_t retry = 3;

    do
    {
        Flash_DisableWrite();
        Dly(10);
    } while (((--retry)> 0) && ((0x02 & Flash_GetStatus())!= 0));

    // This chip is write disabled, so...
    readPage(pageAddr, pData);
}

// Read numOfBytes from the flash
void Flash_ReadBytesAndLock(uint8_t *pData, uint32_t pageAddr, uint32_t numOfBytes)
{
    uint8_t status;

    // Make sure that the writes are disabled
    status = Flash_GetStatus();

    if ((0x02 & status)) {
        Flash_DisableWrite();

        status = Flash_GetStatus();

        // See if the memory writes are now disabled
        while ((0x02 & status) != 0) {
            status = Flash_GetStatus();
            delay_ms(10);
        }
    }

    // This chip is write disabled, so...
    Flash_ReadBytes(pageAddr, pData, numOfBytes);
}

void Flash_EraseSectorAndDone(uint32_t sectorNum) {
    uint8_t status;

    Flash_EnableWriteAndDone();

    // This chip is write enabled, so...
    Flash_EraseSector(sectorNum);

    // Delay for 1ms
    delay_ms(10);

    status = Flash_GetStatus();

    // Wait until the erase cycle completes
    while ((0x81 & status) != 0) {
        status = Flash_GetStatus();
        delay_ms(10);
    }
}

// Write a 4k sector of data
int8_t Flash_WriteSectorAndDone(uint8_t *dataBlock, uint32_t sectorNum)
{
    uint8_t pageData[MAX_BYTE_COUNT_PER_PAGE];
    uint8_t readPageData[MAX_BYTE_COUNT_PER_PAGE];
    uint8_t emptyPageData[MAX_BYTE_COUNT_PER_PAGE];
    volatile uint32_t pageNum = sectorNum * MAX_BYTE_COUNT_PER_SECTOR;
    uint32_t i;

    memset(&emptyPageData[0], 0xFF, MAX_BYTE_COUNT_PER_PAGE);

    // Loop through the dataBlock one page at a time
    for ( i = 0; i < MAX_BYTE_COUNT_PER_SECTOR; i += MAX_BYTE_COUNT_PER_PAGE )
    {
        // Copy a page's worth of data into pageData
        memcpy(&pageData[0], &dataBlock[i], MAX_BYTE_COUNT_PER_PAGE);

        // Write this page data to FLASH
        Flash_WritePageAndDone(&pageData[0], pageNum);

        // Read back the written page
        Flash_ReadPageAndLock(&readPageData[0], pageNum);

        // Check if read back is valid
        if ((memcmp(&readPageData[0], &pageData[0], MAX_BYTE_COUNT_PER_PAGE) != 0) ||
            (memcmp(&readPageData[0], &emptyPageData[0], MAX_BYTE_COUNT_PER_PAGE) == 0) )
        {
            // Try the write again
            // Write this page data to FLASH
            Flash_WritePageAndDone(&pageData[0], pageNum);

            // Read back the written page
            Flash_ReadPageAndLock(&readPageData[0], pageNum);

            // Check if read back is valid
            if ((memcmp(&readPageData[0], &pageData[0], MAX_BYTE_COUNT_PER_PAGE) != 0) ||
                (memcmp(&readPageData[0], &emptyPageData[0], MAX_BYTE_COUNT_PER_PAGE) != 0) )
            {
                // Return a bad return code
                return -1;
            }
        }

        // Increment page number
        pageNum += MAX_BYTE_COUNT_PER_PAGE;
    }

    return 0;
}

// Read a 4k sector of data
void Flash_ReadSectorAndLock(uint8_t *dataBlock, uint32_t sectorNum)
{
    uint8_t pageData[MAX_BYTE_COUNT_PER_PAGE];
    volatile uint32_t pageNum = sectorNum * MAX_BYTE_COUNT_PER_SECTOR;
    uint32_t i;

    // Loop through the dataBlock one page at a time
    for ( i = 0; i < MAX_BYTE_COUNT_PER_SECTOR; i += MAX_BYTE_COUNT_PER_PAGE )
    {
        // Read this page data to FLASH
        Flash_ReadPageAndLock(&pageData[0], pageNum);

        // Copy the page's data to the data block
        memcpy(&dataBlock[i], &pageData[0], MAX_BYTE_COUNT_PER_PAGE);

        // Increment page number
        pageNum += MAX_BYTE_COUNT_PER_PAGE;
    }
}
