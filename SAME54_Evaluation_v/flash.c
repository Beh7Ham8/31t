/*
 * flash.c
 *
 * Created: 8/31/2020 10:44:09 AM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include "atmel_start.h"
#include "lib/cfg_device.h"
#include "calendar.h"
#include "flash.h"
#include "system_definitions.h"
#include "debugMon.h"

NVM_EEPROM NVM_ConfigData;

bool Init_Nvm_ConfigData(void)
{
    return nvm_read_syscfg(&(NVM_ConfigData.syscfg));
}

bool nvm_read_datetime(NVM_SOH *data)
{
    const uint32_t offset = offsetof(NVM_EEPROM, soh);
    if (cfg_eeprom_read(offset, (uint8_t *)data, sizeof(*data)))
    {
        if (data->SOH == 0xFFFFFFFF) data->SOH = 10000000;
        return true;
    }
    return false;
}

bool nvm_write_datetime(NVM_SOH *data)
{
    const uint32_t offset = offsetof(NVM_EEPROM, soh);
    data->marker1 = 0xAA;
    data->marker2 = 0xBB;
    return cfg_eeprom_write(offset, (uint8_t *)data, sizeof(*data));
}

bool nvm_read_syscfg(SYSTEM_CONFIG *data)
{
    const uint32_t offset = offsetof(NVM_EEPROM, syscfg);
    if (cfg_eeprom_read(offset, (uint8_t *)data, sizeof(*data)))
    {
        if (data->Disable_SleepMode == 0xFF) data->Disable_SleepMode = 0;
        if (data->Disable_Balancer == 0xFF) data->Disable_Balancer = 0;
        if (data->Disable_Heater == 0xFF) data->Disable_Heater = 0;
        if (data->Disable_OV_Protection == 0xFF) data->Disable_OV_Protection = 0;
        if (data->Disable_UV_Protection == 0xFF) data->Disable_UV_Protection = 0;
        if (data->Disable_OT_Protection == 0xFF) data->Disable_OT_Protection = 0;
        if (data->Disable_SC_Protection == 0xFF) data->Disable_SC_Protection = 0;
        return true;
    }
    else
    {
        data->Disable_SleepMode = 0;
        data->Disable_Balancer = 0;
        data->Disable_Heater = 0;
        data->Disable_OV_Protection = 0;
        data->Disable_UV_Protection = 0;
        data->Disable_OT_Protection = 0;
        data->Disable_SC_Protection = 0;
        return false;
    }
}

bool nvm_write_syscfg(SYSTEM_CONFIG *data)
{
    const uint32_t offset = offsetof(NVM_EEPROM, syscfg);
    memcpy((uint8_t*)&(NVM_ConfigData.syscfg),(uint8_t*)data,sizeof(SYSTEM_CONFIG));
    return cfg_eeprom_write(offset, (uint8_t *)data, sizeof(*data));
}

bool nvm_read_dbgcfg(uint32_t *bitmaps)
{
    const uint32_t offset = offsetof(NVM_EEPROM, dbgcfg);
    return cfg_eeprom_read(offset, (uint8_t *)bitmaps, sizeof(uint32_t));
}

bool nvm_write_dbgcfg(uint32_t bitmaps)
{
    const uint32_t offset = offsetof(NVM_EEPROM, dbgcfg);
    NVM_ConfigData.dbgcfg = bitmaps;
    return cfg_eeprom_write(offset, (uint8_t *)&bitmaps, sizeof(uint32_t));
}

uint16_t nvm_read_eeprom(uint8_t index, void *data)
{
    struct calendar_date_time datetime;
    NVM_EEPROM *nvm = data;

    switch (index)
    {
    case NVM_DATETIME_IDX:
        calendar_get_date_time(&CALENDAR_0, &datetime);
        memcpy(data, &datetime, sizeof(datetime));
        return sizeof(datetime);

    case NVM_EEPROM_IDX:
        if (nvm_read_datetime(&nvm->soh) && nvm_read_syscfg(&nvm->syscfg)) {
            return sizeof(NVM_EEPROM);
        }
        break;

    case NVM_DBGCFG_IDX:
        if (nvm_read_dbgcfg(data)) return sizeof(uint32_t);
        break;

    default:
        break;
    }
    return 0;
}

uint16_t nvm_write_eeprom(uint8_t index, void *data, uint16_t length)
{
    struct calendar_date_time datetime;
    NVM_EEPROM *nvm = data;

    switch (index)
    {
    case NVM_DATETIME_IDX:
        if (length != sizeof(struct calendar_date_time)) return 0;
        memcpy(&datetime, data, sizeof(datetime));
        calendar_set_date(&CALENDAR_0, &datetime.date);
        calendar_set_time(&CALENDAR_0, &datetime.time);
        SOH_set_alarm();
        if (!Write_SOH_To_Flash(-1)) return 0;
        break;

    case NVM_EEPROM_IDX:
        if (length != sizeof(NVM_EEPROM)) return 0;
        if (!nvm_write_syscfg(&nvm->syscfg)) return 0;
        if (!Write_SOH_To_Flash(nvm->soh.SOH)) return 0;
        break;

    case NVM_DBGCFG_IDX:
        if (length != sizeof(uint32_t)) return 0;
        if (!nvm_write_dbgcfg(*((uint32_t *)data))) return 0;
        Update_Trace(*((uint32_t *)data));
        break;

    default:
        break;
    }
    return nvm_read_eeprom(index, data);
}


bool Is_SCP_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_SC_Protection == 1)
	{
	   return false;
	}
	else
	{
		return true;
	}
}  

bool Is_OVP_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_OV_Protection == 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Is_UVP_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_UV_Protection == 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Is_OTP_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_OT_Protection == 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Is_Heater_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_Heater == 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Is_Balancer_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_Balancer == 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Is_SleepMode_Enabled()
{
	if(NVM_ConfigData.syscfg.Disable_SleepMode == 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}




