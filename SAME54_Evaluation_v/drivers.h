/*
 * drivers.h
 *
 * Created: 5/9/2020 10:52:58 PM
 *  Author: BehroozH
 */ 


#ifndef DRIVERS_H_
#define DRIVERS_H_



void Enable_LowPowerPath(void);
void Disable_LowPowerPath(void);
void Enable_Balancing(void);
void Disable_Balancing(void);
void TurnON_Heater(void);
void TurnOFF_Heater(void);
float Read_DisChargingCurrent();
float Read_DisChargingCurrentLowGain();
float Read_DisChargingCurrentHiGain();
float Get_ChargingCurrent();
float Get_DisChargingCurrent();
void Update_Battery_Status_Word();
uint32_t Get_Battery_Status_Word();
void Open_Contactor(void);

bool IsShelfMode();
bool IsEngineOff();

uint32_t capture_time_sleep();
uint32_t capture_time_wakeup();

#endif /* DRIVERS_H_ */