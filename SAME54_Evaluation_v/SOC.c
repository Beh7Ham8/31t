/*
 * SOC.c
 *
 * Created: 5/11/2020 9:52:37 AM
 *  Author: BehroozH
 */ 

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <machine\fastmath.h>
#include "lib/timing.h"
#include <sys/_stdint.h>
#include "system_definitions.h"
#include "drivers.h"
#include "debugMon.h"
#include "common.h"
#include "lib/datalog.h"
#include "ext_adc.h"
#include "lib/systick.h"

#define DEFAULT_AMP_HOURS_108  8.0;
#define DEFAULT_AMP_HOURS_110  10.0;
#define DEFAULT_AMP_HOURS_120  20.0;


#define CHARGING_CURRENT_THROUGH_CONTACTOR      0.02365
#define DISCHARGING_CURRENT_THROUGH_CONTACTOR   0.00668
#define CHARGING_CURRENT_DUE_TO_ICHARGE         0.00093
#define DISCHARGING_CURRENT_DUE_TO_HEATER       0.0005556
#define DISCHARGING_CURRENT_DUE_TO_BALANCER     0.0000556

// charging current values
const float ENGINE_ON_DELTAV_C2AMPS = 0.0200;

volatile float batterySOC;
volatile float defaultCellAmpHours = DEFAULT_AMP_HOURS_120;

float SOC_Backup __attribute__ ((section(".noinit")));

float VOLTAGE_REFERENCE_TP1 = 1.225;

CauseOfReset causeOfReset;

SW_TIMER batSOCMonitorTimer;
pSW_TIMER pBatterySOCmonitorTimer;


float Get_SOC() 
{
	return ((float)batterySOC/defaultCellAmpHours);
}

void Init_BatterySOC(void) 
{
	if(causeOfReset != POWER_ON_RESET)
	{
		batterySOC = SOC_Backup;
		
		debug_print( DBG_PRINTS_ALL, " Retrieved SOC after reset = %2.3f",batterySOC);
	}
	
	pBatterySOCmonitorTimer = &batSOCMonitorTimer;

	// We will monitor the sleep state every second
	timerInit(pBatterySOCmonitorTimer, PERIODS.SOC);
	
	batterySOC = defaultCellAmpHours;

	//debug_print(DBG_SOC_MEASUREMENTS, "%s,%s,%s,%s,%s\n", "Time", "Cycle Time", "Battery SOC", "Cell Temperature V", "Body Temperature V");
}


void SOC_Monitor() 
{

	static float prevTime = 0.0;
	static uint8_t prevOverUnderCondition = 0;
	
	float current = 0.0;
	float correctedCurrent = 0.0;	
	//float voltageDelta;
	float cycleTime;
	float currentTime = 0.0;	
	float lowDischargeCurrent;
	char StatusText[48];
	float display_current = 0.0;
	
	batterySOC = SOC_Backup;

	if (timerExpired(pBatterySOCmonitorTimer))
	{
 		currentTime = (float) HAL_SYSTICK_GetTicks() / 1000.0;

		if (prevTime == 0)
		{
			cycleTime = (pSystemStatus->flags.mcuInSleep) ? 8.0 : 1.0;
		}
		else
		{
			cycleTime = currentTime - prevTime;
		}
		
		prevTime = currentTime;
		
		batterySOC = SOC_Backup;

		// Reset timer for next monitor cycle
		timerReset(pBatterySOCmonitorTimer);
		

		if (!pSystemStatus->flags.ContactorIsOpen)
		{
			// Contractor is closed
			
			current = Get_Current();			
			
			if(current > 0.01)
			{ 
				strcpy(StatusText," Charging");
				
				display_current = current;
				
				batterySOC += (current * ((float)cycleTime/3600.0 ));

				if (batterySOC > defaultCellAmpHours)
				{
					// We have reached the max charge capacity.
					batterySOC = defaultCellAmpHours;
				}
			} 
			else if (current < 0.01)
			{				
				// Discharging, so decrement SOC
				
				strcpy(StatusText," Discharging");
				
				display_current = current;
				
				batterySOC += (current * ((float)cycleTime/3600.0 ));				

				if (batterySOC < 0) 
				{
					batterySOC = 0;
				}
			}
			else
			{
				// Not enough discharge current. Set to 10 mA average
				strcpy(StatusText," Discharging with low current");
				correctedCurrent = LOW_DISCHARGE_CURRENT;
				display_current = LOW_DISCHARGE_CURRENT;
				
				batterySOC -= (correctedCurrent * ((float)cycleTime/3600.0 ));

				if (batterySOC < 0)
				{
					batterySOC = 0;
				}
			}

		} 
		else 
		{
			// Contactor is open, for now we are gonna ignore the current used by Low power path, 
			// we should only experience self-discharge	
			// No mobile device is connected, so our average current draw is ~18mA / second,
			strcpy(StatusText," Self Discharging Contactor is open");
			
			lowDischargeCurrent = SELF_DISCHARGE_CURRENT;
			display_current = SELF_DISCHARGE_CURRENT;
						
			batterySOC -= (lowDischargeCurrent *((float)cycleTime/3600.0 ));
			
			if (batterySOC < 0)
			{
				batterySOC = 0;
			}
		}
		
		 if((FSM_counters.BattSOC_counter++ % 10)==0)
		 {
			 //debug_print(DBG_SLEEP_MODE, " SOC___FSM |   SOC: %2.3f  %s  I = %2.3f",batterySOC,StatusText,display_current);
			 //debug_print(DBG_PRINTS_ALL, " Sys_tick = %2.4f,  duration = %2.4f, SOC = %2.4f \n", currentTime, cycleTime, batterySOC);
		 }

		


	}

}
