/*
 * debugMon.h
 *
 * Created: 5/12/2020 1:58:02 PM
 *  Author: BehroozH
 */ 


#ifndef DEBUGMON_H_
#define DEBUGMON_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "system_definitions.h"
#include "calendar.h"

typedef enum
{
	/* Application's state machine's initial state. */
	USART_ENABLE=0, USART_TRANSMIT_FIRST_STRING, USART_TRANSMIT_SECOND_STRING, USART_RECEIVE_DONE, USART_TRANSMIT_DEBUG_STRING

	/* TODO: Define states used by the application state machine. */

} DEBUGMON_STATES;

typedef struct
{
    /* The application's current state */
    DEBUGMON_STATES state;

    /* TODO: Define any additional data used by the application. */

    bool InterruptFlag;

    const char* stringPointer;

    char data;


} DEBUGMON_DATA;

extern DEBUGMON_DATA debugMonData;

extern uint32_t Active_Debug_Traces;

#define MAX_DEBUG_BUFFER_SIZE 4096


// Debug flags

 extern int g_debuglevel;
 extern uint8_t CDC_Buff[];

#ifndef DEBUG
#define DEBUG
#endif

#define DBG_VOLTAGE_BALANCER			(1<<0)
#define DBG_SOC							(1<<1)
#define DBG_APP							(1<<2)
#define DBG_COMMAND_PARSER              (1<<3)
#define DBG_TEMPERATURE					(1<<4)
#define DBG_LOGGING						(1<<5)
#define DBG_SDMMC						(1<<6)
#define DBG_SLEEP_MODE					(1<<7)
#define DBG_INITALIZATION				(1<<8)
#define DBG_BLUETOOTH					(1<<9)
 
#define DBG_PRINTS_ALL                  0xFFFFFFFF 
#define DBG_ALWAYS_PRINT                0x80000000
#define DBG_PRINT_NONE					0x00000000

#define DEBUG_FLAGS_ENABLED             (DBG_PRINTS_ALL)

//This debug_print will send the traces to EDBG port
#if 1

 #define debug_print(level,fmt, args...)			do{if (level & Active_Debug_Traces) printf("\r\n%s "fmt,CurrentTime(),##args);}while(0)
	
 #define debug_print_nots(level,fmt, args...)	    do{if (level & Active_Debug_Traces) printf("\r\n "fmt,##args);}while(0)
	 
#else
//This debug_print will send traces to target USB port	 
#define debug_print(level,fmt, args...)			do{if (level & Active_Debug_Traces) {sprintf(CDC_Buff,"\r\n%s "fmt,CurrentTime(),##args); HAL_CDC_Send((uint8_t*) CDC_Buff, strlen(CDC_Buff));}}while(0)	 
	
#define debug_print_nots(level,fmt, args...)	do{if (level & Active_Debug_Traces) {sprintf(CDC_Buff,"\r\n "fmt,##args); HAL_CDC_Send((uint8_t*) CDC_Buff, strlen(CDC_Buff));}}while(0)	
#endif

 #define cmdParset_print(x) printf x
// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************
#define DBG_RX_BUFFER_SIZE	1024
#define DBG_TX_BUFFER_SIZE	256

void Init_Debug();
void Update_Trace(uint32_t trace);

/* 
extern volatile uint32_t dbgRdPtr;
extern volatile uint32_t dbgWrPtr;
extern uint8_t dbg_rx_buffer[DBG_RX_BUFFER_SIZE];

extern uint8_t dbg_tx_buffer[DBG_TX_BUFFER_SIZE];


void DebugMon_Initialize ( void );

void debugUartMonitor( void );

void dbg_print(const char *msg);
*/


#endif /* DEBUGMON_H_ */