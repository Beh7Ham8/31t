#include <atmel_start.h>
#include "system_definitions.h"
#include "calendar.h"
#include "lib\systick.h"
#include "temperature.h"
#include "sleep.h"
#include "SOC.h"
#include "battCells.h"
#include "ext_adc.h"
#include "LEDs.h"
#include "..\usb_msc.h"
#include "LTC_6810.h"
#include "lib\cfg_device.h"
#include "debugMon.h"
#include "usb_c\i2c_bb.h"
#include "flash.h"
#include "driver_init.h"
#include "hal\usb_common.h"
#include "sd_mmc\state.h"
#include "sd_mmc\sd_logger.h"
#include "lib\serial_comm.h"
#include "lib\fw_upgrade.h"
#include "common.h"
#include "system_init.h"
#include "bluenrg_monitor.h"

struct StateMachineTracker FSM_counters;

void Cause_Of_Reset();
static void Test_Delay();

void Init_SW()
{
	HAL_SYSTICK_Initialize();
	HAL_SYSTICK_Start();	
	Init_Calendar();		
	Init_Debug();
	Cause_Of_Reset();
	Print_Header();
	
#if 0 // this breaks USB - include only if you have the STUSB1600 connected
	Init_BB_i2c();	
	Verify1600NVM();
	print_nvm2();
#endif	

	Init_Interrupts();
	Init_Nvm_ConfigData();
	Init_TemperartureMon();
	Init_Sleep();
	Init_BatterySOC();
	Init_BatterySOH();	
    Init_BatteryCells();
	
	// we have to initialize the i2c interface after using the pins for bit-banging
	I2C_0_init();
	Init_Shunt_ADS111x();
	Init_LEDs();	
	HAL_USB_Initialize();
	Init_SDMMC();
	Init_Logger();
	Init_App();	
	Init_SCOMM();	 
	Init_FW();
	Init_BLE();
	
	Calibrate_ADCs();
	
	Init_LTC6810();
	
	cfg_device_config();
	
	
	
}


int main(void)
{
	struct calendar_date_time datetime;
	/* Initializes MCU, drivers and middle ware */
	atmel_start_init();
	
	Init_SW();
	
	
	while (!hri_pm_read_INTFLAG_reg(PM)) {}		
				
	while (1) 
	{	
		
	if( Is_Unserviced_Fault_Exist() == false)
	 Temperature_Monitor();
	 
	
		
	if( Is_Unserviced_Fault_Exist() == false)
	 SOC_Monitor();
	 
	
	if( Is_Unserviced_Fault_Exist() == false)
	   Sleep_Monitor();
	 
	 
	
	if( Is_Unserviced_Fault_Exist() == false)
	 Cells_Monitor(); 
	 
	
	
	if( Is_Unserviced_Fault_Exist() == false)
	  SDMMC_Monitor();	 
	  
	  
	  
	if(Is_Unserviced_Fault_Exist() == false) 
	  Logger_Monitor();
	  
	  
	if( Is_Unserviced_Fault_Exist() == false) 
	  SOH_Monitor();
	  

	  
	if(Is_Unserviced_Fault_Exist() == false)
	  LEDs_Monitor(); 
	
	
	
	if(Is_Unserviced_Fault_Exist() == false)  
	  HAL_MSC_Monitor();
	  
	 
	 if( Is_Unserviced_Fault_Exist() == false) 
	  SCOMM_Monitor();
	  
	 if( Is_Unserviced_Fault_Exist() == false) 
	  BLE_Monitor();
	  
	 App_Monitor();
	 
	 Test_Delay();
	 
	 //Test_ADS_111x_Monitor();  
	 // Hardware_Test();
	  	  		
	}
}


void Test_Delay()
{
	gpio_set_pin_level(A0_UC,true);
	delay_us(25);
	gpio_set_pin_level(A0_UC,false);	
}

void Cause_Of_Reset()
{

	if(hri_rstc_get_RCAUSE_POR_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset Power On.");
		causeOfReset = POWER_ON_RESET;
	}
	if(hri_rstc_get_RCAUSE_BODCORE_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset Brown Out Detection for Core.");
		causeOfReset = BOD_CORE;
	}
	if(hri_rstc_get_RCAUSE_BODVDD_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset Brown Out Detection for VDD.");
		causeOfReset = BOD_VDD;
	}
	if(hri_rstc_get_RCAUSE_NVM_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset on NVM.");
		causeOfReset = NVM;
	}
	if(hri_rstc_get_RCAUSE_EXT_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset on External Reset Pin.");
		causeOfReset = EXT;
	}
	if(hri_rstc_get_RCAUSE_WDT_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset on Watchdog Timer.");
		causeOfReset = WATCH_DOG_TIMEOUT;
	}
	if(hri_rstc_get_RCAUSE_SYST_bit(RSTC))
	{
		debug_print(DBG_PRINTS_ALL,"Reset on SYST.");
		causeOfReset =  SYST;
	}
}
