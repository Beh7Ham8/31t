/*
 * temperature.c
 *
 * Created: 5/8/2020 10:32:52 AM
 *  Author: BehroozH
 */ 
#include "driver_init.h"
#include "lib/timing.h"
#include "lib/stateMachine.h"
#include "adc.h"
#include "temperature.h"
#include "battCells.h"
#include "debugMon.h"
#include "calendar.h"
#include "drivers.h"
#include "common.h"

//#define debug_print(level,fmt, args...)			do{if (level & DEBUG_FLAGS_ENABLED) printf(fmt,##args);}while(0)
//#define debug_print(level,fmt, args...)			do{if (level & DEBUG_FLAGS_ENABLED) printf("\n%s "fmt,CurrentTime(),##args);}while(0)	

STATE_MACHINE temprFSM;
pSTATE_MACHINE pTemperatureFSM;

SW_TIMER tTickTimer;
pSW_TIMER pTprtrTickTimer;

ANALOG_VALUE PosTermT;
ANALOG_VALUE GndT;
ANALOG_VALUE CellsT;
ANALOG_VALUE CountRef;

pANALOG_VALUE pGroundTemperature;
pANALOG_VALUE pPosTermTemperature;
pANALOG_VALUE pCellModuleTemperature;
pANALOG_VALUE pCRef;

uint8_t currentTemperatureRange;
uint8_t previousTemperatureRange;

 float groundTemperature;
 float groundTempVolt;
 float posTermTemperature;
 float posTermTempVolt;
 float cellTemperature;
 float cellTempVolt;
 
 float groundTemperature_Inst;
 float groundTempVolt_Inst;
 float posTermTemperature_Inst;
 float posTermTempVolt_Inst;
 float cellTemperature_Inst;
 float cellTempVolt_Inst;
 
uint16_t T1_Count =0;
uint16_t T2_Count =0;
uint16_t T3_Count =0;


const uint8_t  ADC1_TEMP_CH = 7;
const uint8_t  ADC1_VREF_CH = 6;
const uint8_t TEMPERATURE_MUX_CH = 7;


const float HTR_HYSTER = 2.50;
const float HTR_ON_TEMPERATURE_THREASHOLD = -15.00;
const float HTR_OFF_TEMPERATURE_THREASHOLD = 5.00;
const float GND_BANK_HYSTER = 2.50;
const float GND_TEMPERATURE_FAULT_THREASHOLD = 20.00;
const float BANK_TEMPERATURE_FAULT_THREASHOLD = 20.00;

float R33K = 10;

const uint32_t MUX_DELAY = 1500;

/* Events specific to the protocol SM */
enum 
{
	evTEMPT_INIT_DONE = evMAX_COMMON_EVENTS,
	evTEMPT_ON,
	evTEMPT_OFF
};

enum 
{
	TEMPT_INIT,
	READ_BODY_TEMP,
	READ_CELL_TEMP,
	TEMPT_NUM_STATES
};



const pStateFunc temperatureStateVectorArray[TEMPT_NUM_STATES] = 
{

	(pStateFunc) state_temperature_Init,
	(pStateFunc) state_read_body_temperature,
	(pStateFunc) state_read_cell_temperature
	
};

char* temperatureStateString[TEMPT_NUM_STATES] =
{
	 "Init",
	 "RD_Body",
	 "RD_Cell"	
};

void Temperature_Monitor() 
{	
	// Fire the appropriate event to the state machine depending on
	// what the system has experienced
	if (timerExpired(pTprtrTickTimer)) 
	{	
		 if((FSM_counters.Temp_counter++ % 1)==0)
		 {
			 if(pTemperatureFSM->currentState == 1)
			 {
				  debug_print(DBG_TEMPERATURE, " Temp__FSM |  Grnd_T = %3.2f            Pack_T = %3.2f",groundTemperature,posTermTemperature);
			 }	
			 else if (pTemperatureFSM->currentState == 2)
			 {
				  debug_print(DBG_TEMPERATURE, " Temp__FSM |  Cell_T =: %2.3f  T1 = %u      T2 = %u      T3 = %u",cellTemperature,T1_Count,T2_Count,T3_Count);
				                           
			 }
			 else
			 {
				  debug_print(DBG_TEMPERATURE, " Temp__FSM |    %s",temperatureStateString[pTemperatureFSM->currentState]);
			 }
		 }
		
		
		timerReset(pTprtrTickTimer);		
		
		tickTimerTemperature();

		//updateModuleTempRange();
		
		//debug_print(DBG_TEMPERATURE_MEASAURE, "Board Temperatures A(%2.4f), G(%2.4f) -- Body THR (%2.4f)\r\n", boardTempA, boardTempG, (129.1 * bodyTemperature) - 232.38);		
	}

}


void tickTimerTemperature()
{
	uint32_t timerIncrement = 1;
	
	//debug_print(DBG_FSM_TEMP, "  Tick Temperature init");
	
	event_handler(pTemperatureFSM, evTIMER_TICK, &timerIncrement);
}

void Init_TemperartureMon() 
{
	pTemperatureFSM = &temprFSM;
	pTprtrTickTimer = &tTickTimer;
	
	timerInit(pTprtrTickTimer, PERIODS.TEMPERATURE);
	
	pPosTermTemperature = &PosTermT;
	pGroundTemperature = &GndT;
	pCellModuleTemperature = &CellsT;
	pCRef = &CountRef;
	
	initAnalogVal(pPosTermTemperature);
	initAnalogVal(pGroundTemperature);
	initAnalogVal(pCellModuleTemperature);
	initAnalogVal(pCRef);
	
	// initialize state machine
	stateInit(pTemperatureFSM, temperatureStateVectorArray, TEMPT_NUM_STATES, TEMPT_INIT);
}

void state_temperature_Init(int event, void *param)
{
	 switch (event) 
	 {
		 case evENTER_STATE:

		 //debug_print(DBG_FSM_TEMP, "  Entering Temperature init");
		 
		 break;

		 case evTIMER_TICK:
		 pTemperatureFSM->nextState = READ_BODY_TEMP;
		 break;

		 case evEXIT_STATE:
		 break;
	 }
}
void state_read_body_temperature(int event, void *param)
{
	bool tempertureUpdated = false;
	bool gnd_fault = false;
	bool bank_fault = false;
	
	 switch (event)
	 {
		 case evENTER_STATE:
		 
		 //debug_print(DBG_FSM_TEMP, "  Entering Read BODY");
		
		 break;

		 case evTIMER_TICK:

		 pGroundTemperature->instantaneous = Read_GroundTemperature_Inst();
		 
		 
		 // Update their running averages
		 updateAnalogAverage(pGroundTemperature);
		 
		 // Update the voltage reference value
		 pCRef->instantaneous = Read_Ref1_Inst();//4095
		 updateAnalogAverage(pCRef);
		 
		  if (pCRef->average != 0)
		  {
			  groundTempVolt_Inst = VREF_CONST*((float) pGroundTemperature->instantaneous / (float) pCRef->average);
			  groundTemperature_Inst = Get_TempertureFromVolt(groundTempVolt_Inst);
		  }		 
		 
		 if (isBufferFilled(pGroundTemperature)) 
		 {
			 if (pCRef->average != 0)
			 {
				 groundTempVolt = VREF_CONST*((float) pGroundTemperature->average / (float) pCRef->average);
				 groundTemperature = Get_TempertureFromVolt(groundTempVolt);	
				 tempertureUpdated = true;		 
			 }		 
		 }
		 
		 
		 pPosTermTemperature->instantaneous = Read_PosTermTemperture_Inst();
		 
		 // Update their running averages
		 updateAnalogAverage(pPosTermTemperature);
		 
		 // Update the voltage reference value
		 pCRef->instantaneous = Read_Ref1_Inst();//4095
		 updateAnalogAverage(pCRef);
		 		 
		  if (pCRef->average != 0)
		  {
			  posTermTempVolt_Inst = VREF_CONST*((float) pPosTermTemperature->instantaneous / (float) pCRef->average);
			  posTermTemperature_Inst = Get_TempertureFromVolt(posTermTempVolt_Inst);
		  }
		 
		
		 if (isBufferFilled(pPosTermTemperature))
		 {
			 if (pCRef->average != 0)
			 {
				 posTermTempVolt = VREF_CONST*((float) pPosTermTemperature->average / (float) pCRef->average);
				 posTermTemperature = Get_TempertureFromVolt(posTermTempVolt);
				 tempertureUpdated = true;
			 }
			 
		 }
		 
		 
		 gnd_fault = pSystemStatus->flags.overTemperature_Gnd_Exist;
		 bank_fault = pSystemStatus->flags.overTemperature_Bank_Exist;
		 
		 
		 if(tempertureUpdated)
		 {
			if( (gnd_fault == false) && (groundTemperature > (GND_TEMPERATURE_FAULT_THREASHOLD + GND_BANK_HYSTER)))
			{
			  pSystemStatus->flags.overTemperature_Gnd_Exist = true;
			   debug_print(DBG_TEMPERATURE, "  Temperature |  OverTemp is detected on GND node.");
			}
			else if ((gnd_fault == true) && (groundTemperature < (GND_TEMPERATURE_FAULT_THREASHOLD - GND_BANK_HYSTER)))
			{
				 pSystemStatus->flags.overTemperature_Gnd_Exist = false;
				 debug_print(DBG_TEMPERATURE, "  Temperature |  OverTemp is cleared on GND node.");
			}
			else if ( (bank_fault == false) && (posTermTemperature > (BANK_TEMPERATURE_FAULT_THREASHOLD + GND_BANK_HYSTER)))
			{
				 pSystemStatus->flags.overTemperature_Bank_Exist = true;
				 debug_print(DBG_TEMPERATURE, "  Temperature |  OverTemp is detected on BANK node.");
			}
			else if ( (bank_fault == true) && (posTermTemperature < (BANK_TEMPERATURE_FAULT_THREASHOLD - GND_BANK_HYSTER)))
			{
				pSystemStatus->flags.overTemperature_Bank_Exist = false;
				debug_print(DBG_TEMPERATURE, "  Temperature |  OverTemp is cleared on BANK node.");
			}
			else
			{
				
			}
			
		//	debug_print(DBG_FSM_TEMP, "  Temperature |  Gnd Voltage = %2.3f, Gnd: %2.3f ",groundTempVolt, groundTemperature);
			
		//	debug_print(DBG_FSM_TEMP, "  Temperature |  TermVoltage = %2.3f, PosTerm %2.3f ", posTermTempVolt,posTermTemperature);
			
		 }
		 
		 
		 
		 pTemperatureFSM->nextState = READ_CELL_TEMP;		 
		 break;

		 
		 case evEXIT_STATE:
		 break;

		 default:
		 break;
	 }
}
void state_read_cell_temperature(int event, void *param)
{
	bool heaterON = false;
	
	 switch (event) 
	 {
		 case evENTER_STATE:

		 //debug_print(DBG_FSM_TEMP, "  Entering Read CELL");
		 
		 break;

		 case evTIMER_TICK:
		 // If we time out while executing a command exit and go back to idle

		 pCellModuleTemperature->instantaneous =  Read_CellsTemperature_Inst();
		 updateAnalogAverage(pCellModuleTemperature);

		 // Update the voltage reference value
		 pCRef->instantaneous = Read_Ref1_Inst();//4095
		 updateAnalogAverage(pCRef);
		 
		  if (pCRef->average != 0)
		  {
			  cellTempVolt_Inst = VREF_CONST*((float) pCellModuleTemperature->instantaneous / (float) pCRef->average);
			  cellTemperature_Inst = Get_TempertureFromVolt(cellTempVolt_Inst);
		  }
			 
		 heaterON = pSystemStatus->flags.HeaterON;

		 if (isBufferFilled(pCellModuleTemperature)) 
		 {
			 if (pCRef->average != 0) 
			 {
				 cellTempVolt = VREF_CONST*((float) pCellModuleTemperature->average / (float) pCRef->average);				 
				 cellTemperature = Get_TempertureFromVolt(cellTempVolt);
				 
				 if((heaterON == false) && (cellTemperature < (HTR_ON_TEMPERATURE_THREASHOLD  - HTR_HYSTER)))
				 {
					 TurnON_Heater();
					 pSystemStatus->flags.HeaterON = true;
					 debug_print(DBG_TEMPERATURE, "  Temperature | Heater is turned ON.");	
				 }
				 else if ((heaterON == true) && (cellTemperature > (HTR_OFF_TEMPERATURE_THREASHOLD +  HTR_HYSTER)))
				 {
					 TurnOFF_Heater();
					 pSystemStatus->flags.HeaterON = false;
					 debug_print(DBG_TEMPERATURE, "  Temperature | Heater is turned OFF.");	
				 }
			 }
			 
			 // debug_print(DBG_FSM_TEMP, "Temperature |  Cell Temp Voltage = %2.3f,  Cell Temp = %2.3f",cellTempVolt,cellTemperature);		
		 }
		  
		 pTemperatureFSM->nextState = READ_BODY_TEMP;

		 break;

		 case evEXIT_STATE:
		 break;
		 default:
		 break;
	}
}


uint16_t Read_CellsTemperature_Inst(void)
{
	uint32_t avg;
	
	select_Mux_TC1_GND();
	//delay_us(MUX_DELAY);
	T1_Count = Read_Adc1(TEMPERATURE_MUX_CH);

	
	select_Mux_TC2_P5();
	//delay_us(MUX_DELAY);
	T2_Count = Read_Adc1(TEMPERATURE_MUX_CH);
	
	select_Mux_TC3_P4();
	//delay_us(MUX_DELAY);
	T3_Count = Read_Adc1(TEMPERATURE_MUX_CH);
	
	avg = ((T1_Count + T2_Count + T3_Count)/3);
	
	return avg;
}

float Get_Cell_Temp()
{	
	return cellTemperature;	
}

float Get_Ground_Temp()
{
	return groundTemperature;
}

float Get_Term_Temp()
{
	return posTermTemperature;	
}

uint16_t Read_PosTermTemperture_Inst(void)
{
	uint16_t count;
	
	select_Mux_TC5_BNK();
	//delay_us(MUX_DELAY);
	count = Read_Adc1(TEMPERATURE_MUX_CH);
	
	return count;
}

uint16_t Read_GroundTemperature_Inst(void)
{
	uint16_t count;
	
	select_Mux_TC1_GND();
	//delay_us(MUX_DELAY);
	count = Read_Adc1(TEMPERATURE_MUX_CH);
	
	return count;
}

float Get_TempertureFromVolt(float Vo)
{
	float T;
	
	T = ((1.8663 - Vo)/(11.69))*1000;
	
	return T;
}

uint16_t Read_Ref0_Inst(void)
{
	uint16_t count;
	
	count = Read_Adc0(ADC0_VREF_CH);
	
	return count;
}

uint16_t Read_Ref1_Inst(void)
{
	uint16_t count;
	
	count = Read_Adc1(ADC1_VREF_CH);
	
	return count;
}

float Get_Cell_Temp_Inst()
{
	return cellTemperature_Inst;
}

float Get_Term_Temp_Inst()
{
	return groundTemperature_Inst;
}

float Get_Ground_Temp_Inst()
{
	return posTermTemperature_Inst;
}


void select_Mux_TC1_GND()
{
	//000
	gpio_set_pin_level(A0_UC,false);
	gpio_set_pin_level(A1_UC,false);
	gpio_set_pin_level(A2_UC,false);
}

void select_Mux_TC2_P5()
{
	//001
	gpio_set_pin_level(A0_UC,true);
	gpio_set_pin_level(A1_UC,false);
	gpio_set_pin_level(A2_UC,false);
}

void select_Mux_TC3_P4()
{
	//002
	gpio_set_pin_level(A0_UC,false);
	gpio_set_pin_level(A1_UC,true);
	gpio_set_pin_level(A2_UC,false);
}

void select_Mux_TC4_P5()
{
	//003
	gpio_set_pin_level(A0_UC,true);
	gpio_set_pin_level(A1_UC,true);
	gpio_set_pin_level(A2_UC,false);
}

void select_Mux_TC5_BNK()
{
	//004
	gpio_set_pin_level(A0_UC,false);
	gpio_set_pin_level(A1_UC,false);
	gpio_set_pin_level(A2_UC,true);
}

void select_Mux_TC6_UC()
{
	//005
	gpio_set_pin_level(A0_UC,true);
	gpio_set_pin_level(A1_UC,false);
	gpio_set_pin_level(A2_UC,true);
}



