/*
 * calendar.c
 *
 * Created: 6/11/2020 9:36:33 AM
 *  Author: BehroozH
 */ 


#include <sys\_stdint.h>
#include "hpl_calendar.h"
#include "driver_init.h"
#include <stdio.h>
#include <time.h>
#include "calendar.h"
#include "debugMon.h"
#include "system_definitions.h"
#include "flash.h"
#include "sd_logger.h"

char current_time[12];
char current_date_time[24];

#define SOH_BUF  16
#define SOH_ADDR_OFFSET	  0

static struct calendar_alarm alarm;
static void _Update_SOH(struct calendar_descriptor *const descr);
int32_t _Read_From_EEPROM();

static uint32_t SOH;
static float f_SOH;

bool IsSOH_Updated = false;

static void _set_calendar_date_time(uint16_t year,uint8_t month,uint8_t day,uint8_t hr,uint8_t min,uint8_t sec);

void Init_Calendar()
{
	SOH = 10000000;//10 million- later on read it from flash
	
	calendar_enable(&CALENDAR_0);
	
	_set_calendar_date_time(2000,1,1,12,0,0);

	SOH_set_alarm();
}

void SOH_set_alarm()
{
	//TODO: change it back to one alarm per day
	alarm.cal_alarm.datetime.time.sec = 23;
	alarm.cal_alarm.option            = CALENDAR_ALARM_MATCH_SEC;
	alarm.cal_alarm.mode              = REPEAT;
	
	calendar_set_alarm(&CALENDAR_0, &alarm, _Update_SOH);
}

char* CurrentTime()
{
	
	struct calendar_date_time datetime;
	
	calendar_get_date_time(&CALENDAR_0,&datetime);
	
	sprintf(current_time,"%02d:%02d:%02d",datetime.time.hour,datetime.time.min,datetime.time.sec);
	
	return current_time;
}

char* Current_DateTime()
{
	struct calendar_date_time datetime;
	
	calendar_get_date_time(&CALENDAR_0,&datetime);
	
	sprintf(current_date_time,"%4d/%02d/%02d - %02d:%02d:%02d",datetime.date.year,datetime.date.month,datetime.date.day,datetime.time.hour,datetime.time.min,datetime.time.sec);
	
	return current_date_time;
}

TIME_STAMP Current_DateTime_Short()
{
	struct calendar_date_time datetime;
	
	TIME_STAMP time_stamp;
	
	calendar_get_date_time(&CALENDAR_0,&datetime);
	
	time_stamp.second = datetime.time.sec;
	time_stamp.minute = datetime.time.min;
	time_stamp.hour = datetime.time.hour;
	time_stamp.day = datetime.date.day;
	time_stamp.month = datetime.date.month;
	time_stamp.year = datetime.date.year;
	
	return time_stamp;
}


void Init_BatterySOH()
{
	_Read_From_EEPROM();
}

void SOH_Monitor()
{
	char buf[32];
	static uint32_t counter =0;
	
	if(IsSOH_Updated)
	{
		if(Write_SOH_To_Flash(-1) != 0)
		{
			//debug_print(DBG_PRINTS_ALL, " SOH___FSM |   %2.6f", f_SOH);
			
			IsSOH_Updated = false;			
		}
		else
		{
			debug_print(DBG_PRINTS_ALL, " SOH___FSM |   Failed to write the updated SOH to flash. SOH = %2.6f", f_SOH);
			
			IsSOH_Updated = true;
		}
		
		sprintf(buf,"Calender minute alarm %lu",counter++);
		Queue_Event(buf);
	}
}

float Get_SOH()
{
	return f_SOH;
}

static void _Update_SOH(struct calendar_descriptor *const descr)
{
	SOH -= 1095;
	f_SOH = ((float)SOH/100000);
	//in 5 years we are gonna lose 2,000,000: 1826*1095 = 2 million
	
	IsSOH_Updated = true;
	
}

static void _set_calendar_date_time(uint16_t year,uint8_t month,uint8_t day,uint8_t hr,uint8_t min,uint8_t sec)
{
	struct calendar_date date;
	struct calendar_time time;
	
	date.year  = year;
	date.month = month;
	date.day   = day;

	time.hour = hr;
	time.min  = min;
	time.sec  = sec;
	
	calendar_set_date(&CALENDAR_0, &date);
	calendar_set_time(&CALENDAR_0, &time);
}

int32_t _Read_From_EEPROM()
{
	NVM_SOH sof_flash;
	
	if(nvm_read_datetime(&sof_flash))
	{
		if(sof_flash.SOH != 0xFFFFFFFF && (sof_flash.SOH < 10000000))
		{
			SOH = sof_flash.SOH;	
			debug_print(DBG_PRINTS_ALL, " SOH___FSM |  Initial SOH =  %lu  ", SOH);
		}
		else
		{
			SOH = 10000000;
		}
		
		if((sof_flash.year != 0xFFFF) && (sof_flash.month != 0xFF) && (sof_flash.day != 0xFF) && (sof_flash.hour != 0xFF) && (sof_flash.min != 0xFF)&& (sof_flash.sec != 0xFF))
		{
			_set_calendar_date_time(sof_flash.year,sof_flash.month,sof_flash.day,sof_flash.hour,(sof_flash.min)+1,sof_flash.sec);
			
			calendar_set_alarm(&CALENDAR_0, &alarm, _Update_SOH);
			
			debug_print(DBG_PRINTS_ALL, " SOH___FSM |  Time = %04u/%02u/%02u %02u:%02u:%02u",sof_flash.year,sof_flash.month,sof_flash.day,sof_flash.hour,(sof_flash.min)+1,sof_flash.sec);
		}	
		
		return 1;
	}
	else
	{
		return -1;
	}
			
	
}

uint32_t Write_SOH_To_Flash(uint32_t newSOH)
{
	NVM_SOH sof_flash;
	struct calendar_date_time datetime;

	calendar_get_date_time(&CALENDAR_0,&datetime);
    if (newSOH != 0xFFFFFFFF) {
        SOH = newSOH;
        f_SOH = (float)SOH / 100000.0f;
    }

	sof_flash.sec = datetime.time.sec;
	sof_flash.min = datetime.time.min;
	sof_flash.hour = datetime.time.hour;
	sof_flash.day = datetime.date.day;
	sof_flash.month = datetime.date.month;
	sof_flash.year = datetime.date.year;
	sof_flash.SOH = SOH;

	return nvm_write_datetime(&sof_flash) ? 1 : 0;
}
