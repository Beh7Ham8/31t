/*
 * bluenrg_data.h
 *
 * Created: 10/16/2020 11:14:40 AM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef BLUENRG_DATA_H_
#define BLUENRG_DATA_H_

#include <stdint.h>
#include "lib/packet_def.h"

void Init_BLE(void);
void BLE_Monitor(void);

typedef float(*pParamFunc)(void);

typedef struct {
    uint8_t cmdClass;
    uint8_t cmdID;
    uint8_t type;
    uint8_t idx;
    pParamFunc f;
} HOST_COMMAND_TYPE, *pHOST_COMMAND_TYPE;

typedef struct  TAG_PACKET_FIELDS {
    // Pointer to the data for the packet
    uint8_t     classPlusCmdID;
    uint16_t    length;
    uint8_t     typePlusIndex;
    uint8_t     checksum;
    uint8_t     param[0];
} __attribute__((packed)) PACKET_FIELDS;

typedef union _TAG_PACKET {
    PACKET_FIELDS fields;
    uint8_t       bytes[256];
} __attribute__((packed)) PACKET;

enum {
    BLUENRG_NORM_MODE,
    BLUENRG_DIAG_MODE
} BLUENRG_DATA_MODE;

#define MAX_HOST_CMD_IDS        37
#define MAX_HOST_DIAG_CMD_IDS   80

extern volatile HOST_COMMAND_TYPE mobileCmds[MAX_HOST_CMD_IDS];
extern volatile HOST_COMMAND_TYPE mobileDiagCmds[MAX_HOST_DIAG_CMD_IDS];

#endif /* BLUENRG_DATA_H_ */
