/*
 * common.c
 *
 * Created: 5/28/2020 1:54:01 PM
 *  Author: BehroozH
 */ 
#include "debugMon.h"
#include "common.h"

char* Version = "00.01.00";

float const TERM_VOLT_ENGINE_OFF_COUNT = 10;
float const TERM_VOLT_ENGINE_OFF_VOLT = 0.05;

float const SELF_DISCHARGE_CURRENT = 0.018;
float const LOW_DISCHARGE_CURRENT = 0.01;

float const CELL_BALANCED_THRESHOLD = 3.45;
float const CELL_NEED_BALANCING_THRESHOLD = 3.55;
float const DELTA_NEED_BALANCING = 0.5;
float const CELL_LOW_VOLTAGE_THRESHOLD = 2.75;
float const CELL_HIGH_VOLTAGE_THRESHOLD = 3.7;

float const ADC0_COUNT_TO_VOLT_SCALE = 0.000806; //3.3/4096
float const ADC1_COUNT_TO_VOLT_SCALE = 0.000806; //3.3/4096

T_PERIODS  PERIODS = 
{
	.VOLTAGE = 100,
	.HEARTBEAT = 5000,
	.TEMPERATURE = 1000,
	.LOGGER = 20000,
	.UPDATER = 100,
	.SHUNT = 1000,
	.SLEEP = 10000,
	.LED = 1000,
	.OVER_VOLTAGE = 1800000,
	.SOC = 1000
};

float const R_SHUNT = 0.00025; // 250 Micro Ohms

void Print_Header()
{
	debug_print_nots( DBG_PRINTS_ALL,"*******************************************************************************************************************");
	debug_print_nots( DBG_PRINTS_ALL,"**********************************      Indura Power 31T Smart Battery      ***************************************");
	debug_print_nots( DBG_PRINTS_ALL,"**********************************           FW Version %s                    ***************************************",Version);
	debug_print_nots( DBG_PRINTS_ALL,"*******************************************************************************************************************");
}
