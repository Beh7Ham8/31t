/*
 * temperature.h
 *
 * Created: 5/8/2020 10:33:57 AM
 *  Author: BehroozH
 */ 


#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_
#include "adc.h"

extern const uint8_t  ADC1_VREF_CH;
extern const uint8_t  ADC1_VBUS_CH;
extern const uint8_t  ADC1_HTR_CH;
extern const uint8_t  ADC1_TEMP_CH;

void Temperature_Monitor();
void tickTimerTemperature();
void Init_TemperartureMon();
void state_temperature_Init(int event, void *param);
void state_read_body_temperature(int event, void *param);
void state_read_cell_temperature(int event, void *param);

extern pANALOG_VALUE pGroundTemperature;
extern pANALOG_VALUE pPosTermTemperature;
extern pANALOG_VALUE pCellModuleTemperature;

 uint16_t Read_PosTermTemperture_Inst(void);
 uint16_t Read_CellsTemperature_Inst(void);
 uint16_t Read_GroundTemperature_Inst(void);
 uint16_t Read_Ref1_Inst(void);
 uint16_t Read_Ref0_Inst(void);
 
 float Get_Cell_Temp_Inst();
 float Get_Term_Temp_Inst();
 float Get_Ground_Temp_Inst();
 
 float Get_Cell_Temp();
 float Get_Ground_Temp();
 float Get_Term_Temp();
 
 
 float Get_TempertureFromVolt(float Vo);
 
 void select_Mux_TC1_GND();
 void select_Mux_TC2_P5();
 void select_Mux_TC3_P4();
 void select_Mux_TC4_P5();
 void select_Mux_TC5_BNK();
 void select_Mux_TC6_UC();


#endif /* TEMPERATURE_H_ */