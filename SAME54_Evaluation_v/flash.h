/*
 * flash.h
 *
 * Created: 8/31/2020 10:49:29 AM
 *  Author: patrick.xu@silver-bullet-tech.com
 *
 * Description:
 * The ATSAME5x family MCU provides the SmartEEPROM feature which emulates
 * EEPROM hardware in NVM flash.
 * The battery application configuration and status data will be stored
 * in the EEPROM emulation area. All the data should be defined in the
 * header file and accessed using nvm_read_/nvm_write_ functions. If the
 * data is not initializaed, the nvm_read_ function will do it first.
 */ 
#ifndef FLASH_H_
#define FLASH_H_

#include <stdbool.h>
#include "system_definitions.h"

enum nvm_eeprom_index {
    NVM_DATETIME_IDX,
    NVM_EEPROM_IDX,
    NVM_DBGCFG_IDX
};

typedef struct nvm_soh
{
	uint8_t marker1;
	/*range from 0 to 59*/
	uint8_t sec;
	/*range from 0 to 59*/
	uint8_t min;
	/*range from 0 to 23*/
	uint8_t hour;
	/*range from 1 to 28/29/30/31*/
	uint8_t day;
	/*range from 1 to 12*/
	uint8_t month;
	/*absolute year>= 1970(such as 2000)*/
	uint16_t year;
		
	uint32_t SOH;
		
	uint8_t marker2;
} NVM_SOH;

/*
 * All data saving in EEPROM should be defined in the data structure.
 * The data order will be as the order in the definition.
 */
typedef struct nvm_eeprom
{
    NVM_SOH soh;
    SYSTEM_CONFIG syscfg;
    uint32_t dbgcfg;
} NVM_EEPROM;

extern NVM_EEPROM NVM_ConfigData;

bool Init_Nvm_ConfigData(void);

bool nvm_read_datetime(NVM_SOH *data);
bool nvm_write_datetime(NVM_SOH *data);

bool nvm_read_syscfg(SYSTEM_CONFIG *data);
bool nvm_write_syscfg(SYSTEM_CONFIG *data);

bool nvm_read_dbgcfg(uint32_t *bitmaps);
bool nvm_write_dbgcfg(uint32_t bitmaps);

uint16_t nvm_read_eeprom(uint8_t index, void *data);
uint16_t nvm_write_eeprom(uint8_t index, void *data, uint16_t length);

bool Is_SCP_Enabled();
bool Is_OVP_Enabled();
bool Is_UVP_Enabled();
bool Is_OTP_Enabled();
bool Is_Heater_Enabled();
bool Is_Balancer_Enabled();
bool Is_SleepMode_Enabled();

#endif /* FLASH_H_ */
