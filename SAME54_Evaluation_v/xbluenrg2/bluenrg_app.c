#include <stdio.h>
#include <string.h>
#include "hci.h"
#include "hci_tl.h"
#include "bluenrg_app.h"
#include "bluenrg_proc.h"

bnrg_local_t bluenrg; // Bluetooth Instance

void BlueNRG_Init(void)
{
    memset(&bluenrg, 0, sizeof(bluenrg));
    bluenrg.addr_type = RANDOM_ADDR;
    bluenrg.uuid_type = 0; // fixed
    bluenrg.tx_power = 5;  // 2 dBm
    BlueNRG_ProcessStart(BNRG_DEVICE_NONE);
    hci_init(hci_tl_lowlevel_process, NULL);
}

void BlueNRG_Process(void)
{
    BlueNRG_ProcessTimeout();
}

uint8_t BlueNRG_GetState(void)
{
    uint8_t state = BNRG_NONE;
    switch(bluenrg.state) {
    case BNRG_DEVICE_NONE:
        state = BNRG_NONE;
        break;
    case BNRG_DEVICE_FAILED:
        state = BNRG_FAIL;
        break;
    case BNRG_DEVICE_REBOOT:
    case BNRG_DEVICE_RESET:
    case BNRG_DEVICE_GETVER:
    case BNRG_DEVICE_SETPWR:
    case BNRG_DEVICE_SETMAC:
    case BNRG_DEVICE_GETMAC:
    case BNRG_GATT_INIT:
    case BNRG_GAP_INIT:
    case BNRG_SET_CAP:
    case BNRG_SET_AUTH:
        state = BNRG_INIT;
        break;
    case BNRG_DEVICE_IDLE:
        state = BNRG_IDLE;
        break;
    case BNRG_REMOTE_DISCONNECTING:
        state = BNRG_DONE;
        break;
    case BNRG_START_SCAN:
    case BNRG_SCANNING:
    case BNRG_REMOTE_FOUND:
    case BNRG_REMOTE_CONNECTING:
    case BNRG_REMOTE_DISCOVERY:
    case BNRG_REMOTE_READCHAR:
    case BNRG_REMOTE_READDESC:
    case BNRG_REMOTE_SUBSCRIBING:
        state = BNRG_SCAN;
        break;
    case BNRG_REMOTE_CONNECTED:
        state = BNRG_LINK;
        break;
    case BNRG_REMOTE_WRITING:
    case BNRG_REMOTE_RESP_IND:
        state = BNRG_SEND;
        break;
    }
    return state;
}

void BlueNRG_SetTxPower(uint8_t pwr)
{
    if (pwr <= 7) bluenrg.tx_power = pwr;
}

uint8_t BlueNRG_GetTxPower(void)
{
    return bluenrg.tx_power;
}

void BlueNRG_SetMACAddr(const uint8_t *addr)
{
    if (addr) {
        bluenrg.addr_type = PUBLIC_ADDR;
        memcpy(bluenrg.addr, addr, 6);
    }
    else {
        bluenrg.addr_type = RANDOM_ADDR;
    }
}

void BlueNRG_GetMACAddr(uint8_t addr[6])
{
    memcpy(addr, bluenrg.addr, 6);
}

void BlueNRG_SetSerialNum(const uint8_t *sn, bool custom)
{
    if (sn) {
        for (int i = 0; i < 7; i++) {
            if ((sn[i] >= '0' && sn[i] <= '9') ||
                (sn[i] >= 'A' && sn[i] <= 'F')) continue;
            return;
        }
        memcpy(bluenrg.sn, sn, 7);
        bluenrg.uuid_type = custom ? BNRG_CUSTOM_UUID : BNRG_BOTH_UUID;
    }
    else {
        bluenrg.uuid_type =  BNRG_FIXED_UUID;
    }
}

bool BlueNRG_Enable(void)
{
    if (BlueNRG_GetState() == BNRG_IDLE) {
        BlueNRG_ProcessStart(BNRG_START_SCAN);
    }
    return false;
}

void BlueNRG_Disable(void)
{
    if (BlueNRG_GetState() >= BNRG_SCAN) {
        BlueNRG_ProcessStart(BNRG_REMOTE_DISCONNECTING);
    }
}

void BlueNRG_Reset(void)
{
    if (BlueNRG_GetState() == BNRG_IDLE) {
        BlueNRG_ProcessStart(BNRG_DEVICE_RESET);
    }
}

bool BlueNRG_SendData(uint8_t *data, uint8_t length)
{
    if (BlueNRG_GetState() == BNRG_LINK && data && length > 0) {
        bluenrg.tx_desc = 0;
        bluenrg.tx_size = length;
        bluenrg.tx_data = data;
        BlueNRG_ProcessStart(BNRG_REMOTE_WRITING);
        return true;
    }
    return false;
}

bool BlueNRG_SendAttr(uint8_t *data, uint8_t length)
{
    if (BlueNRG_GetState() == BNRG_LINK && data && length > 0) {
        bluenrg.tx_desc = 1;
        bluenrg.tx_size = length;
        bluenrg.tx_data = data;
        BlueNRG_ProcessStart(BNRG_REMOTE_WRITING);
        return true;
    }
    return false;
}

uint8_t BlueNRG_RecvData(uint8_t *data, uint8_t size)
{
    if (bluenrg.rx_size) {
        memcpy(data, bluenrg.rx_data, bluenrg.rx_size & 0x0F);
        size = bluenrg.rx_size;
        bluenrg.rx_size = 0;
        return size;
    }
    return 0;
}

void BlueNRG_ConfirmIndication(void)
{
    if (BlueNRG_GetState() == BNRG_LINK) {
        BlueNRG_ProcessStart(BNRG_REMOTE_RESP_IND);
        bluenrg.rx_size = 0;
    }
}
