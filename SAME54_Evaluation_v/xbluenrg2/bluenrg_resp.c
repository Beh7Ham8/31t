/*
 * bluenrg_resp.c
 *
 * Created: 10/8/2020 3:09:10 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdio.h>
#include <string.h>
#include "bluenrg_resp.h"
#include "bluenrg_proc.h"

static void hci_hal_default_process(uint16_t opcode, uint8_t status, uint8_t *buffer, uint8_t length)
{
    BlueNRG_ProcessEnd(opcode, status, buffer, length);
}

static const hci_command_status_events_table_type hci_status_events_table[] = {
    /* aci_gap_start_general_discovery_proc */
    {BNRG_EVT_SCAN_GENERAL, hci_hal_default_process},
    /* aci_gap_create_connection */
    {BNRG_EVT_CREATE_CONN, hci_hal_default_process},
    /* aci_gatt_disc_all_primary_services */
    {BNRG_EVT_DISC_ALL_SVC, hci_hal_default_process},
    /* aci_gatt_disc_all_char_of_service */
    {BNRG_EVT_DISC_ALL_CHAR, hci_hal_default_process},
    /* aci_gatt_disc_all_char_desc */
    {BNRG_EVT_DISC_ALL_DESC, hci_hal_default_process},
    /* aci_gatt_write_char_value */
    {BNRG_EVT_WRITE_CHAR, hci_hal_default_process},
    /* aci_gatt_write_char_desc */
    {BNRG_EVT_WRITE_DESC, hci_hal_default_process},
    /* hci_disconnect */
    {BNRG_EVT_DISCONN, hci_hal_default_process}
};

static const hci_command_complete_events_table_type hci_complete_events_table[] = {
    /* hci_reset */
    {BNRG_EVT_RESET, hci_hal_default_process},
    /* hci_read_bd_addr */
    {BNRG_EVT_BDADDR, hci_hal_default_process},

    /* aci_hal_get_firmware_details */
    {BNRG_EVT_VERSION, hci_hal_default_process},
    /* aci_hal_read_config_data */
    {BNRG_EVT_RD_CFG, hci_hal_default_process},
    /* aci_hal_write_config_data */
    {BNRG_EVT_WR_CFG, hci_hal_default_process},
    /* aci_hal_set_tx_power_level */
    {BNRG_EVT_TXPOWER, hci_hal_default_process},

    /* aci_gatt_init */
    {BNRG_EVT_GATT_INIT, hci_hal_default_process},
    /* aci_gatt_add_service */
    {BNRG_EVT_ADD_SVC, hci_hal_default_process},
    /* aci_gatt_add_char */
    {BNRG_EVT_ADD_CHAR, hci_hal_default_process},
    /* aci_gatt_add_char_desc */
    {BNRG_EVT_ADD_DESC, hci_hal_default_process},
    /* aci_gatt_update_char_value */
    {BNRG_EVT_UPD_CHAR, hci_hal_default_process},
    /* aci_gatt_write_without_resp */
    {BNRG_EVT_WRITE_CHAR_NORESP, hci_hal_default_process},
    /* aci_gatt_confirm_indication */
    {BNRG_EVT_RESP_INDICATION, hci_hal_default_process},

    /* aci_gap_init */
    {BNRG_EVT_GAP_INIT, hci_hal_default_process},
    /* aci_gap_set_io_capability */
    {BNRG_EVT_SET_CAP, hci_hal_default_process},
    /* aci_gap_set_authentication_requirement */
    {BNRG_EVT_SET_AUTH, hci_hal_default_process},
    /* aci_gap_terminate_gap_proc */
    {BNRG_EVT_STOP_GAP_PROC, hci_hal_default_process},
};

void BlueNRG_CommandResponse(hci_event_pckt *pckt)
{
    if (pckt->evt == EVT_CMD_STATUS) {
        evt_cmd_status *cs = (void *)pckt->data;
        for (int i = 0; i < (sizeof(hci_status_events_table)/sizeof(hci_status_events_table[0])); i++) {
            if (cs->opcode == hci_status_events_table[i].opcode) {
                uint8_t *ptr = pckt->data + EVT_CMD_STATUS_SIZE;
                uint8_t len = pckt->plen - EVT_CMD_STATUS_SIZE;
                hci_status_events_table[i].process(cs->opcode, cs->status, ptr, len);
                break;
            }
        }
    }
    else if (pckt->evt == EVT_CMD_COMPLETE) {
        evt_cmd_complete *cc = (void *)pckt->data;
        for (int i = 0; i < (sizeof(hci_complete_events_table)/sizeof(hci_complete_events_table[0])); i++) {
            if (cc->opcode == hci_complete_events_table[i].opcode) {
                uint8_t *ptr = pckt->data + EVT_CMD_COMPLETE_SIZE;
                uint8_t len = pckt->plen - EVT_CMD_COMPLETE_SIZE;
                hci_complete_events_table[i].process(cc->opcode, ptr[0], ptr, len);
                break;
            }
        }
    }
}
