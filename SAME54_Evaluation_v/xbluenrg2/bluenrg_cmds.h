/*
 * bluenrg_cmds.h
 *
 * Created: 10/8/2020 4:02:44 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef BLUENRG_CMDS_H_
#define BLUENRG_CMDS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include "bluenrg1_gatt_aci.h"

/*****************
 * HCI LE Commands
 *****************/
bool BlueNRG_GetBDAddr(void);
bool BlueNRG_GetBDAddrResp(uint8_t *data, uint8_t len, uint8_t addr[6]);

/**************
 * HAL Commands
 **************/
bool BlueNRG_GetVersion(void);
bool BlueNRG_GetVersionResp(uint8_t *data, uint8_t len, uint8_t dtm_ver[3], uint8_t ble_ver[3]);

bool BlueNRG_ReadConfig(uint8_t offset);
bool BlueNRG_ReadConfigResp(uint8_t *data, uint8_t len, uint8_t *cfg, uint8_t size);

/***************
 * GATT Commands
 ***************/
bool BlueNRG_AddService(uint8_t uuid_type,
                        Service_UUID_t *uuid,
                        uint8_t service_type,
                        uint8_t max_attribute_records);
bool BlueNRG_AddServiceResp(uint8_t *data, uint8_t len, uint16_t *service_handle);

bool BlueNRG_AddChar(uint16_t service_handle,
                     uint8_t uuid_type,
                     Char_UUID_t *uuid,
                     uint16_t char_value_length,
                     uint8_t char_properties,
                     uint8_t security_permissions,
                     uint8_t gatt_evt_mask,
                     uint8_t enc_key_size,
                     uint8_t is_variable);
bool BlueNRG_AddCharResp(uint8_t *data, uint8_t len, uint16_t *char_handle);

bool BlueNRG_AddCharDesc(uint16_t service_handle,
                         uint16_t char_handle,
                         uint8_t uuid_type,
                         Char_Desc_Uuid_t *uuid,
                         uint8_t desc_value_maxlen,
                         uint8_t desc_value_length,
                         uint8_t desc_value[],
                         uint8_t security_permissions,
                         uint8_t access_permissions,
                         uint8_t gatt_evt_mask,
                         uint8_t enc_key_size,
                         uint8_t is_variable);
bool BlueNRG_AddCharDescResp(uint8_t *data, uint8_t len, uint16_t *desc_handle);

/**************
 * GAP Commands
 **************/
bool BlueNRG_GapInit(uint8_t role, uint8_t privacy_enabled, uint8_t device_name_char_len);
bool BlueNRG_GapInitResp(uint8_t *data, uint8_t len, uint16_t *shandler, uint16_t *dhandler, uint16_t *ahandle);

#ifdef __cplusplus
}
#endif

#endif /* BLUENRG_CMDS_H_ */
