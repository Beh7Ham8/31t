#ifndef HCI_TL_INTERFACE_H
#define HCI_TL_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

int32_t HCI_TL_GetTick(void);

/**
 * @brief  Register hci_tl_interface IO bus services and the IRQ handlers.
 *         This function must be implemented by the user at the application level.
 *         E.g., in the following, it is provided an implementation example in the case of the SPI:
 *         @code
           void hci_tl_lowlevel_init(void)
           {
             tHciIO fops;  
  
             //Register IO bus services 
             fops.Init    = HCI_TL_SPI_Init;
             fops.DeInit  = HCI_TL_SPI_DeInit;
             fops.Send    = HCI_TL_SPI_Send;
             fops.Receive = HCI_TL_SPI_Receive;
             fops.Reset   = HCI_TL_SPI_Reset;
             fops.GetTick = BSP_GetTick;
  
             hci_register_io_bus (&fops);
  
             //Register event irq handler 
             HAL_EXTI_GetHandle(&hexti0, EXTI_LINE_0);
             HAL_EXTI_RegisterCallback(&hexti0, HAL_EXTI_COMMON_CB_ID, hci_tl_lowlevel_isr);
             HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
             HAL_NVIC_EnableIRQ(EXTI0_IRQn);
           }
 *         @endcode
 *
 * @param  None
 * @retval None
 */
void hci_tl_lowlevel_init(void);

void hci_tl_lowlevel_receive(void *pData);
void hci_tl_lowlevel_process(void *pData);

void BlueNRG_SPI_Reset(void);
void BlueNRG_Device_Reboot(void);

#ifdef __cplusplus
}
#endif
#endif /* HCI_TL_INTERFACE_H */
