/*
 * bluenrg_cmds.c
 *
 * Created: 10/8/2020 4:02:24 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <string.h>
#include "hci.h"
#include "hci_tl.h"
#include "bluenrg1_hal_aci.h"
#include "bluenrg1_hci_le.h"
#include "bluenrg1_gap_aci.h"
#include "bluenrg1_gatt_aci.h"
#include "bluenrg_cmds.h"

/*****************
 * HCI LE Commands
 *****************/
bool BlueNRG_GetBDAddr(void)
{
    uint8_t dummy[6];
    return (hci_read_bd_addr(dummy) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_GetBDAddrResp(uint8_t *data, uint8_t len, uint8_t addr[6])
{
    if (len == sizeof(hci_read_bd_addr_rp0)) {
        memcpy(addr, &data[1], 6);
        return true;
    }
    return false;
}

/**************
 * HAL Commands
 **************/
bool BlueNRG_GetVersion(void)
{
    return (aci_hal_get_firmware_details(0,0,0,0,0,0,0,0,0,0,0) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_GetVersionResp(uint8_t *data, uint8_t len, uint8_t dtm_ver[3], uint8_t ble_ver[3])
{
    if (len == sizeof(aci_hal_get_firmware_details_rp0)) {
        dtm_ver[0] = data[1];
        dtm_ver[1] = data[2];
        dtm_ver[2] = data[3];
        ble_ver[0] = data[7];
        ble_ver[1] = data[8];
        ble_ver[2] = data[9];
        return true;
    }
    return false;
}

/* @brief  Read configuration data stored in NVM
 * @param  Offset 0:CONFIG_DATA_PUBADDR_OFFSET 0x80:static random address
 * @retval true:success false:failed
 */
bool BlueNRG_ReadConfig(uint8_t offset)
{
    uint8_t dummy[6];
    return (aci_hal_read_config_data(offset, NULL, dummy) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_ReadConfigResp(uint8_t *data, uint8_t len, uint8_t *cfg, uint8_t size)
{
    /* aci_hal_read_config_data_rp0 */
    if (len >= (size + 2)) {
        memcpy(cfg, &data[2], size);
        return true;
    }
    return false;
}

/***************
 * GATT Commands
 ***************/
bool BlueNRG_AddService(uint8_t uuid_type,
                        Service_UUID_t *uuid,
                        uint8_t service_type,
                        uint8_t max_attribute_records)
{
    return (aci_gatt_add_service(uuid_type, uuid, service_type, max_attribute_records, NULL) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_AddServiceResp(uint8_t *data, uint8_t len, uint16_t *service_handle)
{
    if (len == sizeof(aci_gatt_add_service_rp0)) {
        *service_handle = (uint16_t)data[1] | ((uint16_t)data[2] << 8);
        return true;
    }
    return false;
}

bool BlueNRG_AddChar(uint16_t service_handle,
                     uint8_t uuid_type,
                     Char_UUID_t *uuid,
                     uint16_t char_value_length,
                     uint8_t char_properties,
                     uint8_t security_permissions,
                     uint8_t gatt_evt_mask,
                     uint8_t enc_key_size,
                     uint8_t is_variable)
{
    return (aci_gatt_add_char(service_handle, uuid_type, uuid,
                              char_value_length, char_properties, security_permissions,
                              gatt_evt_mask, enc_key_size, is_variable, NULL) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_AddCharResp(uint8_t *data, uint8_t len, uint16_t *char_handle)
{
    if (len == sizeof(aci_gatt_add_char_rp0)) {
        *char_handle = (uint16_t)data[1] | ((uint16_t)data[2] << 8);
        return true;
    }
    return false;
}

bool BlueNRG_AddCharDesc(uint16_t service_handle,
                         uint16_t char_handle,
                         uint8_t uuid_type,
                         Char_Desc_Uuid_t *uuid,
                         uint8_t desc_value_maxlen,
                         uint8_t desc_value_length,
                         uint8_t desc_value[],
                         uint8_t security_permissions,
                         uint8_t access_permissions,
                         uint8_t gatt_evt_mask,
                         uint8_t enc_key_size,
                         uint8_t is_variable)
{
    return (aci_gatt_add_char_desc(service_handle, char_handle, uuid_type, uuid,
                              desc_value_maxlen, desc_value_length, desc_value,
                              security_permissions, access_permissions,
                              gatt_evt_mask, enc_key_size, is_variable, NULL) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_AddCharDescResp(uint8_t *data, uint8_t len, uint16_t *desc_handle)
{
    if (len == sizeof(aci_gatt_add_char_desc_rp0)) {
        *desc_handle = (uint16_t)data[1] | ((uint16_t)data[2] << 8);
        return true;
    }
    return false;
}

/**************
 * GAP Commands
 **************/
bool BlueNRG_GapInit(uint8_t role,
                     uint8_t privacy_enabled,
                     uint8_t device_name_char_len)
{
    return (aci_gap_init(role, privacy_enabled, device_name_char_len, NULL, NULL, NULL) == BLE_STATUS_SUCCESS);
}

bool BlueNRG_GapInitResp(uint8_t *data,
                         uint8_t len,
                         uint16_t *service_handler,
                         uint16_t *devnam_handler,
                         uint16_t *appearance_handle)
{
    if (len == sizeof(aci_gap_init_rp0)) {
        *service_handler = (uint16_t)data[1] | ((uint16_t)data[2] << 8);
        *devnam_handler = (uint16_t)data[3] | ((uint16_t)data[4] << 8);
        *appearance_handle = (uint16_t)data[5] | ((uint16_t)data[6] << 8);
        return true;
    }
    return false;
}
