/*
 * bluenrg_proc.h
 *
 * Created: 10/8/2020 2:39:36 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef BLUENRG_PROC_H_
#define BLUENRG_PROC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "hci.h"
#include "hci_tl.h"

typedef enum {
    /*****************
     * HCI LE Commands
     *****************/
    BNRG_EVT_RESET = htobs(cmd_opcode_pack(0x03, 0x003)),
    BNRG_EVT_BDADDR = htobs(cmd_opcode_pack(0x04, 0x009)),
    BNRG_EVT_DISCONN = htobs(cmd_opcode_pack(0x01, 0x006)),
    /**************
     * HAL Commands
     **************/
    BNRG_EVT_VERSION = htobs(cmd_opcode_pack(0x3f, 0x001)),
    BNRG_EVT_RD_CFG = htobs(cmd_opcode_pack(0x3f, 0x00d)),
    BNRG_EVT_WR_CFG = htobs(cmd_opcode_pack(0x3f, 0x00c)),
    BNRG_EVT_TXPOWER = htobs(cmd_opcode_pack(0x3f, 0x00f)),
    /***************
     * GATT Commands
     ***************/
    BNRG_EVT_GATT_INIT = htobs(cmd_opcode_pack(0x3f, 0x101)),
    BNRG_EVT_ADD_SVC = htobs(cmd_opcode_pack(0x3f, 0x102)),
    BNRG_EVT_ADD_CHAR = htobs(cmd_opcode_pack(0x3f, 0x104)),
    BNRG_EVT_ADD_DESC = htobs(cmd_opcode_pack(0x3f, 0x105)),
    BNRG_EVT_UPD_CHAR = htobs(cmd_opcode_pack(0x3f, 0x106)),
    BNRG_EVT_DISC_ALL_SVC = htobs(cmd_opcode_pack(0x3f, 0x112)),
    BNRG_EVT_DISC_ALL_CHAR = htobs(cmd_opcode_pack(0x3f, 0x115)),
    BNRG_EVT_DISC_ALL_DESC = htobs(cmd_opcode_pack(0x3f, 0x117)),
    BNRG_EVT_WRITE_CHAR = htobs(cmd_opcode_pack(0x3f, 0x11c)),
    BNRG_EVT_WRITE_DESC = htobs(cmd_opcode_pack(0x3f, 0x121)),
    BNRG_EVT_WRITE_CHAR_NORESP = htobs(cmd_opcode_pack(0x3f, 0x123)),
    BNRG_EVT_RESP_INDICATION = htobs(cmd_opcode_pack(0x3f, 0x125)),
    /**************
     * GAP Commands
     **************/
    BNRG_EVT_GAP_INIT = htobs(cmd_opcode_pack(0x3f, 0x08a)),
    BNRG_EVT_SET_CAP = htobs(cmd_opcode_pack(0x3f, 0x085)),
    BNRG_EVT_SET_AUTH = htobs(cmd_opcode_pack(0x3f, 0x086)),
    BNRG_EVT_SCAN_GENERAL = htobs(cmd_opcode_pack(0x3f, 0x097)),
    BNRG_EVT_CREATE_CONN = htobs(cmd_opcode_pack(0x3f, 0x09c)),
    BNRG_EVT_STOP_GAP_PROC = htobs(cmd_opcode_pack(0x3f, 0x09d)),
    BNRG_EVT_REMOVE_BOND = htobs(cmd_opcode_pack(0x3f, 0x0aa)),
} bnrg_opcode_t;

typedef enum {
    BNRG_DEVICE_NONE,
    BNRG_DEVICE_FAILED,
    BNRG_DEVICE_REBOOT,
    BNRG_DEVICE_RESET,
    BNRG_DEVICE_IDLE,
    BNRG_DEVICE_GETVER,         // 5
    BNRG_DEVICE_SETPWR,
    BNRG_DEVICE_SETMAC,
    BNRG_DEVICE_GETMAC,
    BNRG_GATT_INIT,
    BNRG_GAP_INIT,              // 10
    BNRG_SET_CAP,
    BNRG_SET_AUTH,
    BNRG_START_SCAN,
    BNRG_SCANNING,
    BNRG_REMOTE_DISCONNECTING,  // 15
    BNRG_REMOTE_FOUND,
    BNRG_REMOTE_CONNECTING,
    BNRG_REMOTE_DISCOVERY,
    BNRG_REMOTE_READCHAR,
    BNRG_REMOTE_READDESC,       // 20
    BNRG_REMOTE_SUBSCRIBING,
    BNRG_REMOTE_CONNECTED,
    BNRG_REMOTE_WRITING,
    BNRG_REMOTE_RESP_IND
} bnrg_state_t;

typedef enum {
    BNRG_FIXED_UUID,
    BNRG_CUSTOM_UUID,
    BNRG_BOTH_UUID
} bnrg_uuid_type_t;

typedef enum {
    BNRG_DEV_UNKNOWN,
    BNRG_DEV_ANDROID,
    BNRG_DEV_APPLE
} bnrg_device_type_t;

typedef struct bnrg_local {
    uint8_t state;              // BlueNRG_State_t in bluenrg_app.h
    uint8_t addr_type;          // 0:public 1:random
    uint8_t addr[6];
    uint8_t uuid_type;          // 0:fixed, 1:custom, 2:fixed and custom
    uint8_t sn[7];
    uint8_t tx_power;           // 0-7 (7:8dBm, 6:4dBm 5:2dBm* 4:-2dBm 3:-5dBm 2:-8dBm 1:-11dBm 0:-14dBm)
    uint8_t rx_size;            // bit7: type(0:notify,1:identication) bit0-3:length
    uint8_t rx_data[11];
    uint8_t tx_desc;            // 0:write to char 1:write to desc
    uint8_t tx_retry;
    uint8_t tx_size;
    uint8_t *tx_data;
} bnrg_local_t;

/*
 * Characteristic Properties (Volume 3, Part G, section 3.3.1.1 of Bluetooth Specification 4.1)
 *   Flags:
 *   - 0x00: CHAR_PROP_NONE
 *   - 0x01: CHAR_PROP_BROADCAST (Broadcast)
 *   - 0x02: CHAR_PROP_READ (Read)
 *   - 0x04: CHAR_PROP_WRITE_WITHOUT_RESP (Write w/o resp)
 *   - 0x08: CHAR_PROP_WRITE (Write)
 *   - 0x10: CHAR_PROP_NOTIFY (Notify)
 *   - 0x20: CHAR_PROP_INDICATE (Indicate)
 *   - 0x40: CHAR_PROP_SIGNED_WRITE (Authenticated Signed Writes)
 *   - 0x80: CHAR_PROP_EXT (Extended Properties)
 */
typedef struct bnrg_remote {
    uint8_t device_type;        // 0:unknown 1:android 2:apple
    uint8_t addr_type;          // 0:public 1:random 2:resolvable private 3:non-resolvable private
    uint8_t addr[6];            // mobile address
    uint16_t conn_handle;       // connection handle
    uint16_t svc_handle_start;  // service handle start
    uint16_t svc_handle_end;    // service handle end
    uint16_t char_handle;       // characteristic handle
    uint16_t desc_handle;       // characteristic descriptor handle
    uint8_t flags;              // properties
    uint8_t uuid_type;
} bnrg_remote_t;

void BlueNRG_ProcessStart(uint8_t state);
void BlueNRG_ProcessEnd(uint16_t opcode, uint8_t status, uint8_t *data, uint8_t len);
void BlueNRG_ProcessTimeout(void);

#ifdef __cplusplus
}
#endif

#endif /* BLUENRG_PROC_H_ */
