/*
 * bluenrg_resp.h
 *
 * Created: 10/8/2020 3:09:24 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#ifndef BLUENRG_RESP_H_
#define BLUENRG_RESP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "hci.h"
#include "hci_tl.h"

void BlueNRG_CommandResponse(hci_event_pckt *pckt);

typedef void (*hci_command_response_process)(uint16_t opcode, uint8_t status, uint8_t *buffer, uint8_t length);
typedef struct hci_command_response_table_type_s {
    uint16_t opcode;
    hci_command_response_process process;
} hci_command_status_events_table_type, hci_command_complete_events_table_type;

#ifdef __cplusplus
}
#endif

#endif /* BLUENRG_RESP_H_ */
