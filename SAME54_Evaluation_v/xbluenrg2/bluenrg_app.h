#ifndef __BLUENRG_APP_H__
#define __BLUENRG_APP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

typedef enum {
    BNRG_NONE,   /* no Bluetooth device connected. */
    BNRG_FAIL,   /* Bluetooth failed for unknown reason. it'll switch to BNRG_IDLE or BNRG_NONE once it's recovered. */
    BNRG_INIT,   /* Bluetooth device is initializing. */
    BNRG_IDLE,   /* Bluetooth device is in idle mode and dislinked. */
    BNRG_DONE,   /* Bluetooth is disconnecting and terminating. it'll switch to BNRG_IDLE once it's completed. */
    BNRG_SCAN,   /* Bluetooth is scanning or connecting to a mobile device. it'll switch to BNRG_LINK once connected. */
    BNRG_LINK,   /* Bluetooth has linked to a mobile device and is ready to send data to the mobile. */
    BNRG_SEND,   /* Bluetooth is sending data to a mobile device. it'll switch back to BNRG_LINK once it's done. */
} BlueNRG_State_t;

/* @brief init Bluetooth device */
void BlueNRG_Init(void);

/* @brief user must call this function periodically to handle Bluetooth events */
void BlueNRG_Process(void);

/* @brief see BlueNRG_State_t */
uint8_t BlueNRG_GetState(void);

/* @brief set tx power. it only works after reset.
 * @param pwr:0-7 (7:8dBm, 6:4dBm 5:2dBm* 4:-2dBm 3:-5dBm 2:-8dBm 1:-11dBm 0:-14dBm)
 */
void BlueNRG_SetTxPower(uint8_t pwr);
uint8_t BlueNRG_GetTxPower(void);

/* @brief set public MAC address. it only works after reset.
 *        if it isn't set, a static random address will be used.
 */
void BlueNRG_SetMACAddr(const uint8_t *addr);
void BlueNRG_GetMACAddr(uint8_t addr[6]);

/* @brief set custom UUID. if not set, it uses a fixed UUID.
 *        it only works when restarting a scan.
 * @param sn is 7 characters '0'-'9' or 'A'-'F'.
 * @ custom true:only custom UUID acceptable, false:both fixed and custom UUID acceptable
 */
void BlueNRG_SetSerialNum(const uint8_t *sn, bool custom);

/* @brief start Bluetooth scan and try to link to a mobile device
 *        it is available only when Bluetooth state is in BNRG_IDLE mode.
 */
bool BlueNRG_Enable(void);

/* @brief terminate Bluetooth connection and scan
 *        it is available only when Bluetooth state is scanning or linked.
 */
void BlueNRG_Disable(void);

/* @brief reset Bluetooth device.
 *        it is available only when Bluetooth state is in BNRG_IDLE mode.
 */
void BlueNRG_Reset(void);

/*  @brief send data to a connected mobile device
 *        it is available only when Bluetooth state is in BNRG_LINK mode.
 */
bool BlueNRG_SendData(uint8_t *data, uint8_t length);
bool BlueNRG_SendAttr(uint8_t *data, uint8_t length);

/*  @brief receive data from a connected mobile device
 *        it is available only when Bluetooth state is in BNRG_LINK mode.
 */
uint8_t BlueNRG_RecvData(uint8_t *data, uint8_t size);
void BlueNRG_ConfirmIndication(void);

#ifdef __cplusplus
}
#endif

#endif
