#include <stdio.h>
#include <string.h>
#include "lib/serial_comm.h"
#include "lib/systick.h"
#include "lib/packet.h"
#include "hci.h"
#include "hci_tl.h"
#include "hci_tl_interface.h"
#include "bluenrg_resp.h"
#include "bluenrg_proc.h"

static pFRAME rx_frame;

static int32_t HCI_TL_Read(uint8_t* buffer, uint16_t size)
{
    int32_t len = 0;
    if (rx_frame) {
        if ((rx_frame->length - HEADER_SIZE) <= size) {
            len = rx_frame->length - HEADER_SIZE;
            memcpy(buffer, rx_frame->payload, len);
#if 0
            printf("RX:");
            for (int32_t i = 0; i < len; i++) printf(" %02X", buffer[i]);
            printf("\n");
#endif
        }
        rx_frame = NULL;
    }
    return len;
}

static int32_t HCI_TL_Send(uint8_t* buffer, uint16_t size)
{
    BPACKET packet;

    packet.header.classID = 0xFF;
    packet.header.commandID = 0xFF;
    packet.header.dataType = 0xFF;
    packet.header.dataIndex = 0xFF;
    packet.header.length = size;
    packet.packet = buffer;
#if 0
    printf("TX:");
    for (uint16_t i = 0; i < size; i++) printf(" %02X", buffer[i]);
    printf("\n");
#endif
    if (scom_send(&packet))
        return 0;
    else
        return -1;
}

int32_t HCI_TL_GetTick(void)
{
    /* Return system tick in ms */
    return HAL_SYSTICK_GetTicks();
}

void BlueNRG_SPI_Reset(void)
{
    uint8_t reset_cmd[] = {0xFF, 0x04};
    HCI_TL_Send(reset_cmd, sizeof(reset_cmd));
}

void BlueNRG_Device_Reboot(void)
{
    uint8_t reboot_cmd[] = {0xFF, 0x06};
    HCI_TL_Send(reboot_cmd, sizeof(reboot_cmd));
}

void hci_tl_lowlevel_init(void)
{
    tHciIO fops;
    rx_frame = NULL;
    /* Register IO bus services */
    fops.Init    = NULL;
    fops.DeInit  = NULL;
    fops.Send    = HCI_TL_Send;
    fops.Receive = HCI_TL_Read;
    fops.Reset   = NULL;
    fops.GetTick = HCI_TL_GetTick;
    hci_register_io_bus(&fops);
}

void hci_tl_lowlevel_receive(void *pData)
{
    pFRAME frame = pData;
    if (frame->length > HEADER_SIZE) {
        rx_frame = frame;
        hci_notify_asynch_evt(NULL); /* this function calls HCI_TL_Read */
        hci_user_evt_proc();
    }
    else if (frame->length == HEADER_SIZE) {
        /* Bluetooth dongle plugged in */
        debug_print(DBG_BLUETOOTH, "BLE Dongle Connected.");
        BlueNRG_ProcessStart(BNRG_DEVICE_RESET);
    }
}

void hci_tl_lowlevel_process(void *pData)
{
    hci_spi_pckt *hci_pckt = (hci_spi_pckt *)pData;
    if (hci_pckt->type == HCI_EVENT_PKT) {
        uint32_t i;
        hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;
        if (event_pckt->evt == EVT_LE_META_EVENT) {
            evt_le_meta_event *evt = (void *)event_pckt->data;
            for (i = 0; i < (sizeof(hci_le_meta_events_table)/sizeof(hci_le_meta_events_table_type)); i++) {
                if (evt->subevent == hci_le_meta_events_table[i].evt_code) {
                    hci_le_meta_events_table[i].process((void *)evt->data);
                }
            }
        }
        else if (event_pckt->evt == EVT_VENDOR) {
            evt_blue_aci *blue_evt = (void*)event_pckt->data;
            for (i = 0; i < (sizeof(hci_vendor_specific_events_table)/sizeof(hci_vendor_specific_events_table_type)); i++) {
                if (blue_evt->ecode == hci_vendor_specific_events_table[i].evt_code) {
                    hci_vendor_specific_events_table[i].process((void *)blue_evt->data);
                }
            }
        }
        else {
            for (i = 0; i < (sizeof(hci_events_table)/sizeof(hci_events_table_type)); i++) {
                if (event_pckt->evt == hci_events_table[i].evt_code) {
                    hci_events_table[i].process((void *)event_pckt->data);
                    break;
                }
            }
            if (i >= (sizeof(hci_events_table)/sizeof(hci_events_table_type))) {
                BlueNRG_CommandResponse((hci_event_pckt*)hci_pckt->data);
            }
        }
    }
}
