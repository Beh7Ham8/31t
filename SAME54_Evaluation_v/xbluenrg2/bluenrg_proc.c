/*
 * bluenrg_proc.c
 *
 * Created: 10/8/2020 2:39:26 PM
 *  Author: patrick.xu@silver-bullet-tech.com
 */ 
#include <stdint.h>
#include <string.h>
#include <atmel_start.h>
#include "lib/systick.h"
#include "hci.h"
#include "hci_tl.h"
#include "bluenrg1_hal_aci.h"
#include "bluenrg1_hci_le.h"
#include "bluenrg1_gap_aci.h"
#include "bluenrg1_gatt_aci.h"
#include "bluenrg1_events.h"
#include "bluenrg_proc.h"
#include "bluenrg_cmds.h"
#include "debugMon.h"

static uint8_t mobileAndroidAdvUUID[16] = {0x9D, 0x40, 0x6D, 0x5C, 0xCB, 0xAB, 0x59, 0xA6, 0x64, 0x4F, 0xFE, 0xA7, 0x2D, 0xD0, 0x55, 0xAC};
static uint8_t mobileAndroidCharUUID[16] = {0x8D, 0x94, 0x73, 0x81, 0xB9, 0x49, 0xC9, 0x96, 0x3A, 0x47, 0x99, 0xB8, 0x46, 0x5B, 0x8C, 0xA8};
static uint8_t mobileAppleAdvUUID[16] = {0x99, 0x40, 0x6D, 0x5C, 0xCB, 0xAB, 0x59, 0xA6, 0x64, 0x4F, 0xFE, 0xA7, 0x2D, 0xD0, 0x55, 0xAC};
static uint8_t mobileAppleCharUUID[16] = {0x82, 0x94, 0x73, 0x81, 0xB9, 0x49, 0xC9, 0x96, 0x3A, 0x47, 0x99, 0xB8, 0x46, 0x5B, 0x8C, 0x78};
static uint8_t customAndroidAdvUUID[16], customAppleAdvUUID[16];
static uint8_t customAndroidCharUUID[16], customAppleCharUUID[16];

static uint64_t start_time;
extern bnrg_local_t bluenrg;
static bnrg_remote_t remote;

static void BlueNRG_CreateCustomUUID(uint8_t *mobile, uint8_t *custom, uint8_t *uuid)
{
    memcpy(custom, mobile, 16);
    custom[6] = uuid[3];
    custom[7] = uuid[2];
    custom[10] = uuid[1];
    custom[11] = uuid[0];
}

static uint8_t BlueNRG_CharToHex(uint8_t ch1, uint8_t ch2)
{
    uint8_t hi, lo;
    hi = (ch1 >= 'A') ? (ch1 - 'A' + 10) : (ch1 - '0');
    lo = (ch2 >= 'A') ? (ch2 - 'A' + 10) : (ch2 - '0');
    return hi * 16 + lo;
}

static void BlueNRG_CreateCustomUUIDs(void)
{
    if (bluenrg.uuid_type > 0) {
        uint8_t uuid[4];
        uuid[0] = BlueNRG_CharToHex('0', bluenrg.sn[0]);
        uuid[1] = BlueNRG_CharToHex(bluenrg.sn[1], bluenrg.sn[2]);
        uuid[2] = BlueNRG_CharToHex(bluenrg.sn[3], bluenrg.sn[4]);
        uuid[3] = BlueNRG_CharToHex(bluenrg.sn[5], bluenrg.sn[6]);
        BlueNRG_CreateCustomUUID(mobileAndroidAdvUUID, customAndroidAdvUUID, uuid);
        BlueNRG_CreateCustomUUID(mobileAndroidCharUUID, customAndroidCharUUID, uuid);
        BlueNRG_CreateCustomUUID(mobileAppleAdvUUID, customAppleAdvUUID, uuid);
        BlueNRG_CreateCustomUUID(mobileAppleCharUUID, customAppleCharUUID, uuid);
    }
}

static bool BlueNRG_CompareAdvUUID(uint8_t *uuid)
{
    if (remote.device_type == BNRG_DEV_UNKNOWN || remote.device_type == BNRG_DEV_ANDROID) {
        if (remote.uuid_type == BNRG_CUSTOM_UUID || remote.uuid_type == BNRG_BOTH_UUID) {
            if (memcmp(customAndroidAdvUUID, uuid, 16) == 0) {
                remote.device_type = BNRG_DEV_ANDROID;
                remote.uuid_type = BNRG_CUSTOM_UUID;
                return true;
            }
        }
        if (remote.uuid_type == BNRG_FIXED_UUID || remote.uuid_type == BNRG_BOTH_UUID) {
            if (memcmp(mobileAndroidAdvUUID, uuid, 16) == 0) {
                remote.device_type = BNRG_DEV_ANDROID;
                remote.uuid_type = BNRG_FIXED_UUID;
                return true;
            }
        }
    }
    if (remote.device_type == BNRG_DEV_UNKNOWN || remote.device_type == BNRG_DEV_APPLE) {
        if (remote.uuid_type == BNRG_CUSTOM_UUID || remote.uuid_type == BNRG_BOTH_UUID) {
            if (memcmp(customAppleAdvUUID, uuid, 16) == 0) {
                remote.device_type = BNRG_DEV_APPLE;
                remote.uuid_type = BNRG_CUSTOM_UUID;
                return true;
            }
        }
        if (remote.uuid_type == BNRG_FIXED_UUID || remote.uuid_type == BNRG_BOTH_UUID) {
            if (memcmp(mobileAppleAdvUUID, uuid, 16) == 0) {
                remote.device_type = BNRG_DEV_APPLE;
                remote.uuid_type = BNRG_FIXED_UUID;
                return true;
            }
        }
    }
    return false;
}

static bool BlueNRG_CompareCharUUID(uint8_t *uuid)
{
    if (remote.device_type == BNRG_DEV_ANDROID) {
        if (remote.uuid_type == BNRG_CUSTOM_UUID) {
            if (memcmp(customAndroidCharUUID, uuid, 16) == 0) return true;
        }
        if (remote.uuid_type == BNRG_FIXED_UUID) {
            if (memcmp(mobileAndroidCharUUID, uuid, 16) == 0) return true;
        }
    }
    if (remote.device_type == BNRG_DEV_APPLE) {
        if (remote.uuid_type == BNRG_CUSTOM_UUID) {
            if (memcmp(customAppleCharUUID, uuid, 16) == 0) return true;
        }
        if (remote.uuid_type == BNRG_FIXED_UUID) {
            if (memcmp(mobileAppleCharUUID, uuid, 16) == 0) return true;
        }
    }
    return false;
}

void BlueNRG_ProcessStart(uint8_t state)
{
    /* do nothing when it's doing disconnection */
    if (BNRG_REMOTE_DISCONNECTING == bluenrg.state && state > BNRG_DEVICE_IDLE) {
        start_time = HAL_SYSTICK_GetTicks();
        return;
    }
    switch (state) {
    case BNRG_DEVICE_REBOOT:
        BlueNRG_Device_Reboot();
        break;
    case BNRG_DEVICE_RESET:
        hci_reset();
        break;
    case BNRG_DEVICE_GETVER:
        BlueNRG_GetVersion();
        break;
    case BNRG_DEVICE_SETPWR:
        /* command to set tx power to hi-power in 2 dBm
         * (7:8dBm, 6:4dBm 5:2dBm 4:-2dBm 3:-5dBm 2:-8dBm 1:-11dBm 0:-14dBm)
         */
        aci_hal_set_tx_power_level(1, bluenrg.tx_power);
        break;
    case BNRG_DEVICE_SETMAC:
        /* set public MAC address */
        aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, bluenrg.addr);
        break;
    case BNRG_DEVICE_GETMAC:
        /* read static random address */
        BlueNRG_ReadConfig(0x80);
        break;
    case BNRG_GATT_INIT:
        aci_gatt_init();
        break;
    case BNRG_GAP_INIT:
        BlueNRG_GapInit(GAP_CENTRAL_ROLE, 0, 8);
        break;
    case BNRG_SET_CAP:
        aci_gap_set_io_capability(IO_CAP_DISPLAY_ONLY);
        break;
    case BNRG_SET_AUTH:
        aci_gap_set_authentication_requirement(BONDING, MITM_PROTECTION_NOT_REQUIRED,
                                               SC_IS_NOT_SUPPORTED, KEYPRESS_IS_NOT_SUPPORTED, 8, 16,
                                               DONOT_USE_FIXED_PIN_FOR_PAIRING, 0, 1);
        break;
    case BNRG_START_SCAN:
        /* entry for starting a scan */
        memset(&remote, 0, sizeof(remote));
        remote.uuid_type = bluenrg.uuid_type;
        BlueNRG_CreateCustomUUIDs();
        /* command to start scan for N*0.625ms with duplicate advertising packet filtering */
        aci_gap_start_general_discovery_proc(800, 800, bluenrg.addr_type, 1);
        break;
    case BNRG_REMOTE_CONNECTING:
        /* command to set up connection
         * LE_Scan_Interval,LE_Scan_Window:     N * 0.625ms
         * Conn_Interval_Min/Conn_Interval_Max: N * 1.25ms
         * Supervision_Timeout:                 N * 10 ms
         * Minimum_CE_Length/Maximum_CE_Length: N * 0.625ms
         */
        aci_gap_create_connection(800, 800, remote.addr_type, remote.addr, bluenrg.addr_type, 40, 80, 0, 400, 20, 20);
        break;
    case BNRG_REMOTE_DISCOVERY:
        aci_gatt_disc_all_primary_services(remote.conn_handle);
        break;
    case BNRG_REMOTE_READCHAR:
        aci_gatt_disc_all_char_of_service(remote.conn_handle, remote.svc_handle_start, remote.svc_handle_end);
        break;
    case BNRG_REMOTE_READDESC:
        aci_gatt_disc_all_char_desc(remote.conn_handle, remote.char_handle, remote.char_handle + 2);
        break;
    case BNRG_REMOTE_SUBSCRIBING:
        if (remote.desc_handle && (remote.flags & CHAR_PROP_NOTIFY)) {
            uint16_t enable = 0x0001;
            aci_gatt_write_char_desc(remote.conn_handle, remote.desc_handle, 2, (uint8_t *)&enable);
        }
        break;
    case BNRG_REMOTE_WRITING:
        if (bluenrg.tx_desc)
            aci_gatt_write_char_desc(remote.conn_handle, remote.desc_handle, bluenrg.tx_size, bluenrg.tx_data);
        else
            aci_gatt_write_char_value(remote.conn_handle, remote.char_handle, bluenrg.tx_size, bluenrg.tx_data);
        break;
    case BNRG_REMOTE_RESP_IND:
        aci_gatt_confirm_indication(remote.conn_handle);
        break;
    case BNRG_REMOTE_DISCONNECTING:
        /* It is important to leave an 100 ms blank window before sending any new command */
        if (remote.conn_handle) {
            hci_disconnect(remote.conn_handle, 0x13);
        }
        break;
    }
    if (BNRG_DEVICE_FAILED != bluenrg.state) bluenrg.state = state;
    start_time = HAL_SYSTICK_GetTicks();
    //debug_print(DBG_BLUETOOTH, "BLE [%010lu] State %u", (uint32_t)start_time, bluenrg.state);
}

void BlueNRG_ProcessTimeout(void)
{
    uint32_t timeout = 0;
    switch (bluenrg.state) {
    case BNRG_DEVICE_NONE:
    case BNRG_DEVICE_IDLE:
    case BNRG_REMOTE_CONNECTED:
        timeout = 0;
        break;
    case BNRG_DEVICE_FAILED:
    case BNRG_DEVICE_RESET:
    case BNRG_REMOTE_WRITING:
    case BNRG_REMOTE_RESP_IND:
        timeout = 5000;
        break;
    case BNRG_DEVICE_REBOOT:
    case BNRG_REMOTE_DISCOVERY:
        timeout = 10000;
        break;
    case BNRG_SCANNING:
    case BNRG_REMOTE_FOUND:
    case BNRG_REMOTE_DISCONNECTING:
        timeout = 15000;
        break;
    default:
        timeout = 3000;
        break;
    }
    if (timeout != 0 && (HAL_SYSTICK_GetTicks() - start_time) >= timeout) {
        if (BNRG_DEVICE_FAILED != bluenrg.state) {
            debug_print(DBG_BLUETOOTH, "BLE No Response %u.", bluenrg.state);
        }
        if (BNRG_DEVICE_REBOOT == bluenrg.state)
            BlueNRG_ProcessStart(BNRG_DEVICE_NONE); // Bluetooth unplugged
        else if (BNRG_DEVICE_RESET == bluenrg.state)
            BlueNRG_ProcessStart(BNRG_DEVICE_REBOOT);
        else
            BlueNRG_ProcessStart(BNRG_DEVICE_RESET);
    }
}

/* BLE command complete actions */
void BlueNRG_ProcessEnd(uint16_t opcode, uint8_t status, uint8_t *data, uint8_t len)
{
    switch (opcode) {
    case BNRG_EVT_RESET:
        /* aci_blue_initialized_event() will be called after this event */
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    case BNRG_EVT_VERSION:
        if (status == BLE_STATUS_SUCCESS) {
            uint8_t dtm_ver[3], ble_ver[3];
            BlueNRG_GetVersionResp(data, len, dtm_ver, ble_ver);
            debug_print(DBG_BLUETOOTH, "BLE Dongle DTM:.v%u.%u.%u BLE: v%u.%u.%u",
                        dtm_ver[0], dtm_ver[1], dtm_ver[2], ble_ver[0], ble_ver[1], ble_ver[2]);
            BlueNRG_ProcessStart(BNRG_DEVICE_SETPWR);
        }
        break;
    case BNRG_EVT_TXPOWER:
        if (status == BLE_STATUS_SUCCESS) {
            if (bluenrg.addr_type == PUBLIC_ADDR)
                BlueNRG_ProcessStart(BNRG_DEVICE_SETMAC);
            else
                BlueNRG_ProcessStart(BNRG_DEVICE_GETMAC);
        }
        break;
    case BNRG_EVT_WR_CFG:
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ProcessStart(BNRG_GATT_INIT);
        }
        break;
    case BNRG_EVT_RD_CFG:
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ReadConfigResp(data, len, bluenrg.addr, 6);
            debug_print(DBG_BLUETOOTH, "BLE MAC Addr: %02X:%02X:%02X:%02X:%02X:%02X",
                        bluenrg.addr[5], bluenrg.addr[4], bluenrg.addr[3],
                        bluenrg.addr[2], bluenrg.addr[1], bluenrg.addr[0]);
            BlueNRG_ProcessStart(BNRG_GATT_INIT);
        }
        break;
    case BNRG_EVT_GATT_INIT:
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ProcessStart(BNRG_GAP_INIT);
        }
        break;
    case BNRG_EVT_GAP_INIT:
        if (status == BLE_STATUS_SUCCESS) {
            uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
            BlueNRG_GapInitResp(data, len, &service_handle, &dev_name_char_handle, &appearance_char_handle);
            BlueNRG_ProcessStart(BNRG_SET_CAP);
        }
        break;
    case BNRG_EVT_SET_CAP:
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ProcessStart(BNRG_SET_AUTH);
        }
        break;
    case BNRG_EVT_SET_AUTH:
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ProcessStart(BNRG_DEVICE_IDLE);
        }
        break;
    case BNRG_EVT_SCAN_GENERAL:
        /* hci_le_advertising_report_event() will be called when a device is detected,
         * and aci_gap_proc_complete_event will be called when the scan is done.
         */
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ProcessStart(BNRG_SCANNING);
            debug_print(DBG_BLUETOOTH, "BLE Start Scan");
        }
        break;
    case BNRG_EVT_CREATE_CONN:
        /* hci_le_connection_complete_event will be called after create a connection */
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    case BNRG_EVT_DISC_ALL_SVC:
        /* aci_att_read_by_group_type_resp_event will be called when it's done successfully */
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    case BNRG_EVT_DISC_ALL_CHAR:
        /* aci_att_read_by_type_resp_event will be called when it's done successfully */
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    case BNRG_EVT_DISC_ALL_DESC:
        /* aci_att_find_info_resp_event will be called when it's done successfully */
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    case BNRG_EVT_WRITE_DESC:
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    case BNRG_EVT_WRITE_CHAR:
    case BNRG_EVT_WRITE_CHAR_NORESP:
        if (bluenrg.state != BNRG_REMOTE_WRITING) {
            status = BLE_STATUS_SUCCESS;
        }
        else if (status == BLE_STATUS_SUCCESS) {
            bluenrg.tx_retry = 0;
        }
        else if (status == BLE_STATUS_INSUFFICIENT_RESOURCES) {
            // retry
            if ((++bluenrg.tx_retry) < 10) status = BLE_STATUS_SUCCESS;
        }
        break;
    case BNRG_EVT_RESP_INDICATION:
        if (status == BLE_STATUS_SUCCESS) {
            BlueNRG_ProcessStart(BNRG_REMOTE_CONNECTED);
        }
        break;
    case BNRG_EVT_DISCONN:
        if (status == BLE_STATUS_SUCCESS) {
        }
        break;
    }
    if (status != BLE_STATUS_SUCCESS && bluenrg.state != BNRG_DEVICE_FAILED) {
        debug_print(DBG_BLUETOOTH, "BLE CMD (%u.%02X.%03X) Failed (%02X)",
                    bluenrg.state, (opcode >> 10), (opcode & 0x3FF), status);
        BlueNRG_ProcessStart(BNRG_DEVICE_FAILED);
    }
}

void hci_le_advertising_report_event(uint8_t Num_Reports, Advertising_Report_t Advertising_Report[])
{
    uint8_t i, j;
    if (bluenrg.state == BNRG_REMOTE_FOUND) return; // already found one
    for (i = 0; i < Num_Reports; i++) {
        Advertising_Report_t *report = &Advertising_Report[i];
        if (report->Event_Type != ADV_IND) continue;
        j = 0;
        while (j < report->Length_Data) {
            uint8_t len = report->Data[j];
            uint8_t type = report->Data[j + 1];
            if (len == 17 && (type == 6 || type == 7)) {
                if (BlueNRG_CompareAdvUUID(&report->Data[j + 2])) {
                    remote.addr_type = report->Address_Type;
                    memcpy(remote.addr, report->Address, 6);
                    debug_print(DBG_BLUETOOTH, "BLE Remote Found %02X:%02X:%02X:%02X:%02X:%02X (%u)",
                                remote.addr[5], remote.addr[4], remote.addr[3],
                                remote.addr[2], remote.addr[1], remote.addr[0], remote.addr_type);
                    BlueNRG_ProcessStart(BNRG_REMOTE_FOUND);
                    return;
                }
            }
            j += len + 1;
        }
    }
}

void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
{
    debug_print(DBG_BLUETOOTH, "BLE Remote Connected %02X:%02X:%02X:%02X:%02X:%02X (%u)",
                Peer_Address[5], Peer_Address[4], Peer_Address[3],
                Peer_Address[2], Peer_Address[1], Peer_Address[0], Peer_Address_Type);
    remote.conn_handle = Connection_Handle;
    remote.addr_type = Peer_Address_Type;
    memcpy(remote.addr, Peer_Address, 6);
    BlueNRG_ProcessStart(BNRG_REMOTE_DISCOVERY);
}

void aci_att_read_by_group_type_resp_event(uint16_t Connection_Handle,
                                           uint8_t Attribute_Data_Length,
                                           uint8_t Data_Length,
                                           uint8_t Attribute_Data_List[])
{
    if (remote.conn_handle == Connection_Handle) {
        for (int i = 0; i < Data_Length; i += Attribute_Data_Length) {
            uint8_t *ptr = &Attribute_Data_List[i];
            uint16_t start = (uint16_t)ptr[0] | ((uint16_t)ptr[1] << 8);
            uint16_t end = (uint16_t)ptr[2] | ((uint16_t)ptr[3] << 8);
            /* we are interested in only 128 bit UUIDs */
            if (Attribute_Data_Length == 20 && BlueNRG_CompareAdvUUID(&ptr[4])) {
                remote.svc_handle_start = start;
                remote.svc_handle_end = end;
            }
            debug_print(DBG_BLUETOOTH, "BLE SVC %02X%02X %04X-%04X", ptr[5], ptr[4], start, end);
        }
    }
}

void aci_att_read_by_type_resp_event(uint16_t Connection_Handle,
                                     uint8_t Handle_Value_Pair_Length,
                                     uint8_t Data_Length,
                                     uint8_t Handle_Value_Pair_Data[])
{
    if (remote.conn_handle == Connection_Handle) {
        for (int i = 0; i < Data_Length; i += Handle_Value_Pair_Length) {
            uint8_t *ptr = &Handle_Value_Pair_Data[i];
            uint16_t start = (uint16_t)ptr[0] | ((uint16_t)ptr[1] << 8);
            uint16_t handle = (uint16_t)ptr[3] | ((uint16_t)ptr[4] << 8);
            /* we are interested in only 128 bit UUIDs */
            if (Handle_Value_Pair_Length == 21 && BlueNRG_CompareCharUUID(&ptr[5])) {
                remote.char_handle = handle;
                remote.desc_handle = 0;
                remote.flags = ptr[2];
            }
            debug_print(DBG_BLUETOOTH, "BLE CHAR %02X%02X %04X-%04X %02X", ptr[6], ptr[5], start, handle, ptr[2]);
        }
    }
}

void aci_att_find_info_resp_event(uint16_t Connection_Handle,
                                  uint8_t Format,
                                  uint8_t Event_Data_Length,
                                  uint8_t Handle_UUID_Pair[])
{
    if (remote.conn_handle == Connection_Handle) {
        uint8_t i, len = (Format == 1) ? 4 : 18;
        for (i = 0; i < Event_Data_Length; i+=len) {
            uint8_t *ptr = &Handle_UUID_Pair[i];
            uint16_t handle = (uint16_t)ptr[0] | ((uint16_t)ptr[1] << 8);
            uint16_t uuid = (uint16_t)ptr[2] | ((uint16_t)ptr[3] << 8);
            /* we are interested in only 16 bit UUIDs */
            if (len == 4 && uuid == CHAR_CLIENT_CONFIG_DESC_UUID) {
                remote.desc_handle = handle;
            }
            debug_print(DBG_BLUETOOTH, "BLE DESC %04X %04X", uuid, handle);
        }
    }
}

void aci_gatt_notification_event(uint16_t Connection_Handle,
                                 uint16_t Attribute_Handle,
                                 uint8_t Attribute_Value_Length,
                                 uint8_t Attribute_Value[])
{
    if (remote.conn_handle == Connection_Handle) {
        debug_print(DBG_BLUETOOTH, "BLE NOTIFY %04X (%u)",
                    Attribute_Handle, Attribute_Value_Length);
        for (int i = 0; i < Attribute_Value_Length; i++) printf(" %02X", Attribute_Value[i]);
        bluenrg.rx_size = Attribute_Value_Length;
        if (bluenrg.rx_size > sizeof(bluenrg.rx_data)) bluenrg.rx_size = sizeof(bluenrg.rx_data);
        memcpy(bluenrg.rx_data, Attribute_Value, bluenrg.rx_size);
    }
}

void aci_gatt_indication_event(uint16_t Connection_Handle,
                               uint16_t Attribute_Handle,
                               uint8_t Attribute_Value_Length,
                               uint8_t Attribute_Value[])
{
    if (remote.conn_handle == Connection_Handle) {
        debug_print(DBG_BLUETOOTH, "BLE INDICATE %04X (%u)",
                    Attribute_Handle, Attribute_Value_Length);
        for (int i = 0; i < Attribute_Value_Length; i++) printf(" %02X", Attribute_Value[i]);
        bluenrg.rx_size = Attribute_Value_Length;
        if (bluenrg.rx_size > sizeof(bluenrg.rx_data)) bluenrg.rx_size = sizeof(bluenrg.rx_data);
        memcpy(bluenrg.rx_data, Attribute_Value, bluenrg.rx_size);
        bluenrg.rx_size |= 0x80;
    }
}

void aci_gap_proc_complete_event(uint8_t Procedure_Code,
                                 uint8_t Status,
                                 uint8_t Data_Length,
                                 uint8_t Data[])
{
    if (Status == 0 && Procedure_Code == GAP_GENERAL_DISCOVERY_PROC) {
        if (bluenrg.state == BNRG_REMOTE_FOUND) {
            BlueNRG_ProcessStart(BNRG_REMOTE_CONNECTING);
        }
        else {
            debug_print(DBG_BLUETOOTH, "BLE Stop Scan");
            BlueNRG_ProcessStart(BNRG_DEVICE_IDLE);
        }
    }
}

void aci_gatt_proc_complete_event(uint16_t Connection_Handle, uint8_t Error_Code)
{
    if (remote.conn_handle == Connection_Handle) {
        if (Error_Code == 0) {
            switch (bluenrg.state) {
            case BNRG_REMOTE_DISCOVERY:
                if (remote.svc_handle_end != 0) BlueNRG_ProcessStart(BNRG_REMOTE_READCHAR);
                break;
            case BNRG_REMOTE_READCHAR:
                if (remote.desc_handle == 0) BlueNRG_ProcessStart(BNRG_REMOTE_READDESC);
                break;
            case BNRG_REMOTE_READDESC:
                BlueNRG_ProcessStart(BNRG_REMOTE_SUBSCRIBING);
                break;
            case BNRG_REMOTE_SUBSCRIBING:
                debug_print(DBG_BLUETOOTH, "BLE Subscribed");
                bluenrg.tx_retry = 0;
                bluenrg.tx_size = 0;
                bluenrg.rx_size = 0;
                BlueNRG_ProcessStart(BNRG_REMOTE_CONNECTED);
                break;
            case BNRG_REMOTE_WRITING:
                BlueNRG_ProcessStart(BNRG_REMOTE_CONNECTED);
                break;
            }
        }
        else {
            switch (bluenrg.state) {
            case BNRG_REMOTE_WRITING:
                BlueNRG_ProcessStart(BNRG_REMOTE_DISCONNECTING);
            }
        }
    }
    else if (Error_Code != 0) {
        //debug_print(DBG_BLUETOOTH, "BLE GATT Error %02X", Error_Code);
    }
}

void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
    if (remote.conn_handle == Connection_Handle) {
        debug_print(DBG_BLUETOOTH, "BLE Attr modified: %04X %04X, %u", Attr_Handle, Offset, Attr_Data_Length);
        for (int i = 0; i < Attr_Data_Length; i++) printf(" %02X", Attr_Data[i]);
    }
}

void aci_gatt_proc_timeout_event(uint16_t Connection_Handle)
{
    if (remote.conn_handle == Connection_Handle) {
        //debug_print(DBG_BLUETOOTH, "BLE GATT Timeout");
        bluenrg.state = BNRG_REMOTE_DISCONNECTING;
        BlueNRG_ProcessStart(BNRG_REMOTE_DISCONNECTING);
    }
}

void hci_disconnection_complete_event(uint8_t Status,
                                 uint16_t Connection_Handle,
                                 uint8_t Reason)
{
    if (remote.conn_handle == Connection_Handle && Status == 0) {
        debug_print(DBG_BLUETOOTH, "BLE Remote Disconnected (%02X) %02X:%02X:%02X:%02X:%02X:%02X",
                    Reason, remote.addr[5], remote.addr[4], remote.addr[3],
                    remote.addr[2], remote.addr[1], remote.addr[0]);
        if (remote.device_type == BNRG_DEV_APPLE) {
            aci_gap_remove_bonded_device(remote.addr_type, remote.addr);
        }
        if (Reason == 0x13 || Reason == 0x16) { // connection terminated by remote or local
            BlueNRG_ProcessStart(BNRG_DEVICE_IDLE);
        }
        else if (Reason == 0x08) {  // timeout failure
            BlueNRG_ProcessStart(BNRG_DEVICE_REBOOT);
        }
        else {  // failed to be established
            if ((++bluenrg.tx_retry) < 3)
                BlueNRG_ProcessStart(BNRG_DEVICE_FAILED);
            else
                BlueNRG_ProcessStart(BNRG_DEVICE_REBOOT);
        }
    }
}

void aci_blue_initialized_event(uint8_t Reason_Code)
{
    /* bluetooth device initialized */
    debug_print(DBG_BLUETOOTH, "BLE Reset (%u)", Reason_Code);
    // override BNRG_DEVICE_FAILED state
    if (BNRG_DEVICE_FAILED == bluenrg.state) bluenrg.state = BNRG_DEVICE_RESET;
    if (BNRG_DEVICE_GETVER != bluenrg.state) {
        BlueNRG_ProcessStart(BNRG_DEVICE_GETVER);
    }
}

void aci_gatt_error_resp_event(uint16_t Connection_Handle,
                               uint8_t Req_Opcode,
                               uint16_t Attribute_Handle,
                               uint8_t Error_Code)
{
    //debug_print(DBG_BLUETOOTH, "BLE GATT Error %04X %02X %04X %02X", Connection_Handle, Req_Opcode, Attribute_Handle, Error_Code);
}

void hci_hardware_error_event(uint8_t Hardware_Code)
{
    debug_print(DBG_BLUETOOTH, "BLE HW Error (%u)", Hardware_Code);
    BlueNRG_ProcessStart(BNRG_DEVICE_REBOOT);
}

void aci_hal_fw_error_event(uint8_t FW_Error_Type,
                            uint8_t Data_Length,
                            uint8_t Data[])
{
    debug_print(DBG_BLUETOOTH, "BLE FW Error (%u)", FW_Error_Type);
    BlueNRG_ProcessStart(BNRG_DEVICE_REBOOT);
}
