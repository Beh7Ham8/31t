/*
 * adc.h
 *
 * Created: 5/8/2020 2:04:04 PM
 *  Author: BehroozH
 */ 


#ifndef ADC_H_
#define ADC_H_
#include <sys\_stdint.h>

 #define MAX_ADC_READINGS 16

 #define VREF_CONST  1.2484 //1.2483871//1.2281
 #define VREF_BUS    7.47  // 5*1.225-- 1.225 is the ADC measurement at the VREF. The 5 is because of the 5:1 ratio between R60, R55.

 #define VBUS_ON_OFFSET  0.05    // This the voltage above the idle battery level to determine engine ON
 #define VBUS_OFF_OFFSET 0.05    // This the voltage above the idle battery level to determine engine OFF

 typedef struct _TAG_ANALOG_VALUE
 {
	 uint32_t instantaneous;
	 uint32_t average;
	 float    correctedAverage;
	 uint32_t history[MAX_ADC_READINGS];
	 uint32_t previousAverage;
	 uint32_t headPtr;
	 uint32_t tailPtr;
 }ANALOG_VALUE, *pANALOG_VALUE;

uint16_t Read_Adc0(uint8_t ch);
uint16_t Read_Adc1(uint8_t ch);

 void initAnalogVal(pANALOG_VALUE pVal);
 void updateAnalogAverage(pANALOG_VALUE pVal );
 uint16_t getAnalog_0_Inst(uint8_t ch);
 uint16_t getAnalog_1_Inst(uint8_t ch);
 void Dly(uint16_t d);
 bool isBufferFilled(pANALOG_VALUE pVal);
 void Calibrate_ADCs();




#endif /* ADC_H_ */