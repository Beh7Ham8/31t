/*
 * ext_adc.h
 *
 * Created: 6/11/2020 1:18:12 PM
 *  Author: BehroozH
 * This code works for ADS1114 ultra-small low-power i2c-compatible, 16-Bit ADC (Texas Instrument chip)
 */ 


#ifndef EXT_ADC_H_
#define EXT_ADC_H_

void Init_Test_Ext_Adc();
void Test_ADS_111x_Monitor();
void Read_Current();
float Get_Current();
void Init_Shunt_ADS111x();

#define PGA_MASK     (0x0008)    // Programmable gain amplifier configuration
#define PGA_0        (0x0000)    // 6.144V
#define PGA_1        (0x0001)    // 4.096V
#define PGA_2        (0x0002)    // 2.048V (default)
#define PGA_3        (0x0003)    // 1.024V
#define PGA_4        (0x0004)    // 0.512V
#define PGA_5        (0x0005)    // 0.256V


typedef enum
{
	eGAIN_TWOTHIRDS       = PGA_0,
	eGAIN_ONE             = PGA_1,
	eGAIN_TWO             = PGA_2,
	eGAIN_FOUR            = PGA_3,
	eGAIN_EIGHT           = PGA_4,
	eGAIN_SIXTEEN         = PGA_5
} eADSGain_t;

#endif /* EXT_ADC_H_ */