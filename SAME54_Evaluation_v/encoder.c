/*
 * encoder.c
 *
 * Created: 5/8/2020 2:29:13 PM
 *  Author: BehroozH
 */ 
#include <string.h>
#include "encoder.h"
#include <sys\_stdint.h>

    int dest_pos;
    int escaped;

    void initEncoder()
    {
	    dest_pos = 0;
	    escaped = 0;
    }

    // Encode an array of data
    uint16_t encode(uint8_t *dest, uint16_t dest_size, uint8_t *source, uint16_t source_size) 
	{
	    uint16_t i;
	    uint16_t j = 0;
	    uint8_t c;

	    // Zero sized buffers can break things
	    if (dest_size < 1 || dest == 0 || source == 0)
	    return 0;

	    dest[j] = PACKET_END;

	    for (i = 0; i < source_size; i++)
		{
		    c = source[i];

		    if (c == PACKET_END) 
			{
			    if (++j < dest_size) dest[j] = BYTE_ESC;
			    if (++j < dest_size) dest[j] = BYTE_ESC_END;
			} 
			else if (c == BYTE_ESC) 
			{
			    if (++j < dest_size) dest[j] = BYTE_ESC;
			    if (++j < dest_size) dest[j] = BYTE_ESC_ESC;
			} 
			else 
			{
			    if (++j < dest_size) dest[j] = c;
		    }
	    }

	    if (++j < dest_size) dest[j] = PACKET_END;

	    return ++j;
    }




    // Since packets are bordered with END characters on each side we
    // need to save the data between one set of END characters and
    // ignore the data between the other (there should be nothing so it will
    // only be noise

    uint16_t decode(uint8_t *source, uint16_t length, pPACKET pckt) 
	{
	    int source_pos;
	    uint8_t c;
	    uint8_t beginingFound = 0;

	    // we have not decoded anything yet
	    dest_pos = 0;

	    for (source_pos = 0; source_pos < length; source_pos++) 
		{
		    c = source[source_pos];

		    if (c == PACKET_END) 
			{
			    // End (or beginning) of a packet
			    // If we have not decoded any data, this is the beginning
			    if (dest_pos == 0)
			    {
				    // beginning of the packet
				    beginingFound = 1;
				    continue;
			    }
			    else
			    {
				    // we have already decoded some data, so this is the
				    // end of the packet....
				    // valid packet decoded
				    return 1;
			    }


			 } 
			 else if (escaped) 
			 {
				if (c == BYTE_ESC_END) {
					c = PACKET_END;
				}
				else if (c == BYTE_ESC_ESC) {
					c = BYTE_ESC;
				}
				else 
				{
					//TODO:
					// ESC should only be used to escape ESC_END and ESC_ESC
					// so if we get here, warn the user
					// Then again, this is an embedded application, so we can
					// just ignore the ESC character.
					// i.e. use the escape sequence to escape nothing?
				}
				escaped = 0;
			} 
			else if (c == BYTE_ESC)
			{
				// If we receive an escape char, read the next char
				escaped = 1;
				continue;
			}

			// If we get this far the info is data so add it to the buffer
			if ((beginingFound) && (dest_pos < MAX_PACKET_SIZE))
			{
				// Move the pointer to the current packet
				pckt->bytes[dest_pos++] = c;
			}
	    }

	    // we ran out of bytes and we did not find the end of the packet
	    return 0;

    }

    enum {WAITING_FOR_START_BYTE, WAITING_FOR_END_BYTE, WAITING_FOR_ESCAPED_CHAR, MAX_PACKET_STATE};

    // Decode a message from wired communications (QT terminal)
    uint8_t readAndDecodeByteWired(uint8_t c, pPACKET pckt) {
	    static uint16_t rcvPacket_pos = 0;
	    static uint8_t packetState = WAITING_FOR_START_BYTE;
	    uint8_t decodedByte = 0xFF;
	    
	    // we have not decoded anything yet
	    // Assume that the byte is not escaped
	    
	    //    debug_print(DBG_ALWAYS_PRINT,"Received: %x%s", c, "\r\n");
	    
	    switch (packetState)
	    {
		    case WAITING_FOR_START_BYTE:
		    if (c == PACKET_END)
		    {
			    packetState = WAITING_FOR_END_BYTE;
			    memset (pckt, 0, sizeof(PACKET));
			    rcvPacket_pos = 0;
			    
			    // No further processing is necessary
			    return 0;
		    }
		    break;
		    
		    case WAITING_FOR_END_BYTE:
		    if (c == BYTE_ESC) {
			    // If we receive an escape char, read the next char
			    packetState = WAITING_FOR_ESCAPED_CHAR;
			    return 0;
		    }
		    else if (c == PACKET_END)
		    {
			    // found the end of the packet, so
			    packetState = WAITING_FOR_START_BYTE;
			    return 1;
		    }
		    else
		    {
			    decodedByte = c;
		    }
		    
		    break;
		    
		    case WAITING_FOR_ESCAPED_CHAR:
		    if (c == BYTE_ESC_END)
		    decodedByte = PACKET_END;
		    else if (c == BYTE_ESC_ESC)
		    decodedByte = BYTE_ESC;
		    else {
			    //TODO:
			    // ESC should only be used to escape ESC_END and ESC_ESC
			    // so if we get here, warn the user
			    // Then again, this is an embedded application, so we can
			    // just ignore the ESC character.
			    // i.e. use the escape sequence to escape nothing?
		    }

		    packetState = WAITING_FOR_END_BYTE;
		    break;
		    
		    default:
		    break;
	    }
	    
	    // If we get this far the info is data so add it to the buffer
	    if ((rcvPacket_pos < MAX_PACKET_SIZE)) {
		    // Move the pointer to the current packet
		    pckt->bytes[rcvPacket_pos++] = decodedByte;
	    }

	    // we ran out of bytes and we did not find the end of the packet
	    return 0;
    }
