/*
 * calendar.h
 *
 * Created: 6/11/2020 9:36:56 AM
 *  Author: BehroozH
 */ 


#ifndef CALENDAR_H_
#define CALENDAR_H_
#include "system_definitions.h"

void Init_Calendar();
void Init_BatterySOH();
char* CurrentTime();
char* Current_DateTime();
float Get_SOH();
void SOH_Monitor();
void SOH_set_alarm();
uint32_t Write_SOH_To_Flash(uint32_t newSOH);
TIME_STAMP Current_DateTime_Short();

#endif /* CALENDAR_H_ */