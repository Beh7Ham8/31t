/*
 * SOC.h
 *
 * Created: 5/11/2020 9:53:09 AM
 *  Author: BehroozH
 */ 


#ifndef SOC_H_
#define SOC_H_


extern float VOLTAGE_REFERENCE_TP1;
void Init_BatterySOC(void);
void SOC_Monitor(void); 
float Get_SOC();

extern CauseOfReset causeOfReset;
extern float SOC_Backup;
#endif /* SOC_H_ */