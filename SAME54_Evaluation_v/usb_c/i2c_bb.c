#include <stdint.h>
#include "i2c_bb.h"
//#include "system_definitions.h"
#include "hal/gpio.h"
//#include "hal/clock.h"
#include "USB_PD_defines.h"
#include "hpl_i2c_m_sync.h"
#include "driver_init.h"


#define BASE_CLK_FREQ_BB                        4000000ul
#define STUSB1600_I2C_ADDR  0x28
uint32_t dlyMultiplier;

#define GPIO_SDA GPIO(GPIO_PORTA, 22)	 
#define GPIO_SCL GPIO(GPIO_PORTA, 23)	 

void Init_BB_i2c()
{   
    dlyMultiplier = HAL_CLOCK_GetDelayMultiplier(BASE_CLK_FREQ_BB);
}

void I2C_ReadByteArray(unsigned char devAddr, unsigned char reg, unsigned char *buffer, uint8_t length)//Read values starting from the reg address
{
    unsigned char ad, x;
    uint8_t addr = devAddr<<1;

    i2c_start(); // send i2c data bytes
    ad = i2c_tx(addr & 0xfe);
    if (ad) 
	{ // abort if slave does not acknowledge
        i2c_stop();
        return;
    }
    ad = i2c_tx(reg);
    if (ad) 
	{ // reg
        i2c_stop();
        return;
    }
    i2c_start(); // repeated start
    ad = i2c_tx(addr | 0x01);
    if (ad) 
	{ // addr again including read bit
        i2c_stop();
        return;
    }
        
	for(x=0; x<(length-1); x++)
	{
		buffer[x] = i2c_rx(ACK);
	}
	buffer[length-1] = i2c_rx(NACK);	        

    i2c_stop();

}

void I2C_WriteByteArray(unsigned char devAddr, unsigned char reg, unsigned char *buffer, uint8_t length)//Read values starting from the reg address
{
	
    unsigned char ad, x, stat;
    uint8_t addr = devAddr<<1;

    i2c_start(); // send i2c data bytes
    ad = addr & 0xfe;
    stat = i2c_tx(ad);
    if (stat) 
	{ // abort if slave does not acknowledge
        i2c_stop();
        return;
    }
    stat = i2c_tx(reg);
    if (stat) 
	{
        i2c_stop();
        return;
    }
	
    for (x=0; x<length; x++)
    {
    stat = i2c_tx(buffer[x]);
    if (stat) 
	{
        i2c_stop();
        return;
    }
    }
    i2c_stop();

}

unsigned char read_i2c(unsigned char devAddr, unsigned char reg) {
    unsigned char ad, x;
    uint8_t addr = devAddr<<1;

    i2c_start(); // send i2c data bytes
    ad = i2c_tx(addr & 0xfe);
    if (ad) 
	{ // abort if slave does not acknowledge
        i2c_stop();
        return 0;
    }
    ad = i2c_tx(reg);
    if (ad) 
	{ // reg
        i2c_stop();
        return 0;
    }
    i2c_start(); // repeated start
    ad = i2c_tx(addr | 0x01);
    if (ad) 
	{ // addr again including read bit
        i2c_stop();
        return 0;
    }
    x = i2c_rx(NACK);
    i2c_stop();
    return x;
}

void write_i2c(unsigned char devAddr, unsigned char reg, unsigned char data) {
    unsigned char ad, stat;
    uint8_t addr = devAddr << 1;

    i2c_start(); // send i2c data bytes
    ad = addr & 0xfe;
    stat = i2c_tx(ad);
    if (stat) 
	{ // abort if slave does not acknowledge
        i2c_stop();
        return;
    }
    stat = i2c_tx(reg);
    if (stat) 
	{
        i2c_stop();
        return;
    }
    stat = i2c_tx(data);
    if (stat) 
	{
        i2c_stop();
        return;
    }
    i2c_stop();
}

unsigned char i2c_rx(char ack) 
{
    unsigned char x, d = 0;
    HAL_GPIO_DIR(GPIO_SDA, 1); //hc125 will turn sda output off
    i2c_dly(32);
	
    for (x = 0; x < 8; x++) 
	{
        d <<= 1;
        HAL_GPIO_DIR(GPIO_SCL, 1);
        i2c_dly(64);
        if (HAL_GPIO_GET(GPIO_SDA)) d |= 1;
        HAL_GPIO_DIR(GPIO_SCL, 0);
        i2c_dly(64);
    }
	
    if (ack) HAL_GPIO_DIR(GPIO_SDA, 0);
    else HAL_GPIO_DIR(GPIO_SDA, 1);
	
    HAL_GPIO_DIR(GPIO_SCL, 1);
    i2c_dly(64); // send (N)ACK bit
    HAL_GPIO_DIR(GPIO_SCL, 0);
    HAL_GPIO_DIR(GPIO_SDA, 1);
    return d;
}

char i2c_tx(unsigned char d) 
{
    unsigned char x;
    for (x = 0; x < 8; x++) 
	{
        if (d & 0x80) HAL_GPIO_DIR(GPIO_SDA, 1);
        else HAL_GPIO_DIR(GPIO_SDA, 0);
        i2c_dly(32);
        HAL_GPIO_DIR(GPIO_SCL, 1);
        i2c_dly(64);
        d <<= 1;
        HAL_GPIO_DIR(GPIO_SCL, 0);
        i2c_dly(64);
    }
    HAL_GPIO_DIR(GPIO_SDA, 1);
    HAL_GPIO_DIR(GPIO_SCL, 1);
    i2c_dly(32);
    x = HAL_GPIO_GET(GPIO_SDA); // possible ACK bit
    i2c_dly(32);
    HAL_GPIO_DIR(GPIO_SCL, 0);
    i2c_dly(32);
    return x;
}

void i2c_start(void) 
{
    HAL_GPIO_DIR(GPIO_SDA, 1); // i2c start bit sequence
    i2c_dly(32);
    HAL_GPIO_DIR(GPIO_SCL, 1);
    i2c_dly(32);
    HAL_GPIO_DIR(GPIO_SDA, 0);
    i2c_dly(64);
    HAL_GPIO_DIR(GPIO_SCL, 0);
    i2c_dly(64);
}

void i2c_stop(void) 
{
    HAL_GPIO_DIR(GPIO_SDA, 0); // i2c stop bit sequence
    i2c_dly(32);
    HAL_GPIO_DIR(GPIO_SCL, 1);
    i2c_dly(32);
    HAL_GPIO_DIR(GPIO_SDA, 1);
    i2c_dly(64);
}

void i2c_dly(unsigned char dly) 
{
    volatile uint16_t x;
    volatile uint16_t scaledDly;

    scaledDly = dlyMultiplier * dly;

    for (x = 0; x < dly; x++)
	{
		//        Nop();
		gpio_toggle_pin_level(BLUE_LED);			// hack by Mike
	}

}

uint32_t HAL_CLOCK_GetDelayMultiplier (uint32_t freq)
{
//	return SYS_CLK_SystemFrequencyGet() / freq;
	return 12000000 / freq;			// hack by Mike
}

/* Added for STUSB1600 NVM  */

HAL_StatusTypeDef I2C_Read_USB_PD(uint8_t Port, uint16_t Address ,uint8_t *DataR ,uint8_t Length)
{

	//return  HAL_I2C_Mem_Read(hi2c[Port],(I2cDeviceID_7bit<<1), Address ,AddressSize, DataR, Length ,2000);
	I2C_ReadByteArray((unsigned char) STUSB1600_I2C_ADDR, (unsigned char) Address, (unsigned char *) DataR, Length);//Read values starting from the reg address
	return HAL_OK;	
	
}
HAL_StatusTypeDef I2C_Write_USB_PD(uint8_t Port, uint16_t Address ,uint8_t *DataW ,uint8_t Length)
{
	//return  HAL_I2C_Mem_Write(hi2c[Port],(I2cDeviceID_7bit<<1), Address ,AddressSize, DataW, Length,2000 ); // unmask all alarm status
	I2C_WriteByteArray((unsigned char) STUSB1600_I2C_ADDR, (unsigned char) Address, (unsigned char *) DataW, Length);//Read values starting from the reg address	
	return HAL_OK;
	
}

/* Added for STUSB1600 NVM  */



