/* 
 * File:   i2c_bb.h
 * Author: Juan.Yanez
 *
 * Created on May 10, 2016, 2:46 PM
 */

#ifndef I2C_BB_H
#define	I2C_BB_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define ACK     0xff    // send i2c ack
#define NACK    0x00	// send i2c nack    


typedef enum
{
	NVM_OKK       = 0x00,		/*!< Device OK 		*/
	NVM_ERROR    = 0x01,		/*!< Device ERROR 	*/
	NVM_BUSY     = 0x02,		/*!< Device BUSY 	*/
	NVM_TIMEOUT  = 0x03,		/*!< Device TIMEOUT */
	NVM_WRITTEN = 0x04
} STUSB1602_NVMTypeDef;

typedef enum
{
	HAL_OK       = 0x00U,
	HAL_ERROR    = 0x01U,
	HAL_BUSY     = 0x02U,
	HAL_TIMEOUT  = 0x03U
} HAL_StatusTypeDef;



void Init_BB_i2c();    
unsigned char read_i2c(unsigned char addr, unsigned char reg);
void write_i2c(unsigned char addr, unsigned char reg, unsigned char data);
unsigned char i2c_rx(char ack);
char i2c_tx(unsigned char d);
void i2c_start(void);
void i2c_stop(void);
void i2c_dly(unsigned char dly);
void WriteREG(unsigned char reg, unsigned char data) ;
char ReadREG(unsigned char reg);
void I2C_ReadByteArray(unsigned char devAddr, unsigned char reg, unsigned char *buffer, uint8_t length);
void I2C_WriteByteArray(unsigned char devAddr, unsigned char reg, unsigned char *buffer, uint8_t length);
uint32_t HAL_CLOCK_GetDelayMultiplier (uint32_t freq);

/* Added for STUSB1600 NVM */


uint8_t nvm_flash(uint8_t Port);
uint8_t CUST_EnterWriteMode(uint8_t Port,unsigned char ErasedSector);
uint8_t CUST_EnterReadMode(uint8_t Port);
uint8_t CUST_ReadSector(uint8_t Port,char SectorNum, unsigned char *SectorData);
uint8_t CUST_WriteSector(uint8_t Port,char SectorNum, unsigned char *SectorData);
uint8_t CUST_ExitTestMode(uint8_t Port);
void print_nvm2(void);
void print_1600_regs(void);
uint8_t CUST_VerifyNVM(uint8_t Port);
void Verify1600NVM(void);

HAL_StatusTypeDef I2C_Read_USB_PD(uint8_t Port, uint16_t Address ,uint8_t *DataR ,uint8_t Length);
HAL_StatusTypeDef I2C_Write_USB_PD(uint8_t Port, uint16_t Address ,uint8_t *DataW ,uint8_t Length);

/* Added for STUSB1600 NVM */

#ifdef	__cplusplus
}
#endif

#endif	/* I2C_BB_H */

